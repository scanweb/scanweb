﻿using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.Services;
    using System.Web;
    using System.Web.Script.Serialization;

    public class Map : WebControl, IMap
    {
        /// <summary>
        /// String carrying all the startup script code
        /// </summary>
        private StringBuilder initScript;
        private IList<MapUnit> dataSource = null;
        /// <summary>
        /// Culture information(language)
        /// </summary>
        CultureInfo ci = new System.Globalization.CultureInfo("en-US");

        /// <summary>
        /// driving instructions container div
        /// </summary>
        private string viewStateDirectionsContainer = "div";

        /// <summary>
        /// Drive drirection enum either 'To' or 'From'
        /// </summary>
        private DriveDirection destinationWay;


        private double latitude;
        private double longitude;
        private bool enableunitnumbering;

        public bool EnableUnitNumbering
        {
            get { return enableunitnumbering; }
            set { enableunitnumbering = value; }
        }
        /// <summary>
        /// Locale
        /// </summary>
        /// 
        private string viewStateLocale = "en_US";

        /// <summary>
        /// direction variable in script
        /// </summary>
        private string directionVariable = "directions";

        /// <summary>
        /// direction enable status
        /// </summary>
        private string viewStateEnableDirections = "false";
        private CenterAndZoomLevel centerandzoomlevel = CenterAndZoomLevel.ALL;

        /// <summary>
        /// Key through which map api key has to be added to script
        /// </summary>
        private string googleKey = string.Empty;

        public string GoogleMapKey { set { googleKey = value; } get { return googleKey; } }


        /// <summary>
        /// direction panel variable in script
        /// </summary>
        private string directionPanelVariable = "directionsPanel";

        /// <summary>
        /// geocode variable of script
        /// </summary>
        private string geocodeVariable = "geocoder";

        /// <summary>
        /// destination 'to'
        /// </summary>
        private string viewStateDestinationTo = "to";

        /// <summary>
        /// destination 'from'
        /// </summary>
        private string viewStateDestinationFrom = "from";

        private int zoomLevel = 16;
        /// <summary>
        /// marker latitude
        /// </summary>

        private MapPageType mapPageType = MapPageType.OLD;

        private double swlatitude;
        private double swlongitude;
        private double nelongitude;
        private double nelatitude;
        private string viewStateLatitude = "latitudefield";

        /// <summary>
        /// marker longitude
        /// </summary>
        private string viewStateLongitude = "longitudefield";
        private InfoBoxType infoboxtype = InfoBoxType.BASIC;
        
        private bool enablePanControl = true;
        public bool EnablePanControl
        {
            get { return enablePanControl; }
            set { enablePanControl = value; }
        }

        public double SWLatitude
        {
            get { return swlatitude; }
            set { swlatitude = value; }
        }

        public InfoBoxType InfoBoxType
        {
            get { return infoboxtype; }
            set { infoboxtype = value; }
        }

        public double SWLongitude
        {
            get { return swlongitude; }
            set { swlongitude = value; }

        }

        public double NELatitude
        {
            get { return nelatitude; }
            set { nelatitude = value; }
        }

        public double NELongitude
        {
            get { return nelongitude; }
            set { nelongitude = value; }
        }

        public int ZoomLevel
        {
            get { return zoomLevel; }
            set { zoomLevel = value; }
        }

        public Double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        public Double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }
        public CenterAndZoomLevel CenterAndZoomLevel
        {
            get { return centerandzoomlevel; }
            set { centerandzoomlevel = value; }

        }
        /// <summary>
        /// 
        /// set or get the Marker Longitude
        /// Used for databinding
        /// </summary>
        /// <value>The marker longitude field.</value>
        /// 

        public MapPageType MapPageType
        {
            get { return mapPageType; }
            set { mapPageType = value; }
        }

        /// <summary>
        /// Set or get the container that should be viewing the directions
        /// </summary>
        /// <value>The directions container.</value>
        public string DirectionsContainer
        {
            get
            {
                if (this.ViewState[viewStateDirectionsContainer] != null)
                {
                    return (string)this.ViewState[viewStateDirectionsContainer];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDirectionsContainer] = value; }
        }

        /// <summary>
        /// Enable driving directions of the Map
        /// </summary>
        /// <value><c>true</c> if [enable directions]; otherwise, <c>false</c>.</value>
        public bool EnableDirections
        {
            get
            {
                if (this.ViewState[viewStateEnableDirections] != null)
                {
                    return bool.Parse(this.ViewState[viewStateEnableDirections].ToString());
                }
                return bool.Parse("false");
            }
            set { this.ViewState[viewStateEnableDirections] = value.ToString(); }
        }

        /// <summary>
        /// Set or get the locale of the page
        /// </summary>
        /// <value>The locale.</value>
        public string Locale
        {
            get
            {
                if (this.ViewState[viewStateLocale] != null)
                {
                    return (string)this.ViewState[viewStateLocale];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLocale] = value; }
        }

        /// <summary>
        /// Set or get the <see cref="DriveDirection"/> way
        /// </summary>
        /// <value>The destination way.</value>
        public DriveDirection DestinationWay
        {
            get { return destinationWay; }
            set { destinationWay = value; }
        }

        /// <summary>
        /// Set or get the destination-from string
        /// </summary>
        /// <value>The destination from.</value>
        public string DestinationFrom
        {
            get
            {
                if (this.ViewState[viewStateDestinationFrom] != null)
                {
                    return (string)this.ViewState[viewStateDestinationFrom];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationFrom] = value; }
        }

        /// <summary>
        /// Set or get the destination-to string
        /// </summary>
        /// <value>The destination to.</value>
        public string DestinationTo
        {
            get
            {
                if (this.ViewState[viewStateDestinationTo] != null)
                {
                    return (string)this.ViewState[viewStateDestinationTo];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateDestinationTo] = value; }
        }


        public string MarkerLongitudeField
        {
            get
            {
                if (this.ViewState[viewStateLongitude] != null)
                {
                    return (string)this.ViewState[viewStateLongitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLongitude] = value; }
        }

        /// <summary>
        /// Set or get the data source
        /// </summary>
        /// <value>The data source.</value>
        public IList<MapUnit> DataSource
        {
            set { dataSource = value; }
            get { return dataSource; }
        }

        private IList<MapHotelUnit> hotelList;


        /// <summary>
        /// Set or get the destination-to string
        /// </summary>
        /// <value>The destination to.</value>
        public IList<MapHotelUnit> hotels { get; set; }

        /// <summary>
        /// Set or get the Marker Latitude
        /// Used for databinding
        /// </summary>
        /// <value>The marker latitude field.</value>
        public string MarkerLatitudeField
        {
            get
            {
                if (this.ViewState[viewStateLatitude] != null)
                {
                    return (string)this.ViewState[viewStateLatitude];
                }
                else
                    return string.Empty;
            }
            set { this.ViewState[viewStateLatitude] = value; }
        }

        public bool RecentlyOpenedHotels { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [coming soon hotels].
        /// </summary>
        /// <value><c>true</c> if [coming soon hotels]; otherwise, <c>false</c>.</value>
        public bool ComingSoonHotels { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [other hotels].
        /// </summary>
        /// <value><c>true</c> if [other hotels]; otherwise, <c>false</c>.</value>
        public bool OtherHotels { get; set; }


        protected void RegisterMapKey()
        {
            string sKeyScript = "<script src='http://maps.google.com/maps/api/js?v=3&sensor=false' type='text/javascript'></script>";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "GMapKey", sKeyScript, false);
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "initialize", InitializeMap(), false);
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex);
            }
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            IList<MapUnit> mapUnitList = dataSource;
            if (mapUnitList != null)
            {
                this.hotels = new List<MapHotelUnit>();
                foreach (MapUnit mapUnit in mapUnitList)
                {
                    MapHotelUnit hotel = new MapHotelUnit();
                    double lat = mapUnit.Latitude;
                    double lng = mapUnit.Longitude;
                    hotel.Latitude = lat;
                    hotel.Longitude = lng;
                    if (mapUnit is MapHotelUnit)
                    {
                        hotel = (MapHotelUnit)mapUnit;
                        //hotel.HotelName = mapHotelUnit.HotelName;
                        //if (this.MapPageType == MapPageType.SELECTHOTEL)
                        //{
                        //    hotel.PerNightRate = mapHotelUnit.PerNightRate.ToString();
                        //    hotel.PerStayRate = mapHotelUnit.PerStayRate.ToString();
                        //    hotel.MarkerRateTextType = mapHotelUnit.MarkerRateTextType;
                        //}
                        //hotel.EnableUnitText =  mapHotelUnit.EnableUnitText;
                        //hotel.EnableUnitNumbering = mapHotelUnit.EnableUnitNumbering;
                        ////hotel.Html = mapHotelUnit.GenerateInfoWindowMarkUp();
                        //hotel.IconUrl = mapHotelUnit.IconUrl;
                        //hotel.InfoBoxType = mapHotelUnit.InfoBoxType;
                        //hotel.HotelCategory = mapHotelUnit.HotelCategory;
                    }
                    else
                    {
                        hotel.HotelName = string.Empty;
                        hotel.MarkerRateTextType = MarkerRateTextType.RATEPERNIGHT;
                        hotel.EnableUnitText = false;
                        hotel.EnableUnitNumbering = false;
                        hotel.Html = string.Empty;
                        hotel.IconUrl = string.Empty;
                        hotel.InfoBoxType = InfoBoxType.BASIC;
                        hotel.HotelCategory = string.Empty;
                    }
                    this.hotels.Add(hotel);
                }
            }
        }

        /// <summary>
        /// This method can resolve and get the latitude and longitude
        /// from the parameter passed.
        /// </summary>
        /// <param name="ditem">MapUnit object</param>
        /// <param name="latitude">out latitude</param>
        /// <param name="longitude">out longitude</param>
        private void GetLatitudeLongitude(object ditem, out double latitude, out double longitude)
        {

            if (!string.IsNullOrEmpty(this.MarkerLongitudeField) && !string.IsNullOrEmpty(this.MarkerLatitudeField))
            {
                if (typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).GetType()
                                && typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).GetType())
                {
                    latitude = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).ToString(), ci);
                    longitude = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).ToString(), ci);
                }
                else
                {
                    latitude = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField);
                    longitude = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField);
                }
            }
            else
            {
                latitude = 0;
                longitude = 0;
            }
        }

        private string InitializeMap()
        {
            initScript = new StringBuilder();
            string googlemapsv3key = this.GoogleMapKey ?? null;
            initScript.AppendFormat("<script  type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=true&hl=ru&language={0}&key={1}'></script>\n", this.Locale, googlemapsv3key);

            initScript.Append("<script type='text/javascript'  src='" +
            ResolveUrl("/Templates/Scanweb/Javascript/gmaps-utility-library/markermanager_packed.js?v=" + CmsUtil.GetJSVersion()) + "'></script>\n");

            initScript.Append("<script type='text/javascript'  src='" +
                ResolveUrl("/Templates/Scanweb/Javascript/gmaps-utility-library/MarkerWithLabel_packed.js?v=" + CmsUtil.GetJSVersion()) + "'></script>\n");
            initScript.Append("<script type='text/javascript'  src='" +
            ResolveUrl("/Templates/Scanweb/Javascript/gmaps-utility-library/infobubblecomplied.js?v=" + CmsUtil.GetJSVersion()) + "'></script>\n");

            initScript.Append("<script type='text/javascript'> ");

            if (hotels != null && hotels.Count > 0)
            {
                initScript.Append(@" 
                                    var latlng = new google.maps.LatLng(21,21); //any random latitude and logitude.
                                    var markercollection = [];
                                    var directionsService;
                                    var geocoder;
                                    var mgr;
                                    var nelat;
                                    var nelng;
                                    var swlng;
                                    var swlat;
                                    var infoWindow;
                                    var zoomlevel;
                                    var latitude;
                                    var longitude;
                                    var directionrenderer;
                                    var request;
                                    var recentlyOpenedMarkers = [];
                                    var comingSoonMarkers = [];
                                    var otherMarkers = [];
                                    var map;
                                   //check for DOM ready
                                    $(document).ready(function () { ");
                initScript.Append(" initialize();");
                initScript.Append("});");
                initScript.Append("\n\n");
                initScript.Append(@"function initialize() { ");
                initScript.Append(GenerateHotels(this.hotels));

                StringBuilder strScript = new StringBuilder();
                if (this.MapPageType == MapPageType.HOTELLOCATION)
                {
                    strScript.Append("var opt = {");
                    strScript.Append("center: latlng, zoom: 16,");
                    strScript.Append("disableDefaultUI: true,");
                    strScript.Append("mapTypeId: google.maps.MapTypeId.ROADMAP,disableAutoPan: false,");
                    strScript.Append("panControl: " + enablePanControl.ToString().ToLower() + ",");
                    strScript.Append("zoomControl:true,");
                    strScript.Append("scaleControl:true,");
                    strScript.Append("navigationControl: true,");
                    strScript.Append("navigationControlOptions: { style: google.maps.NavigationControlStyle.LARGE },");
                    strScript.Append("mapTypeControl: false};");

                    //                    initScript.Append(@"            var opt = {
                    //                                                    center: latlng,
                    //                                                    zoom: 16,
                    //                                                    disableDefaultUI: true,
                    //                                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    //                                                    disableAutoPan: false,
                    //                                                    panControl: + enablePanControl + ,
                    //                                                    zoomControl:true,
                    //                                                    scaleControl:true,
                    //                                                    navigationControl: true,
                    //                                                    navigationControlOptions: { style: google.maps.NavigationControlStyle.LARGE },
                    //                                                    mapTypeControl: false
                    //                                                 };");
                }
                else
                {
                    strScript.Append("var opt = {");
                    strScript.Append("center: latlng, zoom: 16,");
                    strScript.Append("disableDefaultUI: true,");
                    strScript.Append("mapTypeId: google.maps.MapTypeId.ROADMAP,disableAutoPan: false,");
                    strScript.Append("panControl: " + enablePanControl.ToString().ToLower() + ",");
                    strScript.Append("zoomControl:true,");
                    strScript.Append("navigationControl: true,");
                    strScript.Append("navigationControlOptions: { style: google.maps.NavigationControlStyle.LARGE },");
                    strScript.Append("mapTypeControl: false};");

                    //                    initScript.Append(@"            var opt = {
                    //                                                    center: latlng,
                    //                                                    zoom: 16,
                    //                                                    disableDefaultUI: true,
                    //                                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    //                                                    disableAutoPan: false,
                    //                                                    panControl:true,
                    //                                                    zoomControl:true,
                    //                                                    navigationControl: true,
                    //                                                    navigationControlOptions: { style: google.maps.NavigationControlStyle.LARGE },
                    //                                                    mapTypeControl: false
                    //                                                 };");
                }

                initScript.Append(strScript.ToString());

                initScript.Append("map = new google.maps.Map(document.getElementById('GMapV3'), opt);\n");
                initScript.Append("google.maps.event.trigger(map, 'resize');");
                initScript.Append("var optmgr = { borderPadding: 50, maxZoom: 25, trackMarkers: true};\n markercollection = [];\n");

                if (this.CenterAndZoomLevel == CenterAndZoomLevel.CITY || this.CenterAndZoomLevel == CenterAndZoomLevel.HOTEL)
                {
                    initScript.AppendFormat("zoomlevel = {0}; latitude = {1}; longitude = {2};", this.ZoomLevel, this.Latitude.ToString().Replace(",", "."), this.Longitude.ToString().Replace(",", "."));
                    initScript.Append("\n ZoomandCenter(zoomlevel,latitude,longitude);\n");
                }

                if (this.CenterAndZoomLevel == CenterAndZoomLevel.COUNTRY)
                {
                    initScript.AppendFormat("nelat= {0}; nelng = {1}; swlng = {2}; swlat = {3};", this.NELatitude.ToString().Replace(",", "."), this.NELongitude.ToString().Replace(",", "."), this.SWLongitude.ToString().Replace(",", "."), this.SWLatitude.ToString().Replace(",", "."));
                    initScript.Append(@"  var swlatlng= new google.maps.LatLng(swlat,swlng);
                                                                      var nelatlng = new google.maps.LatLng(nelat,nelng);  
                                                                      var bounds =   new google.maps.LatLngBounds(swlatlng,nelatlng);
                                                                     map.fitBounds(bounds);
                                                                  ");
                }

                if (this.MapPageType == MapPageType.HOTELLOCATION)
                {
                    if (this.EnableDirections)
                    {
                        initScript.AppendFormat(@"directionsService = new google.maps.DirectionsService();
                                                     geocoder = new google.maps.Geocoder();
                                                     directionrenderer = new google.maps.DirectionsRenderer();
                                                     directionrenderer.setMap(map);
                                                     directionrenderer.setPanel(document.getElementById('{0}'));
                                                     codeLatLng();", this.DirectionsContainer);
                    }
                }
                if (this.MapPageType == MapPageType.SELECTHOTEL || this.MapPageType == MapPageType.FINDAHOTEL)
                {
                    initScript.Append("GenerateLabeledMarkers(hotels,markercollection);\n");
                }
                else
                {
                    initScript.Append("GenerateMarkers(hotels,markercollection);");
                }
                initScript.Append(@"GenerateInfoWindow(markercollection);");

                if (this.hotels.Count == 1 || this.MapPageType == MapPageType.HOTELOVERVIEW || this.MapPageType == MapPageType.HOTELLOCATION || this.MapPageType == MapPageType.CONFIRMATION)
                {
                    initScript.Append("map.setZoom(16);");
                    initScript.AppendFormat("var markerlat = {0};", this.Latitude.ToString().Replace(",", "."));
                    initScript.AppendFormat("var markerlng = {0};", this.Longitude.ToString().Replace(",", "."));
                    initScript.Append(" var markercoord = new google.maps.LatLng(markerlat,markerlng);");
                    initScript.Append("map.setCenter(markercoord);");
                }
                else if (this.CenterAndZoomLevel == CenterAndZoomLevel.ALL)
                {
                    initScript.Append("AutoCenter(markercollection);");
                }

                if (this.MapPageType == MapPageType.FINDAHOTEL || this.MapPageType == MapPageType.EXPANSION)
                {
                    initScript.Append("map.setZoom(8);");
                    initScript.Append("map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(HotelFilter);");
                }

                initScript.Append(@"google.maps.event.addListener(map, 'click', function() {if(infoWindow){infoWindow.close();}});");
                initScript.Append("}");
                initScript.Append("\n\n");
                if (this.EnableDirections)
                {
                    initScript.Append(codeLatLng());
                }
                initScript.Append("\n\n");
                initScript.Append("\n\n");
                initScript.Append(GenerateMarkers());
                initScript.Append("\n\n");
                initScript.Append(GenerateLabeledMarkers());
                initScript.Append("\n\n");
                initScript.Append(GenerateInfoWindow());
                initScript.Append("\n\n");
                initScript.Append(@"function ZoomandCenter(zoom,lat,lng){
                                var center = new google.maps.LatLng(lat,lng);
                                map.setCenter(center);
                                map.setZoom(zoom);
                                    }");
                initScript.Append("\n\n");
                initScript.Append(@"function AutoCenter() 
                                        {
                                            var bounds = new google.maps.LatLngBounds();
                                            $.each(markercollection, function (index, marker) 
                                            {
                                                bounds.extend(marker.position);
                                            });
                                            map.fitBounds(bounds);  //Adjusts all markers in the view port area and zooms accordingly.**
                                ");

                initScript.Append("      }");
                initScript.Append("\n\n");

                if (this.MapPageType == MapPageType.FINDAHOTEL || this.mapPageType == MapPageType.EXPANSION)
                {
                    initScript.Append(@"function SwitchHotelFilter(checkbox,filterText)
                                    {  
                                        var markermgr= new MarkerManager(map);  
//                                          markercollection = [];  
//                                          GenerateLabeledMarkers(hotels,markercollection);
                                          if(checkbox.checked)
                                          {
                                              show(filterText);
                                          }
                                          else
                                          {
                                              hide(filterText);
                                          }
////                                    google.maps.event.addListener(markermgr, 'loaded', function() {
////                                    markermgr.addMarkers(markercollection, 1);
////                                    markermgr.refresh();});
                                    }");
                    initScript.Append("\n\n");

                    initScript.Append(@"function show(category)
                                    {
                                        for (var i=0; i<markercollection.length; i++) 
                                            {
                                               if (markercollection[i].category == category) 
                                               {
                                                  markercollection[i].setVisible(true);
                                               }
                                            }
                                        document.getElementById(category).checked = true;
                                    }");
                    initScript.Append("\n\n");
                    initScript.Append(@"function hide(category) 
                                    {
                                        for (var i=0; i<markercollection.length; i++) 
                                        {
                                            if (markercollection[i].category == category) 
                                            {
                                                 markercollection[i].setVisible(false);
                                            }
                                        }
                                    }");
                }
            }

            if (this.MapPageType == MapPageType.SELECTHOTEL)
            {
                initScript.Append(SwtichUnitText());
            }
            initScript.Append("\n\n");
            initScript.Append("</script>");
            return initScript.ToString();
        }

        private string SwtichUnitText()
        {
            StringBuilder script = new StringBuilder();
            script.Append(@" function SwitchUnitText(rateType)
                {
                   var markermanager = new MarkerManager(map);
                    switch(rateType)
                    {
                        case 'rdoPerNightMap' :
                                for (var i = 0; i < markercollection.length; i++)
                                {{
                                   markercollection[i].labelContent =  markercollection[i].pernightrate;
                                }}
                                            
                                google.maps.event.addListener(markermanager, 'loaded', function() {
                                    markermanager.addMarkers(markercollection, 1);
                                    markermanager.refresh();
                                });
                                
                            
                            break;
                        case 'rdoPerStayMap' :
                               for (var i = 0; i < markercollection.length; i++)
                                {{
                                    markercollection[i].labelContent =markercollection[i].perstayrate;
                                }}
                                google.maps.event.addListener(markermanager, 'loaded', function() {
                                    markermanager.addMarkers(markercollection, 1);
                                    markermanager.refresh();
                                });        
                            break;
                    }
                }");
            return script.ToString();
        }

        private string codeLatLng()
        {
            StringBuilder script = new StringBuilder();
            script.Append("function codeLatLng()  { \n");
            script.AppendFormat("var destfrom = '{0}';", this.DestinationFrom);
            script.AppendFormat("var destto = '{0}';", this.DestinationTo);
            if (this.destinationWay == DriveDirection.TO)
            {
                script.Append("var latlngStr = destfrom.split(',', 2);");
            }
            else
            {
                script.Append("var latlngStr = destto.split(',', 2);");
            }
            script.Append(@"var lat = parseFloat(latlngStr[0]);
                            var lng = parseFloat(latlngStr[1]);
                            var latlng = new google.maps.LatLng(lat, lng);
                            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) 
                            {
                                if (results[0]) 
                                {");
            if (this.destinationWay == DriveDirection.TO)
            {
                script.Append(@"request = {origin: results[0].formatted_address,
                                           destination: destto,
                                           travelMode: google.maps.DirectionsTravelMode.DRIVING,
                                           unitSystem: google.maps.DirectionsUnitSystem.METRIC};");
            }
            else
            {
                script.Append(@" request = {origin: destfrom,
                                            destination: results[0].formatted_address,
                                            travelMode: google.maps.DirectionsTravelMode.DRIVING,
                                            unitSystem: google.maps.DirectionsUnitSystem.METRIC};");

            }
            script.Append(@"directionsService.route(request, function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                               directionrenderer.setDirections(response);
                            } else {
                               validateDestination();
                            }
                        });
                       }
                } else {
                    validateDestination();
                }
                    });
                }
                ");
            return script.ToString();
        }

        private string CategorizeHotelMarker()
        {
            StringBuilder catgHotel = new StringBuilder();
            catgHotel.Append("recentlyOpenedMarkers = [];\n");
            catgHotel.Append("comingSoonMarkers = [];\n");
            catgHotel.Append("otherMarkers = [];");
            catgHotel.Append("for (var i = 0; i < markercollection.length; i++) {\n");
            catgHotel.Append("var marker = markercollection[i];\n");
            catgHotel.Append("if(marker.category=='RecentlyOpened') {recentlyOpenedMarkers.push(marker);}\n");
            catgHotel.Append("else if(marker.category=='ComingSoon') {comingSoonMarkers.push(marker);}\n");
            catgHotel.Append("else {otherMarkers.push(marker);}\n}");
            return catgHotel.ToString();
        }

        private string GenerateHotels(IList<MapHotelUnit> hotels)
        {
            StringBuilder hotelScript = new StringBuilder();
            hotelScript.Append(" hotels = [");
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            foreach (MapHotelUnit hotel in hotels)
            {
                string latitude = hotel.Latitude.ToString().Replace(",", ".");
                string longitude = hotel.Longitude.ToString().Replace(",", ".");
                //hotelScript.Append("['" + hotel.HotelName + "','" + hotel.Html + "'," + latitude + "," + longitude + ",'" + hotel.IconUrl + "','" + hotel.HotelCategory + "','" + hotel.MarkerRateTextType + "','" + hotel.PerNightRate + "','" + hotel.PerStayRate + "','" + hotel.EnableUnitNumbering + "','" + hotel.EnableUnitText + "','" + hotel.InfoBoxType + "','" + javaScriptSerializer.Serialize(hotel) + "'],\n");
                hotelScript.Append(
                    string.Format(
                        "['{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}'],\n",
                        hotel.HotelName, hotel.Html,
                        latitude, longitude, hotel.IconUrl, hotel.HotelCategory, hotel.MarkerRateTextType,
                        hotel.PerNightRate, hotel.PerStayRate, hotel.EnableUnitNumbering, hotel.EnableUnitText,
                        hotel.InfoBoxType, javaScriptSerializer.Serialize(hotel)));
            }
            hotelScript = hotelScript.Remove(hotelScript.Length - 2, 1);
            hotelScript.Append("];");
            return hotelScript.ToString();
        }

        private string GenerateLabeledMarkers()
        {
            StringBuilder markerScript = new StringBuilder();
            markerScript.Append("\n\n function GenerateLabeledMarkers(hotels,markercollection){\n");
            markerScript.Append("var point = 0; \n  \n var text= '';\n var labelCounter = 0;\n var hotel; var labelOffset;\n var labelclass ;\n ");
            markerScript.Append("for (var i = 0; i < hotels.length; i++) {\n");

            markerScript.Append("hotel = hotels[i];\n");
            markerScript.Append(@" if(hotel[9]=='True') { labelCounter=labelCounter+1; text = labelCounter; labelclass = 'LabeledMarker_markerLabelDoubleDigit';
                    if(labelCounter<=9)
                    {
                        labelOffset = new google.maps.Size(-6,-13);
                    }
                    if(labelCounter>9 && labelCounter<=99)
                    {
                        labelOffset = new google.maps.Size(-8,-13);
                    }
                    }");

            markerScript.Append(@" if(hotel[10]=='True') { text = hotel[7]; }");
            if (this.MapPageType == MapPageType.FINDAHOTEL)
            {
                markerScript.Append("point= new google.maps.Point(9,12);");
            }
            else if (this.MapPageType == MapPageType.SELECTHOTEL)
            {
                markerScript.Append("point =  new google.maps.Point(20,20);");
            }
            markerScript.AppendFormat("var image = new google.maps.MarkerImage(hotel[4],null, new google.maps.Point(0, 0), new google.maps.Point(9, 12));\n");
            markerScript.Append("markercollection.push(new MarkerWithLabel({ position: new google.maps.LatLng(hotel[2], hotel[3]),\n");
            markerScript.Append("clickable: true,\n");
            markerScript.Append("html:hotel[1],\n");
            markerScript.Append("map:map,\n");
            markerScript.Append("labelContent: text,\n");
            markerScript.Append("labelClass: labelclass,\n");
            markerScript.Append("labelAnchor: new google.maps.Point(10,10),\n");
            markerScript.Append("labelInBackground: false,\n");
            markerScript.Append("category:hotel[5], \n");
            markerScript.Append("markerratetexttype:hotel[6],\n");
            markerScript.Append("pernightrate:hotel[7],\n");
            markerScript.Append("perstayrate:hotel[8],\n");
            markerScript.Append("icon:image,\n");
            markerScript.Append("mapHotelUnit:hotel[12]");
            markerScript.Append("}\n)\n);\n");
            markerScript.Append("}\n }");
            //markerScript.Append(CategorizeHotelMarker() + "\n\n");
            //markerScript.Append(@"
            //                                    google.maps.event.addListener(markermgr, 'loaded', function() {
            //                                    markermgr.addMarkers(markercollection, 1);
            //                                    markermgr.refresh();});
            //}");
            return markerScript.ToString();
        }

        private string GenerateMarkers()
        {
            StringBuilder markerScript = new StringBuilder();

            markerScript.Append("\n\n function GenerateMarkers(hotels,markercollection){\n");
            markerScript.Append("\n ");
            markerScript.Append("for (var i = 0; i < hotels.length; i++) {\n");
            markerScript.Append("var hotel = hotels[i];\n");
            markerScript.AppendFormat("var image = new google.maps.MarkerImage(hotel[4]);\n");
            markerScript.Append(" latlng=new google.maps.LatLng(hotel[2], hotel[3]);");
            markerScript.Append("markercollection.push(new google.maps.Marker({ position: latlng,\n");
            markerScript.Append("clickable: true,\n");
            markerScript.Append("html:hotel[1],\n");
            markerScript.Append("map:map,\n");
            markerScript.Append("category:hotel[5],\n");
            markerScript.Append("markerratetexttype:hotel[6],\n");
            markerScript.Append("pernightrate:hotel[7],\n");
            markerScript.Append("perstayrate:hotel[8],\n");
            markerScript.Append("icon:image,\n");
            markerScript.Append("mapHotelUnit:hotel[12]");
            markerScript.Append("}));");
            markerScript.Append("}\n");
            if (this.MapPageType == MapPageType.EXPANSION)
            {
                markerScript.Append(@"for (var i = 0; i < markercollection.length; i++) {
                                        if(markercollection[i].category=='Default')
                                            {   
                                                markercollection[i].setVisible(false);
                                            }
                                     }");
            }
            markerScript.Append("}\n");
            return markerScript.ToString();
        }

        private string GenerateInfoWindow()
        {
            StringBuilder infoWindowScript = new StringBuilder();
            infoWindowScript.Append("\n\nfunction GenerateInfoWindow(markers)\n{ \n var options;");
            if (this.InfoBoxType == InfoBoxType.ADVANCED || this.MapPageType == MapPageType.FINDAHOTEL)
            {
                infoWindowScript.Append("options = {");
                if (this.MapPageType == MapPageType.FINDAHOTEL || this.MapPageType == MapPageType.EXPANSION)
                {
                    infoWindowScript.AppendFormat("maxWidth: {0},", GoogleMapConstants.MAX_WIDTH_ADVANCED_INFOBOX);
                    infoWindowScript.AppendFormat("maxHeight: {0},", GoogleMapConstants.MAX_HEIGHT_ADVANCED_INFOBOX);
                }
                if (this.MapPageType == MapPageType.SELECTHOTEL)
                {
                    infoWindowScript.AppendFormat("arrowPosition: {0},", GoogleMapConstants.ARROW_POSITION);
                }
                infoWindowScript.AppendFormat("minWidth: {0},", GoogleMapConstants.MIN_WIDTH_ADVANCED_INFOBOX);
                infoWindowScript.AppendFormat("minHeight: {0},", GoogleMapConstants.MIN_HEIGHT_ADVANCED_INFOBOX);
                infoWindowScript.AppendFormat("Padding: {0}", GoogleMapConstants.PADDING_X_ADVANCED_MARKER_POPUP);
                infoWindowScript.Append("};\n");
            }
            else
            {
                infoWindowScript.Append("options = {");
                infoWindowScript.AppendFormat("minWidth: {0},", GoogleMapConstants.MIN_WIDTH_BASIC_INFOBOX);
                infoWindowScript.AppendFormat("minHeight: {0},", GoogleMapConstants.MIN_HEIGHT_BASIC_INFOBOX);
                infoWindowScript.AppendFormat("maxWidth: {0},", GoogleMapConstants.MAX_WIDTH_BASIC_INFOBOX);
                infoWindowScript.AppendFormat("maxHeight: {0},", GoogleMapConstants.MAX_HEIGHT_BASIC_INFOBOX);
                infoWindowScript.Append("Padding: 2};\n");
            }

            infoWindowScript.Append(" infoWindow = new InfoBubble(options);\n");
            infoWindowScript.Append("for (var i = 0; i < markercollection.length; i++) {\n");
            infoWindowScript.Append("var marker = markercollection[i];\n");
            infoWindowScript.Append("google.maps.event.addListener(marker, 'click', function() {\n");
            //infoWindowScript.Append("infoWindow.setContent(this.html);\n");
            //infoWindowScript.Append("infoWindow.open(map, this);\n");
            infoWindowScript.Append(string.Format("GetGoogleMapHotelContetOverlayInfo(this,infoWindow,'{0}');\n",
                HttpContext.Current.Request.Url.AbsolutePath));
            infoWindowScript.Append("});\n }}");
            return infoWindowScript.ToString();
        }

        #region IMap Members Implementation
        /// <summary>
        /// Method to Center and Zoom the map at a particular point
        /// </summary>
        /// <param name="curPoint">Point at which the map should be centered</param>
        /// <param name="zoomLevel">zoom level</param>
        public void CenterAndZoom(MapPoint curPoint, int zoomLevel)
        {
        }

        /// <summary>
        /// Method to auto-center and zoom the map at a particular point from objects in datasource
        /// </summary>
        public void AutoCenterAndZoom()
        {

        }
        #endregion
    }
}
