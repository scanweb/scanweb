// <copyright file="JScriptGenerator.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Class generates script for the map</summary>

#undef FINDAHOTEL_PERFORMANCE

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Text;
    #endregion // System NameSpaces

    #region Scandic NameSpaces
    using Scandic.Scanweb.CMS.code.Util.Map.GoogleMap;
    #endregion // Scandic NameSpaces

    /// <summary>
    /// Class that handles all Javascript Method 
    /// Generation
    /// </summary>
    internal class JScriptGenerator
    {
        #region Fields
        /// <summary>
        /// Javascript string compressor
        /// </summary>
        JSCompressor jsCompressor = null;
        #endregion // Fields

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        internal JScriptGenerator()
        {
             jsCompressor = new JSCompressor(true, true);
        }
        #endregion // Constructor

        #region Internal Methods
        /// <summary>
        /// Method to Generate the Function to Overlay a point
        /// on the map
        /// </summary>
        /// <param name="sArrayVariable">Variable representing an Array</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        internal string GenerateOverlayLocationMethodBody(string sArrayVariable, string sControlClientID)
        {
            StringBuilder sOverlayLocationMethodBody = new StringBuilder();
            sOverlayLocationMethodBody.AppendFormat(" function OverlayLocation{0}(clong,cLat,popup,sFormattedHtml)\n", sControlClientID);
            sOverlayLocationMethodBody.Append("{\n");
            sOverlayLocationMethodBody.Append("var point = new GPoint(clong,cLat);");
            sOverlayLocationMethodBody.Append("var marker = new google.maps.Marker(point);\n");
            sOverlayLocationMethodBody.Append("if ( popup==1 ) { \n");
            sOverlayLocationMethodBody.Append("google.maps.Event.addListener(marker, 'click', function() {marker.openInfoWindowHtml(sFormattedHtml,{maxWidth:204});});");
            sOverlayLocationMethodBody.Append("\n } \n");
            sOverlayLocationMethodBody.AppendFormat("{0}.push(marker);\n", sArrayVariable);
            sOverlayLocationMethodBody.Append(" }");

            string strOverlayLocationMethodBody = sOverlayLocationMethodBody.ToString();
#if !DEBUG
            strOverlayLocationMethodBody = jsCompressor.Compress(strOverlayLocationMethodBody);
#endif // #if !DEBUG

            return strOverlayLocationMethodBody;
        }

        /// <summary>
        /// Method to Generate the Javascript funtion to Overlay a point that uses a custom
        /// icon on the map
        /// </summary>
        /// <param name="sGMapVariable">The Google map variable.</param>
        /// <param name="sArrayVariable">Variable representing an Array</param>
        /// <param name="arrayVarRecentlyOpened">The array var recently opened.</param>
        /// <param name="arrayVarComingSoon">The array var coming soon.</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <param name="mapPageType">Type of the map page.</param>
        /// <param name="arrayVarPerNightRate">The array var per night rate.</param>
        /// <param name="arrayVarPerStayRate">The array var per stay rate.</param>
        /// <returns>JavaScript function as a string</returns>
        internal string GenerateOverlayCustomMarkerLocationMethodBody(string sGMapVariable, string sArrayVariable, string arrayVarRecentlyOpened, string arrayVarComingSoon, string sControlClientID, MapPageType mapPageType, string arrayVarPerNightRate, string arrayVarPerStayRate)
        {
            StringBuilder sOverlayCustomMarkerLocationMethodBody = new StringBuilder();
            sOverlayCustomMarkerLocationMethodBody.AppendFormat(" function OverlayCustomMarkerLocation{0}(clong,cLat,iconimgUrl,iconshadowimgURL,iconwidth,iconheight,iconshwidth,iconshheight,iconAnchorX,iconAnchorY,infoWinX,infoWinY,enableUnitNumbering,popup,popupMaxWidth,infoBoxType,sFormattedHtml,hotelName,hotelCategory,enableUnitText,markerRateTextType,perNightRate,perStayRate)\n", sControlClientID);
            sOverlayCustomMarkerLocationMethodBody.Append("{\n");

           //Rajneesh | artf1167579 : Corporate Identifier not present in Select Hotel listing | Styling changed.
            sOverlayCustomMarkerLocationMethodBody.AppendFormat(
                @"
                var marker;
                var latlng = new google.maps.LatLng(cLat,clong);
                var icon = new GIcon();
                icon.image =iconimgUrl ;
                icon.shadow =iconshadowimgURL ;
                icon.iconSize = new GSize(iconwidth, iconheight);
                icon.shadowSize = new GSize(iconshwidth, iconshheight);
                icon.iconAnchor = new GPoint(iconAnchorX, iconAnchorY);
                icon.infoWindowAnchor = new GPoint(infoWinX, infoWinY);
                
                var labelOffset;
                if(enableUnitNumbering == ""true"")
                {{
                    labeledMarkerCount++;

                    if(labeledMarkerCount <= 9)
                    {{
                        labelOffset = new GSize(-6, -13);
                    }}
                    else if(labeledMarkerCount > 9 && labeledMarkerCount <= 99)
                    {{
                        labelOffset = new GSize(-8, -13);
                    }}
                    opts = {{
                          ""icon"": icon,
                          ""clickable"": true,
                          ""labelClass"": ""LabeledMarker_markerLabelDoubleDigit"",
                          ""labelText"": labeledMarkerCount,
                          ""labelOffset"": labelOffset
                        }};
                    marker = new LabeledMarker(latlng, opts);
                    
                }}
                else if(enableUnitText == true)
                {{
                    labelOffset = new GSize(-60, -60);
                    opts = {{
                          ""icon"": icon,
                          ""clickable"": true,
                          ""labelClass"": """",
                          ""labelText"": (markerRateTextType == ""RATEPERNIGHT"") ? perNightRate : perStayRate,
                          ""labelOffset"": labelOffset
                        }};
                    marker = new LabeledMarker(latlng, opts);
                    
                    {0}.push(perNightRate);
                    {1}.push(perStayRate);
                }}
                else
                {{
                    marker = new google.maps.Marker(latlng,icon);
                }}

                if ( popup==1 )
                {{
                    if(infoBoxType == ""BASIC"")
                    {{
                        google.maps.Event.addListener(marker, 'click', function() {{ marker.openInfoWindowHtml(sFormattedHtml,{{maxWidth:popupMaxWidth}});}});
                    }}
                    else if(infoBoxType == ""ADVANCED"")
                    {{
                        google.maps.Event.addListener(marker, 'click', function(){{ 
                            marker.openExtInfoWindow(
                              {2},
                              ""custom_info_window"",
                              sFormattedHtml,
                              {{
                                beakOffset: 3,
                                paddingX: {3},
                                paddingY: {4}
                              }}
                            );

                            {5}

                            AdjustAdvancedPopUpWidthAndHeight();
                          }});
                    }}
                    else if(infoBoxType == ""ADVANCED_SELECTHOTEL"" || infoBoxType == ""ADVANCED_CONFIRMATION"" ||infoBoxType == ""ADVANCED_SELECTHOTEL_DNUMBER"")
                    {{
                        google.maps.Event.addListener(marker, 'click', function(){{ 
                            marker.openExtInfoWindow(
                              {2},
                              ""selecthotel_custom_info_window"",
                              sFormattedHtml,
                              {{
                                beakOffset: 0,
                                paddingX: {6},
                                paddingY: {7}
                              }}
                            );
                          }});
                    }}
                }}"
                ,
                arrayVarPerNightRate,
                arrayVarPerStayRate,
                sGMapVariable,
                GoogleMapConstants.PADDING_X_ADVANCED_MARKER_POPUP,
                GoogleMapConstants.PADDING_Y_ADVANCED_MARKER_POPUP,
                (mapPageType == MapPageType.FINDAHOTEL) ? "UpdateSiteCatalystFindAHotel(hotelName, 'Map');" : "UpdateSiteCatalystExpansionPageMap(hotelName);",
                GoogleMapConstants.SH_PADDING_X_ADVANCED_MARKER_POPUP,
                GoogleMapConstants.SH_PADDING_Y_ADVANCED_MARKER_POPUP
                );

            sOverlayCustomMarkerLocationMethodBody.AppendFormat(@"
                switch(hotelCategory)
                {{
                    case ""RecentlyOpened"":
                        {1}.push(marker);
                        break;
                    case ""ComingSoon"":
                        {2}.push(marker);
                        break;
                    case ""Default"":
                        {0}.push(marker);
                        break;
                    default:
                        {0}.push(marker);
                        break;
                }}
            ", sArrayVariable, arrayVarRecentlyOpened, arrayVarComingSoon);

            sOverlayCustomMarkerLocationMethodBody.Append(@"
                latlng = null;
                icon = null;
            ");
            sOverlayCustomMarkerLocationMethodBody.Append(" }");

            string strOverlayCustomMarkerLocationMethodBody = sOverlayCustomMarkerLocationMethodBody.ToString();
#if !DEBUG
            strOverlayCustomMarkerLocationMethodBody = jsCompressor.Compress(strOverlayCustomMarkerLocationMethodBody);
#endif // #if !DEBUG
            return strOverlayCustomMarkerLocationMethodBody;
        }

        /// <summary>
        /// Method to generate the Javascript function to overlay a line
        /// on the map
        /// </summary>
        /// <param name="sArrayVariable">Variable representing the array</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        internal string GenerateOverlayLineMethodBody(string sArrayVariable, string sControlClientID)
        {
            StringBuilder sOverlayLineMethodBody = new StringBuilder();
            sOverlayLineMethodBody.AppendFormat(" function OverlayLine{0}(PointArray,weight,Color,opacity)\n", sControlClientID);
            sOverlayLineMethodBody.Append("{\n");
            sOverlayLineMethodBody.Append("var polyline = new GPolyline(PointArray,Color,weight,opacity);");
            sOverlayLineMethodBody.AppendFormat("{0}.push(polyline);\n", sArrayVariable);
            sOverlayLineMethodBody.Append(" }");

            string strOverlayLineMethodBody = sOverlayLineMethodBody.ToString();
#if !DEBUG
            strOverlayLineMethodBody = jsCompressor.Compress(strOverlayLineMethodBody);
#endif // #if !DEBUG

            return strOverlayLineMethodBody;
        }

        /// <summary>
        /// Method to Overlay points on the map
        /// Instead of overlaying each point or lay individually
        /// it stores them in an array and overlays them together
        /// </summary>
        /// <param name="sGMapVariable">The Google map variable.</param>
        /// <param name="sClientID">The client ID.</param>
        /// <returns>JavaScript code as string</returns>
        internal string GenerateCreateOverlaysMethodBody(string sGMapVariable, string sClientID)
        {
            StringBuilder sCreateOverlaysMethodBody = new StringBuilder();
            sCreateOverlaysMethodBody.Append(@"
                //var markersData = this.req.responseText;
                //var markersXml = GXml.parse(this.req.responseText);
                //var markersXml = loadXMLDoc(markersData);
                //var markers = markersXml.documentElement.getElementsByTagName(""marker"");

                var markersXml = this.req.responseXML;

                var markers = markersXml.getElementsByTagName(""marker"");

                //for (var i = 0; i < markers.length; i++)
                for (var i = 0, c = markers.length; i < c; i++)
                {
                    var longitude = parseFloat(markers[i].getAttribute(""longitude""));
                    var latitude = parseFloat(markers[i].getAttribute(""latitude""));
                    var imageUrl = markers[i].getAttribute(""imageUrl"");
                    var shadowUrl = markers[i].getAttribute(""shadowUrl"");
                    var iconWidth = parseInt(markers[i].getAttribute(""iconWidth""));
                    var iconHeight = parseInt(markers[i].getAttribute(""iconHeight""));
                    var shadowWidth = parseInt(markers[i].getAttribute(""shadowWidth""));
                    var shadowHeight = parseInt(markers[i].getAttribute(""shadowHeight""));
                    var iconAnchorX = parseFloat(markers[i].getAttribute(""iconAnchorX""));
                    var iconAnchorY = parseFloat(markers[i].getAttribute(""iconAnchorY""));
                    var infoAnchorX = parseFloat(markers[i].getAttribute(""infoAnchorX""));
                    var infoAnchorY = parseFloat(markers[i].getAttribute(""infoAnchorY""));
                    var enableNumbering = markers[i].getAttribute(""enableNumbering"");
                    var enablePopup = parseInt(markers[i].getAttribute(""enablePopup""));
                    var popupMaxWidth = parseInt(markers[i].getAttribute(""popupMaxWidth""));
                    var infoBoxType = markers[i].getAttribute(""infoBoxType"");
                    var infoHtml = markers[i].getAttribute(""infoHtml"");
                    infoHtml = infoHtml.replace(/&lt;/gi, ""<"");
                    infoHtml = infoHtml.replace(/&gt;/gi, "">"");
                    var hotelName = markers[i].getAttribute(""hotelName"");
                    var hotelCategory = markers[i].getAttribute(""hotelCategory"");
                    var enableUnitText = markers[i].getAttribute(""enableUnitText"");
                    var markerRateTextType = markers[i].getAttribute(""markerRateTextType"");
                    var perNightRate = markers[i].getAttribute(""perNightRate"");
                    var perStayRate = markers[i].getAttribute(""perStayRate"");
                    ");

            sCreateOverlaysMethodBody.Append("infoHtml = infoHtml.replace(/&quot;/gi, \"\\\"\");");

            sCreateOverlaysMethodBody.AppendFormat(@"OverlayCustomMarkerLocation{0}(longitude,latitude,imageUrl,shadowUrl,iconWidth,iconHeight,shadowWidth,shadowHeight,iconAnchorX,iconAnchorY,infoAnchorX,infoAnchorY,enableNumbering,enablePopup,popupMaxWidth,infoBoxType,infoHtml,hotelName,hotelCategory,enableUnitText,markerRateTextType,perNightRate,perStayRate);", sClientID);
            sCreateOverlaysMethodBody.Append(@"
                    infoHtml = null;
            ");
            sCreateOverlaysMethodBody.AppendLine("}");
            sCreateOverlaysMethodBody.Append(@"
                    markers = null;
                    markersXml = null;
                    this.req = null;
            ");

            string strCreateOverlaysMethodBody = sCreateOverlaysMethodBody.ToString();
#if !DEBUG
            strCreateOverlaysMethodBody = jsCompressor.Compress(strCreateOverlaysMethodBody);
#endif // #if !DEBUG

            return strCreateOverlaysMethodBody;
        }

        /// <summary>
        /// Method to generate the Javascript function to center and Zoom in
        /// at a point on a map
        /// </summary>
        /// <param name="sGMapVariable">Variable representing the map</param>
        /// <param name="sClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        /// <code>
        /// function ZoomIn(clong,cLat,ZoomLevel)
        /// {
        /// map.centerAndZoom(new GPoint(clong,cLat), ZoomLevel);
        /// }
        /// </code>
        internal string GenerateZoomInMethodBody(string sGMapVariable, string sClientID, MapPageType mapPageType)
        {
            StringBuilder sZoomInMethodBody = new StringBuilder();
            //Generate a unique Name for the function
            sZoomInMethodBody.AppendFormat("function ZoomIn{0}(clong,cLat,ZoomLevel)\n", sClientID);
            sZoomInMethodBody.Append("{ \n");
            //sZoomInFunction.AppendFormat("{0}.centerAndZoom(new GPoint(clong,cLat),ZoomLevel)", sGMapVariable);

            //if (mapPageType == MapPageType.SELECTHOTEL)
            //{
            //    sZoomInMethodBody.AppendFormat("{0}.checkResize();\n", sGMapVariable);
            //}
            sZoomInMethodBody.AppendFormat("{0}.setCenter(new google.maps.LatLng(cLat, clong), ZoomLevel);\n", sGMapVariable);
            sZoomInMethodBody.Append("} \n");

            string strZoomInMethodBody = sZoomInMethodBody.ToString();
#if !DEBUG
            strZoomInMethodBody = jsCompressor.Compress(strZoomInMethodBody);
#endif // #if !DEBUG

            return strZoomInMethodBody;
        }

        /// <summary>
        /// Copyright shown by the Google will be reduced in size
        /// </summary>
        /// <param name="gMapVariable">Variable representing the Google Map object</param>
        /// <returns>Javascript code as string</returns>
        internal string GenerateMakeCopyrightSmallerMethodBody(string gMapVariable)
        {
            StringBuilder sMakeCopyrightSmallerMethodBody = new StringBuilder();
            sMakeCopyrightSmallerMethodBody.Append("function MakeCopyrightSmaller()\n");
            sMakeCopyrightSmallerMethodBody.Append("{\n");
            sMakeCopyrightSmallerMethodBody.Append("for(var i = 0; i < ");
            sMakeCopyrightSmallerMethodBody.AppendFormat("{0}.getContainer().childNodes.length;", gMapVariable);
            sMakeCopyrightSmallerMethodBody.Append("++i)\n");
            sMakeCopyrightSmallerMethodBody.Append("{\n");
            sMakeCopyrightSmallerMethodBody.Append("if(");
            sMakeCopyrightSmallerMethodBody.AppendFormat("{0}.getContainer().childNodes[i].innerHTML.indexOf(String.fromCharCode(169)) !== -1)\n", gMapVariable);
            sMakeCopyrightSmallerMethodBody.Append("{\n");
            sMakeCopyrightSmallerMethodBody.AppendFormat("if({0}.getContainer().childNodes[i].offsetLeft < 0)\n", gMapVariable);
            sMakeCopyrightSmallerMethodBody.Append("{\n");
            sMakeCopyrightSmallerMethodBody.AppendFormat("{0}.getContainer().childNodes[i].style.fontSize = '3px';\n", gMapVariable);
            sMakeCopyrightSmallerMethodBody.Append("}\n");
            sMakeCopyrightSmallerMethodBody.Append("break;\n");
            sMakeCopyrightSmallerMethodBody.Append("}\n");
            sMakeCopyrightSmallerMethodBody.Append("}\n");
            sMakeCopyrightSmallerMethodBody.Append("}\n");

            string strMakeCopyrightSmallerMethodBody = sMakeCopyrightSmallerMethodBody.ToString();
#if !DEBUG
            strMakeCopyrightSmallerMethodBody = jsCompressor.Compress(strMakeCopyrightSmallerMethodBody);
#endif // #if !DEBUG

            return strMakeCopyrightSmallerMethodBody;
        }

        ///// <summary>
        ///// Utility Method to generate JavaScript code to Set the Map type
        ///// </summary>
        ///// <param name="mapType">The Type of Map</param>
        ///// <param name="sGMapVariable">Variable representing the Google Map object</param>
        ///// <returns>Javascript code as string</returns>
        //internal string GenerateMapType(GMapType mapType, string sGMapVariable)
        //{
        //    string sMapType = string.Empty;
        //    switch (mapType)
        //    {
        //        case GMapType.MAP:
        //            sMapType = "G_NORMAL_MAP";
        //            break;
        //        case GMapType.HYBRID:
        //            sMapType = "G_HYBRID_MAP";
        //            break;
        //        case GMapType.SATELLITE:
        //            sMapType = "G_SATELLITE_MAP";
        //            break;
        //        default:
        //            sMapType = "G_NORMAL_MAP";
        //            break;
        //    }

        //    return string.Format("{0}.setMapType({1});\n", sGMapVariable, sMapType);
        //}

        /// <summary>
        /// Method that generates JavaScript code to Initialize the map Variable
        /// </summary>
        /// <param name="sGMapVariable">Variable representing the map</param>
        /// <param name="sClientID">Client ID of the control</param>
        /// <param name="bEnableDrag">Set if the map can be dragged</param>
        /// <param name="bEnableInfoWindow">set if there can be pop up windows on the map</param>
        /// <param name="bEnableMapTypeControl">set if the map Type can be changed</param>
        /// <param name="bEnableDblClickZoom">Set if the map can be zoomed with double-click</param>
        /// <param name="bEnableMouseScrollZoom">Set if the map can be zoomed with mouse scroll wheel</param>
        /// <returns>JavaScript code as string</returns>
        internal string GenerateGMapVariableInitialization(string sGMapVariable, string sClientID, bool bEnableDrag, bool bEnableInfoWindow, bool bEnableMapTypeControl, bool bEnableDblClickZoom, bool bEnableMouseScrollZoom, MapPageType mapPageType)
        {
            StringBuilder sInitVarBuilder = new StringBuilder();

            if (mapPageType == MapPageType.SELECTHOTEL)
            {
                sInitVarBuilder.AppendFormat("{1} = new google.maps.Map(document.getElementById('{0}'), {{ size: new GSize(718,399) }});\n", sClientID, sGMapVariable);    
            }
            else
            {
                sInitVarBuilder.AppendFormat("{1} = new google.maps.Map(document.getElementById('{0}'));\n", sClientID, sGMapVariable);
            }

            if (!bEnableDrag)
                sInitVarBuilder.AppendFormat("{0}.disableDragging() ;\n", sGMapVariable);
            if (!bEnableInfoWindow)
                sInitVarBuilder.AppendFormat("{0}.disableInfoWindow() ;\n", sGMapVariable);
            if (bEnableDblClickZoom)
                sInitVarBuilder.AppendFormat("{0}.enableDoubleClickZoom() ;\n", sGMapVariable);
            if (bEnableMouseScrollZoom)
                sInitVarBuilder.AppendFormat("{0}.enableScrollWheelZoom() ;\n", sGMapVariable);
            if (bEnableMapTypeControl)
                sInitVarBuilder.AppendFormat("{0}.addControl(new GMapTypeControl()) ;\n", sGMapVariable);

            return sInitVarBuilder.ToString();
        }

        /// <summary>
        /// Method to Generat the JavaScript code to set the type of
        /// Scroll Control on the Map
        /// </summary>
        /// <param name="scrollType">The Scroll Control to be added</param>
        /// <param name="sGmapVariable">Variable representing the Google Map</param>
        /// <returns>Javascript code as string</returns>
        internal string GenerateScrollControl(GMapScrollControl scrollType, string sGmapVariable)
        {
            string sScrollType = string.Empty;
            switch (scrollType)
            {
                case GMapScrollControl.LARGE:
                    sScrollType = string.Format(" {0}.addControl(new GLargeMapControl());\n", sGmapVariable);
                    break;
                case GMapScrollControl.SMALL:
                    sScrollType = string.Format(" {0}.addControl(new GSmallMapControl());\n", sGmapVariable);
                    break;
                case GMapScrollControl.SMALLZOOMONLY:
                    sScrollType = string.Format(" {0}.addControl(new GSmallZoomControl());\n", sGmapVariable);
                    break;
                case GMapScrollControl.DEFAULTGOOGLEUI:
                    sScrollType = string.Format(@"google.maps.ZoomControlStyle.DEFAULT ",sGmapVariable);
                    /*<!-- var customMapUI = {0}.getDefaultUI(); 
                    var customMapUI ={0};
                    if(customMapUI.controls.menumaptypecontrol == true)
                    {{
                      customMapUI.controls.menumaptypecontrol = false;
                    }}
                    if(customMapUI.controls.maptypecontrol == true)
                    {{
                      customMapUI.controls.maptypecontrol = false;
                    }}
                    {0}.setMap(customMapUI);
                    {0}.removeMapType(google.maps.SATELLITE);
                    {0}.removeMapType(google.maps.PHYSICAL);
                    {0}.removeMapType(google.maps.HYBRID);
                  ", sGmapVariable);-->*/
                    break;
                case GMapScrollControl.NONE:
                default:
                    sScrollType = string.Empty;
                    break;

            }
            return sScrollType;
        }
        #endregion // Internal Methods


        /// <summary>
        /// Generates the filter control prototype method body.
        /// </summary>
        /// <param name="gMapVariable">The g map variable.</param>
        /// <param name="recentlyOpenedHotels">if set to <c>true</c> recently opened hotels check box will be selected by default in the hotel filter.</param>
        /// <param name="comingSoonHotels">if set to <c>true</c> coming soon hotels check box will be selected by default in the hotel filter.</param>
        /// <param name="otherHotels">if set to <c>true</c> other hotels check box will be selected by default in the hotel filter.</param>
        /// <returns>javascript code as string</returns>
        internal string GenerateFilterControlPrototypeMethodBody(string gMapVariable, bool recentlyOpenedHotels, bool comingSoonHotels, bool otherHotels)
        {
            StringBuilder filterMethodBody = new StringBuilder();

            if(recentlyOpenedHotels == true)
            {
                filterMethodBody.Append(@"
                var recentlyOpened = document.getElementById(""RecentlyOpened"");
                recentlyOpened.checked = true;
                ");
            }
            if (comingSoonHotels == true)
            {
                filterMethodBody.Append(@"
                var comingSoon = document.getElementById(""ComingSoon"");
                comingSoon.checked = true;
                ");
            }
            if (otherHotels == true)
            {
                filterMethodBody.Append(@"
                var others = document.getElementById(""Others"");
                others.checked = true;
                ");
            }

            filterMethodBody.AppendFormat(@"
                function FilterControl() {{}};
                FilterControl.prototype = new GControl();
                FilterControl.prototype.initialize = function({0}) {{
                 var filter = document.getElementById(""HotelFilter"");
                 {0}.getContainer().appendChild(filter);
                 return filter;
                }};
            
                FilterControl.prototype.getDefaultPosition = function() {{
                    return new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize({1}, {2}));
                }};
            ", gMapVariable, GoogleMapConstants.PADDING_X_HOTELFILTER, GoogleMapConstants.PADDING_Y_HOTELFILTER);

            string strFilterMethodBody = filterMethodBody.ToString();
//#if !DEBUG
//            strFilterMethodBody = jsCompressor.Compress(strFilterMethodBody);
//#endif // #if !DEBUG

            return strFilterMethodBody;
        }

        /// <summary>
        /// Generates the filter switch method body.
        /// </summary>
        /// <param name="gMapVariable">The google map variable.</param>
        /// <param name="arrayVariable">The array variable.</param>
        /// <param name="arrayVarRecentlyOpened">The array var recently opened.</param>
        /// <param name="arrayVarComingSoon">The array var coming soon.</param>
        /// <returns>javascript code as string</returns>
        internal string GenerateFilterSwitchMethodBody(string gMapVariable, string arrayVariable, string arrayVarRecentlyOpened, string arrayVarComingSoon)
        {
            StringBuilder filterSwitch = new StringBuilder();

            filterSwitch.AppendFormat(@"
                function SwitchHotelFilter(hotelCategory)
                {{
                    {0}.closeExtInfoWindow();
                    switch (hotelCategory)
                    {{
                        case ""RecentlyOpened"" :
                        {{
                            var recentlyOpened = document.getElementById(""RecentlyOpened"");
                            if(recentlyOpened.checked == true)
                            {{
                                recentlyOpenedHotels.addMarkers({2}, 0);
                                recentlyOpenedHotels.refresh();
                            }}
                            else
                            {{
                                recentlyOpenedHotels.clearMarkers();
                            }}
                        }}
                        break;
                        case ""ComingSoon"" :
                        {{
                            var comingSoon = document.getElementById(""ComingSoon"");
                            if(comingSoon.checked == true)
                            {{
                                comingSoonHotels.addMarkers({3}, 0);
                                comingSoonHotels.refresh();
                            }}
                            else
                            {{
                                comingSoonHotels.clearMarkers();
                            }}
                        }}
                        break;
                        case ""Others"" :
                        {{
                            var others = document.getElementById(""Others"");
                            if(others.checked == true)
                            {{
                                markerManager.addMarkers({1}, 0);
                                markerManager.refresh();
                            }}
                            else
                            {{
                                markerManager.clearMarkers();
                            }}
                        }}
                        break;
                    }}
                }}
            ", gMapVariable, arrayVariable, arrayVarRecentlyOpened, arrayVarComingSoon);

            string strFilterSwitch = filterSwitch.ToString();
#if !DEBUG
            strFilterSwitch = jsCompressor.Compress(strFilterSwitch);
#endif // #if !DEBUG

            return strFilterSwitch;
        }

        /// <summary>
        /// Generates the reset map method body.
        /// </summary>
        /// <param name="gMapVariable">The google map variable.</param>
        /// <param name="recentlyOpenedHotels">if set to <c>true</c> recently opened hotels check box will be selected in the hotel filter.</param>
        /// <param name="comingSoonHotels">if set to <c>true</c> coming soon hotels check box will be selected in the hotel filter.</param>
        /// <param name="otherHotels">if set to <c>true</c> other hotels check box will be selected in the hotel filter.</param>
        /// <returns>javascript code as string</returns>
        internal string GenerateResetMapMethodBody(string gMapVariable, bool recentlyOpenedHotels, bool comingSoonHotels, bool otherHotels)
        {
            StringBuilder resetMapMethod = new StringBuilder();

            resetMapMethod.AppendFormat(@"
                function ResetMap()
                {{
                    {0}.closeExtInfoWindow();
                    //{0}.setMapType(G_NORMAL_MAP); //This is not required as there is only on map type.
                    {0}.returnToSavedPosition();
            ", gMapVariable);

            resetMapMethod.AppendFormat(@"
                    var recentlyOpened = document.getElementById(""RecentlyOpened"");
                    if({0}) recentlyOpened.click();
            ",
            recentlyOpenedHotels == true ? "recentlyOpened.checked != true" : "recentlyOpened.checked == true"
            );

            resetMapMethod.AppendFormat(@"
                    var comingSoon = document.getElementById(""ComingSoon"");
                    if({0}) comingSoon.click();
            ",
            comingSoonHotels == true ? "comingSoon.checked != true" : "comingSoon.checked == true"
            );

            resetMapMethod.AppendFormat(@"
                    var others = document.getElementById(""Others"");
                    if({0}) others.click();
            ",
            otherHotels == true ? "others.checked != true" : "others.checked == true"
            );

            resetMapMethod.Append(@"
                }
            ");

            string strResetMapMethod = resetMapMethod.ToString();
#if !DEBUG
            strResetMapMethod = jsCompressor.Compress(strResetMapMethod);
#endif // #if !DEBUG

            return strResetMapMethod;
        }

        
        /// <summary>
        /// Generates the switch unit text method body.
        /// </summary>
        /// <param name="arrayVariable">The array variable.</param>
        /// <param name="arrayVarPerNightRate">The array var per night rate.</param>
        /// <param name="arrayVarPerStayRate">The array var per stay rate.</param>
        /// <returns>javascript code as string</returns>
        internal string GenerateSwitchUnitTextMethodBody(string arrayVariable, string arrayVarPerNightRate, string arrayVarPerStayRate)
        {
            StringBuilder unitTextSwitch = new StringBuilder();
            //Vrushali | Res 2.0 | Per stay and per night functionality not working fixed in GoogleMap.
            unitTextSwitch.AppendFormat(@"
                function SwitchUnitText(rateType)
                {{
                    switch(rateType)
                    {{
                        case ""rdoPerNightMap"" :
                                for (var i = 0; i < {0}.length; i++)
                                {{
                                    {0}[i].setLabelText({1}[i]);
                                }}
                                markerManager.refresh();
                            
                            break;
                        case ""rdoPerStayMap"" :
                               for (var i = 0; i < {0}.length; i++)
                                {{
                                    {0}[i].setLabelText({2}[i]);
                                }}
                                markerManager.refresh();
                            
                            break;
                    }}
                }}
            ", arrayVariable, arrayVarPerNightRate, arrayVarPerStayRate);

            string strUnitTextSwitch = unitTextSwitch.ToString();
#if !DEBUG
            strUnitTextSwitch = jsCompressor.Compress(strUnitTextSwitch);
#endif // #if !DEBUG

            return strUnitTextSwitch;
        }

        internal string GenerateSwitchUnitTextMethodBody(string gMapVariable, string arrayVariable, string arrayVarPerNightRate, string arrayVarPerStayRate)
        {
            StringBuilder unitTextSwitch = new StringBuilder();
            //Vrushali | Res 2.0 | Per stay and per night functionality not working fixed in GoogleMap.
            unitTextSwitch.AppendFormat(@"
                function SwitchUnitText(rateType)
                {{
                    switch(rateType)
                    {{
                        case ""rdoPerNightMap"" :
                             for (var i = 0; i < {0}.length; i++)
                                {{
                                    {0}[i].setLabelText({1}[i]);
                                }}
                           
                            break;
                        case ""rdoPerStayMap"" :
                                for (var i = 0; i < {0}.length; i++)
                                {{
                                    {0}[i].setLabelText({2}[i]);
                                }}
                            
                            break;
                    }}
                }}
            ", arrayVariable, arrayVarPerNightRate, arrayVarPerStayRate, gMapVariable);

            string strUnitTextSwitch = unitTextSwitch.ToString();
#if !DEBUG
            strUnitTextSwitch = jsCompressor.Compress(strUnitTextSwitch);
#endif // #if !DEBUG

            return strUnitTextSwitch;
        }
    }
}
