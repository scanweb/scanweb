// <copyright file="GMapEnums.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Enums used by GMap</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Data;
    using System.Configuration;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    #endregion // System NameSpaces

    /// <summary>
    /// Enumerator for different types of Maps
    /// </summary>
    public enum GMapType
    {
        /// <summary>
        /// Satellite map
        /// </summary>
        SATELLITE,
        /// <summary>
        /// Plain Map
        /// </summary>
        MAP,
        /// <summary>
        /// Hybrid map
        /// </summary>
        HYBRID
    }

    /// <summary>
    /// Enumerator for the different types of scroll controls
    /// </summary>
    public enum GMapScrollControl
    {
        /// <summary>
        /// Small Scroll Control
        /// </summary>
        SMALL,
        /// <summary>
        /// Large Scroll Control
        /// </summary>
        LARGE,
        /// <summary>
        /// Small Control for Zoom only
        /// </summary>
        SMALLZOOMONLY,
        /// <summary>
        /// This adds all the dafault buttons to the map as in www.maps.google.com
        /// </summary>
        DEFAULTGOOGLEUI,
        /// <summary>
        /// No Scroll or Zoom Control
        /// </summary>
        NONE
    }

    /// <summary>
    /// This enum indicates kind of map we are showing
    /// (Whether FAH or older pages)
    /// </summary>
    public enum GMapComplexity
    {
        /// <summary>
        /// Except FAH page
        /// </summary>
        SIMPLE,

        /// <summary>
        /// For FIND A HOTEL
        /// </summary>
        COMPLEX
    }
}
