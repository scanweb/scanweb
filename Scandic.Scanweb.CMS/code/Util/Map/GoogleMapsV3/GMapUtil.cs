// <copyright file="GMapControl.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Control which generates the javascript to render the google map</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Data;
    using System.Configuration;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    #endregion // System NameSpaces

    /// <summary>
    /// Static class have static util methods
    /// </summary>
    public static class GMapUtil
    {
        ///// <summary>
        ///// Gets the url for the marker
        ///// </summary>
        ///// <param name="hotelCategory">type of hotel</param>
        ///// <param name="enableUnitNumbering">marker numbering</param>
        ///// <param name="iconUrl">icon url</param>
        ///// <param name="shadowUrl">shadow url</param>
        //public static void GetMarkerUrls(string hotelCategory, bool enableUnitNumbering, out string iconUrl, out string shadowUrl)
        //{
        //    switch (hotelCategory)
        //    {
        //        case AppConstants.DEFAULT:
        //            {
        //                if (enableUnitNumbering)
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel.png");
        //                else
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_greyedout.png");

        //                shadowUrl = string.Empty;
        //            }
        //            break;
        //        case AppConstants.RECENTLYOPENED_VALUE:
        //            {
        //                if (enableUnitNumbering)
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel.png");
        //                else
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/recentlyopened_hotel_greyedout.png");

        //                shadowUrl = string.Empty;
        //            }
        //            break;
        //        case AppConstants.COMINGSOON_VALUE:
        //            {
        //                if (enableUnitNumbering)
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel.png");
        //                else
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/comingsoon_hotel_greyedout.png");

        //                shadowUrl = string.Empty;
        //            }
        //            break;
        //        default:
        //            {
        //                if (enableUnitNumbering)
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel.png");
        //                else
        //                    iconUrl = string.Format("http://{0}/{1}", Request.Url.Host, "/Templates/Scanweb/Styles/Default/Images/Icons/regular_hotel_greyedout.png");

        //                shadowUrl = string.Empty;
        //            }
        //            break;
        //    }
        //}
    }
}
