// <copyright file="HelperColorConvertor.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Color converter class</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Drawing;
    #endregion // System NameSpaces

    /// <summary>
    /// Helper Class to Convert a Color into its
    /// Hexagecimal representation
    /// </summary>
    internal class HelperColorConvertor
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public HelperColorConvertor()
        {
        }

        /// <summary>
        /// Static method which converts Color to its
        /// HEX value
        /// </summary>
        /// <param name="sColor">Color that has to be converted</param>
        /// <returns>Hexagecimal representation of the color</returns>
        public static string ConvertColor(Color sColor)
        {
            Byte R = sColor.R;
            Byte G = sColor.G;
            Byte B = sColor.B;
            return string.Format("#{0}{1}{2}", R.ToString("X").PadLeft(2, Char.Parse("0")), G.ToString("X").PadLeft(2, Char.Parse("0")), B.ToString("X").PadLeft(2, Char.Parse("0")));
        }
    }
}
