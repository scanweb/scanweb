// <copyright file="HelperColorConvertor.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Binded data resolver class</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMapsV3
{
    #region System NameSpaces
    using System;
    using System.Data;
    using System.Collections;
    #endregion // System NameSpaces

    /// <summary>
    /// Helper Class to resolve data
    /// </summary>
    internal class HelperDataResolver
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HelperDataResolver()
        {
        }

        /// <summary>
        /// Helper method to cast datasource object
        /// to IEnumerable
        /// </summary>
        /// <param name="ds">Data source object</param>
        /// <returns>returns Datasource as IEnumerable</returns>
        public static IEnumerable GetResolvedDataSource(object ds)
        {
            if (ds is IEnumerable)
                return (IEnumerable)ds;
            else if (ds is DataTable)
                return (IEnumerable)(((DataTable)ds).DefaultView);
            else if (ds is DataSet)
            {
                DataView dv = ((DataSet)ds).Tables[0].DefaultView;
                return (IEnumerable)dv;
            }
            else if (ds is IList)
                return (IEnumerable)((IList)ds);
            else
                return null;
        }

    }
}
