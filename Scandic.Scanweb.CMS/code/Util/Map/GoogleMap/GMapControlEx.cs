// <copyright file="GMapControlEx.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>18-Sep-2009</date>
// <version></version>
// <summary>Google Map control to display the google map in FindAHotel page and associated new pages</summary>

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Collections;
using System.Data;
using System.Globalization;
using EPiServer.Core;
using Scandic.Scanweb.CMS.code.Util.Map;

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMap
{
    public class GMapControlEx : GMapControl
    {
        CultureInfo ci = new System.Globalization.CultureInfo("en-US");
        private StringBuilder initFunction;
        private string zoomAndCenter = string.Empty;
        private StringBuilder pointOverlay;
        private StringBuilder lineOverlay;
        private string funcNameKey = "Init";
        private string functionName = "InitMap";
        private string directionVariable = "directions";
        private string directionPanelVariable = "directionsPanel";
        private string geocodeVariable = "geocoder";
        private string arrayVariable = "arrayPoints";
        private JScriptGenerator scriptGenerator;
        private bool overrideEnabledrag;


        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <remarks>
        /// Since Google Maps needs to rendered as a DIV
        /// We set the Textwriter to render as a DIV
        ///</remarks>
        public GMapControlEx()
            : base()
        {
            initFunction = new StringBuilder();
            pointOverlay = new StringBuilder();
            lineOverlay = new StringBuilder();
            scriptGenerator = new JScriptGenerator();
            this.mapType = GMapType.MAP;
            this.mapScrollType = GMapScrollControl.NONE;
            this.enableDrag = true;
            this.enableInfoWindow = true;
        }

        /// <summary>
        /// Init method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.viewStateLatitude = viewStateLatitude + this.ClientID;
            this.viewStateLongitude = viewStateLongitude + this.ClientID;
            this.viewStateHTMLText = viewStateHTMLText + this.ClientID;
            this.viewStateDataBoundText = viewStateDataBoundText + this.ClientID;
            this.viewStateIcon = viewStateIcon + this.ClientID;
            this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
            this.viewStateIconShadow = viewStateIconShadow + this.ClientID;
        }

        /// <summary>
        /// Pre Render method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            funcNameKey = funcNameKey + this.ClientID;
            functionName = functionName + this.ClientID;
            GMapVariable = GMapVariable + this.ClientID;
            arrayVariable = arrayVariable + this.ClientID;
            RegisterAllScripts();
        }

        /// <summary>
        /// Render method
        /// </summary>
        /// <param name="writer">Instance of HtmlTextWriter</param>
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (this.Site != null && this.Site.DesignMode)
            {
                writer.RenderBeginTag("h2");
                writer.Write(this.ID);
                writer.RenderEndTag();
            }
        }

        /// <summary>
        /// Method which registers all JavaScripts and the map key
        /// </summary>
        private void RegisterAllScripts()
        {
            string sKeyScript = string.Empty;
            sKeyScript = string.Format("<script src='http://maps.google.com/maps?file=api&v=2&key={0}' type='text/javascript'></script>", googleKey);
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("GMapKey"))
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GMapKey", sKeyScript);

            if (!this.Page.ClientScript.IsStartupScriptRegistered(funcNameKey))
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), funcNameKey, InitializeMap());
        }

        /// <summary>
        /// Method to Overlay a marker on the Map.
        /// </summary>
        /// <param name="curMarker">Marker to be overlayed</param>
        public void OverlayMarker(MapMarker curMarker)
        {
            if (curMarker.Icon != null)
                pointOverlay.AppendFormat(ci, "OverlayCustomMarkerLocation{0}({1},{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},0,'{13}');\n", this.ClientID, curMarker.Point.Longitude, curMarker.Point.Latitude,
                    curMarker.Icon.ImageURL, curMarker.Icon.ShadowImageURL, curMarker.Icon.IconSize.Width, curMarker.Icon.IconSize.Height, curMarker.Icon.ShadowSize.Width, curMarker.Icon.ShadowSize.Height, curMarker.Icon.IconAnchor.Longitude,
                    curMarker.Icon.IconAnchor.Latitude, curMarker.Icon.InfoWindowAnchor.Longitude, curMarker.Icon.InfoWindowAnchor.Latitude, string.Empty);
            else
                pointOverlay.AppendFormat(ci, "OverlayLocation{3}({0},{1},0,'{2}');\n", curMarker.Point.Longitude, curMarker.Point.Latitude, string.Empty, this.ClientID);
        }

        /// <summary>
        /// Method to Overlay a marker on the map with a popup
        /// Enable drag cannot be false
        /// </summary>
        /// <param name="curMarker">Marker to be overlayed</param>
        /// <param name="sFormattedHtml">HTML to be displayed in the pop up</param>
        public void OverlayMarker(MapMarker curMarker, string sFormattedHtml)
        {
            sFormattedHtml = sFormattedHtml.Replace("'", string.Empty);
            overrideEnabledrag = true;
            if (curMarker.Icon != null)
                pointOverlay.AppendFormat(ci, "OverlayCustomMarkerLocation{0}({1},{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},1,'{13}');\n", this.ClientID, curMarker.Point.Longitude, curMarker.Point.Latitude,
                    curMarker.Icon.ImageURL, curMarker.Icon.ShadowImageURL, curMarker.Icon.IconSize.Width, curMarker.Icon.IconSize.Height, curMarker.Icon.ShadowSize.Width, curMarker.Icon.ShadowSize.Height, curMarker.Icon.IconAnchor.Longitude,
                    curMarker.Icon.IconAnchor.Latitude, curMarker.Icon.InfoWindowAnchor.Longitude, curMarker.Icon.InfoWindowAnchor.Latitude, sFormattedHtml);
            else
                pointOverlay.AppendFormat(ci, "OverlayLocation{3}({0},{1},1,'{2}');\n", curMarker.Point.Longitude, curMarker.Point.Latitude, sFormattedHtml, this.ClientID);
        }
        

        /// <summary>
        /// Method that Generates all the Javascript
        /// </summary>
        /// <returns>Javascript code as a string</returns>
        private string InitializeMap()
        {
            initFunction.Append("</form>");
            initFunction.Append("<script type='text/javascript' language='javascript'>\n");
            initFunction.Append("if(GBrowserIsCompatible()){\n");
            initFunction.AppendFormat("var {0};", GMapVariable);
            initFunction.AppendFormat("var {0}= new Array();", arrayVariable);
            initFunction.Append("var locBounds = new GLatLngBounds();\n");
            if (overrideEnabledrag)
                enableDrag = true;

            initFunction.AppendFormat
                ("{0}\n", scriptGenerator.GenerateGMapVariableInitialization(GMapVariable, this.ClientID, 
                enableDrag, enableInfoWindow, enableMapTypeControl, enableDblClickZoom, enableMouseScrollZoom));


            if (EnableDirections)
            {
                initFunction.AppendFormat("var {0};\n", directionVariable);
                initFunction.AppendFormat("var {0};\n", directionPanelVariable);

                initFunction.AppendFormat("var {0} = new GClientGeocoder();\n", geocodeVariable);
                initFunction.Append("var locPoint = new GLatLng();\n");

                initFunction.AppendFormat("{0} = document.getElementById(\"" + DirectionsContainer + "\");\n", directionPanelVariable);
                initFunction.AppendFormat("{0} = new GDirections({1}, {2});\n", directionVariable, GMapVariable, directionPanelVariable);

                if (DestinationWay == DriveDirection.TO)
                {
                    initFunction.AppendFormat("{0}.getLatLng(\"" + DestinationTo + "\", function(point)\n", geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat("{0}.loadFromWaypoints([\"" + DestinationFrom + "\", point], ", directionVariable);
                    initFunction.Append("{locale:\"" + Locale + "\"});\n");
                }
                else
                {
                    initFunction.AppendFormat("{0}.getLatLng(\"" + DestinationFrom + "\", function(point)\n", geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat("{0}.loadFromWaypoints([point, \"" + DestinationTo + "\"], ", directionVariable);
                    initFunction.Append("{locale:\"" + Locale + "\"});\n");
                }

                initFunction.Append("}\n");
                initFunction.Append("}\n");
                initFunction.Append(");\n");
            }

            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateScrollControl(mapScrollType, GMapVariable));
            initFunction.AppendFormat("{0}\n", pointOverlay.ToString());
            initFunction.AppendFormat("{0}\n", lineOverlay.ToString());
            initFunction.AppendFormat("{0}\n", zoomAndCenter);
            initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateNewOverLayFunction(GMapVariable, this.ClientID));

            initFunction.Append("}\n");
            if (zoomAndCenter.Trim().Length > 0)
                initFunction.AppendFormat("{0}\n", scriptGenerator.GenerateZoomInFunction(GMapVariable, this.ClientID));
            if (pointOverlay.Length > 0)
            {
                initFunction.AppendFormat
                    ("{0}\n", scriptGenerator.GenerateOverLayPointFuncCall(arrayVariable, this.ClientID));
                initFunction.AppendFormat
                    ("{0}\n", scriptGenerator.GenerateCustomOverLayPointFuncCall(arrayVariable, this.ClientID));
            }
            if (lineOverlay.Length > 0)
                initFunction.AppendFormat
                    ("{0}\n", scriptGenerator.GenerateOverlayLineFuncCall(arrayVariable, this.ClientID));
            initFunction.Append("setTimeout('MakeCopyrightSmaller();', 500);\n");

            #region MakeCopyrightSmaller()
            initFunction.Append("function MakeCopyrightSmaller()\n");
            initFunction.Append("{\n");
            initFunction.Append("for(var i = 0; i < ");
            initFunction.AppendFormat("{0}.getContainer().childNodes.length;", GMapVariable);
            initFunction.Append("++i)\n");
            initFunction.Append("{\n");
            initFunction.Append("if(");
            initFunction.AppendFormat("{0}.getContainer().childNodes[i].innerHTML.indexOf(String.fromCharCode(169))\n", GMapVariable);
            initFunction.Append("!== -1)\n");
            initFunction.Append("{\n");
            initFunction.AppendFormat("if({0}.getContainer().childNodes[i].offsetLeft < 0)\n", GMapVariable);
            initFunction.Append("{\n");
            initFunction.AppendFormat("{0}.getContainer().childNodes[i].style.fontSize = '3px';\n", GMapVariable);
            initFunction.Append("}\n");
            initFunction.Append("break;\n");
            initFunction.Append("}\n");
            initFunction.Append("}\n");
            initFunction.Append("}\n");
            #endregion

            initFunction.Append("</script>\n");
            initFunction.Append("<form>");
            return initFunction.ToString();
        }

        /// <summary>
        /// DataBinding Method
        /// </summary>
        /// <param name="e">Event Args</param>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            IEnumerable test = HelperDataResolver.GetResolvedDataSource(dataSource);
            if (test != null)
            {
                foreach (object ditem in test)
                {
                    try
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        if (typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).GetType()
                            && typeof(string) == DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).GetType())
                        {
                            lat = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField).ToString(), ci);
                            lng = double.Parse(DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField).ToString(), ci);
                        }
                        else
                        {
                            lat = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLatitudeField);
                            lng = (double)DataBinder.GetPropertyValue(ditem, this.MarkerLongitudeField);
                        }

                        MapPoint gp = new MapPoint(lat, lng);
                        MapMarker gm = null;                     
                        if (ditem.GetType().Name.Equals("GoogleMapLocalAttractionUnit"))
                        {
                            try
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append("<div style=\"font-size:12px;\"><b>" + (string)DataBinder.GetPropertyValue(ditem, "localAttractionName") + "</b></div>");
                                sb.Append("<div style=\"font-size:10px;\">");
                                sb.Append((string)DataBinder.GetPropertyValue(ditem, "localAttractionAddress"));
                                sb.Append("</div>");
                                sb.Append("<div style=\"font-size:10px;\">");
                                sb.Append("<div class=\"LinkListItem\">");
                                sb.Append("<div class=\"LastLink\">");
                                                                
                                if (!String.IsNullOrEmpty((string)DataBinder.GetPropertyValue(ditem, "localAttractionLinkText")))
                                {
                                    if (((string)DataBinder.GetPropertyValue(ditem, "localAttractionLinkText")).Trim() != string.Empty)
                                    {
                                        sb.Append("<a class=\"IconLink\" href=\"" + (string)DataBinder.GetPropertyValue(ditem, "localAttractionURL") + "\" target=\"_blank\">" + ((string)DataBinder.GetPropertyValue(ditem, "localAttractionLinkText")) + "</a>");
                                    }
                                }
                                sb.Append("</div>");
                                sb.Append("</div>");
                                sb.Append("</div>");
                                sText = sb.ToString();
                            }
                            catch (Exception ex)
                            {
                                if (!(ex is ArgumentNullException))
                                    throw ex;
                            }

                            gm = new MapMarker(gp);

                            if (MarkerIconURL != null && this.MarkerIconURL.Length > 0)
                            {
                                MapIcon gi = new MapIcon(MarkerIconURL, MarkerIconShadowURL, new MapSize(31, 27), new MapSize(45, 29), new MapPoint(15, 13), new MapPoint(15, 13));
                                gm.Icon = gi;
                            }
                        }
                        if (ditem.GetType().Name.Equals("GoogleMapsUnit"))
                        {
                            gm = new MapMarker(gp);

                            if (MarkerIconURL != null && this.MarkerIconURL.Length > 0)
                            {
                                MapIcon gi = new MapIcon(MarkerIconURL, MarkerIconShadowURL, new MapSize(31, 27), new MapSize(45, 29), new MapPoint(15, 13), new MapPoint(15, 13));
                                gm.Icon = gi;
                            }

                            if (!String.IsNullOrEmpty(DataBinder.GetPropertyValue(ditem, "html") as string))
                                sText = (DataBinder.GetPropertyValue(ditem, "html") as string) ?? String.Empty;
                        }
                        if (ditem.GetType().Name.Equals("GoogleMapsHotelUnit"))
                        {
                            try
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append("<div style=\"font-size:12px;\"><b>" + (string)DataBinder.GetPropertyValue(ditem, "hotelName") + "</b></div>");
                                sb.Append("<div style=\"font-size:10px;\">");
                                sb.Append((string)DataBinder.GetPropertyValue(ditem, "address") + ", ");
                                sb.Append((string)DataBinder.GetPropertyValue(ditem, "zip") + " ");
                                sb.Append((string)DataBinder.GetPropertyValue(ditem, "city") + ", ");
                                sb.Append("</div>");
                                sb.Append
                                    ("<div style=\"font-size:10px;\">" +
                                    (string)DataBinder.GetPropertyValue(ditem, "Country") + "</div>");
                                sb.Append("<div style=\"font-size:10px;\">");
                                sb.Append("<div class=\"LinkListItem\">");
                                sb.Append("<div class=\"NotLastLink\">");
                                sb.Append("<a class=\"IconLink\" href=\"" + 
                                    (string)DataBinder.GetPropertyValue(ditem, "hotelPageURL") + "?GMapID=" +
                                    (string)DataBinder.GetPropertyValue(ditem, "hotelName") + "\" target=\"_top\">" + 
                                    LanguageManager.Instance.Translate("/Templates/Scanweb/Util/GoogleMaps/GmapControl/HotelPageURL") + "</a>");

                                sb.Append("</div>");
                                sb.Append("<div class=\"LastLink\">");
                                sb.Append("<a class=\"IconLink\" href=\"" + (string)DataBinder.GetPropertyValue(ditem, "hotelbookURL") + "&GMapID=" + (string)DataBinder.GetPropertyValue(ditem, "hotelName") + "\" target=\"_top\">" + LanguageManager.Instance.Translate("/Templates/Scanweb/Util/GoogleMaps/GmapControl/HotelBookURL") + "</a>");
                                sb.Append("</div>");
                                sb.Append("</div>");
                                sb.Append("</div>");
                                sText = sb.ToString();

                            }
                            catch (Exception ex)
                            {
                                if (!(ex is ArgumentNullException))
                                    throw ex;
                            }

                            if (ScandicIcon)
                            {

                                gm = new MapMarker(gp);

                                if (MarkerIconURL != null && this.MarkerIconURL.Length > 0)
                                {
                                    MapIcon gi = new MapIcon(MarkerIconURL, MarkerIconShadowURL, new MapSize(31, 27), new MapSize(45, 29), new MapPoint(15, 13), new MapPoint(15, 13));
                                    gm.Icon = gi;
                                }

                            }
                            else
                            {

                                gm = new MapMarker(gp);
                            }
                        }

                        if (sText.Trim() != string.Empty)
                        {
                            OverlayMarker(gm, sText);
                        }
                        else
                        {
                            OverlayMarker(gm);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Could not Resolve Data source Fields: " + ex.ToString());
                    }
                }
            }
        }

        #region IMap Members

        /// <summary>
        /// Method to Center and Zoom the map at a particular point
        /// </summary>
        /// <param name="curPoint">Point at which the map should be centered</param>
        /// <param name="ZoomLevel">Zoom level</param>
        public void CenterAndZoom(Point curPoint, uint zoomLevel)
        {
            zoomAndCenter = string.Format(ci, "ZoomIn{3}({0},{1},{2});", curPoint.Longitude, curPoint.Latitude, zoomLevel, this.ClientID);
        }

        /// <summary>
        /// Method to auto-center and zoom the map at a particular point from objects in datasource
        /// </summary>
        public void AutoCenterAndZoom()
        {
            StringBuilder gobjectsBoundsString = new StringBuilder();
            IEnumerable gobjectsIEnum = HelperDataResolver.GetResolvedDataSource(DataSource);
            if (gobjectsIEnum != null)
            {
                foreach (object gobjects in gobjectsIEnum)
                {
                    try
                    {
                        string sText = string.Empty;
                        double lat = 0;
                        double lng = 0;

                        if (typeof(string) == DataBinder.GetPropertyValue(gobjects, this.MarkerLatitudeField).GetType()
                            && typeof(string) == DataBinder.GetPropertyValue(gobjects, this.MarkerLongitudeField).GetType())
                        {
                            lat = double.Parse(DataBinder.GetPropertyValue(gobjects, this.MarkerLatitudeField).ToString(), ci);
                            lng = double.Parse(DataBinder.GetPropertyValue(gobjects, this.MarkerLongitudeField).ToString(), ci);
                        }
                        else
                        {
                            lat = (double)DataBinder.GetPropertyValue(gobjects, this.MarkerLatitudeField);
                            lng = (double)DataBinder.GetPropertyValue(gobjects, this.MarkerLongitudeField);
                        }

                        gobjectsBoundsString.Append(scriptGenerator.ExtendBounds(lat.ToString().Replace(',', '.'), lng.ToString().Replace(',', '.')));
                    }
                    catch (Exception AutoCenterAndZoomEx) { throw new Exception(AutoCenterAndZoomEx.ToString()); }
                }
            }

            zoomAndCenter = gobjectsBoundsString.ToString() + string.Format(ci, "ZoomIn{0}(locBounds.getCenter().lng(),locBounds.getCenter().lat(), map{0}.getBoundsZoomLevel(locBounds));", this.ClientID);
        }

        #endregion
    }
}
