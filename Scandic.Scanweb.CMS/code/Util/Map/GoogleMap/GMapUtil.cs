// <copyright file="GMapControl.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Control which generates the javascript to render the google map</summary>

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMap
{
    /// <summary>
    /// Static class have static util methods
    /// </summary>
    public static class GMapUtil
    {
    }
}