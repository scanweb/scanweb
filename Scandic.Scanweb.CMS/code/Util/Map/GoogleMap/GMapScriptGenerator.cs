using System;
using System.Text;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.code.Util.Map.GoogleMap
{
    /// <summary>
    /// Class that handles all Javascript Method 
    /// Generation
    /// </summary>
    internal class GMapScriptGenerator : IMapScriptGenerator
    {
        
        #region IMapScriptGenerator Members
        /// <summary>
        /// RegisterMapKey
        /// </summary>
        /// <param name="mapKey"></param>
        /// <returns>MapKey</returns>
        public string RegisterMapKey(string mapKey)
        {
            string sKeyScript = 
                string.Format
                ("<script src='http://maps.google.com/maps?file=api&v=2&key={0}' type='text/javascript'></script>", mapKey);
            return sKeyScript;
        }

        /// <summary>
        /// GetMapKey
        /// </summary>
        /// <returns>MapKey</returns>
        public string GetMapKey()
        {
            return "GMapKey";
        }

        /// <summary>
        /// Method to Generate the Function to Overlay a point
        /// on the map
        /// </summary>
        /// <param name="sArrayVariable">Variable representing an Array</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        public string GenerateOverLayPointFuncCall(string sArrayVariable, string sControlClientID)
        {
            StringBuilder sOverLayFunction = new StringBuilder();
            sOverLayFunction.AppendFormat
                (" function OverlayLocation{0}(clong,cLat,popup,sFormattedHtml)\n", sControlClientID);
            sOverLayFunction.Append("{\n");
            sOverLayFunction.Append("var point = new GPoint(clong,cLat);");
            sOverLayFunction.Append("var marker = new GMarker(point);\n");
            sOverLayFunction.Append("if ( popup==1 ) { \n");
            sOverLayFunction.Append
                ("GEvent.addListener(marker, 'click', function() {marker.openInfoWindowHtml(sFormattedHtml,{maxWidth:204});});");
            sOverLayFunction.Append("\n } \n");
            sOverLayFunction.AppendFormat("{0}.push(marker);\n", sArrayVariable);
            sOverLayFunction.Append(" }");
            return sOverLayFunction.ToString();

        }
        /// <summary>
        /// Method to Generate the Javascript funtion to Overlay a point that uses a custom
        /// icon on the map
        /// </summary>
        /// <param name="sArrayVariable">Variable representing an Array</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        public string GenerateCustomOverLayPointFuncCall(string sArrayVariable, string sControlClientID)
        {
            StringBuilder sOverLayFunction = new StringBuilder();
            sOverLayFunction.AppendFormat
                (" function OverlayCustomMarkerLocation{0}(clong,cLat,iconimgUrl,iconshadowimgURL,iconwidth,iconheight,iconshwidth,iconshheight,iconAnchorX,iconAnchorY,infoWinX,infoWinY,popup,sFormattedHtml)\n", sControlClientID);
            sOverLayFunction.Append("{\n");
            sOverLayFunction.Append("var point = new GPoint(clong,cLat);\n");
            sOverLayFunction.Append("var icon = new GIcon();\n");
            sOverLayFunction.Append("icon.image =iconimgUrl ;\n");
            sOverLayFunction.Append("icon.shadow =iconshadowimgURL  ;\n");
            sOverLayFunction.Append("icon.iconSize = new GSize(iconwidth, iconheight);\n");
            sOverLayFunction.Append("icon.shadowSize = new GSize(iconshwidth, iconshheight);\n");
            sOverLayFunction.Append("icon.iconAnchor = new GPoint(iconAnchorX, iconAnchorY);\n");
            sOverLayFunction.Append("icon.infoWindowAnchor = new GPoint(infoWinX, infoWinY);\n");
            sOverLayFunction.Append("var marker = new GMarker(point,icon);\n");
            sOverLayFunction.Append("if ( popup==1 ) { \n");
            sOverLayFunction.Append("GEvent.addListener(marker, 'click', function() {");
            sOverLayFunction.Append("marker.openInfoWindowHtml(sFormattedHtml,{maxWidth:204});});");
            sOverLayFunction.Append(" } \n");
            sOverLayFunction.AppendFormat("{0}.push(marker);\n", sArrayVariable);
            sOverLayFunction.Append(" }");
            return sOverLayFunction.ToString();

        }

        /// <summary>
        /// Method to generate the Javascript function to overlay a line
        /// on the map
        /// </summary>
        /// <param name="sArrayVariable">Variable representing the array</param>
        /// <param name="sControlClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        public string GenerateOverlayLineFuncCall(string sArrayVariable, string sControlClientID)
        {
            StringBuilder sOverLayFunction = new StringBuilder();
            sOverLayFunction.AppendFormat
                (" function OverlayLine{0}(PointArray,weight,Color,opacity)\n", sControlClientID);
            sOverLayFunction.Append("{\n");
            sOverLayFunction.Append("var polyline = new GPolyline(PointArray,Color,weight,opacity);");
            sOverLayFunction.AppendFormat("{0}.push(polyline);\n", sArrayVariable);
            sOverLayFunction.Append(" }");
            return sOverLayFunction.ToString();
        }

        /// <summary>
        /// Method to Overlay points on the map
        /// Instead of overlaying each point or lay individually 
        /// it stores them in an array and overlays them together
        /// </summary>
        /// <returns>JavaScript code as string</returns>
        public string GenerateNewOverLayFunction(string sGMapVariable, string sClientID)
        {
            StringBuilder sOverLayFunction = new StringBuilder();
            sOverLayFunction.AppendFormat("for (var i = 0; i < arrayPoints{0}.length; i++)\n", sClientID);
            sOverLayFunction.Append("{\n ");
            sOverLayFunction.AppendFormat("{0}.addOverlay(arrayPoints{1}[i]);\n", sGMapVariable, sClientID);
            sOverLayFunction.Append("}\n");

            return sOverLayFunction.ToString();
        }

        /// <summary>
        /// Method to generate the Javascript function to center and Zoom in
        /// at a point on a map
        /// </summary>
        /// <param name="sGMapVariable">Variable representing the map</param>
        /// <param name="sClientID">Client ID of the control</param>
        /// <returns>JavaScript function as a string</returns>
        ///<code>
        /// function ZoomIn(clong,cLat,ZoomLevel)
        ///	{
        ///		map.centerAndZoom(new GPoint(clong,cLat), ZoomLevel);
        ///	}
        /// </code>
        public string GenerateZoomInFunction(string sGMapVariable, string sClientID)
        {
            StringBuilder sZoomInFunction = new StringBuilder();
            sZoomInFunction.AppendFormat("function ZoomIn{0}(clong,cLat,ZoomLevel)", sClientID);
            sZoomInFunction.Append("\n{");
            sZoomInFunction.AppendFormat("{0}.setCenter(new GLatLng(cLat, clong), ZoomLevel)", sGMapVariable);
            sZoomInFunction.Append("\n; }");
            return sZoomInFunction.ToString();
        }

        /// <summary>
        /// ExtendBounds
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <returns>ExtendBounds</returns>
        public string ExtendBounds(string lat, string lng)
        {
            StringBuilder sExtendBoundsFunction = new StringBuilder();
            sExtendBoundsFunction.AppendFormat("locBounds.extend(new GLatLng({0}, {1}));\n", lat, lng);
            return sExtendBoundsFunction.ToString();
        }

        /// <summary>
        /// Utility Method to generate JavaScript code to Set the Map type
        /// </summary>
        /// <param name="mapType">The Type of Map</param>
        /// <param name="sGMapVariable">Variable representing the Google Map object</param>
        /// <returns>Javascript code as string</returns>
        public string GenerateMapType(MapType mapType, string sGMapVariable)
        {
            string sMapType = string.Empty;
            switch (mapType)
            {
                case MapType.MAP:
                    sMapType = "G_NORMAL_MAP";
                    break;
                case MapType.HYBRID:
                    sMapType = "G_HYBRID_MAP";
                    break;
                case MapType.SATELLITE:
                    sMapType = "G_SATELLITE_MAP";
                    break;
                default:
                    sMapType = "G_NORMAL_MAP";
                    break;
            }

            return string.Format("{0}.setMapType({1});\n", sGMapVariable, sMapType);
        }

        /// <summary>
        /// Method that generates JavaScript code to Initialize the map Variable
        /// </summary>
        /// <param name="sGMapVariable">Variable representing the map</param>
        /// <param name="sClientID">Client ID of the control</param>
        /// <param name="bEnableDrag">Set if the map can be dragged</param>
        /// <param name="bEnableInfoWindow">set if there can be pop up windows on the map</param>
        /// <param name="bEnableMapTypeControl">set if the map Type can be changed</param>
        /// <param name="bEnableDblClickZoom">Set if the map can be zoomed with double-click</param>
        /// <param name="bEnableMouseScrollZoom">Set if the map can be zoomed with mouse scroll wheel</param>
        /// <returns>JavaScript code as string</returns>
        public string GenerateGMapVariableInitialization
            (string sGMapVariable, string sClientID, bool bEnableDrag, bool bEnableInfoWindow, 
            bool bEnableMapTypeControl, bool bEnableDblClickZoom, bool bEnableMouseScrollZoom)
        {
            StringBuilder sInitVarBuilder = new StringBuilder();
            sInitVarBuilder.AppendFormat("{1} = new GMap2(document.getElementById('{0}'));\n", sClientID, sGMapVariable);

            if (!bEnableDrag)
                sInitVarBuilder.AppendFormat("{0}.disableDragging() ;\n", sGMapVariable);
            if (!bEnableInfoWindow)
                sInitVarBuilder.AppendFormat("{0}.disableInfoWindow() ;\n", sGMapVariable);
            if (bEnableDblClickZoom)
                sInitVarBuilder.AppendFormat("{0}.enableDoubleClickZoom() ;\n", sGMapVariable);
            if (bEnableMouseScrollZoom)
                sInitVarBuilder.AppendFormat("{0}.enableScrollWheelZoom() ;\n", sGMapVariable);
            if (bEnableMapTypeControl)
                sInitVarBuilder.AppendFormat("{0}.addControl(new GMapTypeControl()) ;\n", sGMapVariable);

            return sInitVarBuilder.ToString();
        }

        /// <summary>
        /// Method to Generat the JavaScript code to set the type of
        /// Scroll Control on the Map
        /// </summary>
        /// <param name="scrollType">The Scroll Control to be added</param>
        /// <param name="sGmapVariable">Variable representing the Google Map</param>
        /// <returns>Javascript code as string</returns>
        public string GenerateScrollControl(MapScrollControl scrollType, string sGmapVariable)
        {
            string sScrollType = string.Empty;
            switch (scrollType)
            {
                case MapScrollControl.LARGE:
                    sScrollType = string.Format(" {0}.addControl(new GLargeMapControl());\n", sGmapVariable);
                    break;
                case MapScrollControl.SMALL:
                    sScrollType = string.Format(" {0}.addControl(new GSmallMapControl());\n", sGmapVariable);
                    break;
                case MapScrollControl.SMALLZOOMONLY:
                    sScrollType = string.Format(" {0}.addControl(new GSmallZoomControl());\n", sGmapVariable);
                    break;
                case MapScrollControl.NONE:
                default:
                    sScrollType = string.Empty;
                    break;

            }
            return sScrollType;
        }

        /// <summary>
        /// Method that Generates all the Javascript
        /// </summary>
        /// <returns>Javascript code as a string</returns>
        public string GenerateStartUpScript(InitParams initParams)
        {
            StringBuilder initFunction = new StringBuilder();
            initFunction.Append("</form>");
            initFunction.Append("<script type='text/javascript' language='javascript'>\n");
            initFunction.Append("if(GBrowserIsCompatible()){\n");
            initFunction.AppendFormat("var {0};", initParams.mapVariable);
            initFunction.AppendFormat("var {0}= new Array();", initParams.arrayVariable);
            initFunction.Append("var locBounds = new GLatLngBounds();\n");

            initFunction.AppendFormat
                ("{0}\n", GenerateGMapVariableInitialization
                (initParams.mapVariable, initParams.clientID, initParams.enableDrag, 
                initParams.enableInfoWindow, initParams.enableMapTypeControl, initParams.enableDblClickZoom, 
                initParams.enableMouseScrollZoom));

            if (initParams.enableDirections)
            {
                initFunction.AppendFormat("var {0};\n", initParams.directionVariable);
                initFunction.AppendFormat("var {0};\n", initParams.directionPanelVariable);

                initFunction.AppendFormat("var {0} = new GClientGeocoder();\n", initParams.geocodeVariable);
                initFunction.Append("var locPoint = new GLatLng();\n");

                initFunction.AppendFormat
                    ("{0} = document.getElementById(\"" + initParams.DirectionsContainer + "\");\n", 
                    initParams.directionPanelVariable);
                initFunction.AppendFormat
                    ("{0} = new GDirections({1}, {2});\n", initParams.directionVariable, 
                    initParams.mapVariable, initParams.directionPanelVariable);

                if (initParams.destinationWay == DriveDirection.TO)
                {
                    initFunction.AppendFormat
                        ("{0}.getLatLng(\"" + initParams.DestinationTo + "\", function(point)\n", 
                        initParams.geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat
                        ("{0}.loadFromWaypoints([\"" + initParams.DestinationFrom + "\", point], ", 
                        initParams.directionVariable);
                    initFunction.Append("{locale:\"" + initParams.Locale + "\"});\n");
                }
                else
                {
                    initFunction.AppendFormat
                        ("{0}.getLatLng(\"" + initParams.DestinationFrom + "\", function(point)\n", 
                        initParams.geocodeVariable);
                    initFunction.Append("{ if (!point){\n");
                    initFunction.Append("\n");
                    initFunction.Append("} else {\n");
                    initFunction.AppendFormat
                        ("{0}.loadFromWaypoints([point, \"" + initParams.DestinationTo + "\"], ", 
                        initParams.directionVariable);
                    initFunction.Append("{locale:\"" + initParams.Locale + "\"});\n");
                }

                initFunction.Append("}\n");
                initFunction.Append("}\n");
                initFunction.Append(");\n");
            }

            initFunction.AppendFormat("{0}\n", GenerateScrollControl(initParams.mapScrollType, initParams.mapVariable));
            initFunction.AppendFormat("{0}\n", initParams.pointOverlay.ToString());
            initFunction.AppendFormat("{0}\n", initParams.lineOverlay.ToString());
            initFunction.AppendFormat("{0}\n", initParams.zoomAndCenter);
            initFunction.AppendFormat("{0}\n", GenerateNewOverLayFunction(initParams.mapVariable, initParams.clientID));

            initFunction.Append("}\n");
            if (initParams.zoomAndCenter.Trim().Length > 0)
                initFunction.AppendFormat("{0}\n", GenerateZoomInFunction(initParams.mapVariable, initParams.clientID));
            if (initParams.pointOverlay.Length > 0)
            {
                initFunction.AppendFormat
                    ("{0}\n", GenerateOverLayPointFuncCall(initParams.arrayVariable, initParams.clientID));
                initFunction.AppendFormat
                    ("{0}\n", GenerateCustomOverLayPointFuncCall(initParams.arrayVariable, initParams.clientID));
            }
            if (initParams.lineOverlay.Length > 0)
                initFunction.AppendFormat("{0}\n", GenerateOverlayLineFuncCall(initParams.arrayVariable, initParams.clientID));


            initFunction.Append("setTimeout('MakeCopyrightSmaller();', 500);\n");

            #region MakeCopyrightSmaller()
            initFunction.Append("function MakeCopyrightSmaller()\n");
            initFunction.Append("{\n");
            initFunction.Append("for(var i = 0; i < ");
            initFunction.AppendFormat("{0}.getContainer().childNodes.length;", initParams.mapVariable);
            initFunction.Append("++i)\n");
            initFunction.Append("{\n");
            initFunction.Append("if(");
            initFunction.AppendFormat
                ("{0}.getContainer().childNodes[i].innerHTML.indexOf(String.fromCharCode(169))\n", initParams.mapVariable);
            initFunction.Append("!== -1)\n");
            initFunction.Append("{\n");
            initFunction.AppendFormat("if({0}.getContainer().childNodes[i].offsetLeft < 0)\n", initParams.mapVariable);
            initFunction.Append("{\n");
            initFunction.AppendFormat("{0}.getContainer().childNodes[i].style.fontSize = '3px';\n", initParams.mapVariable);
            initFunction.Append("}\n");
            initFunction.Append("break;\n");
            initFunction.Append("}\n");
            initFunction.Append("}\n");
            initFunction.Append("}\n");
            #endregion

            initFunction.Append("</script>\n");
            initFunction.Append("<form>");
            return initFunction.ToString();
        }

        /// <summary>
        /// Method to Overlay a marker on the Map.
        /// </summary>
        /// <param name="curMarker">Marker to be overlayed</param>
        public void GenerateCreateMarkerScriptCall(MapMarker curMarker)
        {
            StringBuilder createMarkerScriptCall = new StringBuilder();
            if (curMarker.Icon != null)
            {
                createMarkerScriptCall.AppendFormat(
                    ci,
                    "CreateCustomMarker{0}({1},{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},0,'{13}');\n",
                    this.ClientID,
                    curMarker.Point.Longitude,
                    curMarker.Point.Latitude,
                    curMarker.Icon.ImageURL,
                    curMarker.Icon.ShadowImageURL,
                    curMarker.Icon.IconSize.Width,
                    curMarker.Icon.IconSize.Height,
                    curMarker.Icon.ShadowSize.Width,
                    curMarker.Icon.ShadowSize.Height,
                    curMarker.Icon.IconAnchor.Longitude,
                    curMarker.Icon.IconAnchor.Latitude,
                    curMarker.Icon.InfoWindowAnchor.Longitude,
                    curMarker.Icon.InfoWindowAnchor.Latitude,
                    string.Empty
                    );
            }
            else
            {
                createMarkerScriptCall.AppendFormat(
                    ci,
                    "CreateMarker{3}({0},{1},0,'{2}');\n",
                    curMarker.Point.Longitude,
                    curMarker.Point.Latitude,
                    string.Empty,
                    this.ClientID
                    );
            }

            return createMarkerScriptCall;
        }

        /// <summary>
        /// Method to Overlay a marker on the map with a popup
        /// Enable drag cannot be false
        /// </summary>
        /// <param name="curMarker">Marker to be overlayed</param>
        /// <param name="sFormattedHtml">HTML to be displayed in the pop up</param>
        public void GenerateCreateMarkerScriptCall(MapMarker curMarker, string sFormattedHtml)
        {
            StringBuilder createMarkerScriptCall = new StringBuilder();
            sFormattedHtml = sFormattedHtml.Replace("'", string.Empty);
            overrideEnabledrag = true;
            if (curMarker.Icon != null)
            {
                createMarkerScriptCall.AppendFormat(
                    ci,
                    "CreateCustomMarker{0}({1},{2},'{3}','{4}',{5},{6},{7},{8},{9},{10},{11},{12},1,'{13}');\n",
                    this.ClientID,
                    curMarker.Point.Longitude,
                    curMarker.Point.Latitude,
                    curMarker.Icon.ImageURL,
                    curMarker.Icon.ShadowImageURL,
                    curMarker.Icon.IconSize.Width,
                    curMarker.Icon.IconSize.Height,
                    curMarker.Icon.ShadowSize.Width,
                    curMarker.Icon.ShadowSize.Height,
                    curMarker.Icon.IconAnchor.Longitude,
                    curMarker.Icon.IconAnchor.Latitude,
                    curMarker.Icon.InfoWindowAnchor.Longitude,
                    curMarker.Icon.InfoWindowAnchor.Latitude,
                    sFormattedHtml
                    );
            }
            else
            {
                createMarkerScriptCall.AppendFormat(
                    ci,
                    "CreateMarker{3}({0},{1},1,'{2}');\n",
                    curMarker.Point.Longitude,
                    curMarker.Point.Latitude,
                    sFormattedHtml,
                    this.ClientID
                    );
            }

            return createMarkerScriptCall;
        }

        #endregion
    }
}
