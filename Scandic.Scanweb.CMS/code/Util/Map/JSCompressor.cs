// <copyright file="JSCompressor.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>27-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Control which compress a string containing javascript</summary>
// <version>1.0</version>


using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace Scandic.Scanweb.CMS.code.Util.Map
{

    /// <summary>
    /// This class is used to compress JavaScript code.
    /// </summary>
    /// <remarks>Compression in this case consists of removing comments
    /// and unnecessary whitespace.  It can also remove almost all line
    /// feed characters between lines provided that semi-colons have been
    /// used to indicate all statement endpoints.</remarks>
    public class JSCompressor
    {
        /// <summary>
        /// Line feed removal and variable name compression flags
        /// </summary>
        private bool removeLineFeeds, compressVarNames, varCompTest;

        /// <summary>
        /// Variable name compression tracking
        /// </summary>
        private char[] newVarName;

        private int varNamePos;

        /// <summary>
        /// Uncompressed sections removed by #pragma
        /// </summary>
        private StringCollection scNoComps;

        /// <summary>
        ///  Literals removed during initial pass
        /// </summary>
        private StringCollection scLiterals;

        /// <summary>
        /// Regular expression and match evaluator used to re-insert literals
        /// and uncompressed sections.
        /// </summary>
        private Regex reInsLit,
                      reExtNoComp,
                      reDelNoComp,
                      reFuncParams,
                      reFindVars,
                      reStripVarPrefix,
                      reStripParens,
                      reStripAssign;

        private MatchEvaluator meExtNoComp, meInsLit;
        private int literalCount, noCompCount;
       
        /// <summary>
        /// Get or set the line feed removal option.
        /// </summary>
        public bool LineFeedRemoval
        {
            get { return removeLineFeeds; }
            set { removeLineFeeds = value; }
        }

        /// <summary>
        /// Get or set the variable name compression option.
        /// </summary>
        public bool CompressVariableNames
        {
            get { return compressVarNames; }
            set { compressVarNames = value; }
        }

        /// <summary>
        /// This is used to test variable name compression only
        /// </summary>
        /// <remarks>If set to true, only variable names will be compressed.
        /// This makes it easier to debug possible issues with the variable
        /// name compression code.</remarks>
        public bool TestVariableNameCompression
        {
            get { return varCompTest; }
            set { varCompTest = value; }
        }

        /// <summary>
        /// Default constructor.  Line feed removal defaults to true, variable
        /// name compression defaults to false.
        /// </summary>
        /// <overloads>There are three overloads for the constructor.</overloads>
        public JSCompressor() : this(true)
        {
        }

        /// <summary>
        /// This version takes the line feed removal option.  Variable name
        /// compression defaults to false.
        /// </summary>
        /// <param name="removeLFs">Pass true to remove line feeds wherever
        /// possible, false to leave them in the script.</param>
        public JSCompressor(bool removeLFs)
        {
            removeLineFeeds = removeLFs;
            scLiterals = new StringCollection();
            scNoComps = new StringCollection();
           
            compressVarNames = true;
        }

        /// <summary>
        /// This version takes the line feed removal option and variable name
        /// compression option.
        /// </summary>
        /// <param name="removeLFs">Pass true to remove line feeds wherever
        /// possible, false to leave them in the script.</param>
        /// <param name="compressVars">Pass true to compress variable names
        /// or false to leave variable names intact.</param>
        public JSCompressor(bool removeLFs, bool compressVars) : this(removeLFs)
        {
            compressVarNames = compressVars;
        }

        /// <summary>
        /// Compress the specified JavaScript code.
        /// </summary>
        /// <param name="strScript">The script to compress</param>
        /// <returns>The compressed script</returns>
        public string Compress(string strScript)
        {
            string strCompressed;
            char[] achScriptChars;

            if (strScript == null || strScript.Length == 0)
                return strScript;

            scLiterals.Clear();
            scNoComps.Clear();
            if (reInsLit == null)
            {
                reExtNoComp = new Regex(@"//\s*#pragma\s*NoCompStart.*?" +
                                        @"//\s*#pragma\s*NoCompEnd.*?\n",
                                        RegexOptions.Multiline | RegexOptions.Singleline |
                                        RegexOptions.IgnoreCase);
                reDelNoComp = new Regex(@"//\s*#pragma\s*NoComp(Start|End).*\n",
                                        RegexOptions.Multiline | RegexOptions.IgnoreCase);
                reInsLit = new Regex("\xFE|\xFF");
                meInsLit = new MatchEvaluator(OnMarkerFound);
                meExtNoComp = new MatchEvaluator(OnNoCompFound);

                reFuncParams = new Regex(@"function.*?\((.*?)\)(.*?|\n)?\{",
                                         RegexOptions.IgnoreCase | RegexOptions.Singleline);
                reFindVars = new Regex(@"(var\s+.*?)(;|$)",
                                       RegexOptions.IgnoreCase | RegexOptions.Multiline);
                reStripVarPrefix = new Regex(@"^var\s+",
                                             RegexOptions.IgnoreCase);
                reStripParens = new Regex(@"\(.*?,.*?\)|\[.*?,.*?\]",
                                          RegexOptions.IgnoreCase);
                reStripAssign = new Regex(@"(=.*?)(,|;|$)",
                                          RegexOptions.IgnoreCase);
            }

            strCompressed = reExtNoComp.Replace(strScript, meExtNoComp);
            achScriptChars = strCompressed.ToCharArray();
            CompressArray(achScriptChars);
            strCompressed = new String(achScriptChars);
            strCompressed = strCompressed.Replace("\0", String.Empty);
            if (!varCompTest)
            {
                strCompressed = Regex.Replace(strCompressed, @"^[\s]+|[ \f\r\t\v]+$",
                                              String.Empty, RegexOptions.Multiline);
                strCompressed = Regex.Replace(strCompressed, @"([\s]){2,}", "$1");
                if (removeLineFeeds)
                {
                    strCompressed = Regex.Replace(strCompressed, @"([+-])\n\1",
                                                  "$1 $1");
                    strCompressed = Regex.Replace(strCompressed, @"([^+-][+-])\n",
                                                  "$1");
                    strCompressed = Regex.Replace(strCompressed,
                                                  @"([\xFE{}([,<>/*%&|^!~?:=.;])\n", "$1");
                    strCompressed = Regex.Replace(strCompressed,
                                                  @"\n([{}()[\],<>/*%&|^!~?:=.;+-])", "$1");
                }

                strCompressed = Regex.Replace(strCompressed,
                                              @"[ \f\r\t\v]?([\n\xFE\xFF/{}()[\];,<>*%&|^!~?:=])[ \f\r\t\v]?",
                                              "$1");
                strCompressed = Regex.Replace(strCompressed, @"([^+]) ?(\+)",
                                              "$1$2");
                strCompressed = Regex.Replace(strCompressed, @"(\+) ?([^+])",
                                              "$1$2");
                strCompressed = Regex.Replace(strCompressed, @"([^-]) ?(\-)",
                                              "$1$2");
                strCompressed = Regex.Replace(strCompressed, @"(\-) ?([^-])",
                                              "$1$2");
                if (removeLineFeeds)
                {
                    strCompressed = Regex.Replace(strCompressed,
                                                  @"(\W(if|while|for)\([^{]*?\))\n", "$1");
                    strCompressed = Regex.Replace(strCompressed,
                                                  @"(\W(if|while|for)\([^{]*?\))((if|while|for)\([^{]*?\))\n",
                                                  "$1$3");
                    strCompressed = Regex.Replace(strCompressed,
                                                  @"([;}]else)\n", "$1 ");
                }
            }

            if (compressVarNames || varCompTest)
                strCompressed = CompressVariables(strCompressed);
            noCompCount = literalCount = 0;
            strCompressed = reInsLit.Replace(strCompressed, meInsLit);

            return strCompressed;
        }

        /// <summary>
        /// Replace literals with a marker so that they don't interfere with
        /// subsequent parsing steps.
        /// </summary>
        /// <param name="achScriptChars"></param>
        /// <param name="nStartPos"></param>
        /// <param name="nEndPos"></param>
        private void ExtractLiteral(char[] achScriptChars, int nStartPos,
                                    int nEndPos)
        {
            int nLen = nEndPos - nStartPos + 1;

            scLiterals.Add(new String(achScriptChars, nStartPos, nLen));

            achScriptChars[nStartPos] = '\xFF';

            Array.Clear(achScriptChars, nStartPos + 1, nLen - 1);
        }

        /// <summary>
        /// Determine if we have the start of a regular expression statement
        /// </summary>
        /// <param name="achScriptChars"></param>
        /// <param name="nCurPos"></param>
        /// <returns>true or flase</returns>
        private static bool IsRegExpStart(char[] achScriptChars, int nCurPos)
        {
            char ch;

            while (nCurPos-- > 0)
            {
                ch = achScriptChars[nCurPos];
                if (Char.IsWhiteSpace(ch) == false)
                    return (ch == '(' || ch == ';' || ch == '=') ? true : false;
            }

            return true;
        }

        /// <summary>
        /// Parse the input array and compress it by removing comments and
        /// pulling out literals so they don't interfere with the final
        /// compression steps.
        /// </summary>
        /// <param name="achScriptChars"></param>
        private void CompressArray(char[] achScriptChars)
        {
            bool bInComment = false;
            int nIdx, nLen = achScriptChars.Length, nStartPos = -1;
            char chCur, chNext, chEnd = '\0';

            for (nIdx = 0; nIdx < nLen; nIdx++)
            {
                chCur = achScriptChars[nIdx];
                if (nStartPos > -1)
                {
                    if (bInComment == true)
                    {
                        if (chEnd == '*')
                        {
                            if (nIdx - nStartPos > 2 &&
                                achScriptChars[nIdx - 1] == '*' && chCur == '/')
                            {
                                Array.Clear(achScriptChars, nStartPos,
                                            nIdx - nStartPos + 1);
                                nStartPos = -1;
                                bInComment = false;
                            }
                        }
                        else if (chCur == '\r' || chCur == '\n')
                        {
                            Array.Clear(achScriptChars, nStartPos,
                                        nIdx - nStartPos + 1);
                            nStartPos = -1;
                            bInComment = false;
                        }
                    }
                    else if (chCur == chEnd)
                    {
                        ExtractLiteral(achScriptChars, nStartPos, nIdx);
                        nStartPos = -1;
                    }
                    else if (chCur == '\\')
                        nIdx++;
                }
                else if (nIdx < nLen - 1)
                {
                    if (chCur == '/')
                    {
                        chNext = achScriptChars[nIdx + 1];

                        if (chNext == '*' || chNext == '/')
                        {
                            nStartPos = nIdx++;
                            chEnd = chNext;
                            if (nIdx < nLen - 1 && chNext == '*' &&
                                achScriptChars[nIdx + 1] == '@')
                                nStartPos = -1;
                            else
                                bInComment = true;
                        }
                        else if (JSCompressor.IsRegExpStart(achScriptChars, nIdx))
                        {
                            nStartPos = nIdx;
                            chEnd = chCur;
                        }
                    }
                    else if (chCur == '\'' || chCur == '\"')
                    {
                        chEnd = chCur;
                        nStartPos = nIdx;
                    }
                    else if (chCur == '\r' && !varCompTest)
                        achScriptChars[nIdx] = '\n';
                }
            }
        }

        /// <summary>
        /// Replace a literal or uncompressed section marker with the
        /// next entry from the appropriate collection.
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private string OnMarkerFound(Match match)
        {
            if (match.Value == "\xFE")
                return scNoComps[noCompCount++];

            return scLiterals[literalCount++];
        }

        /// <summary>
        /// Extract the sections that the user doesn't want compressed
        /// and save them for reinsertion at the end without the #pragmas.
        /// They are replaced with a marker character.
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private string OnNoCompFound(Match match)
        {
            scNoComps.Add(reDelNoComp.Replace(match.Value, String.Empty));
            return "\xFE";
        }

        /// <summary>
        /// This is used to compress variable names
        /// </summary>
        /// <param name="script"></param>
        /// <returns>script</returns>
        private string CompressVariables(string script)
        {
            StringCollection scVariables = new StringCollection();
            string[] varNames;
            string name = null, matchName;
            bool incVarName;

            MatchCollection matches = reFuncParams.Matches(script);

            foreach (Match m in matches)
            {
                varNames = m.Groups[1].Value.Split(',');
                foreach (string s in varNames)
                {
                    name = s.Trim();

                    if (name.Length != 0 && !scVariables.Contains(name))
                        scVariables.Add(name);
                }
            }
            matches = reFindVars.Matches(script);

            foreach (Match m in matches)
            {
                name = reStripVarPrefix.Replace(m.Groups[1].Value, String.Empty);
                name = reStripParens.Replace(name, String.Empty);
                name = reStripAssign.Replace(name, "$2");

                varNames = name.Split(',');
                foreach (string s in varNames)
                {
                    name = s.Trim();

                    if (name.Length != 0 && !scVariables.Contains(name))
                        scVariables.Add(name);
                }
            }
            newVarName = new char[10];
            newVarName[0] = '\x60';
            varNamePos = 0;
            incVarName = true;

            foreach (string replaceName in scVariables)
            {
                if (incVarName)
                {
                    do
                    {
                        IncrementVariableName();

                        name = new String(newVarName, 0, varNamePos + 1);
                        matchName = @"\W" + name + @"\W";
                    } while (Regex.IsMatch(script, matchName));

                    incVarName = false;
                }
                if (name.Length < replaceName.Length)
                {
                    incVarName = true;
                    script = Regex.Replace(script,
                                           @"(\W)" + replaceName + @"(?=\W)", "$1" + name);
                }
            }

            return script;
        }

        /// <summary>
        /// This is used to increment the compressed variable name
        /// </summary>
        private void IncrementVariableName()
        {
            if (newVarName[varNamePos] != 'z')
                newVarName[varNamePos]++;
            else
            {
                if (varNamePos == 0)
                {
                    newVarName[0] = '_';
                    varNamePos++;
                }
                else
                {
                    if (newVarName[varNamePos - 1] == '_' ||
                        newVarName[varNamePos - 1] == 'z')
                    {
                        if (newVarName[varNamePos - 1] == '_')
                            newVarName[varNamePos] = 'a';
                        else
                            newVarName[varNamePos - 1] = 'a';

                        varNamePos++;
                    }
                    else
                        newVarName[varNamePos - 1]++;
                }

                newVarName[varNamePos] = 'a';
            }
        }
    }
}