// <copyright file="MapLocalAttractionUnit.cs" company="Sapient">
// Copyright (c) 2009 All Right Reserved</copyright>
// <author>Aneesh Lal G A</author>
// <email>alal3@sapient.com</email>
// <date>05-Oct-2009</date>
// <version>Release - FindAHotel</version>
// <summary>Local Attraction Unit class, implements MapUnit and carries pop-up data</summary>

#region System NameSpaces
using System;
using System.Text;
#endregion

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    /// <summary>
    /// Represents a Google Map LocalAttraction Unit
    /// </summary>
    public class MapLocalAttractionUnit : MapUnit
    {
        #region Private Fields

        /// <summary>
        /// Name of the local attraction
        /// </summary>
        private string localAttractionName;

        /// <summary>
        /// link text to local attraction
        /// </summary>
        private string localAttractionLinkText;

        /// <summary>
        /// address of the local attraction
        /// </summary>
        private string localAttractionAddress;

        /// <summary>
        /// link to the local attraction
        /// </summary>
        private string localAttractionURL;

        #endregion 

        #region Public Properties

        /// <summary>
        /// Local Attraction Name for GoogleMap Input
        /// </summary>
        /// <value>The name of the local attraction.</value>
        public string LocalAttractionName
        {
            get { return this.localAttractionName; }
            set { this.localAttractionName = value; }
        }

        /// <summary>
        /// Local Attraction Link Text for GoogleMap Input
        /// </summary>
        /// <value>The local attraction link text.</value>
        public string LocalAttractionLinkText
        {
            get { return this.localAttractionLinkText; }
            set { this.localAttractionLinkText = value; }
        }

        /// <summary>
        /// Local Attraction Address for GoogleMap Input
        /// </summary>
        /// <value>The local attraction address.</value>
        public string LocalAttractionAddress
        {
            get { return this.localAttractionAddress; }
            set { this.localAttractionAddress = value; }
        }

        /// <summary>
        /// Local Attraction URL for GoogleMap Input
        /// </summary>
        /// <value>The local attraction URL.</value>
        public string LocalAttractionURL
        {
            get { return this.localAttractionURL; }
            set { this.localAttractionURL = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// No Parameter Constructor
        /// </summary>
        public MapLocalAttractionUnit()
        {
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="longitude">longitude</param>
        /// <param name="latitude">latitude</param>
        /// <param name="zoom">The zoom.</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        /// <param name="attractionName">Name of the local attraction</param>
        /// <param name="attractionLinkText">link text to local attraction</param>
        /// <param name="AttractionAddress">address of the local attraction</param>
        /// <param name="localAttractionURL">link to the local attraction</param>
        public MapLocalAttractionUnit(
            double longitude,
            double latitude,
            int zoom,
            string iconUrl,
            string shadowUrl,
            string attractionName,
            string attractionLinkText,
            string AttractionAddress,
            string localAttractionURL)
            : base(longitude, latitude, zoom, iconUrl, shadowUrl)
        {
            this.localAttractionName = attractionName;
            this.localAttractionLinkText = attractionLinkText;
            this.localAttractionAddress = AttractionAddress;
            this.localAttractionURL = localAttractionURL;
        }

        #endregion

        #region Internal Overrides

        /// <summary>
        /// Generates the html markup for the pop-up of local
        /// attraction
        /// </summary>
        /// <returns>returns pop-up html content</returns>
        internal override string GenerateInfoWindowMarkUp()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div style=\"font-size:12px;\"><b>" + localAttractionName + "</b></div>");
            sb.Append("<div style=\"font-size:10px;\">");
            sb.Append(localAttractionAddress);
            sb.Append("</div>");
            sb.Append("<div style=\"font-size:10px;\">");
            sb.Append("<div class=\"LinkListItem\">");
            sb.Append("<div class=\"LastLink\">");
            if (!String.IsNullOrEmpty(localAttractionLinkText))
            {
                if ((localAttractionLinkText).Trim() != string.Empty)
                {
                    sb.Append("<a class=\"IconLink\" href=\"" + localAttractionURL + "\" target=\"_blank\">" +
                              (localAttractionLinkText) + "</a>");
                }
            }
            sb.Append("</div>");
            sb.Append("</div>");
            sb.Append("</div>");
            return sb.ToString();
        }

        #endregion
    }
}