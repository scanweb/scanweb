////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  MapHotelUnit                                           //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:  Aneesh Lal G A                                         //
// Author email id				:  alal3@sapient.com                            		  //
// Creation Date				:  05-Oct-2009	    								      //
// Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
// Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.Core;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace Scandic.Scanweb.CMS.code.Util.Map
{
    /// <summary>
    /// Represents a Map Hotel Unit
    /// </summary>
    [Serializable]
    public class MapHotelUnit : MapUnit
    {
        #region Private Fields

        /// <summary>
        /// Sorting order of the hotel
        /// </summary>
        private int hotelNumber;

        /// <summary>
        /// Name of the hotel
        /// </summary>
        private string hotelName;

        /// <summary>
        /// Smaller image to come on the map unit
        /// </summary>
        private string hotelBookingImage;

        /// <summary>
        /// description
        /// </summary>
        private string hotelDescription;

        //private string propCode;

        /// <summary>
        /// Address
        /// </summary>
        private string address;

        /// <summary>
        /// zip code
        /// </summary>
        private string zip;

        /// <summary>
        /// city where the hotel belongs to
        /// </summary>
        private string city;

        /// <summary>
        /// country on which the hotel belongs to
        /// </summary>
        private string country;

        /// <summary>
        /// Postal City on which the hotel belongs to
        /// </summary>
        private string postalCity;

        /// <summary>
        /// Hotel landing page url
        /// </summary>
        private string hotelPageURL;

        /// <summary>
        /// Url redirects to show booking module
        /// </summary>
        private string hotelBookURL;

        /// <summary>
        /// opera id of hotel
        /// </summary>
        private string hotelOperaID;

        /// <summary>
        /// distance to the city center from the hotel
        /// </summary>
        private double cityCenterDistance;

        /// <summary>
        /// promotional page link UI display text
        /// </summary>
        private string promotionalPageLinkText;

        /// <summary>
        /// promotional page link
        /// </summary>
        private string promotionalPageLink;

        /// <summary>
        /// display promo link on popup boolean
        /// </summary>
        private bool displayPromoLink;

        ///// <summary>
        ///// Text on marker
        ///// </summary>
        //private string unitText;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets hotelName
        /// </summary>
        /// <value>The name of the hotel.</value>
        public string HotelName
        {
            get { return this.hotelName; }
            set { this.hotelName = value; }
        }

        /// <summary>
        /// Gets or sets hotelBookingImage
        /// </summary>
        /// <value>The hotel booking image.</value>
        public string HotelBookingImage
        {
            get { return hotelBookingImage; }
            set
            {
                hotelBookingImage = string.Empty;

                if (!String.IsNullOrEmpty(value))
                {
                    var imageWidth = Int32.Parse(ConfigurationManager.AppSettings["HotelThumbnailImageWidth"]);
                    hotelBookingImage = WebUtil.GetImageVaultImageUrl(value, imageWidth);                    
                }
            }
        }

        /// <summary>
        /// Gets or sets hotelDescription
        /// </summary>
        /// <value>The hotel description.</value>
        public string HotelDescription
        {
            get { return this.hotelDescription; }
            set { this.hotelDescription = value; }
        }

        /// <summary>
        /// Gets or sets address
        /// </summary>
        /// <value>The address.</value>
        public string Address
        {
            get { return this.address; }
            set { this.address = value; }
        }

        /// <summary>
        /// Gets or sets zip code
        /// </summary>
        /// <value>The zip.</value>
        public string Zip
        {
            get { return this.zip; }
            set { this.zip = value; }
        }

        /// <summary>
        /// Gets or sets city
        /// </summary>
        /// <value>The city.</value>
        public string City
        {
            get { return this.city; }
            set { this.city = value; }
        }

        /// <summary>
        /// Gets or sets country
        /// </summary>
        /// <value>The country.</value>
        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }

        public string PostalCity
        {
            get { return this.postalCity; }
            set { this.postalCity = value; }
        }


        /// <summary>
        /// Gets or sets phoneNumber
        /// </summary>
        /// <value>The phone number.</value>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets faxNumber
        /// </summary>
        /// <value>The fax number.</value>
        public string FaxNumber { get; set; }

        /// <summary>
        /// Gets or sets imageURL
        /// </summary>
        /// <value>The image URL.</value>
        public string ImageURL { get; set; }

        /// <summary>
        /// Gets or sets hotelPageURL
        /// </summary>
        /// <value>The hotel page URL.</value>
        public string HotelPageURL
        {
            get { return this.hotelPageURL; }
            set { this.hotelPageURL = value; }
        }

        /// <summary>
        /// Gets or sets hoteBookURL
        /// </summary>
        /// <value>The hotel book URL.</value>
        public string HotelBookURL
        {
            get { return this.hotelBookURL; }
            set { this.hotelBookURL = value; }
        }

        /// <summary>
        /// Gets or sets hotelOperaID
        /// </summary>
        /// <value>The hotel opera ID.</value>
        public string HotelOperaID
        {
            get { return hotelOperaID; }
            set { hotelOperaID = value; }
        }

        /// <summary>
        /// Gets or sets the hotel category.
        /// </summary>
        /// <value>The hotel category.</value>
        public string HotelCategory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enable unit text].
        /// </summary>
        /// <value><c>true</c> if [enable unit text]; otherwise, <c>false</c>.</value>
        public bool EnableUnitText { get; set; }

        /// <summary>
        /// Gets or sets the type of the marker text.
        /// </summary>
        /// <value>The type of the marker text.</value>
        public MarkerRateTextType MarkerRateTextType { get; set; }

        /// <summary>
        /// Gets or sets the per night rate.
        /// </summary>
        /// <value>The per night rate.</value>
        [ScriptIgnore]
        public string PerNightRate;

        /// <summary>
        /// Gets or sets the per stay rate.
        /// </summary>
        /// <value>The per stay rate.</value>
        [ScriptIgnore]
        public string PerStayRate;

        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        /// <value>The currency code.</value>
        public string CurrencyCode { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public MapHotelUnit()
        {
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="longitude">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="zoom">zoom level</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        /// <param name="hotelName">The name of the hotel</param>
        /// <param name="hotelDescription">The description of the hotel</param>
        /// <param name="address">The adress of the hotel</param>
        /// <param name="zip">The zip of the hotel</param>
        /// <param name="city">The city of the hotel</param>
        /// <param name="country">The country of the hotel</param>
        /// <param name="hotelPageURL">The URL of the hotel landing page</param>
        /// <param name="bookHotelURL">The URL to book this hotel</param>
        public MapHotelUnit(
            double longitude,
            double latitude,
            int zoom,
            string iconUrl,
            string shadowUrl,
            string hotelName,
            string hotelDescription,
            string address,
            string zip,
            string postalCity,
            string city,
            string country,
            string hotelPageURL,
            string bookHotelURL
            )
            : base(longitude, latitude, zoom, iconUrl, shadowUrl)
        {
            this.hotelName = hotelName;
            this.hotelDescription = hotelDescription;
            this.address = address;
            this.zip = zip;
            this.postalCity = postalCity;
            this.city = city;
            this.country = country;
            this.hotelPageURL = hotelPageURL;
            this.hotelBookURL = bookHotelURL;
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="longitude">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="zoom">zoom level</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        /// <param name="hotelName">The name of the hotel</param>
        /// <param name="hotelBookingImage">The hotel booking image.</param>
        /// <param name="hotelDescription">The description of the hotel</param>
        /// <param name="address">The adress of the hotel</param>
        /// <param name="zip">The zip of the hotel</param>
        /// <param name="city">The city of the hotel</param>
        /// <param name="country">The country of the hotel</param>
        /// <param name="hotelPageURL">The URL of the hotel landing page</param>
        /// <param name="bookHotelURL">The URL to book this hotel</param>
        /// <param name="cityCenterDistance">distance to city center</param>
        /// <param name="infoBoxType">type of the pop-up(BASIC/ADVANCED)</param>
        /// <param name="enableUnitNumbering">true if number has to appearon marker, false otherwise</param>
        /// <param name="hotelNumber">The hotel number.</param>
        /// <param name="opreaId">Opera id for a hotel</param>
        /// <param name="hotelCategory">The hotel category.</param>
        /// <param name="promotionalPageLinkText">The promotional page link text.</param>
        /// <param name="promotionalPageLink">The promotional page link.</param>
        /// <param name="displayPromoLink">if set to <c>true</c> [display promo link].</param>
        public MapHotelUnit(
            double longitude,
            double latitude,
            int zoom,
            string iconUrl,
            string shadowUrl,
            string hotelName,
            string hotelBookingImage,
            string hotelDescription,
            string address,
            string zip,
            string postalCity,
            string city,
            string country,
            string hotelPageURL,
            string bookHotelURL,
            double cityCenterDistance,
            InfoBoxType infoBoxType,
            bool enableUnitNumbering,
            int hotelNumber,
            string opreaId,
            string hotelCategory,
            string promotionalPageLinkText,
            string promotionalPageLink,
            bool displayPromoLink
            )
            : base(longitude, latitude, zoom, iconUrl, shadowUrl)
        {
            this.hotelName = hotelName;
            this.HotelBookingImage = hotelBookingImage;
            this.hotelDescription = hotelDescription;
            this.address = address;
            this.zip = zip;
            this.postalCity = postalCity;
            this.city = city;
            this.country = country;
            this.hotelPageURL = hotelPageURL;
            this.hotelBookURL = bookHotelURL;
            this.cityCenterDistance = cityCenterDistance;
            this.InfoBoxType = infoBoxType;
            this.EnableUnitNumbering = enableUnitNumbering;
            this.hotelNumber = hotelNumber;
            this.hotelOperaID = opreaId;
            this.HotelCategory = hotelCategory;
            this.promotionalPageLinkText = promotionalPageLinkText;
            this.promotionalPageLink = promotionalPageLink;
            this.displayPromoLink = displayPromoLink;
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="longitude">Longitude</param>
        /// <param name="latitude">Latitude</param>
        /// <param name="zoom">zoom level</param>
        /// <param name="iconUrl">The icon URL.</param>
        /// <param name="shadowUrl">The shadow URL.</param>
        /// <param name="hotelName">The name of the hotel</param>
        /// <param name="hotelBookingImage">The hotel booking image.</param>
        /// <param name="hotelDescription">The description of the hotel</param>
        /// <param name="address">The adress of the hotel</param>
        /// <param name="zip">The zip of the hotel</param>
        /// <param name="city">The city of the hotel</param>
        /// <param name="country">The country of the hotel</param>
        /// <param name="hotelPageURL">The URL of the hotel landing page</param>
        /// <param name="bookHotelURL">The URL to book this hotel</param>
        /// <param name="cityCenterDistance">distance to city center</param>
        /// <param name="infoBoxType">type of the pop-up(BASIC/ADVANCED)</param>
        /// <param name="enableUnitNumbering">true if number has to appearon marker, false otherwise</param>
        /// <param name="hotelNumber">The hotel number.</param>
        /// <param name="opreaId">Opera id for a hotel</param>
        /// <param name="hotelCategory">The hotel category.</param>
        /// <param name="promotionalPageLinkText">The promotional page link text.</param>
        /// <param name="promotionalPageLink">The promotional page link.</param>
        /// <param name="displayPromoLink">if set to <c>true</c> [display promo link].</param>
        /// <param name="enableUnitText">if set to <c>true</c> [enable unit text].</param>
        /// <param name="markerRateTextType">Type of the marker rate text.</param>
        /// <param name="perNightRate">Rate per night.</param>
        /// <param name="perStayRate">Rate per stay.</param>
        /// <param name="currencyCode">The currency code.</param>
        public MapHotelUnit(
            double longitude,
            double latitude,
            int zoom,
            string iconUrl,
            string shadowUrl,
            string hotelName,
            string hotelBookingImage,
            string hotelDescription,
            string address,
            string zip,
            string postalCity,
            string city,
            string country,
            string hotelPageURL,
            string bookHotelURL,
            double cityCenterDistance,
            InfoBoxType infoBoxType,
            bool enableUnitNumbering,
            int hotelNumber,
            string opreaId,
            string hotelCategory,
            string promotionalPageLinkText,
            string promotionalPageLink,
            bool displayPromoLink,
            bool enableUnitText,
            MarkerRateTextType markerRateTextType,
            string currencyCode,
            string perNightRate,
            string perStayRate
            )
            : base(longitude, latitude, zoom, iconUrl, shadowUrl)
        {
            this.hotelName = hotelName;
            this.HotelBookingImage = hotelBookingImage;
            this.hotelDescription = hotelDescription;
            this.address = address;
            this.zip = zip;
            this.postalCity = postalCity;
            this.city = city;
            this.country = country;
            this.hotelPageURL = hotelPageURL;
            this.hotelBookURL = bookHotelURL;
            this.cityCenterDistance = cityCenterDistance;
            this.InfoBoxType = infoBoxType;
            this.EnableUnitNumbering = enableUnitNumbering;
            this.hotelNumber = hotelNumber;
            this.hotelOperaID = opreaId;
            this.HotelCategory = hotelCategory;
            this.promotionalPageLinkText = promotionalPageLinkText;
            this.promotionalPageLink = promotionalPageLink;
            this.displayPromoLink = displayPromoLink;
            this.EnableUnitText = enableUnitText;
            this.MarkerRateTextType = markerRateTextType;
            this.CurrencyCode = currencyCode;
            this.PerNightRate = perNightRate;
            this.PerStayRate = perStayRate;
        }

        #endregion

        #region Internal Overrides

        /// <summary>
        /// Generates the html markup for the pop-up of map unit
        /// </summary>
        /// <returns>returns pop-up html content as a string</returns>
        /// A sample basic info box html generated here is as given below
        /// <div style="font-size:12px;">
        ///     <b>Scandic Glostrup</b>
        /// </div>
        /// <div style="font-size:10px;">Roskildevej 550, 2605 Brøndby , </div>
        /// <div style="font-size:10px;">Denmark</div>
        /// <div style="font-size:10px;">
        ///     <div class="LinkListItem">
        ///         <div class="NotLastLink">
        ///             <a class="IconLink" href="/en/Hotels/Countries/Denmark/Copenhagen/Hotels/Scandic-Glostrup/?GMapID=Scandic Glostrup" target="_top">View details of this hotel</a>
        ///         </div>
        ///         <div class="LastLink">
        ///             <a class="IconLink" href="/en/DLRedirect/?HO=744&GMapID=Scandic Glostrup" target="_top">Make a booking at this hotel</a>
        ///         </div>
        ///     </div>
        /// </div>
        /// An advanced info box should be as in the given format
        ///<div class=""hotelDetails"">
        ///    <h3>
        ///        <strong>{hotelNumber}</strong>
        ///        <strong id=""hotelName""><a href=""{hotelPageURL}?GMapID={hotelName}"">{hotelName}</a></strong>
        ///        <span id=""lblDistance"">({cityCenterDistance.ToString()} {kmToCityCentre})</span>
        ///    </h3>
        ///    <span id=""address"">{address}, {zip}, {city}, {country},{postalCity}</span>
        ///    <div class=""hotelDetailsWrapper"">
        ///        <img id=""imgHotel"" alt=""hotel"" src=""{HotelBookingImage}""/>
        ///        <p id=""details"">{hotelDescription}</p>
        ///    </div>
        ///     <div class=""popUpBottomArea"">
        ///        <div class=""popUpLinks"">
        ///            <a class=""IconLink"" id=""lnkFulldescription"" target=""_top"" href=""{hotelPageURL}?GMapID={hotelName}""><strong>{FullDescription}</strong></a>
        ///            <a class=""IconLink"" id=""lnkRoadDirections"" target=""_top"" href=""{hotelPageURL}?hotelpage=location&location=detailedmap&GMapID={hotelName}""><strong>{RoadDirections}</strong></a>
        ///        </div>
        ///         <div class=""ButtonLink"">
        ///                <div>
        ///                    <a class=""jqModal"" onclick=""javascript:populateDestination('{hotelName}','{"HOTEL:" + hotelOperaID}');return false;"" target=""_top"" title=""Check rates"" href=""#"">{CheckRates}</a>
        ///                </div>
        ///         </div>
        ///    </div>
        ///</div>
        /// An advanced select hotel page popup should be as in the given format
        /// <div class=""gmapPopupContainer"">
        ///  <h2 class=""headerTxt"">Scandic Anglais</h2>
        ///  <div class=""hotelInfoCnt"">
        ///    <div class=""imageHolder"">
        ///      <div class=""hd sprite""></div>
        ///      <div class=""cnt"">
        ///        <img src=""Templates/Booking/Styles/Default/Images/reservation2.0/hotelInfo.png""/>
        ///      </div>
        ///      <div class=""ft sprite""></div>
        ///    </div>
        ///    <div class=""desc"">
        ///      <p>
        ///        <span>Humlegårdsgatan 23, 102 44, Stockholm, Sweden</span>
        ///      </p>
        ///      <p>Overlooking Stockholm!s harbour, this hotel has an exercise area with saunas, a playroom, restaurant and bar.</p>
        ///    </div>
        ///  </div>
        ///  <div class=""gmapBtm"">
        ///    <ul class=""links"">
        ///      <li>
        ///        <a href=""#"" class=""goto spriteIcon"">Full description</a>
        ///      </li>
        ///      <li>
        ///        <a href=""#"" class=""overlay spriteIcon"">Image gallery</a>
        ///      </li>
        ///    </ul>
        ///    <div class=""actionBtn sprite"">
        ///      <a href=""#"" class=""buttonInner sprite"">Rooms and rates</a>
        ///      <span class=""buttonRt sprite""></span>
        ///    </div>
        ///  </div>
        ///</div>
        internal override string GenerateInfoWindowMarkUp()
        {
            StringBuilder sb = new StringBuilder();
            if (this.InfoBoxType == InfoBoxType.BASIC)
            {
                sb.Append("<div style=\"font-size:12px;\"><b>" + hotelName + "</b></div>");
                sb.Append("<div style=\"font-size:10px;\">");

                if (!string.IsNullOrEmpty(address))
                {
                    sb.Append(address);
                    if (!string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        sb.Append(", ");
                }
                if (!string.IsNullOrEmpty(zip))
                {
                    sb.Append(zip);
                    if (!string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        sb.Append(", ");
                }

                if (city == postalCity)
                {
                    sb.Append(postalCity);
                    sb.Append(", ");
                }
                else
                {
                    if (!string.IsNullOrEmpty(postalCity))
                    {
                        sb.Append(postalCity);
                        sb.Append(", ");
                    }
                    if (!string.IsNullOrEmpty(city))
                    {
                        sb.Append(city);
                        if (!string.IsNullOrEmpty(country))
                            sb.Append(", ");
                    }
                }
                sb.Append("</div>");
                sb.Append("<div style=\"font-size:10px;\">" + country + "</div>");


                sb.Append("<div style=\"font-size:10px;\">");
                sb.Append("<div class=\"LinkListItem\">");
                sb.Append("<div class=\"NotLastLink\">");

                sb.Append("<a class=\"IconLink\" href=\"" + hotelPageURL + "?GMapID=" + hotelNumber +
                          "\" target=\"_top\">" +
                          LanguageManager.Instance.Translate(
                              "/Templates/Scanweb/Util/GoogleMaps/GmapControl/HotelPageURL") + "</a>");
                sb.Append("</div>");
                sb.Append("<div class=\"LastLink\">");

                sb.Append("<a class=\"IconLink\" href=\"" + hotelBookURL + "&GMapID=" + hotelNumber +
                          "\" target=\"_top\">" +
                          LanguageManager.Instance.Translate(
                              "/Templates/Scanweb/Util/GoogleMaps/GmapControl/HotelBookURL") + "</a>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            else if (this.InfoBoxType == InfoBoxType.ADVANCED)
            {
                string truncatedKmToCityCentre = GetTruncatedCityCtrDistance();

                string truncatedStreetAddr = GetTruncatedStreetAddress();

                string filteredHotelDescription = GetFilteredHotelDescription();

                string truncatedPromoPageLinkText = GetTruncatedPromoPageLinkText();

                string currentLanguage = Utility.GetCurrentLanguage();
                sb.AppendFormat(
                    @"<div class=""hotelDetails""><h3><strong>{0}</strong><strong id=""hotelName""><a href=""{11}?GMapID={1}"">{1}</a></strong><span id=""lblDistance""> ({2} {3})</span></h3><span id=""address"">{4}</span><div class=""hotelDetailsWrapper""><img id=""imgHotel"" alt=""hotel"" src=""{5}""/><p id=""details"">{6}</p></div><div class=""popUpBottomArea""><div class=""popUpLinks""><a class=""IconLink"" target=""_top"" href=""{11}?GMapID={1}"">{12}</a><a class=""IconLink"" target=""_top"" href=""{7}?hotelpage=location&location=detailedmap&GMapID={1}"">{8}</a></div><div class=""actionBtn fltRt marright5""><a class=""jqModal buttonInner btnmapnew rumaptext"" onclick=""javascript:populateDestination('{1}','{10}');return false;"" target=""_top"" title=""Check rates"" href=""#"">{9}</a><a href=""#"" class=""buttonRt scansprite maphandsymb"" onclick=""javascript:populateDestination('{1}','{10}');return false;""></a></div></div></div>",
                    hotelNumber > 0 ? hotelNumber.ToString() + '.' : string.Empty,
                    hotelName,
                    cityCenterDistance.ToString(),
                    truncatedKmToCityCentre,
                    truncatedStreetAddr,
                    HotelBookingImage,
                    filteredHotelDescription,
                    hotelPageURL,
                    WebUtil.GetTranslatedText(
                        "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/RoadDirections", currentLanguage),
                    WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/CheckRates",
                                              currentLanguage),
                                              string.IsNullOrEmpty(hotelOperaID) ? string.Empty : "HOTEL:" + hotelOperaID,
                    displayPromoLink == true
                        ? (!string.IsNullOrEmpty(promotionalPageLink) ? promotionalPageLink : "#")
                        : hotelPageURL,
                    displayPromoLink == true
                        ? (!string.IsNullOrEmpty(truncatedPromoPageLinkText) ? truncatedPromoPageLinkText : "&nbsp;")
                        : WebUtil.GetTranslatedText(
                            "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription",
                            currentLanguage)
                    );

                sb = sb.Replace("\r\n", string.Empty);
                //sb = sb.Replace("'", "\\'");    
            }
            else if (this.InfoBoxType == InfoBoxType.ADVANCED_SELECTHOTEL ||
                     this.InfoBoxType == InfoBoxType.ADVANCED_SELECTHOTEL_DNUMBER)
            {
                string fullStreetAddr = GetFullStreetAddress();

                string filteredHotelDescription = GetFilteredHotelDescription();

                string currentLanguage = Utility.GetCurrentLanguage();
                string rumaptextCSS = "buttonInner";
                if (currentLanguage.ToUpper() == LanguageConstant.LANGUAGE_RUSSIA_RU)
                {
                    rumaptextCSS = "buttonInner rumaptextnew";
                }
                sb.AppendFormat(
                    @"<div class=""gmapPopupContainer""><h2 class=""headerTxt"">{10}</h2><div class=""hotelInfoCnt""><div class=""imageHolder""><div class=""hd sprite""></div><div class=""cnt""><img src=""{1}""/></div><div class=""ft sprite""></div></div><div class=""desc""><strong>{2}</strong><p>{3}</p></div></div><div class=""gmapBtm""><ul class=""links""><li><a target=""_top"" href=""{5}?GMapID={0}"" class=""goto scansprite"">{4}</a></li><li><a href=""#"" onclick=""parent.createImageGallery('{6}')"" class=""overlay jqModal scansprite"">{7}</a></li></ul><div class=""actionBtn""><a target=""_top"" href=""javascript:UpdateSHMapSelection();RedirectToSelectRate({8});"" class=""{11}"">{9}</a><a class=""buttonRt scansprite"" href=""javascript:UpdateSHMapSelection();RedirectToSelectRate({8});""></a></div></div></div>",
                    hotelNumber,
                    HotelBookingImage,
                    fullStreetAddr,
                    filteredHotelDescription,
                    WebUtil.GetTranslatedText(
                        "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription",
                        currentLanguage),
                    hotelPageURL,
                    Scandic.Scanweb.CMS.DataAccessLayer.GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                    "?methodToCall=GetImageGallery&HotelID=" + hotelOperaID + "&HotelName=" + hotelName,
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/imagegallery", currentLanguage),
                    hotelOperaID,
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates", currentLanguage),
                    hotelName,
                    rumaptextCSS
                    );

                sb = sb.Replace("\r\n", string.Empty);
            }
            else if (this.InfoBoxType == InfoBoxType.ADVANCED_CONFIRMATION)
            {
                string fullStreetAddr = GetFullStreetAddress();

                string filteredHotelDescription = GetFilteredHotelDescription();

                string currentLanguage = Utility.GetCurrentLanguage();
                sb.AppendFormat(
                    @"<div class=""gmapPopupContainer""><h2 class=""headerTxt"">{10}</h2><div class=""hotelInfoCnt""><div class=""imageHolder""><div class=""hd sprite""></div><div class=""cnt""><img src=""{1}""/></div><div class=""ft sprite""></div></div><div class=""desc""><strong>{2}</strong><p>{3}</p></div></div><div class=""gmapBtm""><ul class=""links""><li><a target=""_top"" href=""{5}?GMapID={0}"" class=""goto scansprite"">{4}</a></li><li><a href=""#"" onclick=""parent.createImageGallery('{6}')"" class=""overlay jqModal scansprite"">{7}</a></li></ul><div class=""actionBtn"" style=""display:none""><a target=""_top"" href=""javascript:RedirectToSelectRate({8});"" class=""buttonInner"">{9}</a><a class=""buttonRt scansprite"" href=""javascript:RedirectToSelectRate({8});""></a></div></div></div>",
                    hotelNumber,
                    HotelBookingImage,
                    fullStreetAddr,
                    filteredHotelDescription,
                    WebUtil.GetTranslatedText(
                        "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription",
                        currentLanguage),
                    hotelPageURL,
                    Scandic.Scanweb.CMS.DataAccessLayer.GlobalUtil.GetUrlToPage("ReservationAjaxSearchPage") +
                    "?methodToCall=GetImageGallery&HotelID=" + hotelOperaID,
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/imagegallery", currentLanguage),
                    hotelOperaID,
                    WebUtil.GetTranslatedText("/bookingengine/booking/selecthotel/roomsandrates", currentLanguage),
                    HotelName
                    );
                sb = sb.Replace("\r\n", string.Empty);
            }
            else if (this.InfoBoxType == InfoBoxType.FIND_YOUR_DESTINATION)
            {
                string truncatedKmToCityCentre = GetTruncatedCityCtrDistance();

                string truncatedStreetAddr = GetTruncatedStreetAddress();

                string filteredHotelDescription = GetFilteredHotelDescription();

                string truncatedPromoPageLinkText = GetTruncatedPromoPageLinkText();

                string currentLanguage = Utility.GetCurrentLanguage();
                sb.AppendFormat(
                    @"<div class=""hotelDetails""><h3><strong>{0}</strong><strong id=""hotelName""><a target=""_top"" href=""{11}?GMapID={1}"">{1}</a></strong><span id=""lblDistance""> ({2} {3})</span></h3><span id=""address"">{4}</span><div class=""hotelDetailsWrapper""><img id=""imgHotel"" alt=""hotel"" src=""{5}""/><p id=""details"">{6}</p></div><div class=""popUpBottomArea""><div class=""popUpLinks""><a class=""IconLink"" target=""_top"" href=""{11}?GMapID={1}"">{12}</a><a class=""IconLink"" target=""_top"" href=""{7}?hotelpage=location&location=detailedmap&GMapID={1}"">{8}</a></div><div class=""actionBtn fltRt marright5""><a class=""jqModal buttonInner btnmapnew rumaptext"" href=""javascript:parent.populateDestination('{1}','{10}');"" target=""_top"" title=""Check rates"">{9}</a><a class=""buttonRt scansprite maphandsymb"" href=""javascript:parent.populateDestination('{1}','{10}');""></a></div></div></div>",
                    hotelNumber > 0 ? hotelNumber.ToString() + '.' : string.Empty,
                    hotelName,
                    cityCenterDistance.ToString(),
                    truncatedKmToCityCentre,
                    truncatedStreetAddr,
                    HotelBookingImage,
                    filteredHotelDescription,
                    hotelPageURL,
                    WebUtil.GetTranslatedText(
                        "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/RoadDirections", currentLanguage),
                    WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/CheckRates",
                                              currentLanguage),
                                              string.IsNullOrEmpty(hotelOperaID) ? string.Empty : "HOTEL:" + hotelOperaID,
                    displayPromoLink == true
                        ? (!string.IsNullOrEmpty(promotionalPageLink) ? promotionalPageLink : "#")
                        : hotelPageURL,
                    displayPromoLink == true
                        ? (!string.IsNullOrEmpty(truncatedPromoPageLinkText) ? truncatedPromoPageLinkText : "&nbsp;")
                        : WebUtil.GetTranslatedText(
                            "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/FullDescription",
                            currentLanguage)
                    );

                sb = sb.Replace("\r\n", string.Empty);
                //sb = sb.Replace("'", "\\'");
            }
             sb = sb.Replace("'", "&#39;");
            return sb.ToString();
        }

        /// <summary>
        /// Gets the truncated promo page link text.
        /// </summary>
        /// <returns>truncated promo link display text</returns>
        private string GetTruncatedPromoPageLinkText()
        {
            string taglessPromotionalPageLinkText = RemoveHtmlTags(promotionalPageLinkText);

            try
            {
                if (!string.IsNullOrEmpty(taglessPromotionalPageLinkText))
                {
                    if (taglessPromotionalPageLinkText.Length >
                        GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_PROMOLINKTEXT)
                    {
                        taglessPromotionalPageLinkText = taglessPromotionalPageLinkText.Substring(0,
                                                                                                  GoogleMapConstants.
                                                                                                      MAX_LENGTH_ADVANCED_INFOBOX_PROMOLINKTEXT);
                        taglessPromotionalPageLinkText += GoogleMapConstants.TRIPPLE_DOTS;
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while parsing promolink text in the method - GetTruncatedPromoPageLinkText");
            }
            return taglessPromotionalPageLinkText;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// This method truncate the city center distance string if
        /// it overflows the popup.
        /// </summary>
        /// <returns>truncated city center dist. string</returns>
        private string GetTruncatedCityCtrDistance()
        {
            string kmToCityCentre =
                WebUtil.GetTranslatedText("/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/KmToCityCentre", Utility.GetCurrentLanguage());
            try
            {
                if (!string.IsNullOrEmpty(kmToCityCentre))
                {
                    int hotelNumberLength = hotelNumber > 0
                                                ? hotelNumber.ToString().Length + '.'.ToString().Length
                                                : string.Empty.Length;
                    int hotelNameLength = !string.IsNullOrEmpty(hotelName) ? hotelName.Length : 0;
                    StringBuilder cityCenterString = new StringBuilder();
                    cityCenterString.AppendFormat(@" ({0} {1})", cityCenterDistance.ToString(),
                                                  WebUtil.GetTranslatedText(
                                                      "/Templates/Scanweb/Units/Placeable/HotelListingAndGoogleMap/KmToCityCentre"));
                    int cityCenterStringLength = cityCenterString.Length;
                    int totalHotelNameHeadingLength = hotelNumberLength + hotelNameLength + cityCenterStringLength;

                    if (totalHotelNameHeadingLength > GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_HEADING)
                    {
                        int extraCharacterLength = totalHotelNameHeadingLength -
                                                   GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_HEADING;
                        if (kmToCityCentre.Length - extraCharacterLength - GoogleMapConstants.TRIPPLE_DOTS.Length > 0)
                        {
                            kmToCityCentre = kmToCityCentre.Substring(0,
                                                                      kmToCityCentre.Length - extraCharacterLength -
                                                                      GoogleMapConstants.TRIPPLE_DOTS.Length);
                            kmToCityCentre += GoogleMapConstants.TRIPPLE_DOTS;
                        }
                        else
                        {
                            kmToCityCentre = GoogleMapConstants.TRIPPLE_DOTS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while parsing city center distance in the method - GetTruncatedCityCtrDistance");
            }

            return kmToCityCentre;
        }

        /// <summary>
        /// This method truncate the street address if it overflows the
        /// popup
        /// </summary>
        /// <returns>truncated address</returns>
        private string GetTruncatedStreetAddress()
        {
            StringBuilder streetAddress = new StringBuilder();

            try
            {
                if (!string.IsNullOrEmpty(address))
                {
                    streetAddress.Append(address);
                    if (!string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        streetAddress.Append(", ");
                }
                if (!string.IsNullOrEmpty(zip))
                {
                    streetAddress.Append(zip);
                    if (!string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        streetAddress.Append(", ");
                }
                if (!string.IsNullOrEmpty(postalCity))
                {
                    if (city.Trim() == postalCity.Trim())
                    {
                        streetAddress.Append(postalCity);
                        if (!string.IsNullOrEmpty(city))
                            streetAddress.Append(", ");
                    }
                    else
                    {
                        streetAddress.Append(postalCity);
                        if (!string.IsNullOrEmpty(city))
                            streetAddress.Append(", ");

                        if (!string.IsNullOrEmpty(city))
                        {
                            streetAddress.Append(city);
                            if (!string.IsNullOrEmpty(country))
                                streetAddress.Append(", ");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(country))
                {
                    streetAddress.Append(country);
                }

                if (streetAddress.Length == 0)
                {
                    streetAddress.Append("&nbsp;");
                }

                if (streetAddress.Length > GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_ADDRESS)
                {
                    int extraCharacterLength = streetAddress.Length -
                                               GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_ADDRESS;
                    streetAddress =
                        streetAddress.Remove(GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_ADDRESS -
                                             GoogleMapConstants.TRIPPLE_DOTS.Length,
                                             extraCharacterLength + GoogleMapConstants.TRIPPLE_DOTS.Length);
                    streetAddress.Append(GoogleMapConstants.TRIPPLE_DOTS);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while parsing street address in the method - GetTruncatedStreetAddress");
            }

            return streetAddress.ToString();
        }

        /// <summary>
        /// GetFullStreetAddress
        /// </summary>
        /// <returns>FullStreetAddress</returns>
        private string GetFullStreetAddress()
        {
            StringBuilder streetAddress = new StringBuilder();

            try
            {
                if (!string.IsNullOrEmpty(address))
                {
                    streetAddress.Append(address);
                    if (!string.IsNullOrEmpty(zip) || !string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        streetAddress.Append(", ");
                }
                if (!string.IsNullOrEmpty(zip))
                {
                    streetAddress.Append(zip);
                    if (!string.IsNullOrEmpty(city) || !string.IsNullOrEmpty(country))
                        streetAddress.Append(", ");
                }
                if (city.Trim() == postalCity.Trim())
                {
                    streetAddress.Append(postalCity);
                    if (!string.IsNullOrEmpty(country))
                        streetAddress.Append(", ");
                }
                else
                {
                    if (!string.IsNullOrEmpty(postalCity))
                    {
                        streetAddress.Append(postalCity);
                        if (!string.IsNullOrEmpty(city))
                            streetAddress.Append(", ");
                    }
                    if (!string.IsNullOrEmpty(city))
                    {
                        streetAddress.Append(city);
                        if (!string.IsNullOrEmpty(country))
                            streetAddress.Append(", ");
                    }
                }

                if (!string.IsNullOrEmpty(country))
                {
                    streetAddress.Append(country);
                }

                if (streetAddress.Length == 0)
                {
                    streetAddress.Append("&nbsp;");
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while parsing street address in the method - GetFullStreetAddress");
            }

            return streetAddress.ToString();
        }

        /// <summary>
        /// Gets the filtered hotel description.
        /// </summary>
        /// <returns>Filtered and truncated hotel description</returns>
        private string GetFilteredHotelDescription()
        {
            string taglessHotelDescription = RemoveHtmlTags(hotelDescription);

            try
            {
                if (!string.IsNullOrEmpty(taglessHotelDescription))
                {
                    if (taglessHotelDescription.Length > GoogleMapConstants.MAX_LENGTH_ADVANCED_INFOBOX_SHORTDESCRIPTION)
                    {
                        taglessHotelDescription = taglessHotelDescription.Substring(0,
                                                                                    GoogleMapConstants.
                                                                                        MAX_LENGTH_ADVANCED_INFOBOX_SHORTDESCRIPTION);
                        taglessHotelDescription += GoogleMapConstants.TRIPPLE_DOTS;
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while parsing short description in the method - GetFilteredHotelDescription");
            }
            return taglessHotelDescription;
        }

        /// <summary>
        /// Remove all the html tags from the hotel description
        /// This will avoid formatting of the hotelDescription
        /// </summary>
        /// <param name="data">string to be formatted</param>
        /// <returns>resulting string</returns>
        private string RemoveHtmlTags(string data)
        {
            try
            {
                if (!string.IsNullOrEmpty(data))
                {
                    Regex tagPattern = new Regex("<[^>]*>");
                    data = tagPattern.Replace(data, string.Empty);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex,
                                            "Error while removing html tags from input data in the method - RemoveHtmlTags");
            }

            return data;
        }

        #endregion
    }
}