//  Description					:   MailManager                                           //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Net.Mail;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Util.MailManager
{
    /// <summary>
    /// This class contains members of mail manager.
    /// </summary>
    internal class MailManager
    {
        public enum ContentChangeRequestStatus
        {
            rejected,
            completed
        }

        private MailMessage MailToSend;
        private SmtpClient MailClient;

        /// <summary>
        /// Constructor of mail manager.
        /// </summary>
        /// <param name="Header"></param>
        /// <param name="authorName"></param>
        /// <param name="authorEmail"></param>
        /// <param name="webSite"></param>
        /// <param name="mailBody"></param>
        /// <param name="mailSubject"></param>
        /// <param name="requestStatus"></param>
        /// <param name="comments"></param>
        public MailManager(string Header, string authorName, string authorEmail, string mailBody,
                           string mailSubject, ContentChangeRequestStatus requestStatus, string comments)
        {
            string MailFrom =
                LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/contentChangeRequest/MailFrom");
            this.MailToSend = new MailMessage();
            this.MailToSend.To.Add(new MailAddress(authorEmail));
            this.MailToSend.From = new MailAddress(MailFrom);
            this.MailToSend.Body = Header + " \n\n";
            this.MailToSend.BodyEncoding = System.Text.Encoding.UTF8;

            if (requestStatus.ToString() == "completed")
            {
                this.MailToSend.Subject =
                    LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/contentChangeRequest/Completed/Mailsubject");
                this.MailToSend.Body +=
                    LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/contentChangeRequest/Completed/MailBody") + " \n\n";
                this.MailToSend.Body += comments + " \n\n";
            }
            else
            {
                this.MailToSend.Subject =
                    LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/contentChangeRequest/Rejected/Mailsubject");
                this.MailToSend.Body +=
                    LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/contentChangeRequest/Rejected/MailBody") +
                    " \n\n";
                this.MailToSend.Body += comments + " \n\n";
            }
            this.MailClient = new SmtpClient();
        }

        /// <summary>
        /// This method facilitates in sending out the mail.
        /// </summary>
        /// <returns>True/False</returns>
        public bool SendMail()
        {
            try
            {
                this.MailClient.Send(this.MailToSend);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}