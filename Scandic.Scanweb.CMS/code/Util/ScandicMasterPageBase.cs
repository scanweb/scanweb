using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using System;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller;
using System.Configuration;

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// Scandic Master Page Base
    /// </summary>
    public class ScandicMasterPageBase : System.Web.UI.MasterPage
    {        
        /// <summary>
        /// Returns the right title tag. Title, if it is set, and PageName otherwise.
        /// </summary>
        protected string GetTitle()
        {
            PageBase page = (PageBase) Page;

            if (page.CurrentPage["Title"] != null)
            {
                return page.CurrentPage["Title"].ToString();
            }
            else
            {
                if (page.CurrentPage["Heading"] != null)
                {
                    return page.CurrentPage["Heading"].ToString();
                }
                else
                {
                    return page.CurrentPage.PageName.ToString();
                }
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            PageBase page = (PageBase)Page;
            PageDataCollection pages = ContentDataAccessManager.GetChildren(page.CurrentPage.PageLink);
            if (pages != null && pages.Count > 0)
            {
                if (string.Equals(page.CurrentPage.ParentLink.ToString(), ConfigurationManager.AppSettings["RootPageID"], StringComparison.InvariantCultureIgnoreCase))
                {
                    LastVisitedPageSessionWrapper.SelectedMainMenuLink = page.CurrentPage.PageLink;
                }
                else
                {
                    LastVisitedPageSessionWrapper.SelectedMainMenuLink = page.CurrentPage.ParentLink;
                }

                LastVisitedPageSessionWrapper.SelectedSubMenuPageName = page.CurrentPage.PageName.Trim();
            }
            else
            {
                PageDataCollection pagecollections = ContentDataAccessManager.GetChildren(page.CurrentPage.ParentLink);
                var pageData = ContentDataAccessManager.GetPageData(page.CurrentPage.ParentLink);
                if (pagecollections != null && pagecollections.Count > 0 &&
                    !string.Equals(pageData.ParentLink.ToString(), ConfigurationManager.AppSettings["RootPageID"], StringComparison.InvariantCultureIgnoreCase))
                {                    
                    LastVisitedPageSessionWrapper.SelectedMainMenuLink = pageData.ParentLink;
                    LastVisitedPageSessionWrapper.SelectedSubMenuPageName = pageData.PageName;
                }
                else
                {
                    LastVisitedPageSessionWrapper.SelectedMainMenuLink = page.CurrentPage.ParentLink;
                    LastVisitedPageSessionWrapper.SelectedSubMenuPageName = page.CurrentPage.PageName.Trim();
                }
            }
        }

        /// <summary>
        /// On Load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            Page.ClientScript.RegisterStartupScript(typeof (Page), "RenderAlteratingTables",
                                                    "<script type=\"text/javascript\">RenderAllAlteratingRowsTable();</script>");
        }
    }
}