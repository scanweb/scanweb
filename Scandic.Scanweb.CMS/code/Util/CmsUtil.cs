//  Description					:   CmsUtil                                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using System.Configuration;

namespace Scandic.Scanweb.CMS
{
    /// <summary>
    /// CmsUtil
    /// </summary>
    public class CmsUtil
    {
        /// <summary>
        /// This method does 2 things.
        /// 1.Get the page id to be redirected from the command argument of the button and redirect to that page.
        /// 2.Make SessionWrapper.IsMenuClicked = true, so that this flag decides how LeftMenu.ascx will be rendered.
        /// </summary>
        /// <param name="button">Button which carry command argument</param>
        public static void RedirectAndSetFlag(LinkButton button)
        {
           if (null != button)
            {
                string pageLink = button.CommandArgument as string;
                RedirectAndSetFlag(pageLink);
            }
            
        }

        public static void RedirectAndSetFlag(string pageLink)
        {
            string redirectUrl = "/.";
            if (!string.IsNullOrEmpty(pageLink))
            {
                PageReference currentPageReference = new PageReference(pageLink);
                PageData redirectingPage = DataFactory.Instance.GetPage(currentPageReference);
                redirectUrl = redirectingPage.LinkURL;
            }
            System.Web.HttpContext.Current.Response.Redirect(redirectUrl);
        }
        public static string GetCSSVersion()
        {
            return ConfigurationManager.AppSettings["CSSVersion"];
        }
        public static string GetJSVersion()
        {
            return ConfigurationManager.AppSettings["JSVersion"];
        }
    }
}