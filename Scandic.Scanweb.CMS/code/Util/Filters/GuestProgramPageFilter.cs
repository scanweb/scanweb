//  Description					:   GuestProgramPageFilter                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer.Core;
using EPiServer.Filters;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using EPiServer.DataAbstraction;
using System.Configuration;
using System;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Util.Filters
{
    /// <summary>
    /// GuestProgramPageFilter
    /// </summary>
    public class GuestProgramPageFilter
    {
        /// <summary>
        /// OnFilter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnFilter(object sender, FilterEventArgs e)
        {
            PageDataCollection pageList = e.Pages;
            pageList = Filter(pageList);
            pageList = FilterCountryCityPages(pageList);
        }

        private PageDataCollection FilterCountryCityPages(PageDataCollection subMenuPages)
        {
            PageDataCollection allPages = subMenuPages;
            if ((allPages != null) && (allPages.Count > 0))
            {

                PageData currPage;

                int CountryPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CountryPageTypeID]);
                int CityPageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings[AppConstants.CityPageTypeID]);
                for (int p = 0; p < allPages.Count; p++)
                {
                    currPage = allPages[p];
                    if (currPage.PageTypeID == CountryPageTypeID || currPage.PageTypeID == CityPageTypeID)
                    {
                        allPages.RemoveAt(p);
                        p--;
                    }
                }
            }
            return allPages;
        }
        /// <summary>
        /// Filters out pages based on the guest program criteria. 
        /// </summary>
        /// <param name="allPages"></param>
        /// <returns></returns>
        public PageDataCollection Filter(PageDataCollection pageList)
        {
            PageDataCollection allPages = pageList;
            if ((allPages != null) && (allPages.Count > 0))
            {
                bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;
                PageData currPage;

                for (int p = 0; p < allPages.Count; p++)
                {
                    currPage = allPages[p];
                    bool tabLoggedInVisible = (currPage["GuestProgramVisibleWhenLoggedIn"] != null)
                                                  ? ((bool) currPage["GuestProgramVisibleWhenLoggedIn"])
                                                  : false;
                    bool tabLoggedInNotVisible = (currPage["GuestProgramNotVisibleWhenLoggedIn"] != null)
                                                     ? ((bool) currPage["GuestProgramNotVisibleWhenLoggedIn"])
                                                     : false;
                    if ((!isLoggedIn && tabLoggedInVisible == true) || (isLoggedIn && tabLoggedInNotVisible == true))
                    {
                        allPages.RemoveAt(p);
                        p--;
                    }
                }
            }
            return allPages;
        }
    }
}