﻿//  Description					:   PageCollection                                        //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.DataAbstraction;
using System.Configuration;
using EPiServer;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Util.Filters
{
    /// <summary>
    /// PageCollection
    /// </summary>
    public class PageCollection
    {
        /// <summary>
        /// Remove the pages whose 'PageVisibleInMenu' is unchecked
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EPiServer.Filters.FilterEventArgs"/> instance containing the event data.</param>
        public void RemoveInvisible(object sender, FilterEventArgs e)
        {
            PageData page;
            PageDataCollection pageList = e.Pages;
            pageList = FilterInvisible(pageList);
        }

        /// <summary>
        /// Filters out invisible pages from the whole page list.
        /// </summary>
        /// <param name="pageList"></param>
        /// <returns></returns>
        public PageDataCollection FilterInvisible(PageDataCollection pageList)
        {
            PageDataCollection allPages = pageList;
            if ((allPages != null) && (allPages.Count > 0))
            {
                for (int pageCount = 0; pageCount < allPages.Count; pageCount++)
                {
                    if (allPages[pageCount].StopPublish <= DateTime.Now || null == allPages[pageCount]["PageVisibleInMenu"])
                    {
                        allPages.RemoveAt(pageCount);
                        pageCount--;
                    }
                }
            }
            return allPages;
        }
    }
}
