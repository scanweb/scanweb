﻿using System;
using System.Configuration;
using System.Web;
using Scandic.Scanweb.Core;
using System.Text.RegularExpressions;

namespace Scandic.Scanweb.CMS.Util.MobileRedirect
{
    /// <summary>
    /// Wurfl
    /// </summary>
    public sealed class Wurfl
    {
        #region Declaration

        private static Wurfl instance;
        private static object wurflSyncLock = new object();
        private string mobileDeviceUserAgents;
        private string mobileDeviceUserAgentsVersion;
        private string tabletUserAgents;
        private string redirectionFlag;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Wurfl()
        {
            Initialize();
        }

        #endregion

        #region Property

        
        /// <summary>
        /// Constructor
        /// </summary>
        public static Wurfl Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (wurflSyncLock)
                    {
                        if (instance == null)
                        {
                            instance = new Wurfl();
                        }
                    }
                }

                return instance;
            }
        }

        #endregion

        #region Methods

        /// <summary> 
        /// To be called once in the app, loads the WURFL database in memory. 
        /// </summary> 
        public void Initialize()
        {
            try
            {
                mobileDeviceUserAgents = ConfigurationManager.AppSettings["MobileDeviceUserAgents"];
                mobileDeviceUserAgentsVersion = ConfigurationManager.AppSettings["MobileDeviceUserAgentsVersion"];
                tabletUserAgents = ConfigurationManager.AppSettings["TabletUserAgents"];
                redirectionFlag = ConfigurationManager.AppSettings["EnableMobileDeviceRedirection"];

            }
            catch (Exception ex)
            {
                AppLogger.LogFatalException(ex, "Error building WURFL Mangager.");
            }
        }

            public bool CheckMobileRedirection(string userAgent, string currentHostURL, string previousHostURL)
            {
                bool redirect = false;
                if (!string.IsNullOrEmpty(userAgent))
                {
                    if (!string.IsNullOrEmpty(redirectionFlag))
                    {
                        bool performRedirection = false;
                        bool.TryParse(redirectionFlag, out performRedirection);
                        if (performRedirection)
                        {

                            //If url referrel is same as current url host, return false.
                            if (!string.IsNullOrEmpty(previousHostURL))
                            {
                                int prevHostLen = previousHostURL.LastIndexOf('.') == -1 ? previousHostURL.Length : previousHostURL.LastIndexOf(".");
                                int currHostLen = currentHostURL.LastIndexOf('.') == -1 ? currentHostURL.Length : currentHostURL.LastIndexOf(".");
                                if (string.Equals(previousHostURL.Substring(0, prevHostLen), currentHostURL.Substring(0, currHostLen), StringComparison.InvariantCultureIgnoreCase))
                                    return false;
                            }

                            redirect = IsMobileDevice(userAgent) && !IsTablet(userAgent);
                        }

                    }
                }
                return redirect;
            }

        /// <summary>
        /// Checks whether it is a mobile device or not
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns>mobile device or not</returns>
        public bool IsMobileDevice(string userAgent)
            {
                try
                {
                    if (!string.IsNullOrEmpty(userAgent))
                    {                        
                        if (!string.IsNullOrEmpty(redirectionFlag))
                        {
                            bool checkDevice = false;
                            bool.TryParse(redirectionFlag, out checkDevice);
                            if (checkDevice)
                            {                                
                                var mobileRegx = new Regex(mobileDeviceUserAgents, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                                var mobilVersionRegx = new Regex(mobileDeviceUserAgentsVersion, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                                return (mobileRegx.IsMatch(userAgent) || mobilVersionRegx.IsMatch(userAgent.Substring(0, 4)));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    AppLogger.LogFatalException(ex, string.Format("IsMobileDevice: Error fetching device info for User Agent - {0}", userAgent));
                }                                
            return false;
        }

        /// <summary>
        /// Checks whether its a tablet or not
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns>true or false</returns>
        public bool IsTablet(string userAgent)
            {
                try
                {
                    if (!string.IsNullOrEmpty(userAgent))
                    {
                        if (!string.IsNullOrEmpty(redirectionFlag))
                        {
                            bool checkDevice = false;
                            bool.TryParse(redirectionFlag, out checkDevice);
                            if (checkDevice)
                            {
                                var tabletRegx = new Regex(tabletUserAgents, RegexOptions.IgnoreCase | RegexOptions.Multiline);                                
                                return tabletRegx.IsMatch(userAgent);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    AppLogger.LogFatalException(ex, string.Format("IsTablet: Error fetching device info for User Agent - {0}", userAgent));
                }               
            return false;
        }

        #endregion
    }
}