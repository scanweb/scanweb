//  Description					:   Utils                                                 //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// This class contains utility functions.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Gets the status of IsNoImagesForPopUp
        /// </summary>
        /// <param name="page"></param>
        /// <returns>True/False</returns>
        public static bool IsNoImagesForPopUp(PageData page)
        {
            bool showImagePopup = false;
            int countNulls = 0;

            if (page != null)
            {
                ArrayList listProperties = new ArrayList();

                if (!String.IsNullOrEmpty(page["GeneralImage1"] as string))
                    listProperties.Add(page["GeneralImage1"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage2"] as string))
                    listProperties.Add(page["GeneralImage2"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage3"] as string))
                    listProperties.Add(page["GeneralImage3"] as string);
                if (!String.IsNullOrEmpty(page["GeneralImage4"] as string))
                    listProperties.Add(page["GeneralImage4"] as string);

                if (!String.IsNullOrEmpty(page["RoomImage1"] as string))
                    listProperties.Add(page["RoomImage1"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage2"] as string))
                    listProperties.Add(page["RoomImage2"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage3"] as string))
                    listProperties.Add(page["RoomImage3"] as string);
                if (!String.IsNullOrEmpty(page["RoomImage4"] as string))
                    listProperties.Add(page["RoomImage4"] as string);

                if (!String.IsNullOrEmpty(page["RestBarImage1"] as string))
                    listProperties.Add(page["RestBarImage1"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage2"] as string))
                    listProperties.Add(page["RestBarImage2"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage3"] as string))
                    listProperties.Add(page["RestBarImage3"] as string);
                if (!String.IsNullOrEmpty(page["RestBarImage4"] as string))
                    listProperties.Add(page["RestBarImage4"] as string);

                if (!String.IsNullOrEmpty(page["LeisureImage1"] as string))
                    listProperties.Add(page["LeisureImage1"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage2"] as string))
                    listProperties.Add(page["LeisureImage2"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage3"] as string))
                    listProperties.Add(page["LeisureImage3"] as string);
                if (!String.IsNullOrEmpty(page["LeisureImage4"] as string))
                    listProperties.Add(page["LeisureImage4"] as string);

                foreach (string image in listProperties)
                {
                    if (!String.IsNullOrEmpty(image))
                    {
                        countNulls += 1;
                    }
                }

                if (countNulls != 0)
                    showImagePopup = true;
            }

            return showImagePopup;
        }
    }
}