//  Description					: LangAltText                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.code.Booking;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// Class handles alttexts
    /// </summary>
    public class LangAltText : ImageAltText
    {
        
    }
}