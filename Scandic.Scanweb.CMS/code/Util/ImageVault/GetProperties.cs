//  Description					:   GetProperties                                         //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using ImageStoreNET.Developer;
using ImageStoreNET.Developer.Core;

namespace Scandic.Scanweb.CMS.Util.ImageVault
{
    /// <summary>
    /// GetProperties
    /// </summary>
    public class GetProperties
    {
        /// <summary>
        /// Gets the title of an ImageVault image
        /// </summary>
        /// <param name="language">Languge prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>title of an ImageVault image</returns>
        public string GetImageTitle(string language, string image)
        {
            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                IVFileData ivFile = IVDataFactory.GetFile(ub.Id,
                                                          ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);
                return HttpUtility.HtmlEncode(ivFile.Title);
            }
            catch
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the language controlled description of an ImageVault image
        /// </summary>
        /// <param name="language">Languge prefix</param>
        /// <param name="image">ImageVault image property as string</param>
        /// <returns>language controlled description of an ImageVault image</returns>
        public string GetImageDescription(string language, string image)
        {
            string rightAltText = "Description_" + language.ToUpper();

            IVUrlBuilder ub = IVUrlBuilder.ParseUrl(image);

            try
            {
                IVFileData ivFile = IVDataFactory.GetFile(ub.Id,
                                                          ImageStoreNET.Developer.Security.IVAccessLevel.IgnoreAccess);

                foreach (IVMetaData meta in ivFile.MetaData)
                {
                    if (meta.Name == rightAltText)
                    {
                        if (meta.Value != "")
                            return HttpUtility.HtmlEncode(meta.Value);
                    }
                }

                return String.Empty;
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}