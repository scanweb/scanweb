////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  generateXmlSiteMap                                     //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.PlugIn;
using EPiServer.Web.WebControls;

namespace Scandic.Scanweb.CMS.Util.generateXmlSiteMap
{
    /// <summary>
    /// This jobb creates a Sitemap.XML for each language.
    /// Urls in specifik languages, fits under it's correspondig domainUrl
    /// </summary>
    [ScheduledPlugIn(DisplayName = "Generate SiteMap.XML",
        Description = "This job creates a Sitemap.XML for each language.")]
    public class generateXmlSiteMap
    {
        private static List<int> GetPageTypeIDs()
        {
            List<int> includedPageTypeIDs = new List<int>();
            string guids = ConfigurationManager.AppSettings["SiteMapPageTypeGUIDs"] as string;

            if (guids != null)
            {
                foreach (string guid in guids.Split(','))
                {
                    Guid pageTypeGuid = new Guid(guid);
                    PageType pageType = PageType.Load(pageTypeGuid);
                    includedPageTypeIDs.Add(pageType.ID);
                }
            }
            return includedPageTypeIDs;
        }

        public static string Execute()
        {

            List<int> includedPageTypeIDs = GetPageTypeIDs();

            string siteMapPathRoot = ConfigurationManager.AppSettings["SiteMapXMLPath"] as string ?? string.Empty;

            string siteMapXmlEn = siteMapPathRoot + "sitemap.com.xml";
            string siteMapXmlSv = siteMapPathRoot + "sitemap.se.xml";
            string siteMapXmlNo = siteMapPathRoot + "sitemap.no.xml";
            string siteMapXmlDa = siteMapPathRoot + "sitemap.dk.xml";
            string siteMapXmlFi = siteMapPathRoot + "sitemap.fi.xml";
            string siteMapXmlDe = siteMapPathRoot + "sitemap.de.xml";
            string siteMapXmlRu = siteMapPathRoot + "sitemap.ru.xml";

            XmlTextWriter writerEn = new XmlTextWriter(siteMapXmlEn, System.Text.Encoding.UTF8);
            XmlTextWriter writerSv = new XmlTextWriter(siteMapXmlSv, System.Text.Encoding.UTF8);
            XmlTextWriter writerNo = new XmlTextWriter(siteMapXmlNo, System.Text.Encoding.UTF8);
            XmlTextWriter writerDa = new XmlTextWriter(siteMapXmlDa, System.Text.Encoding.UTF8);
            XmlTextWriter writerFi = new XmlTextWriter(siteMapXmlFi, System.Text.Encoding.UTF8);
            XmlTextWriter writerDe = new XmlTextWriter(siteMapXmlDe, System.Text.Encoding.UTF8);
            XmlTextWriter writerRu = new XmlTextWriter(siteMapXmlRu, System.Text.Encoding.UTF8);

            writerEn.Formatting = Formatting.Indented;
            writerSv.Formatting = Formatting.Indented;
            writerNo.Formatting = Formatting.Indented;
            writerDa.Formatting = Formatting.Indented;
            writerFi.Formatting = Formatting.Indented;
            writerDe.Formatting = Formatting.Indented;
            writerRu.Formatting = Formatting.Indented;

            writerEn.WriteStartDocument();
            writerSv.WriteStartDocument();
            writerNo.WriteStartDocument();
            writerDa.WriteStartDocument();
            writerFi.WriteStartDocument();
            writerDe.WriteStartDocument();
            writerRu.WriteStartDocument();

            writerEn.WriteStartElement("urlset");
            writerSv.WriteStartElement("urlset");
            writerNo.WriteStartElement("urlset");
            writerDa.WriteStartElement("urlset");
            writerFi.WriteStartElement("urlset");
            writerDe.WriteStartElement("urlset");
            writerRu.WriteStartElement("urlset");

            writerEn.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerSv.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerNo.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerDa.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerFi.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerDe.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writerRu.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

            writerEn.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerSv.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerNo.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerDa.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerFi.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerDe.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");
            writerRu.WriteAttributeString("xmlns:xhtml", "http://www.w3.org/1999/xhtml");

            PageTreeData ScanWebPageTree = new PageTreeData();
            ScanWebPageTree.PageLink = PageReference.StartPage;

            ScanWebPageTree.ExpandAll = true;
            ScanWebPageTree.EnableVisibleInMenu = false;
            ScanWebPageTree.RequiredAccess = EPiServer.Security.AccessLevel.Read;
            ScanWebPageTree.PublishedStatus = PagePublishedStatus.Published;

            foreach (PageData pd in ScanWebPageTree)
            {
                if (PageShouldBeVisibleInSiteMap(pd, includedPageTypeIDs))
                {
                    GetPagesInDiffLanguages(writerEn, writerSv, writerNo, writerDa, writerFi, writerDe, writerRu, pd);
                }
            }

            writerEn.WriteEndElement();
            writerSv.WriteEndElement();
            writerNo.WriteEndElement();
            writerDa.WriteEndElement();
            writerFi.WriteEndElement();
            writerDe.WriteEndElement();// urlset        
            writerRu.WriteEndElement();// urlset 

            writerEn.Close();
            writerSv.Close();
            writerNo.Close();
            writerDa.Close();
            writerFi.Close();
            writerDe.Close();
            writerRu.Close();

            return "OK";
        }


        /// <summary>
        /// GetPagesInDiffLanguages
        /// </summary>
        /// <param name="writerEn"></param>
        /// <param name="writerSv"></param>
        /// <param name="writerNo"></param>
        /// <param name="writerDa"></param>
        /// <param name="writerFi"></param>
        /// <param name="pd"></param>
        private static void GetPagesInDiffLanguages(XmlWriter writerEn, XmlWriter writerSv, XmlWriter writerNo, XmlWriter writerDa, XmlWriter writerFi,
            XmlWriter writerDe, XmlWriter writerRu, PageData pd)
        {
            PageDataCollection pageInDiffLanguages = DataFactory.Instance.GetLanguageBranches(pd.PageLink);
            PageDataCollection pageLanguages = pageInDiffLanguages;
            foreach (PageData pageinSpecificLang in pageInDiffLanguages)
            {
                switch (pageinSpecificLang.LanguageID)
                {
                    case "en": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicCom"], writerEn, pageinSpecificLang, pageLanguages); break;
                    case "sv": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicSe"], writerSv, pageinSpecificLang, pageLanguages); break;
                    case "no": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicNo"], writerNo, pageinSpecificLang, pageLanguages); break;
                    case "da": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicDa"], writerDa, pageinSpecificLang, pageLanguages); break;
                    case "fi": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicFi"], writerFi, pageinSpecificLang, pageLanguages); break;
                    case "de": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicDe"], writerDe, pageinSpecificLang, pageLanguages); break;
                    case "ru-RU": WriteXmlToSpecificLangSiteMap(System.Configuration.ConfigurationManager.AppSettings["ScandicRU-RU"], writerRu, pageinSpecificLang, pageLanguages); break;
                }
            }
        }

        /// <summary>
        /// returns the Host URL for the supplied language
        /// </summary>
        /// <param name="languageID">languge ID for respective domain</param>
        /// <returns></returns>
        private static string GetHostURLByLanguage(string languageID)
        {
            string language = string.Empty;
            switch (languageID.ToLower())
            {
                case "en": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicCom"]); break;
                case "sv": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicSe"]); break;
                case "no": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicNo"]); break;
                case "da": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicDa"]); break;
                case "fi": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicFi"]); break;
                case "de": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicDe"]); break;
                case "ru-ru": language = Convert.ToString(ConfigurationManager.AppSettings["ScandicRU-RU"]); break;
            }
            return language;
        }

        /// <summary>
        /// Function to Create the Page URL's for site map xml
        /// </summary>
        /// <param name="pd">page data object</param>
        /// <returns></returns>
        private static string SitemapPageURL(PageData pd)
        {
            string outputurl = string.Empty;
            try
            {
                UrlBuilder url = new UrlBuilder(EPiServer.UriSupport.AddLanguageSelection(pd.LinkURL, pd.LanguageID));
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pd.PageLink, System.Text.UTF8Encoding.UTF8);
                outputurl = url.ToString();
                if ((outputurl.Length >= 4) && (outputurl.Substring(0, 4) == "/en/" || outputurl.Substring(0, 4) == "/sv/" || outputurl.Substring(0, 4) == "/no/"
                    || outputurl.Substring(0, 4) == "/da/" || outputurl.Substring(0, 4) == "/fi/" || outputurl.Substring(0, 4) == "/de/"))
                {
                    outputurl = outputurl.Remove(0, 3);
                }
                else if ((outputurl.Length >= 7) && (outputurl.Substring(0, 7) == "/ru-RU/"))
                {
                    outputurl = outputurl.Remove(0, 6);
                }
            }
            catch (System.UriFormatException ex)
            {

            }
            return outputurl;

        }

        /// <summary>
        /// WriteXmlToSpecificLangSiteMap
        /// </summary>
        /// <param name="hostUrl"></param>
        /// <param name="writer"></param>
        /// <param name="pd"></param>
        private static void WriteXmlToSpecificLangSiteMap(string hostUrl, XmlWriter writer, PageData pd, PageDataCollection pageLanguages)
        {
            string outputurl = SitemapPageURL(pd);
            writer.WriteStartElement("url");
            writer.WriteElementString("loc", hostUrl + outputurl);
            writer.WriteElementString("changefreq", "daily");

            List<int> includedPageTypeIDs = GetPageTypeIDs();
            //logic to write the URL all pages in alternate languages in Sitemap.XML
            foreach (var item in pageLanguages)
            {
                //if (pd.LanguageID != item.LanguageID) //checks of other language
                //{
                    if (PageShouldBeVisibleInSiteMap(pd, includedPageTypeIDs))
                    {
                        WriteXmlForAlternatePageLanguagesURLInSiteMap(GetHostURLByLanguage(item.LanguageID), writer, item);
                    }
                //}
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// WriteXmlToSpecificLangSiteMap
        /// </summary>
        /// <param name="hostUrl"></param>
        /// <param name="writer"></param>
        /// <param name="pd"></param>
        private static void WriteXmlForAlternatePageLanguagesURLInSiteMap(string hostUrl, XmlWriter writer, PageData pd)
        {
            string outputurl = SitemapPageURL(pd);
            writer.WriteStartElement("xhtml:link");
            writer.WriteAttributeString("rel", "alternate");
            writer.WriteAttributeString("hreflang", pd.LanguageID);
            writer.WriteAttributeString("href", hostUrl + outputurl);
            writer.WriteEndElement();
        }

        /// <summary>
        /// PageShouldBeVisibleInSiteMap
        /// </summary>
        /// <param name="pd"></param>
        /// <param name="includedPageTypes"></param>
        /// <returns>True/False</returns>
        private static bool PageShouldBeVisibleInSiteMap(PageData pd, List<int> includedPageTypes)
        {
            return ((int)pd["PageShortcutType"] == (int)PageShortcutType.Normal
                    && pd.CheckPublishedStatus(PagePublishedStatus.Published)
                    && !PropertyValueEqualsVisible("ExcludeFromXMLSiteMap", pd)
                    && includedPageTypes.Contains(pd.PageTypeID));
        }

        /// <summary>
        /// PropertyValueEqualsVisible
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="pd"></param>
        /// <returns>True/False</returns>
        private static bool PropertyValueEqualsVisible(string propertyName, PageData pd)
        {
            if (pd[propertyName] != null)
            {
                return ((int)pd[propertyName] ==
                        (int)Scandic.Scanweb.CMS.SpecializedProperties.SelectVisibility.Visibility.Visible);
            }
            else
            {
                return false;
            }
        }
    }
}