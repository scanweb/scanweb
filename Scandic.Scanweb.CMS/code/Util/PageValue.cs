﻿//  Description					:   PageValue                                             //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// Page Value
    /// </summary>
    public static class PageValue
    {
        /// <summary>
        /// GetPageData
        /// </summary>
        /// <param name="pageReference">Page reference</param>
        /// <param name="language">Language</param>
        /// <returns>Page data</returns>
        public static PageData GetPageData(PageReference pageReference, string language)
        {
            PageData pageData = null;
            PageVersionCollection pageVersions = PageVersion.List(pageReference);
            foreach (PageVersion pageVersion in pageVersions)
            {
                if (pageVersion.LanguageBranch == language)
                {
                    ILanguageSelector languageSelector = new LanguageSelector(language);
                    pageData = DataFactory.Instance.GetPage(pageVersion.ID, languageSelector);
                    break;
                }
            }
            return pageData;
        }
    }
}
