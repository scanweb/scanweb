//  Description					:   ScandicUserControlBase                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Web;
using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// Scandic User Control Base
    /// </summary>
    public class ScandicUserControlBase : UserControlBase
    {
        /// <summary>
        /// Returns the PageData object of the root page
        /// </summary>
        public static PageData RootPage
        {
            get { return DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess); }
        }

        /// <summary>
        /// GetDeepLinkingURL
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns>Url</returns>
        protected string GetDeepLinkingURL(string hotelID)
        {
            PageReference deepLinkingPageLink = RootPage["DeepLinkingPage"] as PageReference;
            if (deepLinkingPageLink != null)
            {
                PageData deepLinkingPage = DataFactory.Instance.GetPage(RootPage["DeepLinkingPage"] as PageReference);
                string deepLinkingURL = GetFriendlyURLToPage(deepLinkingPageLink, deepLinkingPage.LinkURL);
                return EPiServer.UriSupport.AddQueryString(deepLinkingURL, "HO", hotelID);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get Page Title
        /// </summary>
        /// <param name="page"></param>
        /// <returns>Page title</returns>
        protected string GetPageTitle(PageData page)
        {
            return page["Heading"] as string ?? page.PageName;
        }

        /// <summary>
        /// GetHotelPageURL
        /// </summary>
        /// <param name="hotelPage"></param>
        /// <param name="val"></param>
        /// <returns>Page url</returns>
        protected string GetHotelPageURL(PageData hotelPage, string val)
        {
            UrlBuilder url = new UrlBuilder((UriSupport.AddQueryString(hotelPage.LinkURL, "hotelpage", val)));
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, hotelPage.PageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// Get Friendly Url to Page
        /// </summary>
        /// <param name="pageLink"></param>
        /// <param name="linkURL"></param>
        /// <returns>Url</returns>
        protected string GetFriendlyURLToPage(PageReference pageLink, string linkURL)
        {
            UrlBuilder url = new UrlBuilder(linkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, pageLink, System.Text.UTF8Encoding.UTF8);
            return url.ToString();
        }

        /// <summary>
        /// IsLoggedIn
        /// </summary>
        /// <returns>true or false</returns>
        protected bool IsLoggedIn()
        {
            bool boolTrue = true;

            bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;

            return isLoggedIn;
        }
    }
}