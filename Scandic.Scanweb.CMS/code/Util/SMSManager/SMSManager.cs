//  Description					:   SMSManager                                            //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.IO;
using System.Net;

namespace Scandic.Scanweb.CMS.code.Util.SMSManagerOLD
{
    /// <summary>
    /// Class handles SMS Communication
    /// </summary>
    public class SMSManager
    {
        private string mobilePNumber = "";
        private string messageInternal = "";
        public string errMessage = "";

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="mobilePhoneNumber">Recipients mobile phone number</param>
        /// <param name="message">Message that is sent to the recipient</param>
        public SMSManager(string mobilePhoneNumber, string message)
        {
            mobilePNumber = mobilePhoneNumber;
            messageInternal = message;
        }

        /// <summary>
        /// Sends the SMS
        /// </summary>
        public bool SendSMS()
        {
            try
            {
                HttpPost(ConfigurationManager.AppSettings["SMSTarget"].ToString(), BuildParameters());
            }
            catch (Exception ex)
            {
                errMessage = errMessage + ex.ToString();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Builds the parameters that are sent to the SMS Supplier
        /// </summary>
        private string BuildParameters()
        {
            string parameters = "user=" + ConfigurationManager.AppSettings["SMSUser"].ToString();
            parameters = parameters + "&pass=" + ConfigurationManager.AppSettings["SMSPass"].ToString();
            parameters = parameters + "&customerid=" + ConfigurationManager.AppSettings["SMSCustomerid"].ToString();
            parameters = parameters + "&listid=" + ConfigurationManager.AppSettings["SMSListid"].ToString();
            parameters = parameters + "&deliveryid=" + ConfigurationManager.AppSettings["SMSDeliveryid"].ToString();
            parameters = parameters + "&originalid=" + mobilePNumber;
            parameters = parameters + "&mobilephone=" + mobilePNumber;
            parameters = parameters + "&prop_10001=" + messageInternal;
            return parameters;
        }

        /// <summary>
        /// HttpPost
        /// </summary>
        /// <param name="url"></param>
        /// <param name="parameters"></param>
        private static void HttpPost(string url, string parameters)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.Default))
            {
                writer.Write(parameters);
            }
            WebResponse response = request.GetResponse();
        }
    }
}