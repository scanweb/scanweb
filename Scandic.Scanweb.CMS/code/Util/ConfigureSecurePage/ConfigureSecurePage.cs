//  Description					:   ConfigureSecurePage                                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Web;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using Scandic.Scanweb.BookingEngine.Security;
using Scandic.Scanweb.BookingEngine.Security.Configuration;

namespace Scandic.Scanweb.CMS.code.Util.ConfigureSecurePage
{
    /// <summary>
    /// This class contains members of secure configuration.
    /// </summary>
    public class ConfigureSecurePage
    {
        private const string SECURE_PROTOCOL_STRING = "https://";
        private const string INSECURE_PROTOCOL_STRING = "http://";
        private const string SECURE_SETTINGS_VAR = "SecureWebPageSettings";

        /// <summary>
        /// This will redirect the page using secured channel based on the settings.
        /// </summary>
        /// <param name="pageData">Reference of the Page</param>
        /// <returns>True or false based on the settings</returns>
        public static void MakeWebPageSecured(PageData pageData)
        {
            string Result = string.Empty;
            if (pageData != null)
            {
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["SecuredPageID"])).ID;
                if (pageData.PageTypeID == hotelPageTypeID)
                {
                    if (HttpContext.Current.Request.Url.Scheme != "https")
                    {
                        Result = string.Concat(SECURE_PROTOCOL_STRING, HttpContext.Current.Request.Url.Authority,
                                               HttpContext.Current.Response.ApplyAppPathModifier(
                                                   HttpContext.Current.Request.RawUrl));
                        HttpContext.Current.Response.Redirect(Result);
                    }
                }
                else
                {
                    SecurityType Secure = SecurityType.Insecure;
                    if (HttpContext.Current.Application[SECURE_SETTINGS_VAR] != null)
                    {
                        SecureWebPageSettings Settings =
                            (SecureWebPageSettings) HttpContext.Current.Application[SECURE_SETTINGS_VAR];
                        Secure = RequestEvaluator.Evaluate(HttpContext.Current.Request, Settings, false);
                    }
                    if (Secure == SecurityType.Insecure)
                    {
                        if (pageData["Secured"] != null)
                        {
                            if ((bool) pageData["Secured"])
                            {
                                if (HttpContext.Current.Request.Url.Scheme != "https")
                                {
                                    Result = string.Concat(SECURE_PROTOCOL_STRING,
                                                           HttpContext.Current.Request.Url.Authority,
                                                           HttpContext.Current.Response.ApplyAppPathModifier(
                                                               HttpContext.Current.Request.RawUrl));
                                    HttpContext.Current.Response.Redirect(Result);
                                }
                            }
                        }
                        else
                        {
                            if (HttpContext.Current.Request.Url.Scheme.Equals("https"))
                            {
                                Result = string.Concat(INSECURE_PROTOCOL_STRING,
                                                       HttpContext.Current.Request.Url.Authority,
                                                       HttpContext.Current.Response.ApplyAppPathModifier(
                                                           HttpContext.Current.Request.RawUrl));
                                HttpContext.Current.Response.Redirect(Result);
                            }
                        }
                    }
                    else
                    {
                        if (Secure == SecurityType.Secure)
                        {
                            if (HttpContext.Current.Request.Url.Scheme != "https")
                            {
                                Result = string.Concat(SECURE_PROTOCOL_STRING, HttpContext.Current.Request.Url.Authority,
                                                       HttpContext.Current.Response.ApplyAppPathModifier(
                                                           HttpContext.Current.Request.RawUrl));
                                HttpContext.Current.Response.Redirect(Result);
                            }
                        }
                    }
                }
            }
        }
    }
}