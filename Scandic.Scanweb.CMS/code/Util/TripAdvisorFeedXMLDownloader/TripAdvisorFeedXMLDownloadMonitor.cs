﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.PlugIn;
using System.Configuration;
using Scandic.Scanweb.Core;
using System.Net;
using System.Text;

namespace Scandic.Scanweb.CMS.code.Util.TripAdvisorFeedXMLDownloader
{
    [ScheduledPlugIn(DisplayName = "Download TripAdvisor Feed XML")]
    public static class TripAdvisorFeedXMLDownloadMonitor
    {
        private static string TAFeedUrl = Convert.ToString(ConfigurationManager.AppSettings["TripAdvisorRatingFeedsWebUrl"]);

        public static string Execute()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Download Trip Advisor Feed file started at " + DateTime.Now);
            TripAdvisorFeedXMLDownloadManager.DownloadTripAdvisorFeedXML(TAFeedUrl);
            sb.Append("Download Trip Advisor Feed file completed at " + DateTime.Now);
            return sb.ToString();
        }
    }
}
