﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Scandic.Scanweb.Core;
using System.Configuration;


namespace Scandic.Scanweb.CMS.code.Util.TripAdvisorFeedXMLDownloader
{
    public static class TripAdvisorFeedXMLDownloadManager
    {
        private static string TAFeedLocalURLPath = Convert.ToString(ConfigurationManager.AppSettings["TripAdvisorRatingFeedsUrl"]);

        public static void DownloadTripAdvisorFeedXML(string TAFeedUrl)
        {
            WebRequest request;
            WebResponse resp = null;
            StreamReader sr = null;
            StreamWriter sw = null;
            try
            {
                request = WebRequest.Create(TAFeedUrl);
                if (request != null)
                {
                    request.ContentType = "text/xml";
                    resp = request.GetResponse();
                    if (resp != null)
                    {
                        sr = new StreamReader(resp.GetResponseStream());
                        sw = new StreamWriter(TAFeedLocalURLPath);
                        string s = sr.ReadToEnd();
                        sw.Write(s);
                    }
                    AppLogger.LogInfoMessage("Trip Advisor Feed File Downloaded Successfully at location: " + TAFeedLocalURLPath + ". Download Time: " + DateTime.Now);
                }
            }
            catch (WebException notFoundEx)
            {
                AppLogger.LogCustomException(notFoundEx, notFoundEx.GetType().ToString(), "Trip Advisor Feed File not found at " + TAFeedUrl + ". refer the inner exception " + notFoundEx.InnerException);
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, ex.GetType().ToString(), "Trip Advisor Feed File Download Failed from URL: " + TAFeedUrl + ". refer the inner exception " + ex.InnerException);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                    sw = null;
                }
                if (sr != null)
                {
                    sr.Close();
                    sr = null;
                }
                if (resp != null)
                {
                    resp.Close();
                    resp = null;
                }
            }
        }
    }
}
