using EPiServer;
using EPiServer.Core;
using Scandic.Scanweb.CMS.SpecializedProperties;

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// Class used to determine if the booking and/or login controls should be visible or not
    /// </summary>
    public class OWSVisibilityControl
    {
        /// <summary>
        /// RootPage
        /// </summary>
        private static PageData RootPage
        {
            get { return DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess); }
        }

        /// <summary>
        /// Returns true if booking modules should be visible
        /// </summary>
        public static bool BookingModuleShouldBeVisible
        {
            get
            {
                if (RootPage["BookingControlVisibility"] != null)
                {
                    if (((int) RootPage["BookingControlVisibility"] == (int) SelectVisibility.Visibility.Visible))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return OWSAvailability.GetInstance().IsBookingAvailable;
                }
            }
        }

        /// <summary>
        /// Return true if login control should be visible
        /// </summary>
        public static bool LoginShouldBeVisible
        {
            get
            {
                if (RootPage["BookingControlVisibility"] != null)
                {
                    if (((int) RootPage["BookingControlVisibility"] == (int) SelectVisibility.Visibility.Visible))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return OWSAvailability.GetInstance().IsLoginAvailable;
                }
            }
        }
    }
}