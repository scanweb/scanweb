////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  OWSAvailability                                        //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// This class <see cref="OWSAvailability"/> contains members to check OWSAvailability.  
    /// </summary>
    public class OWSAvailability
    {
        private static bool isBookingAvailable = true;
        private static bool isLoginAvailable = true;

        /// <summary>
        /// Constructor
        /// </summary>
        private OWSAvailability()
        {
            OWSMonitor.UpdateBookingServicesStatusEvent +=
                new OWSMonitor.UpdateServicesStatus(OWSMonitor_UpdateBookingServicesStatusEvent);
            OWSMonitor.UpdateLoginServicesStatusEvent +=
                new OWSMonitor.UpdateServicesStatus(OWSMonitor_UpdateLoginServicesStatusEvent);
        }

        /// <summary>
        /// Gets instance of OWSAvailability
        /// </summary>
        /// <returns></returns>
        public static OWSAvailability GetInstance()
        {
            if (owsAvailablity == null)
                owsAvailablity = new OWSAvailability();
            return owsAvailablity;
        }

        /// <summary>
        /// Gets IsBookingAvailable
        /// </summary>
        public bool IsBookingAvailable
        {
            get { return isBookingAvailable; }
        }

        /// <summary>
        /// Gets IsLoginAvailable
        /// </summary>
        public bool IsLoginAvailable
        {
            get { return isLoginAvailable; }
        }

        /// <summary>
        /// OWSMonitor_UpdateBookingServicesStatusEvent
        /// </summary>
        /// <param name="currentStatus"></param>
        private void OWSMonitor_UpdateBookingServicesStatusEvent(bool currentStatus)
        {
            isBookingAvailable = currentStatus;
        }

        /// <summary>
        /// OWSMonitor_UpdateLoginServicesStatusEvent
        /// </summary>
        /// <param name="currentStatus"></param>
        private void OWSMonitor_UpdateLoginServicesStatusEvent(bool currentStatus)
        {
            isLoginAvailable = currentStatus;
        }

        private static OWSAvailability owsAvailablity = new OWSAvailability();
    }
}