//  Description					:   OWSMonitor                                            //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Text;
using EPiServer.PlugIn;
using Scandic.Scanweb.BookingEngine.Domain;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// This class contains all members of OWSMonitor
    /// </summary>
    [ScheduledPlugIn(DisplayName = "OWS Monitor")]
    public static class OWSMonitor
    {
        private static bool IsBookingAvailable = true;
        private static bool IsLoginAvailable = true;

        public static event UpdateServicesStatus UpdateBookingServicesStatusEvent;
        public static event UpdateServicesStatus UpdateLoginServicesStatusEvent;

        public delegate void UpdateServicesStatus(bool currentStatus);

        /// <summary>
        /// Execute
        /// </summary>
        /// <returns></returns>
        public static string Execute()
        {
            string availabilityServiceURL = ConfigurationManager.AppSettings["OWS.Availability.Service"];
            string reservationServiceURL = ConfigurationManager.AppSettings["OWS.Reservation.Service"];
            string membershipServiceURL = ConfigurationManager.AppSettings["OWS.Membership.Service"];
            string securityServiceURL = ConfigurationManager.AppSettings["OWS.Security.Service"];
            string nameServiceURL = ConfigurationManager.AppSettings["OWS.Name.Service"];
            string infoServiceURL = ConfigurationManager.AppSettings["OWS.Information.Service"];
            bool availabilityServiceAvailable = IsAvailabilityServiceAvailable();
            bool reservationServiceAvailable = IsReservationServiceAvailable();
            bool membershipServiceAvailable = IsMembershipServiceAvailable();
            bool securityServiceAvailable = IsSecurityServiceAvailable();
            bool nameServiceAvailable = IsNameServiceAvailable();
            bool infoServiceAvailable = IsInfoServiceAvailable();

            StringBuilder sb = new StringBuilder();

            if (
                !IsBookingAvailable.Equals((availabilityServiceAvailable && reservationServiceAvailable &&
                                            membershipServiceAvailable)))
            {
                IsBookingAvailable = availabilityServiceAvailable && reservationServiceAvailable &&
                                     membershipServiceAvailable;
                if (OWSMonitor.UpdateBookingServicesStatusEvent != null)
                {
                    OWSMonitor.UpdateBookingServicesStatusEvent(IsBookingAvailable);
                }
            }

            if (
                !IsLoginAvailable.Equals((securityServiceAvailable && nameServiceAvailable && membershipServiceAvailable)))
            {
                IsLoginAvailable = securityServiceAvailable && nameServiceAvailable && membershipServiceAvailable;
                if (OWSMonitor.UpdateLoginServicesStatusEvent != null)
                {
                    OWSMonitor.UpdateLoginServicesStatusEvent(IsLoginAvailable);
                }
            }

            IsBookingAvailable = availabilityServiceAvailable && reservationServiceAvailable &&
                                 membershipServiceAvailable;
            IsLoginAvailable = securityServiceAvailable && nameServiceAvailable && membershipServiceAvailable;

            sb.Append(string.Format("{0} {1}", availabilityServiceURL,
                                    (availabilityServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0} {1}", reservationServiceURL,
                                    (reservationServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0} {1}", membershipServiceURL,
                                    (membershipServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0} {1}", securityServiceURL,
                                    (securityServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0} {1}", nameServiceURL, (nameServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0} {1}", infoServiceURL, (infoServiceAvailable ? "Available" : "Not Available")));
            sb.Append(", ");
            sb.Append(string.Format("{0}: {1}", "BookingIsAvailable", IsBookingAvailable.ToString()));
            sb.Append(", ");
            sb.Append(string.Format("{0}: {1}", "LoginIsAvailable", IsLoginAvailable.ToString()));

            return sb.ToString();
        }

        /// <summary>
        ///Method call to check the health of OWS-Information service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsInfoServiceAvailable()
        {
            bool infoAvailable = false;
            try
            {
                InformationDomain informationDomain = new InformationDomain();
                infoAvailable = informationDomain.GetCreditCardTypeForOWSMonitor();
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return infoAvailable;
        }

        /// <summary>
        ///Method call to check the health of OWS-Availability Service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsAvailabilityServiceAvailable()
        {
            bool availAvailable = false;
            try
            {
                AvailabilityDomain availabilityDomain = new AvailabilityDomain();
                availAvailable = availabilityDomain.AvailabilityForOWSMonitor();
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return availAvailable;
        }

        /// <summary>
        ///Method call to check the health of OWS-Reservation Service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsReservationServiceAvailable()
        {
            bool reservationAvailable = false;
            try
            {
                ReservationDomain reservationDomain = new ReservationDomain();
                reservationAvailable = reservationDomain.FetchReservationForOWSMonitor(AppConstants.RESERVATION_NO);
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return reservationAvailable;
        }

        /// <summary>
        ///Method call to check the health of OWS-Membership Service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsMembershipServiceAvailable()
        {
            bool membershipAvailable = false;
            try
            {
                LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                membershipAvailable = loyaltyDomain.FetchPromoSubscriptionForOWSMonitor();
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return membershipAvailable;
        }

        /// <summary>
        ///Method call to check the health of OWS-Security Service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsSecurityServiceAvailable()
        {
            bool securityAvailable = false;
            try
            {
                LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                securityAvailable = loyaltyDomain.AuthenticateUserForOWSMonitor(AppConstants.MEMBERSHIP_NO,
                                                                                AppConstants.PASSWORD);
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return securityAvailable;
        }

        /// <summary>
        ///Method call to check the health of OWS-Name Service
        /// </summary>
        /// <returns>True if service is up & running else False</returns>
        private static bool IsNameServiceAvailable()
        {
            bool nameAvailable = false;
            try
            {
                LoyaltyDomain loyaltyDomain = new LoyaltyDomain();
                nameAvailable = loyaltyDomain.FetchNameForOWSMonitor(AppConstants.NAMEID);
            }
            catch (System.Net.WebException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return nameAvailable;
        }
    }
}