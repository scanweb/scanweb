//<remarks>
//====================================================================
// Name: FlashDC.cs
// 
// Purpose :This is a control which is used to add dynamic contents in CMS pages.Edior can use external URL or       // flash file to the page using this control.The class implements IDynamicContent interface method and properties.
// Construction Date: 04/01/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region System Namespace

using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using EPiServer.DynamicContent;
using EPiServer.Globalization;
using EPiServer.SpecializedProperties;
using Scandic.Scanweb.CMS.code.SpecializedProperties;

#endregion

namespace Scandic.Scanweb.CMS.Util.DynamicContent
{
    /// <summary>
    /// This is a control which is used to add dynamic contents in CMS pages.
    /// The class implements IDynamicContent interface method and properties.
    /// </summary>
    public class FlashDC : TemplatePage, IDynamicContent
    {
        #region Private Members

        private PropertyDocumentUrl flashDocumentUrl;
        private PropertyNumber flashWidth;
        private PropertyNumber flashHeight;
        private SelectFlashProperty play;
        private SelectFlashProperty loop;
        private SelectFlashProperty menu;
        private SelectFlashProperty quality;
        private SelectFlashProperty scale;
        private SelectFlashProperty align;
        private SelectFlashProperty salign;
        private SelectFlashProperty wmode;
        private SelectFlashProperty bgcolor;
        private PropertyString flashvars;

        #endregion

        #region Constants

        #endregion

        #region Constructor

        /// <summary>
        /// Setup properties for flash file.
        /// </summary>
        public FlashDC()
        {
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;
            flashDocumentUrl = new PropertyDocumentUrl();
            flashDocumentUrl.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/FlashfileURL",
                                                                                      currentLanguage);
            flashWidth =
                new PropertyNumber(ConfigurationManager.AppSettings["FlashWidth"] != null
                                       ? Convert.ToInt32(ConfigurationManager.AppSettings["FlashWidth"])
                                       : 0);
            flashWidth.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/FlashWidth",
                                                                                currentLanguage);
            flashHeight =
                new PropertyNumber(ConfigurationManager.AppSettings["FlashHeight"] != null
                                       ? Convert.ToInt32(ConfigurationManager.AppSettings["FlashHeight"])
                                       : 0);
            flashHeight.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/FlashHeight",
                                                                                 currentLanguage);
            play = new SelectFlashProperty("play");
            play.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Play", currentLanguage);
            loop = new SelectFlashProperty("loop");
            loop.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Loop", currentLanguage);
            menu = new SelectFlashProperty("menu");
            menu.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Menu", currentLanguage);
            quality = new SelectFlashProperty("quality");
            quality.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Quality",
                                                                             currentLanguage);

            scale = new SelectFlashProperty("scale");
            scale.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Scale", currentLanguage);

            align = new SelectFlashProperty("align");
            align.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Align", currentLanguage);

            salign = new SelectFlashProperty("salign");
            salign.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Salign", currentLanguage);

            wmode = new SelectFlashProperty("wmode");
            wmode.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Wmode", currentLanguage);

            bgcolor = new SelectFlashProperty("bgcolor");
            bgcolor.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Bgcolor",
                                                                             currentLanguage);

            flashvars = new PropertyString();
            flashvars.Name = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Flashvars",
                                                                               currentLanguage);
        }

        #endregion

        #region IDynamicContent Members

        /// <summary>
        /// This is use for pass rendering  to other control.
        /// </summary>
        /// <param name="hostPage">Page which hosts Dynamic contents</param>
        /// <returns>Control which is used for rendering Dynamic contents.</returns>
        public Control GetControl(PageBase hostPage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This is used for adding properties for Dynamic contents.
        /// </summary>
        public PropertyDataCollection Properties
        {
            get
            {
                PropertyDataCollection flashProperties = new PropertyDataCollection();
                flashProperties.Add(flashDocumentUrl);
                flashProperties.Add(flashWidth);
                flashProperties.Add(flashHeight);
                flashProperties.Add(play);
                flashProperties.Add(loop);
                flashProperties.Add(menu);
                flashProperties.Add(quality);
                flashProperties.Add(scale);
                flashProperties.Add(align);
                flashProperties.Add(salign);
                flashProperties.Add(wmode);
                flashProperties.Add(bgcolor);
                flashProperties.Add(flashvars);
                return flashProperties;
            }
        }

        /// <summary>
        /// Render all properties for the flash file.
        /// </summary>
        /// <param name="hostPage">Page which hosts Dynamic contents</param>
        /// <returns>string embeded with all flash properties.</returns>
        public string Render(PageBase hostPage)
        {
            string playOption = string.Empty;
            if (play.ToString().Equals("select"))
            {
                if (HttpContext.Current.Request.Browser.Type.ToString().Substring(0, 2) == "Fi")
                {
                    playOption = "True";
                }
                else
                {
                    playOption = string.Empty;
                }
            }
            else if (play.ToString().Equals("play on start"))
            {
                playOption = "True";
            }
            else if (play.ToString().Equals("do not play on start"))
            {
                playOption = "False";
            }
            string loopOption = string.Empty;
            if (loop.ToString().Equals("select"))
            {
                if (HttpContext.Current.Request.Browser.Type.ToString().Substring(0, 2) == "Fi")
                {
                    loopOption = "True";
                }
                else
                {
                    loopOption = string.Empty;
                }
            }
            else if (loop.ToString().Equals("play in loop"))
            {
                loopOption = "True";
            }
            else if (loop.ToString().Equals("do not play in loop"))
            {
                loopOption = "False";
            }
            string menuOption = string.Empty;
            if (menu.ToString().Equals("select"))
            {
                if (HttpContext.Current.Request.Browser.Type.ToString().Substring(0, 2) == "Fi")
                {
                    menuOption = "True";
                }
                else
                {
                    menuOption = string.Empty;
                }
            }
            else if (menu.ToString().Equals("menu visible"))
            {
                menuOption = "True";
            }
            else if (menu.ToString().Equals("menu invisible"))
            {
                menuOption = "False";
            }
            string qualityOption = string.Empty;
            if (quality.ToString().Equals("select"))
            {
                qualityOption = string.Empty;
            }
            else
            {
                qualityOption = quality.ToString();
            }
            string scaleOption = string.Empty;
            if (scale.ToString().Equals("select"))
            {
                scaleOption = string.Empty;
            }
            else
            {
                scaleOption = scale.ToString();
            }
            string alignOption = string.Empty;
            if (align.ToString().Equals("select"))
            {
                alignOption = string.Empty;
            }
            else
            {
                alignOption = align.ToString();
            }
            string salignOption = string.Empty;
            if (salign.ToString().Equals("select"))
            {
                salignOption = string.Empty;
            }
            else
            {
                salignOption = salign.ToString();
            }
            string wmodeOption = string.Empty;
            if (wmode.ToString().Equals("select"))
            {
                wmodeOption = string.Empty;
            }
            else
            {
                wmodeOption = wmode.ToString();
            }
            string Option = string.Empty;
            if (wmode.ToString().Equals("select"))
            {
                wmodeOption = string.Empty;
            }
            else
            {
                wmodeOption = wmode.ToString();
            }
            string bgcolorOption = string.Empty;
            if (bgcolor.ToString().Equals("select"))
            {
                bgcolorOption = string.Empty;
            }
            else
            {
                bgcolorOption = bgcolor.ToString();
            }
            return
                string.Format(
                    "<object width=\"{1}\" height=\"{2}\" align=\"{8}\"><param name=\"movie\" value=\"{0}\"><param name=\"play\" value=\"{3}\"><param name=\"loop\" value=\"{4}\"><param name=\"menu\" value=\"{5}\"><param name=\"quality\" value=\"{6}\"><param name=\"scale\" value=\"{7}\"><param name=\"salign\" value=\"{9}\"><param name=\"wmode\" value=\"{10}\"><param name=\"bgcolor\" value=\"{11}\"><param name=\"flashvars\" value=\"{12}\"><embed src=\"{0}\" width=\"{1}\" height=\"{2}\" play=\"{3}\" loop=\"{4}\" menu=\"{5}\" quality=\"{6}\" scale=\"{7}\" salign=\"{9}\" wmode=\"{10}\" bgcolor=\"{11}\" flashvars =\"{12}\"></embed></object>",
                    flashDocumentUrl.ToString(), flashWidth.ToString(), flashHeight.ToString(), playOption, loopOption,
                    menuOption, qualityOption, scaleOption, alignOption, salignOption, wmodeOption,
                    bgcolorOption.ToString(), flashvars.ToString());
        }

        /// <summary>
        /// This is use for pass rendering  to other control or not.
        /// </summary>
        public bool RendersWithControl
        {
            get { return false; }
        }

        /// <summary>
        /// State property is used to maintain state for the Flash control.
        /// </summary>
        public string State
        {
            get
            {
                return flashDocumentUrl.ToString() + "|" + flashWidth.ToString() + "|" + flashHeight.ToString() + "|" +
                       play.ToString() + "|" + loop.ToString() + "|" + menu.ToString() + "|" + quality.ToString() + "|" +
                       scale.ToString() + "|" + align.ToString() + "|"
                       + salign.ToString() + "|" + wmode.ToString() + "|" + bgcolor.ToString() + "|" +
                       flashvars.ToString();
            }
            set
            {
                string[] parts = value.Split('|');
                if (parts.Length == 13)
                {
                    flashDocumentUrl.ParseToSelf(parts[0]);
                    flashWidth.ParseToSelf(parts[1]);
                    flashHeight.ParseToSelf(parts[2]);
                    play.ParseToSelf(parts[3]);
                    loop.ParseToSelf(parts[4]);
                    menu.ParseToSelf(parts[5]);
                    quality.ParseToSelf(parts[6]);
                    scale.ParseToSelf(parts[7]);
                    align.ParseToSelf(parts[8]);
                    salign.ParseToSelf(parts[9]);
                    wmode.ParseToSelf(parts[10]);
                    bgcolor.ParseToSelf(parts[11]);
                    flashvars.ParseToSelf(parts[12]);
                }
            }
        }

        #endregion
    }
}