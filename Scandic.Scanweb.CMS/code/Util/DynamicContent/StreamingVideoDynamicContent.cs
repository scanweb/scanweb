//<remarks>
//====================================================================
// Name: PicSearchDynamicContent.cs
// 
// Purpose :This is a control which is used to add dynamic contents in CMS pages.Edior can use       
// streaming vedio's to the page using this control.The class implements IDynamicContent interface method and 
// properties.
// Construction Date: 05/05/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region System Namespace

using System;
using System.Web.UI;
using EPiServer;
using EPiServer.Core;
using EPiServer.DynamicContent;
using EPiServer.Globalization;
using Scandic.Scanweb.CMS.code.SpecializedProperties;

    #endregion

    #region EpiServer NameSpaces

#endregion

namespace Scandic.Scanweb.CMS.code.Util.DynamicContent
{
    /// <summary>
    /// This is a control which is used to add dynamic contents from streaming video into CMS pages.
    /// The class implements IDynamicContent interface method and properties.
    /// </summary>
    public class StreamingVideoDynamicContent : IDynamicContent
    {
        #region Private Members

        private StreamingVideoProperty streamVideoSearchContent;

        #endregion

        #region Constructor

        /// <summary>
        /// Setup properties for Streaming Video file.
        /// </summary>
        public StreamingVideoDynamicContent()
        {
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;
            streamVideoSearchContent = new StreamingVideoProperty();
            streamVideoSearchContent.Name =
                EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/streamVideoSearchContent",
                                                                  currentLanguage);
        }

        #endregion

        #region IDynamicContent Members

        /// <summary>
        /// This is use for pass rendering  to other control.
        /// </summary>
        /// <param name="hostPage">Page which hosts Dynamic contents</param>
        /// <returns>Control which is used for rendering Dynamic contents.</returns>
        public Control GetControl(PageBase hostPage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This is used for adding properties for Dynamic contents.
        /// </summary>
        public PropertyDataCollection Properties
        {
            get
            {
                PropertyDataCollection flashProperties = new PropertyDataCollection();
                flashProperties.Add(streamVideoSearchContent);
                return flashProperties;
            }
        }

        /// <summary>
        /// Render all properties for the pic search contents.
        /// </summary>
        /// <param name="hostPage">Page which hosts Dynamic contents</param>
        /// <returns>string embeded with pic search content properties.</returns>
        public string Render(PageBase hostPage)
        {
            return streamVideoSearchContent.ToString();
        }

        /// <summary>
        /// This is use for pass rendering  to other control or not.
        /// </summary>
        public bool RendersWithControl
        {
            get { return false; }
        }

        /// <summary>
        /// State property is used to maintain state for the PicSearch control.
        /// </summary>
        public string State
        {
            get
            {
                string returnValue = string.Empty;
                if (streamVideoSearchContent.Value == null)
                {
                    returnValue = null;
                }
                else
                {
                    returnValue =
                        Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes
                                                   (streamVideoSearchContent.Value.ToString()));
                }
                return returnValue;
            }
            set
            {
                if (value != null)
                {
                    byte[] toDecodeByte = Convert.FromBase64String(value);
                    System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                    System.Text.Decoder utf8Decode = encoder.GetDecoder();
                    int charCount = utf8Decode.GetCharCount(toDecodeByte, 0, toDecodeByte.Length);
                    char[] decodedChar = new char[charCount];
                    utf8Decode.GetChars(toDecodeByte, 0, toDecodeByte.Length, decodedChar, 0);
                    string result = new String(decodedChar);
                    streamVideoSearchContent.Value = result;
                }
            }
        }

        #endregion
    }
}