//  Description					: ScandicTemplatePage                                     //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;

using BVNetwork.FileNotFound;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.SearchModule;
using Scandic.Scanweb.BookingEngine.Controller.Session.UserProfileModule;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.CMS.Util.ImageVault;
using System.Text.RegularExpressions;
using Scandic.Scanweb.BookingEngine.Web.code.Attributes;
using Scandic.Scanweb.Core;
using BVNetwork.FileNotFound;
using Scandic.Scanweb.Entity;


namespace Scandic.Scanweb.CMS.Util
{
    /// <summary>
    /// Contains members to handle ScandicTemplatePage
    /// </summary>
    [AccessibleWhenSessionExpired(true, "")]
    public class ScandicTemplatePage : TemplatePage
    {
        /// <summary>
        /// Gets query access level.
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Security.AccessLevel QueryAccess()
        {
            EPiServer.Security.AccessLevel accessLevel = EPiServer.Security.AccessLevel.Undefined;
            if (CurrentPage != null)
            {
                TimeSpan span = DateTime.Now.Subtract(CurrentPage.StopPublish);
                if (span.Days > 0)
                {
                    accessLevel = EPiServer.Security.AccessLevel.Read;
                }
                else
                {
                    accessLevel = CurrentPage.QueryAccess();
                }
            }
            return accessLevel;
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            CheckIfApplicationRestoredByBrowser();
            PerformSessionValidityOperation();
        }

        private void CheckIfApplicationRestoredByBrowser()
        {
            var attSessionExpired = Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionExpired)) as AccessibleWhenSessionExpired;

            if (!attSessionExpired.Accessible)
            {
                var pageStartTimeCookie = Request.Cookies[AppConstants.PageStartTimeCookieId];

                if (pageStartTimeCookie != null)
                {
                    DateTime pageStartTime;
                    if (DateTime.TryParse(pageStartTimeCookie.Value, out pageStartTime))
                    {
                        var pageElapsedTime = DateTime.Now.Subtract(pageStartTime);

                        if (pageElapsedTime.Minutes > AppConstants.PageRestoreTimeWindow && Session.IsNewSession)
                        {
                            Response.Redirect("/");
                        }
                    }

                }
            }

        }

        /// <summary>
        /// 1. checks whether request is made for the first time. And creates a cookie Host which contains
        /// the Machine name of the server where the request goes to. 
        /// 2. checks session validity
        /// </summary>
        private void PerformSessionValidityOperation()
        {
            var attSessionInvalid = Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionInValid)) as AccessibleWhenSessionInValid;
            var hostCookieName = Request.Cookies.Get("host") == null ? "not set" : Request.Cookies.Get("host").Value;
            bool isFirstRequest = string.Equals(hostCookieName, "not set") ? true : false;

            if (isFirstRequest)
            {
                HttpCookie transitCookie = new HttpCookie("host", System.Environment.MachineName);
                transitCookie.HttpOnly = true;
                HttpContext.Current.Response.Cookies.Add(transitCookie);
                hostCookieName = transitCookie.Value;
            }

            if (attSessionInvalid != null && !attSessionInvalid.Accessible)
            {
                CheckSessionValidity(hostCookieName);
            }

        }
        /// <summary>
        /// Checks whether session is valid or not and also logs the Cookie "host" value and machine name
        /// if session lost due to sticky session both the cookie host value and current host name would be different
        /// else it is the case of accessing pages directly where session is required
        /// </summary>
        /// <param name="hostNameInCookie"></param>
        private void CheckSessionValidity(string hostNameInCookie)
        {
            string cookieHostValue = hostNameInCookie;
            if (SearchCriteriaSessionWrapper.SearchCriteria == null)
            {
                string sessionId = string.Empty;

                //This is not the actual ASP.NET sessionId but the identifier that is created at start of booking process to track
                //the user inputs during the booking flow.
                if (UNTSessionWrapper.UserActionStore != null && !string.IsNullOrEmpty(UNTSessionWrapper.UserActionStore.SessionIdentifier))
                {
                    sessionId = UNTSessionWrapper.UserActionStore.SessionIdentifier;
                }

                string logMessage = String.Format(
                    "SCANWEB DESKTOP | Session Data Not Valid | Current cookie Host Value: {0} | Current Host Name: {1} | Current request URL: {2} | Url Referrer: {3} | UserAgent: {4} | IP Address: {5} | SessionId: {6}",
                        cookieHostValue, System.Environment.MachineName, Request.Url, Request.UrlReferrer, Request.UserAgent, Utility.GetUserIPAddress(), sessionId);

                AppLogger.LogSessionNotValidScenarios(logMessage);
                string strSessInv = string.Empty;
                if (!String.Equals(cookieHostValue, System.Environment.MachineName, StringComparison.InvariantCultureIgnoreCase))
                {
                    HttpCookie transitCookie = new HttpCookie("host", System.Environment.MachineName);
                    transitCookie.HttpOnly = true;
                    HttpContext.Current.Response.Cookies.Add(transitCookie);
                    strSessInv = "SessionInvalid";
                    ShowSessionExpiryErrorWithQueryString(strSessInv);
                }               

            }

        }

        /// <summary>
        /// This method ensure that if session pages are accesed out order from expected booking flow then it will redirect to the home page.
        /// </summary>
        private void CheckSessionDataValidity()
        {
            var attSessionInvalid = Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionInValid)) as AccessibleWhenSessionInValid;

            if (attSessionInvalid != null && !attSessionInvalid.Accessible)
            {
                if (SearchCriteriaSessionWrapper.SearchCriteria == null)
                {
                    Response.Redirect("/");
                }
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //
        }
        
        /// <summary>
        /// Override the OnLoad method to redirect to correct language versions 
        /// </summary>
        /// <param name="e"></param>
        /// <remarks>Modified during Release 1.8.2 | Hygiene release | Offer page redirect</remarks>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            HttpCookie pageStartTimeCookie = new HttpCookie(AppConstants.PageStartTimeCookieId, DateTime.Now.ToString());
            pageStartTimeCookie.HttpOnly = true;
            Response.Cookies.Add(pageStartTimeCookie);

            if (!IsPostBack) UserNavTracker.TrackNavigation();
            PageData currentPage = CurrentPage;
            string correctLanguageString = string.Empty;
            string urlHost = Request.Url.Host.ToString().ToLower();
            if (urlHost.Contains(SiteHostConstants.SITEHOST_COM))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_ENGLISH;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_SWEDISH))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_SWEDISH;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_DANISH))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_DANISH;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_NORWEGIAN))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_NORWEGIAN;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_FINNISH))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_FINNISH;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_GERMANY))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGUAGE_GERMANY;
            }
            else if (urlHost.Contains(SiteHostConstants.SITEHOST_RUSSIAN))
            {
                correctLanguageString = CurrentPageLanguageConstant.LANGAUGE_RUSSIAN;
            }

            if (LanguageSelection.HostLanguageMappings.ContainsKey(Request.Url.Host))
            {
                if (!string.IsNullOrEmpty(correctLanguageString))
                {
                    LanguageSelector selectedLanguage = new LanguageSelector(correctLanguageString);
                    currentPage = DataFactory.Instance.GetPage(CurrentPage.PageLink, selectedLanguage);
                    if (currentPage == null)
                    {
                        string masterLanguageBranch = CurrentPage.MasterLanguageBranch;
                        LanguageSelector masterLanguage = new LanguageSelector(masterLanguageBranch);
                        currentPage = DataFactory.Instance.GetPage(CurrentPage.PageLink, masterLanguage);
                    }
                }
            }
            if (currentPage == null)
                currentPage = CurrentPage;

            if (UserLoggedInSessionWrapper.UserLoggedIn)
            {
                try
                {
                    bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;
                    if (isLoggedIn)
                    {
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    }
                }
                catch (Exception)
                {
                }
            }
            if (Request.QueryString["SessionExpired"] != null && Request.QueryString["SessionExpired"].ToLower() == "true")
            {
                string errorHeader = LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/Errorpage/sessionexpiredheader");
                string errorMessage = LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/Errorpage/sessionexpiredtext");
                ShowSessionTimeoutError(errorHeader, errorMessage);
            }
            else if(WebUtil.IsSessionValid(null, false))
            {
                CheckSessionDataValidity();
            }
                
            
            if (Response.StatusCode != 301)
            {
                string browserUrl = Page.Request.RawUrl.ToUpper();

                if (!IsEditOrPreviewMode && !browserUrl.Contains("GMAPID=") && !browserUrl.Contains("CMPID=")
                    && !Page.Request.Path.Equals("/default.aspx"))
                {
                    string pagetypes301Redirect = System.Configuration.ConfigurationManager.AppSettings["301Redirect"];
                    string pageTypeToCheck = string.Format("*-{0}-*", currentPage.PageTypeName);

                    if (!string.IsNullOrEmpty(pagetypes301Redirect) && pagetypes301Redirect.IndexOf(pageTypeToCheck) != -1)
                    {
                        string shortUrl = currentPage["PageExternalURL"] as string ?? string.Empty;
                        shortUrl = "/" + shortUrl.ToUpper();
                        string hostLanguage = LanguageSelection.GetLanguageFromHost();
                        string linkUrl = UriSupport.AddLanguageSelection(Page.Request.Url.ToString(), hostLanguage);
                        UrlBuilder appUrl = new UrlBuilder(linkUrl);
                        EPiServer.Global.UrlRewriteProvider.ConvertToExternal(appUrl, CurrentPage.PageLink, System.Text.UTF8Encoding.UTF8);
                        //if (!(appUrl.ToString().ToUpper().Contains(browserUrl)) || shortUrl == browserUrl)

                        if (!(HttpUtility.UrlDecode(appUrl.ToString()).ToUpper().Contains(HttpUtility.UrlDecode(browserUrl))) || shortUrl == browserUrl)
                        {
                            Response.Status = "301 Moved Permanently";
                            Response.AddHeader("Location", appUrl.ToString());
                            Response.End();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the domain ending
        /// </summary>
        /// <returns>Domain as string</returns>
        protected string GetDomain()
        {
            string domain = Request.Url.Host;
            int iPos = domain.LastIndexOf('.') + 1;
            return domain.Substring(iPos, domain.Length - iPos);
        }

        /// <summary>
        /// Returns the PageData object of the root page
        /// </summary>
        public static PageData RootPage
        {
            get { return DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess); }
        }

        /// <summary>
        /// Gets the PropertyData object for a specific ImageVault image property
        /// </summary>
        /// <returns>A PropertyData object with the image</returns>
        protected PropertyData GetImageFromImageVault(string imageVaultPropertyName)
        {
            PropertyData pd = PropertyData.CreatePropertyDataObject("ImageStoreNET", "ImageStoreNET.ImageType");
            string strImage = CurrentPage[imageVaultPropertyName] as string;

            if (!String.IsNullOrEmpty(strImage))
            {
                pd.Value = strImage;
            }

            return pd;
        }

        /// <summary>
        /// Gets the alt text from an ImageVault property in the correct langauge
        /// </summary>
        protected string GetAltText(string imageVaultPropertyName)
        {
            LangAltText altText = new LangAltText();

            return altText.GetAltText(CurrentPage.LanguageID, CurrentPage[imageVaultPropertyName].ToString());
        }

        /// <summary>
        /// Sets the Session varables with error header and error message
        /// and redirects to the global error page
        /// </summary>
        /// <param name="errHeader">String with error header</param>
        /// <param name="errMessage">String with error message</param>
        protected void ShowApplicationError(string errHeader, string errMessage)
        {
            GenericErrorPageSessionWrapper.GenericErrorHeader = errHeader;
            GenericErrorPageSessionWrapper.GenericErrorMessage = errMessage;
            PageData page = GetPage(PageReference.RootPage);
            string url = GetPage((PageReference)page["ErrorPage"]).LinkURL;
            Response.Redirect(url);
        }

        /// <summary>
        /// Sets the Session varables with error header and error message and query string
        /// and redirects to the global error page
        /// </summary>
        /// <param name="errHeader">String with error header</param>
        /// <param name="errMessage">String with error message</param>
        protected void ShowApplicationErrorWithQueryString(string errHeader, string errMessage, string queryString)
        {
            GenericErrorPageSessionWrapper.GenericErrorHeader = errHeader;
            GenericErrorPageSessionWrapper.GenericErrorMessage = errMessage;
            PageData page = GetPage(PageReference.RootPage);
            string url = string.Format("{0}&{1}=true", (GetPage((PageReference)page["ErrorPage"]).LinkURL), queryString);
            Response.Redirect(url);
        }

        protected void ShowSessionExpiryErrorWithQueryString(string queryString)
        {
            PageData page = GetPage(PageReference.RootPage);
            string url = GetPage((PageReference)page["SessionTimeoutPage"]).LinkURL;
            if (!string.IsNullOrEmpty(queryString))
                url = string.Format("{0}&{1}=true", url, queryString);
            Response.Redirect(url);
        }

        /// <summary>
        /// Sets the Session varables with error header and error message
        /// and redirects to the global error page
        /// </summary>
        /// <param name="errHeader">String with error header</param>
        /// <param name="errMessage">String with error message</param>
        protected void ShowSessionTimeoutError(string errHeader, string errMessage)
        {
            var attSessionExpired = Attribute.GetCustomAttribute(this.GetType(), typeof(AccessibleWhenSessionExpired)) as AccessibleWhenSessionExpired;

            if (!attSessionExpired.Accessible)
            {
                var referenceKey = attSessionExpired.ExpiredPageKey;
                GenericErrorPageSessionWrapper.GenericErrorHeader = errHeader;
                GenericErrorPageSessionWrapper.GenericErrorMessage = errMessage;
                PageData page = GetPage(PageReference.RootPage);
                string url = GetPage((PageReference)page["SessionTimeoutPage"]).LinkURL;
                if (!string.IsNullOrEmpty(referenceKey))
                {
                    if (url.IndexOf("?") > 0)
                    {
                        url = string.Format("{0}&referenceKey={1}", url, referenceKey);
                    }
                    else
                    {
                        url = string.Format("{0}?referenceKey={1}", url, referenceKey);
                    }
                }
                Response.Redirect(url);
            }

        }


        /// <summary>
        /// Returns true if the user is logged in
        /// </summary>
        /// <returns>Bool with login status</returns>
        protected bool IsLoggedIn()
        {
            bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;
            return isLoggedIn;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEditOrPreviewMode
        {
            get
            {
                if (Request.QueryString["id"] != null)
                {
                    PageReference pageVersionReference = PageReference.Parse(Request.QueryString["id"]);

                    if ((pageVersionReference != null && pageVersionReference.WorkID > 0) || (HttpContext.Current.Request.UrlReferrer != null &&
                        Regex.IsMatch(HttpContext.Current.Request.UrlReferrer.LocalPath, "/admin/|/edit/", RegexOptions.IgnoreCase)))
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageLink"></param>
        /// <returns></returns>
        public override PageData GetPage(PageReference pageLink)
        {
            PageData currentPage = base.GetPage(pageLink);
            if (Page.Request.Url.AbsoluteUri.Contains(currentPage.LinkURL) || IsPageIdExistsInAbsoluteUri(Page.Request, currentPage))
            {
                if (currentPage["PermanentRedirectURL"] != null && !IsEditOrPreviewMode)
                {
                    PermanentRedirect(pageLink);
                }
                else
                {
                    if (WebUtil.CheckUnPublishedPage(currentPage, this))
                    {
                        return null;
                    }
                }
            }
            return currentPage;
        }

        private bool IsPageIdExistsInAbsoluteUri(HttpRequest currentRequest, PageData currentPage)
        {
            bool result = false;
            if (currentRequest.Url.AbsoluteUri.Contains("?") && currentPage.LinkURL.Contains("?"))
            {
                result = string.Equals(Page.Request.QueryString["id"], Convert.ToString(currentPage.PageLink.ID));
            }
            return result;
        }
        private void PermanentRedirect(PageReference pageLink)
        {
            PageData currentPage = base.GetPage(pageLink);
            string redirectedURL = currentPage["PermanentRedirectURL"] as string ?? string.Empty;
            bool isRedirectURLConfigured = !string.IsNullOrEmpty(redirectedURL) ? true : false;
            if (isRedirectURLConfigured)
            {
                if (redirectedURL.IndexOf(AppConstants.SLASH) == 0)
                {
                    redirectedURL = string.Format("{0}{1}{2}", AppConstants.HTTP_PROTOCOL, Request.Url.Host, redirectedURL);
                }
                if (redirectedURL.IndexOf(AppConstants.HTTP_PROTOCOL) != 0 && redirectedURL.IndexOf(AppConstants.SECURE_PROTOCOL) != 0)
                {
                    redirectedURL = string.Format("{0}{1}", AppConstants.HTTP_PROTOCOL, redirectedURL);
                }
            }
            bool isImmediateRedirect = (currentPage["RedirectPermanentelyNow"] != null) ? true : false;
            bool ispageExpired = false;
            TimeSpan span = DateTime.Now.Subtract(currentPage.StopPublish);
            if (span.Days > 0)
            {
                ispageExpired = true;
            }

            if ((!isRedirectURLConfigured) && (ispageExpired))
            {
                System.Security.Principal.IPrincipal user = Context.User;
                if ((user != null) && (!user.Identity.IsAuthenticated))
                {
                    //base.AccessDenied();
                }
            }
            else if ((isRedirectURLConfigured && !isImmediateRedirect && !ispageExpired)
                     || (!isRedirectURLConfigured && !ispageExpired))
            {
                if (UserLoggedInSessionWrapper.UserLoggedIn)
                {
                    try
                    {
                        bool isLoggedIn = UserLoggedInSessionWrapper.UserLoggedIn;
                        if (isLoggedIn)
                        {
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                if (Request.QueryString["SessionExpired"] != null && Request.QueryString["SessionExpired"].ToLower() == "true")
                {
                    string errorHeader = LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/Errorpage/sessionexpiredheader");
                    string errorMessage = LanguageManager.Instance.Translate("/Templates/Scanweb/Pages/Errorpage/sessionexpiredtext");
                    ShowSessionTimeoutError(errorHeader, errorMessage);
                    Response.End();
                }
            }
            else
            {
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", redirectedURL);
                Response.End();
            }
        }
    }
}