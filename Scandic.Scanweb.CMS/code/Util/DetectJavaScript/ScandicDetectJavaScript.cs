using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.Util.DetectJavaScript
{
    /// <summary>
    /// ScandicDetectJavaScript
    /// </summary>
    public class ScandicDetectJavaScript : WebControl
    {
        private string hiddenID = "scanweb" + "_jsdetection";
        private string myvalue = "x";

        private bool DoesNeedValidation
        {
            get { return (DetectJavaScript.GetState() == DetectJavaScript.JavaScriptState.Undefined); }
        }

        /// <summary>
        /// RenderContents
        /// </summary>
        /// <param name="writer"></param>
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (this.DoesNeedValidation)
            {
                string c = "<INPUT type=\"hidden\" id=\"" + hiddenID + "\" name=\"" + hiddenID + "\" />";
                writer.Write(c);

                string script = "<script language=\"javascript\" type=\"text/javascript\">";
                script += "document.forms[0]." + hiddenID + ".value = \"" + myvalue + "\";";
                script += "document.forms[0].submit();";
                script += "</script>";

                writer.Write(script);
            }
            base.RenderContents(writer);
        }

        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            bool isEnabled = false;
            if (this.DoesNeedValidation)
            {
                if (HttpContext.Current.Request[hiddenID] == myvalue)
                    isEnabled = true;
                else
                    isEnabled = false;

                DetectJavaScript.SetValue(isEnabled);
            }
        }
    }
}
