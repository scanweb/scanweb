using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.CMS.Util.DetectJavascript
{
    /// <summary>
    /// The JavascriptTest control is designed to provide a quick and
    /// reliable way of determining if the client browser supports
    /// javascript and if the javascript is enabled (on) in the client
    /// browser.
    /// </summary>
    [DefaultProperty("Enabled"), ToolboxData("<{0}:JavascriptTest runat=server></{0}:JavascriptTest>")]
    public class JavascriptControl : Control, IPostBackDataHandler
    {
        /// <summary>
        /// SetValue
        /// </summary>
        /// <param name="isEnabled"></param>
        public static void SetValue(bool isEnabled)
        {
            if (isEnabled)
                SetValue(JavaScriptState.Enabled);
            else
                SetValue(JavaScriptState.Disabled);
        }

        /// <summary>
        /// SetValue
        /// </summary>
        /// <param name="state">JavaScriptState</param>
        public static void SetValue(JavaScriptState state)
        {
            MiscellaneousSessionWrapper.IsJavascriptEnabled = state;

        }

        /// <summary>
        ///        GetState
        /// </summary>
        /// <returns>JavaScriptState</returns>
        public static JavaScriptState GetState()
        {
            return (JavaScriptState)MiscellaneousSessionWrapper.IsJavascriptEnabled;
        }

        /// <summary>
        /// IsEnabled
        /// </summary>
        /// <returns>True/False</returns>
        public static bool IsEnabled()
        {
            switch (GetState())
            {
                case JavaScriptState.Enabled:
                    return true;
                default:
                    return false;
            }
        }

        public enum JavaScriptState
        {
            Enabled,
            Disabled,
            Undefined
        }

        /// <summary>
        /// Event which is raised when state changes
        /// </summary>
        public event EventHandler EnabledChanged;

        /// <summary>
        /// A recursive function that unwinds the control tree to find
        /// the first HtmlForm control's client ID value.
        /// </summary>
        /// <remarks>
        /// This method is hard-coded to return a HtmlForm. It also 
        /// assumes that it is getting a top level control, such as the 
        /// Page control, and that this control contains the HtmlForm.
        /// </remarks>
        /// <param name="parentControl">Control</param>
        /// <returns>String</returns>
        private string GetFormName(Control parentControl)
        {
            string Name = "";
            foreach (Control childControl in parentControl.Controls)
            {
                if (childControl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlForm")
                {
                    Name = childControl.ClientID;
                    break;
                }
                else
                {
                    if (childControl.HasControls())
                    {
                        Name = GetFormName(childControl);
                    }
                }
            }
            return Name;
        }

        #region Public Attributes

        /// <summary>
        /// Public attribute that returns the name of the hidden
        /// html element that is updated by javascript to test
        /// if javascript is enabled.
        /// </summary>
        /// <remarks>
        /// The ClientID is the ClientID of this control (JavascriptTest).
        /// </remarks>
        protected string HelperID
        {
            get { return "__" + ClientID + "_State"; }
        }


        /// <summary>
        /// Public attribute indicating if javascript is enabled
        /// on the clients' browsers.
        /// </summary>
        public bool Enabled
        {
            get
            {
                object obj = ViewState[ClientID + "_Enabled"];
                if (obj == null)
                {
                    return false;
                }
                else
                {
                    return (bool) obj;
                }
            }

            set
            {
                ViewState[ClientID + "_Enabled"] = value;
            }
        }

        #endregion Public Attributes

        #region Control Events

        /// <summary>
        /// Initialize settings needed during the lifetime of the 
        /// incoming web request.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Page != null)
            {
                Page.RegisterRequiresPostBack(this);
            }
        }


        /// <summary>
        /// Perform any updates before the output is rendered.
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (Page != null)
            {
                this.Page.ClientScript.RegisterHiddenField(HelperID, Enabled.ToString());
                if (!this.Page.ClientScript.IsStartupScriptRegistered("JavascriptTest_Startup") &&
                    (GetState() == JavaScriptState.Undefined))
                {
                    // Form the script to be registered at client side.
                    StringBuilder sb = new StringBuilder();
                    string sFormName = GetFormName(this.Page);

                    sb.Append("<script lang='javascript'>");
                    sb.Append("if (document." + sFormName + "." + HelperID + ".value == 'False')");
                    sb.Append("{");
                    sb.Append("document." + sFormName + "." + HelperID + ".value = 'True';");
                    sb.Append("document." + sFormName + ".submit();");
                    sb.Append("}");
                    sb.Append("</script>");
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "JavascriptTest_Startup", sb.ToString());
                }
            }
        }


        /// <summary>
        /// Process incoming form data and update properties accordingly,
        /// </summary>
        /// <param name="postDataKey">String</param>
        /// <param name="postCollection">NameValueCollection</param>
        /// <returns></returns>
        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            string value = postCollection[HelperID];
            if (value != null)
            {
                bool newValue =
                    (String.Compare(value, "true", true, System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) ==
                     0);
                bool oldValue = Enabled;

                Enabled = newValue;
                return (newValue != oldValue);
            }
            return false;
        }


        /// <summary>
        /// Raise change events in response to state changes between
        /// the current and previous postbacks.
        /// </summary>
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            if (EnabledChanged != null)
            {
                EnabledChanged(this, EventArgs.Empty);
            }
        }

        #endregion "Control Events"
    }
}