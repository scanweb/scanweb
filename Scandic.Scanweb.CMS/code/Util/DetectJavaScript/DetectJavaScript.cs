using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Scandic.Scanweb.CMS.Util.DetectJavaScript
{
    /// <summary>
    /// DetectJavaScript
    /// </summary>
    public class DetectJavaScript
    {
        /// <summary>
        /// SetValue
        /// </summary>
        /// <param name="isEnabled"></param>
        public static void SetValue(bool isEnabled)
        {
            if (isEnabled)
                SetValue(JavaScriptState.Enabled);
            else
                SetValue(JavaScriptState.Disabled);
        }

        /// <summary>
        /// SetValue
        /// </summary>
        /// <param name="state"></param>
        public static void SetValue(JavaScriptState state)
        {
            HttpContext.Current.Session[SessionValue] = state;
        }

        /// <summary>
        /// GetState
        /// </summary>
        /// <returns>JavaScriptState</returns>
        public static JavaScriptState GetState()
        {
            return (JavaScriptState)HttpContext.Current.Session[SessionValue];
        }

        /// <summary>
        /// IsEnabled
        /// </summary>
        /// <returns>True/False</returns>
        public static bool IsEnabled()
        {
            switch (GetState())
            {
                case JavaScriptState.Enabled:
                    return true;
                default:
                    return false;
            }
        }

        public const string SessionValue = "IsJavascriptEnabled";

        public enum JavaScriptState
        {
            Enabled,
            Disabled,
            Undefined
        }

    }
}
