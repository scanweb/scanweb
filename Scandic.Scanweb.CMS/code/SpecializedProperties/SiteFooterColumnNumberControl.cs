//  Description					: SiteFooterColumnNumber
//	This Control is used to display the links in a particular column																			  
//----------------------------------------------------------------------------------------
//  Author						: Kshipra Thombre  
//  Author email id				:                           							  
//  Creation Date				: 18th March  2010									  
// 	Version	#					: 2.0													  
//----------------------------------------------------------------------------------------
//  Revison History				: -NA-													  
// 	Last Modified Date			:														  
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// SiteFooterColumnNumberControl
    /// </summary>
    public class SiteFooterColumnNumberControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            string numberOfCoulmns = ConfigurationManager.AppSettings.Get("SiteFooter.DisplayColumn");
            inputControl.Items.Add(new ListItem("Select", string.Empty));
            if (!string.IsNullOrEmpty(numberOfCoulmns))
            {
                int coulmnCount;
                if (int.TryParse(numberOfCoulmns, out coulmnCount))
                {
                    for (int count = 2; count <= coulmnCount; count++)
                    {
                        string value = count.ToString();
                        inputControl.Items.Add(new ListItem(value, value));
                    }
                }
            }
            try
            {
                inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
                
            }
        }

        /// <summary>
        /// Gets SelectRateCoulmnNumber
        /// </summary>
        public SelectRateCoulmnNumber SelectRateCoulmnNumber
        {
            get { return PropertyData as SelectRateCoulmnNumber; }
        }
    }
}