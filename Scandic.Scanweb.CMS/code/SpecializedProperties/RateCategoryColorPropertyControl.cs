//<remarks>
//====================================================================
// Name: RateCategoryColorProperty.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Rate Category CMS pages.
// Construction Date: 04/01/2009
//
// Author :Vrushali Pawale, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2010 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>


using System;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Rate category color properties
    /// </summary>
    public class RateCategoryColorPropertyControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// Sets up edit controls.
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            string colorsString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryColors");
            string defaultColorString = ConfigurationManager.AppSettings.Get("SelectRate.RateCategoryDefaultColor");
            if (!string.IsNullOrEmpty(defaultColorString))
                inputControl.Items.Add(new ListItem("Select", defaultColorString));
            else
                inputControl.Items.Add(new ListItem("Select", string.Empty));
            if (!string.IsNullOrEmpty(colorsString))
            {
                char[] splitWith = new char[] {';'};
                string[] colorsArray = colorsString.Split(splitWith);
                if (colorsArray != null && colorsArray.Length > 0)
                {
                    for (int count = 0; count < colorsArray.Length; count++)
                    {
                        string eachcolorString = colorsArray[count];
                        if (!string.IsNullOrEmpty(eachcolorString))
                            inputControl.Items.Add(new ListItem(eachcolorString, eachcolorString));
                    }
                }
            }
            try
            {
                if (this.PropertyData.Value != null)
                    inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }

        /// <summary>
        /// Gets the color of the selected rate category.
        /// </summary>
        /// <value>The color of the selected rate category.</value>
        public RateCategoryColorProperty SelectedRateCategoryColor
        {
            get { return PropertyData as RateCategoryColorProperty; }
        }
    }
}