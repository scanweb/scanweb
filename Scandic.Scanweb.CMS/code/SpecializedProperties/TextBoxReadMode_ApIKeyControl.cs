﻿//<remarks>
//====================================================================
// Name: TextBoxReadMode_ApIKeyControl.cs
// 
// Purpose :This is a read only TextBox specially designed for API key Guid Genration in PartnerPages Date: 12/09/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region using
using System;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;
using Scandic.Scanweb.Core;
#endregion using

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// TextBoxReadMode_ApIKeyControl
    /// </summary>
    public class TextBoxReadMode_ApIKeyControl : EPiServer.Web.PropertyControls.PropertyTextBoxControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();
            if (CurrentPage.Property["APIKey"].Value != null)
            {
                if (CurrentPage.Property["APIKey"].Value.ToString() != AppConstants.APIKEY_DEFAULT_TEXT)
                {
                    MiscellaneousSessionWrapper.InitialAPIKey = CurrentPage.Property["APIKey"].Value.ToString();

                }
            }

            TextBox inputcontrol = this.EditControl;
            inputcontrol.ReadOnly = true;
            try
            {
                if (this.PropertyData.Value != null)
                {
                    inputcontrol.Text = this.PropertyData.Value.ToString();
                }
            }
            catch (NullReferenceException)
            {
            }
        }

        /// <summary>
        /// TextBoxReadMode_ApIKeyProperty
        /// </summary>
        public TextBoxReadMode_ApIKeyProperty SelectedTrackingCode
        {
            get { return PropertyData as TextBoxReadMode_ApIKeyProperty; }
        }
    }
}