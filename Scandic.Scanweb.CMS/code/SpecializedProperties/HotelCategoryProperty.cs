//<remarks>
//====================================================================
// Name: HotelCategoryProperty.cs
// 
// Purpose :This is a user control which is used to add dynamic contents in a dropdown list in CMS pages.This control// is inherited from PropertyLongString. 
// Construction Date: 04/01/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region Using

using System;
using EPiServer.Core;
using EPiServer.PlugIn;

#endregion

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData which implements PropertyLongString class.
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class HotelCategoryProperty : PropertyLongString
    {
        #region Private Members

        private string controlName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor which accepts control name as parameter.
        /// </summary>
        /// <param name="controlName"></param>
        public HotelCategoryProperty(string controlName)
        {
            this.controlName = controlName;
        }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public HotelCategoryProperty()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// This is a overriden method from PropertyLongString class.Which creates HotelCategoryPropertyControl                /// instance.
        /// </summary>
        /// <returns>IPropertyControl</returns>
        public override IPropertyControl CreatePropertyControl()
        {
            return new HotelCategoryPropertyControl();
        }

        #endregion
    }
}