using System;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// PropertyControl implementation used for rendering TopAreaWidth data.
    /// </summary>
    public class WidthWithBoxControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            inputControl.Items.Add(new ListItem("Left, center and box 492x265", ((int)WidthWithBox.Width.LeftCenterWithBox).ToString()));
            inputControl.Items.Add(new ListItem("Left, center and no box 718x265",
                                                ((int)WidthWithBox.Width.LeftCenterWithNoBox).ToString()));

            try
            {
                inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }


        /// <summary>
        /// Gets the TopAreaWidth instance for this IPropertyControl.
        /// </summary>
        /// <value>The property that is to be displayed or edited.</value>
        public WidthWithBox SelectVisibility
        {
            get { return PropertyData as WidthWithBox; }
        }
    }
}