//  Description					: SelectHotelMenuNavigationList                           //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using
using System;
using EPiServer.PlugIn;
#endregion using

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Contains members of SelectHotelMenuNavigationList
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectHotelMenuNavigationList : EPiServer.Core.PropertyLongString
    {
        /// <summary>
        /// This is a overriden method from PropertyLongString class.
        /// This creates SelectHotelMenuNavigationListControl instance.
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectHotelMenuNavigationListControl();
        }
    }
}