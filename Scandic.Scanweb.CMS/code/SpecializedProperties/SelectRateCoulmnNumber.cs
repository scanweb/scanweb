//  Description					:   SelectRateCoulmnNumber                                //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Custom property implementation.
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectRateCoulmnNumber : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Creates property control.
        /// </summary>
        /// <returns>property control.</returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectRateCoulmnNumberControl();
        }

        /// <summary>
        /// Holds visibility types.
        /// </summary>
        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }
    }
}