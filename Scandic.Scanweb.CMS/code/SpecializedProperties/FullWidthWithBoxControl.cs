﻿/// <summary>
/// PropertyControl implementation used for rendering TopAreaWidth data.
/// </summary>
using System.Web.UI.WebControls;
using System;
using Scandic.Scanweb.CMS.SpecializedProperties;
public class FullWidthWithBoxControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
{
    /// <summary>
    /// SetupEditControls
    /// </summary>
    protected override void SetupEditControls()
    {
        base.SetupEditControls();

        DropDownList inputControl = this.EditControl;
        inputControl.Items.Add(new ListItem("Left, center and no box 718x265",
                                            ((int)FullWidthWithBox.Width.LeftCenterWithNoBox).ToString()));
        inputControl.Items.Add(new ListItem("Full page (964x265)", ((int)FullWidthWithBox.Width.Full).ToString()));

        try
        {
            inputControl.SelectedValue = this.PropertyData.Value.ToString();
        }
        catch (NullReferenceException)
        {
        }
    }

    /// <summary>
    /// Gets the TopAreaWidth instance for this IPropertyControl.
    /// </summary>
    /// <value>The property that is to be displayed or edited.</value>
    public FullWidthWithBox SelectVisibility
    {
        get { return PropertyData as FullWidthWithBox; }
    }
}