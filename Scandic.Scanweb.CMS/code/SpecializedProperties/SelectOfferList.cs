//  Description					:   SelectOfferList                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
// Revison History				:   													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectOfferList : EPiServer.Core.PropertyLongString
    {
        /// <summary>
        /// Constructor of SelectOfferList class.
        /// </summary>
        public SelectOfferList()
        {
        }

        /// <summary>
        /// CreatePropertyControl
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectOfferListControl();
        }
    }
}