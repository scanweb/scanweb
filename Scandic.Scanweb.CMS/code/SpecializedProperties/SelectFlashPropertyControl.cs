//<remarks>
//====================================================================
// Name: SelectFlashPropertyControl.cs
// 
// Purpose :This is a user control which is used to add dynamic contents in a dropdown list in CMS pages.This control// is inherited from PropertyDataControl. 
// Construction Date: 04/01/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region Using

using System.Web.UI.WebControls;
using EPiServer.Globalization;
using EPiServer.Web.PropertyControls;

#endregion

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// This control is used to display Dynamic contents in a dropdown List.This control is inherited from               /// PropertyDataControl.
    /// </summary>
    public class SelectFlashPropertyControl : PropertyDataControl
    {
        #region Member Declarations

        private string controlName = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor set control Name for the control.
        /// </summary>
        /// <param name="controlName"></param>
        public SelectFlashPropertyControl(string controlName)
        {
            this.controlName = controlName;
        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public SelectFlashPropertyControl()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Property to hold Dropdown List.
        /// </summary>
        protected DropDownList EditControl { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// This method will be called when the edit is called.
        /// </summary>
        protected override void SetupEditControls()
        {
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;
            ListItem item = null;
            switch (controlName)
            {
                case "quality":
                    {
                        string propertyValueforquality = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "low";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/low",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "autolow";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/autolow",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "autohigh";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/autohigh",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "medium";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/medium",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "high";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/high",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "best";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/best",
                                                                                      currentLanguage);
                        if (propertyValueforquality.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "scale":
                    {
                        string propertyValueforscale = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforscale.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "default";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/default",
                                                                                      currentLanguage);
                        if (propertyValueforscale.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "noorder";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/noorder",
                                                                                      currentLanguage);
                        if (propertyValueforscale.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "exactfit";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/exactfit",
                                                                                      currentLanguage);
                        if (propertyValueforscale.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "align":
                    {
                        string propertyValueforalign = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "l";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/left",
                                                                                      currentLanguage);
                        if (propertyValueforalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "t";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/top",
                                                                                      currentLanguage);
                        if (propertyValueforalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "r";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/right",
                                                                                      currentLanguage);
                        if (propertyValueforalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "b";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/bottom",
                                                                                      currentLanguage);
                        if (propertyValueforalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "salign":
                    {
                        string propertyValueforsalign = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "l";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/left",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "r";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/right",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "t";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/top",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "b";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/bottom",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "tl";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/topleft",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "tr";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/topright",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "bl";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/bottomleft",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "br";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/bottomright",
                                                                                      currentLanguage);
                        if (propertyValueforsalign.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "wmode":
                    {
                        string propertyValueforwmode = this.ToString();
                        item = new ListItem();
                        item.Value = "opaque";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/opaque",
                                                                                      currentLanguage);
                        if (propertyValueforwmode.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "window";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/window",
                                                                                      currentLanguage);
                        if (propertyValueforwmode.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);

                        item = new ListItem();
                        item.Value = "transparent";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/transparent",
                                                                                      currentLanguage);
                        if (propertyValueforwmode.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "play":
                    {
                        string propertyValueforplay = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforplay.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "play on start";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/playonstart",
                                                                                      currentLanguage);
                        if (propertyValueforplay.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "do not play on start";
                        item.Text =
                            EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/dontplayonstart",
                                                                              currentLanguage);
                        if (propertyValueforplay.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "menu":
                    {
                        string propertyValueformenu = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueformenu.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "menu visible";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/menuvisible",
                                                                                      currentLanguage);
                        if (propertyValueformenu.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "menu invisible";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate(
                            "/pagetypes/common/menuinvisible", currentLanguage);
                        if (propertyValueformenu.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "loop":
                    {
                        string propertyValueforloop = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforloop.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "play in loop";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/playinloop",
                                                                                      currentLanguage);
                        if (propertyValueforloop.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "do not play in loop";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate(
                            "/pagetypes/common/dontplayinloop", currentLanguage);
                        if (propertyValueforloop.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }
                case "bgcolor":
                    {
                        string propertyValueforbgColor = this.ToString();
                        item = new ListItem();
                        item.Value = "select";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/select",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#FF0000";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Red",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#0000FF";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Blue",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#00C000";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Green",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#000000";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Black",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#FFFFFF";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/White",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        item = new ListItem();
                        item.Value = "#FFFF00";
                        item.Text = EPiServer.Core.LanguageManager.Instance.Translate("/pagetypes/common/Yellow",
                                                                                      currentLanguage);
                        if (propertyValueforbgColor.Equals(item.Value))
                        {
                            item.Selected = true;
                        }
                        EditControl.Items.Add(item);
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// This will create the edit control.
        /// </summary>
        public override void CreateEditControls()
        {
            this.EditControl = new DropDownList();
            this.EditControl.Width = 100;
            this.EditControl.EnableViewState = true;
            this.ApplyControlAttributes(this.EditControl);
            this.Controls.Add(this.EditControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// IsValueActive
        /// </summary>
        /// <param name="value"></param>
        /// <returns>True/false</returns>
        public bool IsValueActive(string value)
        {
            return false;
        }

        /// <summary>
        /// This is a Implemetion method of PropertyDataControl class.
        /// </summary>
        public override void ApplyEditChanges()
        {
            base.SetValue(EditControl.SelectedValue);
        }

        #endregion
    }
}