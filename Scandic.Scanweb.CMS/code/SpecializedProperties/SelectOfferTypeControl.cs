using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// PropertyControl implementation used for rendering SelectOfferType data.
    /// </summary>
    public class SelectOfferTypeControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            inputControl.Items.Add
                (new ListItem("Meeting Offer", ((int) SelectOfferType.OfferType.MeetingOffer).ToString()));
            inputControl.Items.Add
                (new ListItem("Loyalty Offer", ((int) SelectOfferType.OfferType.LoyaltyOffer).ToString()));
            inputControl.Items.Add
                (new ListItem("Other Offer", ((int) SelectOfferType.OfferType.OtherOffer).ToString()));

            inputControl.SelectedValue = this.PropertyData.Value as string;
        }


        /// <summary>
        /// Gets the SelectOfferType instance for this IPropertyControl.
        /// </summary>
        /// <value>The property that is to be displayed or edited.</value>
        public SelectOfferType SelectOfferType
        {
            get { return PropertyData as SelectOfferType; }
        }
    }
}