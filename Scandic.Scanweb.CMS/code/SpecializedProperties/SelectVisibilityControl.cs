//  Description					:   SelectVisibilityControl                               //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// PropertyControl implementation used for rendering SelectVisibility data.
    /// </summary>
    public class SelectVisibilityControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            inputControl.Items.Add(new ListItem(string.Empty, string.Empty));
            inputControl.Items.Add(new ListItem("Yes", ((int) SelectVisibility.Visibility.Visible).ToString()));
            inputControl.Items.Add(new ListItem("No", ((int) SelectVisibility.Visibility.NotVisible).ToString()));

            try
            {
                inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }

        /// <summary>
        /// Gets the SelectVisibility instance for this IPropertyControl.
        /// </summary>
        /// <value>The property that is to be displayed or edited.</value>
        public SelectVisibility SelectVisibility
        {
            get { return PropertyData as SelectVisibility; }
        }
    }
}