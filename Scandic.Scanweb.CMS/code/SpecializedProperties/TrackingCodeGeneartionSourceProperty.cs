﻿//<remarks>
//====================================================================
// Name: TrackingCodeGeneartionSourceProperty.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Partner CMS pages.
// Construction Date: 23/08/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// TrackingCodeGeneartionSourceProperty
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class TrackingCodeGeneartionSourceProperty : EPiServer.Core.PropertyString
    {
        /// <summary>
        /// Creates property control.
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new TrackingCodeGeneartionSourceControl();
        }

        /// <summary>
        /// Holds different types of Visibility
        /// </summary>
        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }
    }
}