﻿//<remarks>
//====================================================================
// Name: BlockCodeListBoxControl.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Partner CMS pages.
// Construction Date: 23/08/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region using

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.PropertyControls;
using Scandic.Scanweb.Core;

#endregion using

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// BlockCodeListBoxControl
    /// </summary>
    public class BlockCodeListBoxControl : PropertyDataControl
    {
        #region Private Members

        private ListBox blockCodeListBoxControl;

        #endregion

        #region Properties

        /// <summary>
        /// Property for the BlockCodeListBoxControl control
        /// </summary>
        protected ListBox SelectBlockCodeListBoxControl
        {
            get { return blockCodeListBoxControl; }
            set { blockCodeListBoxControl = value; }
        }

        #endregion Properties

        #region Protected Methods

        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            Dictionary<string, Block> blockPagesCollection = new Dictionary<string, Block>();
            string propertyValue = this.ToString();
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            if ((null != rootPage) && (null != rootPage["BlockCategoryContainer"]))
            {
                PageReference blockCategoryContainer = (PageReference) rootPage["BlockCategoryContainer"];
                PageDataCollection blockCategories = DataFactory.Instance.GetChildren(blockCategoryContainer);
                foreach (PageData blockCategoryPage in blockCategories)
                {
                    PageDataCollection blockCodePages = DataFactory.Instance.GetChildren(blockCategoryPage.PageLink);
                    for (int i = blockCodePages.Count - 1; i >= 0; i--)
                    {
                        if (!blockCodePages[i].CheckPublishedStatus(PagePublishedStatus.Published))
                            blockCodePages.Remove(blockCodePages[i]);
                    }
                    int blockCodePageCount = blockCodePages.Count;
                    for (int count = 0; count < blockCodePageCount; count++)
                    {
                        string blockID = (null != blockCodePages[count]["OperaID"])
                                             ? blockCodePages[count]["OperaID"].ToString().Trim()
                                             : string.Empty;
                        string blockName = (null != blockCodePages[count]["PageName"].ToString().Trim())
                                               ? blockCodePages[count]["PageName"].ToString().Trim()
                                               : string.Empty;
                        ListItem availableBlockName = new ListItem(blockName, blockName);
                        if (propertyValue.Contains("{" + availableBlockName.Value + "}"))
                        {
                            availableBlockName.Selected = true;
                        }
                        this.SelectBlockCodeListBoxControl.Items.Add(availableBlockName);
                    }
                    try
                    {
                        if (this.PropertyData.Value != null)
                            this.blockCodeListBoxControl.SelectedValue = this.PropertyData.Value.ToString();
                    }
                    catch (NullReferenceException)
                    {
                        
                    }
                }
            }
        }

        /// <summary>
        /// This will be called when save activity will happen.
        /// </summary>
        /// <param name="inputControl">ListBox</param>
        protected virtual void SaveValuesForListBox(ListBox inputControl)
        {
            string str = string.Empty;
            foreach (ListItem blockItem in this.SelectBlockCodeListBoxControl.Items)
            {
                if (blockItem.Selected)
                {
                    str = str + ((str.Length == 0) ? "{" + blockItem.Text + "}" : (",{" + blockItem.Text + "}"));
                }
            }
            base.SetValue(str);
        }

        #endregion Protected Methods

        #region Public Methods


        /// <summary>
        /// This will create the HotelMenuNavigationListControl
        /// </summary>
        public override void CreateEditControls()
        {
            this.SelectBlockCodeListBoxControl = new ListBox();
            this.SelectBlockCodeListBoxControl.Rows = 7;
            this.SelectBlockCodeListBoxControl.SelectionMode = ListSelectionMode.Multiple;
            this.SelectBlockCodeListBoxControl.EnableViewState = false;
            this.ApplyControlAttributes(this.SelectBlockCodeListBoxControl);
            this.Controls.Add(this.SelectBlockCodeListBoxControl);
            this.SetupEditControls();
        }


        /// <summary>
        /// This will update the changes.
        /// </summary>
        public override void ApplyEditChanges()
        {
            this.SaveValuesForListBox(this.SelectBlockCodeListBoxControl);
        }

        #endregion  Public Methods
    }
}