//<remarks>
//====================================================================
// Name: HotelCategoryPropertyControl.cs
// 
// Purpose :This is a user control which is used to add dynamic contents in a dropdown list in CMS pages.This control// is inherited from PropertyDataControl. 
// Construction Date: 04/01/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region Using

using System.Web.UI.WebControls;
using EPiServer.Globalization;
using EPiServer.Web.PropertyControls;
using Scandic.Scanweb.Core;

#endregion

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// This control is used to display Dynamic contents in a dropdown List.This control is inherited from              
    /// </summary>
    public class HotelCategoryPropertyControl : PropertyDataControl
    {
        #region Member Declarations

        #endregion

        #region Constructors

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public HotelCategoryPropertyControl()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Property to hold Dropdown List.
        /// </summary>
        protected DropDownList EditControl { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// This method will be called when the edit is called.
        /// </summary>
        protected override void SetupEditControls()
        {
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;
            ListItem item = null;
            EditControl.Items.Clear();

            string propertyValueforquality = this.ToString();
            item = new ListItem();
            item.Value = AppConstants.DEFAULT;
            item.Text = AppConstants.DEFAULT;
            if (propertyValueforquality.Equals(item.Value))
            {
                item.Selected = true;
            }
            EditControl.Items.Add(item);
            item = new ListItem();
            item.Value = AppConstants.COMINGSOON_VALUE;
            item.Text = AppConstants.COMINGSOON;
            if (propertyValueforquality.Equals(item.Value))
            {
                item.Selected = true;
            }
            EditControl.Items.Add(item);
            item = new ListItem();
            item.Value = AppConstants.RECENTLYOPENED_VALUE;
            item.Text = AppConstants.RECENTLYOPENED;
            if (propertyValueforquality.Equals(item.Value))
            {
                item.Selected = true;
            }
            EditControl.Items.Add(item);
        }

        /// <summary>
        /// This will create the edit control.
        /// </summary>
        public override void CreateEditControls()
        {
            this.EditControl = new DropDownList();
            this.EditControl.Width = 100;
            this.EditControl.EnableViewState = true;
            this.ApplyControlAttributes(this.EditControl);
            this.Controls.Add(this.EditControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// IsValueActive
        /// </summary>
        /// <param name="value"></param>
        /// <returns>True/False</returns>
        public bool IsValueActive(string value)
        {
            return false;
        }

        /// <summary>
        /// This is a Implemetion method of PropertyDataControl class.
        /// </summary>
        public override void ApplyEditChanges()
        {
            base.SetValue(EditControl.SelectedValue);
        }

        #endregion
    }
}