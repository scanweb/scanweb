//  Description					:   SelectOfferType                                       //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:                   									  //
//---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectOfferType : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Holds different offertypes 
        /// </summary>
        public enum OfferType
        {
            MeetingOffer = 0,
            LoyaltyOffer = 1,
            OtherOffer = 2
        }

        /// <summary>
        /// Creates property control.
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectOfferTypeControl();
        }
    }
}