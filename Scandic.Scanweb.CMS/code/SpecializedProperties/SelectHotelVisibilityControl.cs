//  Description					:   SelectHotelVisibilityControl                          //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web.PropertyControls;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// SelectHotelVisibilityControl data class
    /// </summary>
    public class SelectHotelVisibilityControl : PropertyDataControl
    {
       /// <summary>
       /// Constructor
       /// </summary>
        public SelectHotelVisibilityControl()
        {
        }

        /// <summary>
        /// Sets up edit controls.
        /// </summary>
        protected override void SetupEditControls()
        {
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference countryContainer = (PageReference) rootPage["CountryContainer"];

            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            int hotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;

            string propertyValue = this.ToString();

            PageDataCollection countryPages = DataFactory.Instance.GetChildren(countryContainer);

            foreach (PageData countryPage in countryPages)
            {
                ListItem c = new ListItem("--- " + countryPage.PageName.ToUpper(), string.Empty, true);
                c.Attributes.CssStyle.Add("color", "red");
                c.Attributes.CssStyle.Add("font-weight", "bold");
                this.EditControl.Items.Add(c);

                PageDataCollection cityPages = DataFactory.Instance.GetChildren(countryPage.PageLink);

                foreach (PageData cityPage in cityPages)
                {
                    ListItem ci = new ListItem("------ " + cityPage.PageName, string.Empty, true);
                    ci.Attributes.CssStyle.Add("color", "blue");
                    ci.Attributes.CssStyle.Add("font-weight", "bold");
                    this.EditControl.Items.Add(ci);

                    PageDataCollection containerPages = DataFactory.Instance.GetChildren(cityPage.PageLink);

                    foreach (PageData containerPage in containerPages)
                    {
                        if (containerPage.PageTypeID == hotelContainerPageTypeID)
                        {
                            PageDataCollection hotelPages = DataFactory.Instance.GetChildren(containerPage.PageLink);

                            foreach (PageData hotelPage in hotelPages)
                            {
                                if (hotelPage.PageTypeID == hotelPageTypeID)
                                {
                                    ListItem h = new ListItem("--------- " + hotelPage.PageName,
                                                              hotelPage["OperaID"] as string, true);
                                    if (propertyValue.Contains("{" + h.Value + "}"))
                                    {
                                        h.Selected = true;
                                    }
                                    this.EditControl.Items.Add(h);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Applies edit changes.
        /// </summary>
        public override void ApplyEditChanges()
        {
            this.SaveValuesForListBox(this.EditControl);
        }

        /// <summary>
        /// Creates edit controls.
        /// </summary>
        public override void CreateEditControls()
        {
            this.EditControl = new ListBox();
            this.EditControl.Width = 500;
            this.EditControl.Rows = 30;
            this.EditControl.SelectionMode = ListSelectionMode.Multiple;
            this.EditControl.EnableViewState = false;
            this.ApplyControlAttributes(this.EditControl);
            this.Controls.Add(this.EditControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// Checks wether the value is active or not.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsValueActive(string value)
        {
            return this.PropertyMultipleValue.IsValueActive(value);
        }

        /// <summary>
        /// Saves values for list box.
        /// </summary>
        /// <param name="inputControl"></param>
        protected virtual void SaveValuesForListBox(ListBox inputControl)
        {
            string str = string.Empty;
            foreach (ListItem item in this.EditControl.Items)
            {
                if (item.Selected && item.Value.Length > 0)
                {
                    str = str + ((str.Length == 0) ? "{" + item.Value + "}" : (",{" + item.Value + "}"));
                }
            }
            base.SetValue(str);
        }

        /// <summary>
        /// Gets/Sets EditControl
        /// </summary>
        protected ListBox EditControl { get; set; }

        /// <summary>
        /// Gets PropertyMultipleValue
        /// </summary>
        protected PropertyMultipleValue PropertyMultipleValue
        {
            get
            {
                PropertyMultipleValue propertyData = base.PropertyData as PropertyMultipleValue;
                if (propertyData == null)
                {
                    throw new ScanWebGenericException(
                        "This property control requires that the Property inherits from PropertyMultipleValue");
                }
                return propertyData;
            }
        }
    }
}
