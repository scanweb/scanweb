//  Description					:   SelectOfferListControl                                //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.WebControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.Security;
using EPiServer.Web.PropertyControls;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Contains members of SelectOfferListControl
    /// </summary>
    public class SelectOfferListControl : PropertyDataControl
    {
        /// <summary>
        /// Gets/Sets EditControl 
        /// </summary>
        protected ListBox EditControl { get; set; }

        /// <summary>
        /// This method will be called when the edit is called.
        /// </summary>
        protected override void SetupEditControls()
        {
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference offerContainer = (PageReference) rootPage["OfferOverviewPage"];

            string propertyValue = this.ToString();

            PageDataCollection masterOfferCategory = DataFactory.Instance.GetChildren(offerContainer);
            int masterOfferCategoryCount = masterOfferCategory.Count;
            for (int masterOfferCategoryCounter = 0;
                 masterOfferCategoryCounter < masterOfferCategoryCount;
                 masterOfferCategoryCounter++)
            {
                ListItem masterCategoryList =
                    new ListItem("--- " + masterOfferCategory[masterOfferCategoryCounter].PageName.ToUpper(),
                                 string.Empty, true);

                masterCategoryList.Attributes.CssStyle.Add("color", "red");
                masterCategoryList.Attributes.CssStyle.Add("font-weight", "bold");
                this.EditControl.Items.Add(masterCategoryList);

                PageDataCollection offerCategory =
                    DataFactory.Instance.GetChildren(masterOfferCategory[masterOfferCategoryCounter].PageLink);
                int offerCategoryCount = offerCategory.Count;
                for (int offerCategoryCounter = 0; offerCategoryCounter < offerCategoryCount; offerCategoryCounter++)
                {
                    ListItem offerCategoryList =
                        new ListItem("------ " + offerCategory[offerCategoryCounter].PageName.ToUpper(),
                                     offerCategory[offerCategoryCounter].PageLink.ID.ToString(), true);

                    offerCategoryList.Attributes.CssStyle.Add("color", "blue");
                    offerCategoryList.Attributes.CssStyle.Add("font-weight", "bold");
                    this.EditControl.Items.Add(offerCategoryList);

                    PageDataCollection offers =
                        DataFactory.Instance.GetChildren(offerCategory[offerCategoryCounter].PageLink);
                    int offerCount = offers.Count;
                    for (int offerCounter = 0; offerCounter < offerCount; offerCounter++)
                    {
                        PageData p = offers[offerCounter];

                        if (p.CheckPublishedStatus(PagePublishedStatus.Published) &&
                            p.QueryDistinctAccess(AccessLevel.Read))
                        {
                            ListItem offerList = new ListItem("--------- " + offers[offerCounter].PageName.ToUpper(),
                                                              offers[offerCounter].PageLink.ID.ToString(), true);
                            if (propertyValue.Contains("{" + offerList.Value + "}"))
                            {
                                offerList.Selected = true;
                            }
                            this.EditControl.Items.Add(offerList);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This will be called when save activity will happen.
        /// </summary>
        /// <param name="inputControl"></param>
        protected virtual void SaveValuesForListBox(ListBox inputControl)
        {
            string str = string.Empty;
            foreach (ListItem item in this.EditControl.Items)
            {
                if (item.Selected && item.Value.Length > 0)
                {
                    str = str + ((str.Length == 0) ? "{" + item.Value + "}" : (",{" + item.Value + "}"));
                }
            }
            base.SetValue(str);
        }

        /// <summary>
        /// This will create the edit control.
        /// </summary>
        public override void CreateEditControls()
        {
            this.EditControl = new ListBox();
            this.EditControl.Width = 500;
            this.EditControl.Rows = 30;
            this.EditControl.SelectionMode = ListSelectionMode.Multiple;
            this.EditControl.EnableViewState = false;
            this.ApplyControlAttributes(this.EditControl);
            this.Controls.Add(this.EditControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// This will update the changes.
        /// </summary>
        public override void ApplyEditChanges()
        {
            this.SaveValuesForListBox(this.EditControl);
        }

        /// <summary>
        /// Checks whether the value is active or not.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsValueActive(string value)
        {
            return this.PropertyMultipleValue.IsValueActive(value);
        }

        /// <summary>
        /// Gets PropertyMultipleValue
        /// </summary>
        protected PropertyMultipleValue PropertyMultipleValue
        {
            get
            {
                PropertyMultipleValue propertyData = base.PropertyData as PropertyMultipleValue;
                if (propertyData == null)
                {
                    throw new ScanWebGenericException(
                        "This property control requires that the Property inherits from PropertyMultipleValue");
                }
                return propertyData;
            }
        }
    }
}