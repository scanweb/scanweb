//  Description					: SiteFooterColumnNumber
//	This Control is used to display the links in a particular column																			  
//----------------------------------------------------------------------------------------
/// Author						: Kshipra Thombre  
/// Author email id				:                           							  
/// Creation Date				: 18th March  2010									  
///	Version	#					: 2.0													  
///---------------------------------------------------------------------------------------
/// Revison History				: -NA-													  
///	Last Modified Date			:														  
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Site Footer Column Number 
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SiteFooterColumnNumber : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Create Property Control
        /// </summary>
        /// <returns>Property Control</returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SiteFooterColumnNumberControl();
        }

        /// <summary>
        /// Visibility
        /// </summary>
        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }
    }
}