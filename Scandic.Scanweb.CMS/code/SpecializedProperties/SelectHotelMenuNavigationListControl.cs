//  Description					: SelectHotelMenuNavigationListControl                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

#region using
using System.Web.UI.WebControls;
using EPiServer.Web.PropertyControls;
#endregion using

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// SelectHotelMenuNavigationListControl entity
    /// </summary>
    public class SelectHotelMenuNavigationListControl : PropertyDataControl
    {
     
        #region Properties

        /// <summary>
        /// Property for the Hotel Menue navigation control
        /// </summary>
        protected ListBox HotelMenuNavigationListControl { get; set; }

        #endregion Properties

        #region Protected Methods

        /// <summary>
        /// This method will be called when the edit is called.
        /// The tabs currently configured are 
        /// Location,Facilities,Rooms,Offers,Meetings,GuestProgram,AdditionalFacilitiesTabName
        /// </summary>
        protected override void SetupEditControls()
        {
            string propertyValue = this.ToString();

            string currentLanguage = CurrentPage.LanguageID.ToString();
           
            ListItem hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location"),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Location",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities", currentLanguage),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Facilities",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms", currentLanguage),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Rooms",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers", currentLanguage),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Offers",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings", currentLanguage),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/Meetings",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            hotelItem =
                new ListItem(
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram", currentLanguage),
                    EPiServer.Core.LanguageManager.Instance.Translate(
                        "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram", currentLanguage),
                    true);
            if (
                propertyValue.Contains("{" +
                                       EPiServer.Core.LanguageManager.Instance.Translate(
                                           "/Templates/Scanweb/Pages/HotelLandingPage/HotelMenuNavigation/GuestProgram",
                                           currentLanguage) + "}"))
            {
                hotelItem.Selected = true;
            }
            this.HotelMenuNavigationListControl.Items.Add(hotelItem);

            if (CurrentPage["AdditionalFacilitiesTabName"] != null &&
                !string.IsNullOrEmpty(CurrentPage["AdditionalFacilitiesTabName"].ToString()))
            {
                hotelItem = new ListItem(CurrentPage["AdditionalFacilitiesTabName"].ToString(),
                                         CurrentPage["AdditionalFacilitiesTabName"].ToString(),
                                         true);
                if (propertyValue.Contains("{" + CurrentPage["AdditionalFacilitiesTabName"].ToString() + "}"))
                {
                    hotelItem.Selected = true;
                }
                this.HotelMenuNavigationListControl.Items.Add(hotelItem);
            }
        }

        /// <summary>
        /// This will be called when save activity will happen.
        /// </summary>
        /// <param name="inputControl"></param>
        protected virtual void SaveValuesForListBox(ListBox inputControl)
        {
            string str = string.Empty;
            foreach (ListItem hotelItem in this.HotelMenuNavigationListControl.Items)
            {
                if (hotelItem.Selected)
                {
                    str = str + ((str.Length == 0) ? "{" + hotelItem.Text + "}" : (",{" + hotelItem.Text + "}"));
                }
            }
            base.SetValue(str);
        }

        #endregion Protected Methods

        #region Public Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public SelectHotelMenuNavigationListControl()
        {
        }

        /// <summary>
        /// This will create the HotelMenuNavigationListControl
        /// </summary>
        public override void CreateEditControls()
        {
            this.HotelMenuNavigationListControl = new ListBox();
            this.HotelMenuNavigationListControl.Rows = 7;
            this.HotelMenuNavigationListControl.SelectionMode = ListSelectionMode.Multiple;
            this.HotelMenuNavigationListControl.EnableViewState = false;
            this.ApplyControlAttributes(this.HotelMenuNavigationListControl);
            this.Controls.Add(this.HotelMenuNavigationListControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// This will update the changes.
        /// </summary>
        public override void ApplyEditChanges()
        {
            this.SaveValuesForListBox(this.HotelMenuNavigationListControl);
        }

        #endregion Public Methods
    }
}