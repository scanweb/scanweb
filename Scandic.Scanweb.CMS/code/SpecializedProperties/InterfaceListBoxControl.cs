﻿//<remarks>
//====================================================================
// Name: InterfaceListBoxControl.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Partner CMS pages.
// Construction Date: 23/08/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region using

using System;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Web.UI.WebControls;
using EPiServer.Globalization;
using EPiServer.Web.PropertyControls;

#endregion using

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Contains members of InterfaceListBoxControl
    /// </summary>
    public class InterfaceListBoxControl : PropertyDataControl
    {
        #region Private Members
        private ListBox interfaceListBoxControl;

        #endregion

        #region Properties

        /// <summary>
        /// Property for the InterfaceListBoxControl control
        /// </summary>
        protected ListBox SelectInterfaceListBoxControl
        {
            get { return interfaceListBoxControl; }
            set { interfaceListBoxControl = value; }
        }

        #endregion Properties

        #region Protected Methods
        /// <summary>
        /// Sets up edit controls.
        /// </summary>
        protected override void SetupEditControls()
        {
            char[] splitWith = new char[] {';'};
            string propertyValue = this.ToString();
            string currentLanguage = ContentLanguage.SpecificCulture.Parent.Name;

            string interfaceListContentDisplayText =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListContentDropDownDisplayText");
            string interfaceListContentUrlConstantv1 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlContentConstantv1");
            string interfaceListContentUrlConstantv2 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlContentConstantv2");
            string interfaceListContentDropDownValue =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListContentDropDownValue");

            if (!string.IsNullOrEmpty(interfaceListContentDisplayText) &&
                !string.IsNullOrEmpty(interfaceListContentUrlConstantv1) &&
                !string.IsNullOrEmpty(interfaceListContentDropDownValue))
            {
                string[] sourceContentInterfaceArray = interfaceListContentDisplayText.Split(splitWith);
                string[] sourceContentInterfaceValue = interfaceListContentDropDownValue.Split(splitWith);
                string interfaceContentUrlConstant = interfaceListContentUrlConstantv1;
                if (sourceContentInterfaceArray != null && sourceContentInterfaceArray.Length > 0 &&
                    sourceContentInterfaceValue != null && sourceContentInterfaceValue.Length > 0 &&
                    !string.IsNullOrEmpty(interfaceContentUrlConstant) &&
                    sourceContentInterfaceArray.Length.Equals(sourceContentInterfaceValue.Length))
                {
                    for (int count = 0; count < sourceContentInterfaceArray.Length; count++)
                    {
                        string eachContentInterfaceStringText = sourceContentInterfaceArray[count];
                        string eachContentInterfaceStringValue = sourceContentInterfaceValue[count];
                        StringBuilder contentInterfaceValue = new StringBuilder();
                        if (!string.IsNullOrEmpty(eachContentInterfaceStringText) &&
                            !string.IsNullOrEmpty(eachContentInterfaceStringValue))
                        {
                            if (eachContentInterfaceStringText.ToUpperInvariant().Contains("V1"))
                                contentInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                   interfaceListContentUrlConstantv1,
                                                                   eachContentInterfaceStringValue);
                            else
                                contentInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                   interfaceListContentUrlConstantv2,
                                                                   eachContentInterfaceStringValue);

                            ListItem availableInterface = new ListItem(eachContentInterfaceStringText,
                                                                       contentInterfaceValue.ToString());
                            if (propertyValue.Contains("{" + availableInterface.Value + "}"))
                            {
                                availableInterface.Selected = true;
                            }
                            this.SelectInterfaceListBoxControl.Items.Add(availableInterface);
                        }
                    }
                }
            }

            string interfaceListAvailabiltyDisplayText =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListAvailabilityDropDownDisplayText");
            string interfaceListAvailabiltyUrlConstantv1 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlAvailabilityConstantv1");
            string interfaceListAvailabiltyUrlConstantv2 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlAvailabilityConstantv2");
            string interfaceListAvailabiltyDropDownValue =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListAvailabilityDropDownValue");
            if (!string.IsNullOrEmpty(interfaceListAvailabiltyDisplayText) &&
                !string.IsNullOrEmpty(interfaceListAvailabiltyUrlConstantv1) &&
                !string.IsNullOrEmpty(interfaceListAvailabiltyDropDownValue))
            {
                string[] sourceAvailabiltyInterfaceArray = interfaceListAvailabiltyDisplayText.Split(splitWith);
                string[] sourceAvailabiltyInterfaceValue = interfaceListAvailabiltyDropDownValue.Split(splitWith);
                string interfaceAvailabiltyUrlConstant = interfaceListAvailabiltyUrlConstantv1;
                if (sourceAvailabiltyInterfaceArray != null && sourceAvailabiltyInterfaceArray.Length > 0 &&
                    sourceAvailabiltyInterfaceValue != null && sourceAvailabiltyInterfaceValue.Length > 0 &&
                    !string.IsNullOrEmpty(interfaceAvailabiltyUrlConstant) &&
                    sourceAvailabiltyInterfaceArray.Length.Equals(sourceAvailabiltyInterfaceValue.Length))
                {
                    for (int count = 0; count < sourceAvailabiltyInterfaceArray.Length; count++)
                    {
                        string eachAvailabiltyInterfaceStringText = sourceAvailabiltyInterfaceArray[count];
                        string eachAvailabiltyInterfaceStringValue = sourceAvailabiltyInterfaceValue[count];
                        StringBuilder AvailabiltyInterfaceValue = new StringBuilder();
                        if (!string.IsNullOrEmpty(eachAvailabiltyInterfaceStringText) &&
                            !string.IsNullOrEmpty(eachAvailabiltyInterfaceStringValue))
                        {
                            if (eachAvailabiltyInterfaceStringText.ToUpperInvariant().Contains("V1"))
                                AvailabiltyInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                       interfaceListAvailabiltyUrlConstantv1,
                                                                       eachAvailabiltyInterfaceStringValue);
                            else
                                AvailabiltyInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                       interfaceListAvailabiltyUrlConstantv2,
                                                                       eachAvailabiltyInterfaceStringValue);

                            ListItem availableInterface = new ListItem(eachAvailabiltyInterfaceStringText,
                                                                       AvailabiltyInterfaceValue.ToString());
                            if (propertyValue.Contains("{" + availableInterface.Value + "}"))
                            {
                                availableInterface.Selected = true;
                            }
                            this.SelectInterfaceListBoxControl.Items.Add(availableInterface);
                        }
                    }
                }
            }
            string interfaceListAdminDisplayText =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListAdminDropDownDisplayText");
            string interfaceListAdminUrlConstantv1 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlAdminConstantv1");
            string interfaceListAdminUrlConstantv2 =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceUrlAdminConstantv2");
            string interfaceListAdminDropDownValue =
                ConfigurationManager.AppSettings.Get("SelectSource.InterfaceListAdminDropDownValue");
            if (!string.IsNullOrEmpty(interfaceListAdminDisplayText) &&
                !string.IsNullOrEmpty(interfaceListAdminUrlConstantv1) &&
                !string.IsNullOrEmpty(interfaceListAdminDropDownValue))
            {
                string[] sourceAdminInterfaceArray = interfaceListAdminDisplayText.Split(splitWith);
                string[] sourceAdminInterfaceValue = interfaceListAdminDropDownValue.Split(splitWith);
                string interfaceAdminUrlConstant = interfaceListAdminUrlConstantv1;
                if (sourceAdminInterfaceArray != null && sourceAdminInterfaceArray.Length > 0 &&
                    sourceAdminInterfaceValue != null && sourceAdminInterfaceValue.Length > 0 &&
                    !string.IsNullOrEmpty(interfaceAdminUrlConstant) &&
                    sourceAdminInterfaceArray.Length.Equals(sourceAdminInterfaceValue.Length))
                {
                    for (int count = 0; count < sourceAdminInterfaceArray.Length; count++)
                    {
                        string eachAdminInterfaceStringText = sourceAdminInterfaceArray[count];
                        string eachAdminInterfaceStringValue = sourceAdminInterfaceValue[count];
                        StringBuilder AdminInterfaceValue = new StringBuilder();
                        if (!string.IsNullOrEmpty(eachAdminInterfaceStringText) &&
                            !string.IsNullOrEmpty(eachAdminInterfaceStringValue))
                        {
                            if (eachAdminInterfaceStringText.ToUpperInvariant().Contains("V1"))
                                AdminInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                 interfaceListAdminUrlConstantv1,
                                                                 eachAdminInterfaceStringValue);
                            else
                                AdminInterfaceValue.AppendFormat(CultureInfo.InvariantCulture, "{0}{1}",
                                                                 interfaceListAdminUrlConstantv2,
                                                                 eachAdminInterfaceStringValue);
                            ListItem availableInterface = new ListItem(eachAdminInterfaceStringText,
                                                                       AdminInterfaceValue.ToString());
                            if (propertyValue.Contains("{" + availableInterface.Value + "}"))
                            {
                                availableInterface.Selected = true;
                            }
                            this.SelectInterfaceListBoxControl.Items.Add(availableInterface);
                        }
                    }
                }
            }
            try
            {
                if (this.PropertyData.Value != null)
                    this.interfaceListBoxControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }


        /// <summary>
        /// This will be called when save activity will happen.
        /// </summary>
        /// <param name="inputControl"></param>
        protected virtual void SaveValuesForListBox(ListBox inputControl)
        {
            string str = string.Empty;
            foreach (ListItem interfaceItem in this.SelectInterfaceListBoxControl.Items)
            {
                if (interfaceItem.Selected)
                {
                    str = str +
                          ((str.Length == 0) ? "{" + interfaceItem.Value + "}" : (",{" + interfaceItem.Value + "}"));
                }
            }
            base.SetValue(str);
        }

        #endregion Protected Methods

        #region Public Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public InterfaceListBoxControl()
        {
        }

        /// <summary>
        /// This will create the HotelMenuNavigationListControl
        /// </summary>
        public override void CreateEditControls()
        {
            this.SelectInterfaceListBoxControl = new ListBox();
            this.SelectInterfaceListBoxControl.Rows = 10;
            this.SelectInterfaceListBoxControl.SelectionMode = ListSelectionMode.Multiple;
            this.SelectInterfaceListBoxControl.EnableViewState = false;
            this.ApplyControlAttributes(this.SelectInterfaceListBoxControl);
            this.Controls.Add(this.SelectInterfaceListBoxControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// This will update the changes.
        /// </summary>
        public override void ApplyEditChanges()
        {
            this.SaveValuesForListBox(this.SelectInterfaceListBoxControl);
        }
    }

    #endregion Public Methods
}