﻿//<remarks>
//====================================================================
// Name: TrackingCodeGeneartionSourceControl.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Partner CMS pages.
// Construction Date: 23/08/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

using System;
using System.Configuration;
using System.Web.UI.WebControls;
using Scandic.Scanweb.BookingEngine.Controller.Session.MiscellaneousModule;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// TrackingCodeGeneartionSourceControl.
    /// </summary>
    public class TrackingCodeGeneartionSourceControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// Sets up edit controls.
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();
            if (CurrentPage.Property["TrackingCode"] != null)
            {
                MiscellaneousSessionWrapper.PreviousTrackingCode =
                    CurrentPage.Property["TrackingCode"].Value.ToString();
            }
            DropDownList inputControl = this.EditControl;

            string sourceTrackcode = ConfigurationManager.AppSettings.Get("SelectSource.TrackingCode");

            if (!string.IsNullOrEmpty(sourceTrackcode))
            {
                char[] splitWith = new char[] {';'};
                string[] sourceTrackcodeArray = sourceTrackcode.Split(splitWith);
                if (sourceTrackcodeArray != null && sourceTrackcodeArray.Length > 0)
                {
                    for (int count = 0; count < sourceTrackcodeArray.Length; count++)
                    {
                        string eachcolorString = sourceTrackcodeArray[count];
                        if (!string.IsNullOrEmpty(eachcolorString))
                            inputControl.Items.Add(new ListItem(eachcolorString, eachcolorString));
                    }
                }
                try
                {
                    if (this.PropertyData.Value != null)
                    {
                        inputControl.SelectedValue = this.PropertyData.Value.ToString();
                        inputControl.Attributes.Add("disabled", "disabled");
                    }
                }
                catch (NullReferenceException)
                {
                }
            }
        }

        /// <summary>
        /// Gets the color of the selected rate category.
        /// </summary>
        /// <value>The color of the selected rate category.</value>
        public TrackingCodeGeneartionSourceProperty SelectedTrackingCode
        {
            get { return PropertyData as TrackingCodeGeneartionSourceProperty; }
        }
    }
}