using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectHotelVisibility : EPiServer.Core.PropertyLongString
    {
        /// <summary>
        /// CreatePropertyControl
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectHotelVisibilityControl();
        }
    }
}