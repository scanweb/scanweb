﻿//<remarks>
//====================================================================
// Name: BlockCodeListBoxControlProperty.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Partner CMS pages.
// Construction Date: 23/08/2011
//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// BlockCodeListBoxControlProperty
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class BlockCodeListBoxControlProperty : EPiServer.Core.PropertyString
    {
        /// <summary>
        /// This is a overriden method from PropertyLongString class.
        /// This creates SelectHotelMenuNavigationListControl instance.
        /// </summary>
        /// <returns>IPropertyControl</returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new BlockCodeListBoxControl();
        }
    }
}