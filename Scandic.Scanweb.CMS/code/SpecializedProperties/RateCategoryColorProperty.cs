//<remarks>
//====================================================================
// Name: RateCategoryColorProperty.cs
// 
// Purpose :This is a class which is used to add dynamic contents in a dropdown list in Rate Category CMS pages.
// Construction Date: 04/01/2009
//
// Author :Vrushali Pawale, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2010 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>


using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// RateCategoryColorProperty
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class RateCategoryColorProperty : EPiServer.Core.PropertyString
    {
        /// <summary>
        /// Create Property Control
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new RateCategoryColorPropertyControl();
        }

        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }
    }
}