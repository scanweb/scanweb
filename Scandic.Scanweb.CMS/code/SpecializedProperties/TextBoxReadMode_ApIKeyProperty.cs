﻿//<remarks>
//====================================================================
// Name: TextBoxReadMode_ApIKeyProperty.cs
// 
// Purpose :This is a read only TextBox specially designed for API key Guid Genration in PartnerPages Date: 12/09/2011//
// Author :Abhishek Kumar, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2011 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Text Box Read Mode ApIKeyProperty
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class TextBoxReadMode_ApIKeyProperty : EPiServer.Core.PropertyLongString
    {
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new TextBoxReadMode_ApIKeyControl();
        }

        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }
    }
}