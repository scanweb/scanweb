//<remarks>
//====================================================================
// Name: PicSearchProperty.cs
// 
// Purpose :This is a user control which is used to add dynamic contents in a multiline Text box list in CMS pages.This control
// is inherited from PropertyLongString. 
// Construction Date: 05/05/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region Using

using EPiServer.Core;

#endregion

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData which implements PropertyLongString class.
    /// </summary>
    public class StreamingVideoProperty : PropertyLongString
    {
        #region Methods

        /// <summary>
        /// This is a overriden method from PropertyLongString class.Which creates StreamingVideoPropertyControl                /// instance.
        /// </summary>
        /// <returns>IPropertyControl</returns>
        public override IPropertyControl CreatePropertyControl()
        {
            return new StreamingVideoPropertyControl();
        }

        #endregion
    }
}