//<remarks>
//====================================================================
// Name: StreamingVideoPropertyControl.cs
// 
// Purpose :This is a user control which is used to add dynamic contents in a multiline Text box in CMS pages.This control
// is inherited from PropertyDataControl. 
// Construction Date: 05/05/2009
//
// Author :Ranajit Kumar Nayak, Sapient
// Revison History : -NA-													  
// Last Modified Date :	
// ====================================================================
// Copyright (C) 2008 Scandic.  All Rights Reserved.
// ====================================================================
//</remarks>

#region Using
using System.Web.UI.WebControls;
using EPiServer.Web.PropertyControls;
#endregion

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// This control is used to display Dynamic contents in a Multiline Textbox.This control is inherited fromPropertyDataControl
    /// PropertyDataControl.
    /// </summary>
    public class StreamingVideoPropertyControl : PropertyDataControl
    {
        #region Member Declarations
        private TextBox textBoxEditControl;
        #endregion

        #region Methods

        /// <summary>
        /// This method will be called when the edit is called.
        /// </summary>
        protected override void SetupEditControls()
        {
            this.textBoxEditControl.Text = this.ToString();
        }

        /// <summary>
        /// This will create the edit control.
        /// </summary>
        public override void CreateEditControls()
        {
            this.textBoxEditControl = new TextBox();
            this.textBoxEditControl.TextMode = TextBoxMode.MultiLine;
            this.textBoxEditControl.Width = 500;
            this.textBoxEditControl.Height = 300;
            this.textBoxEditControl.EnableViewState = true;
            this.ApplyControlAttributes(this.textBoxEditControl);
            this.Controls.Add(this.textBoxEditControl);
            this.SetupEditControls();
        }

        /// <summary>
        /// This will used to get state of text box values.
        /// </summary>
        /// <param name="value">value saved during ApplyEditChanges</param>
        /// <returns>True if control value saved or False.</returns>
        public bool IsValueActive(string value)
        {
            return false;
        }

        /// <summary>
        /// This is a Implemetion method of PropertyDataControl class.
        /// </summary>
        public override void ApplyEditChanges()
        {
            base.SetValue(textBoxEditControl.Text);
        }

        #endregion
    }
}