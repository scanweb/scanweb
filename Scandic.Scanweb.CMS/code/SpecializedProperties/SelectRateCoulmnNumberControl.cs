using System;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.code.SpecializedProperties
{
    /// <summary>
    /// SelectRateCoulmnNumberControl
    /// </summary>
    public class SelectRateCoulmnNumberControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            string numberOfCoulmns = ConfigurationManager.AppSettings.Get("SelectRate.NoOfRateCoulmn");
            inputControl.Items.Add(new ListItem("Select", string.Empty));
            if (!string.IsNullOrEmpty(numberOfCoulmns))
            {
                int coulmnCount;
                if (int.TryParse(numberOfCoulmns, out coulmnCount))
                {
                    for (int count = 1; count <= coulmnCount; count++)
                    {
                        string value = count.ToString();
                        inputControl.Items.Add(new ListItem(value + "-column", value));
                    }
                }
            }
            try
            {
                inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }

        /// <summary>
        /// SelectRateCoulmnNumber
        /// </summary>
        public SelectRateCoulmnNumber SelectRateCoulmnNumber
        {
            get { return PropertyData as SelectRateCoulmnNumber; }
        }
    }
}