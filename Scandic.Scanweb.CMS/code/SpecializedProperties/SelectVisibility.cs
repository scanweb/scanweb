////////////////////////////////////////////////////////////////////////////////////////////
//  Description					:  SelectVisibility                                       //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                              							  //
// Creation Date				: 	    								                  //
//	Version	#					:                                                         //
//--------------------------------------------------------------------------------------- //
// Revision History			    :                                                         //
//	Last Modified Date			:	                                                      //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class SelectVisibility : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Holds different visibility options.
        /// </summary>
        public enum Visibility
        {
            Visible = 1,
            NotVisible = 0
        }

        /// <summary>
        /// CreatePropertyControl
        /// </summary>
        /// <returns>IPropertyControl</returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new SelectVisibilityControl();
        }
    }
}