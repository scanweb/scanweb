using System;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// PropertyControl implementation used for rendering TopAreaWidth data.
    /// </summary>
    public class TopAreaWidthControl : EPiServer.Web.PropertyControls.PropertySelectControlBase
    {
        /// <summary>
        /// SetupEditControls
        /// </summary>
        protected override void SetupEditControls()
        {
            base.SetupEditControls();

            DropDownList inputControl = this.EditControl;
            inputControl.Items.Add(new ListItem(string.Empty, string.Empty));
            inputControl.Items.Add(new ListItem("Center (472x265)", ((int)TopAreaWidth.Width.Center).ToString()));
            inputControl.Items.Add(new ListItem("Center and right (718x265)",
                                                ((int) TopAreaWidth.Width.CenterRight).ToString()));
            inputControl.Items.Add(new ListItem("Full page (964x265)", ((int)TopAreaWidth.Width.Full).ToString()));

            try
            {
                inputControl.SelectedValue = this.PropertyData.Value.ToString();
            }
            catch (NullReferenceException)
            {
            }
        }


        /// <summary>
        /// Gets the TopAreaWidth instance for this IPropertyControl.
        /// </summary>
        /// <value>The property that is to be displayed or edited.</value>
        public TopAreaWidth SelectVisibility
        {
            get { return PropertyData as TopAreaWidth; }
        }
    }
}