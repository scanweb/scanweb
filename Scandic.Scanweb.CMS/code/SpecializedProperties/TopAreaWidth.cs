//  Description					:   TopAreaWidth                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class TopAreaWidth : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Width
        /// </summary>
        public enum Width
        {
            Center = 2,
            CenterRight = 1,
            Full = 0
        }

        /// <summary>
        /// CreatePropertyControl
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new TopAreaWidthControl();
        }
    }
}