﻿//  Description					:   TopAreaWidth                                          //
//																						  //
//----------------------------------------------------------------------------------------//
/// Author						:                                                         //
/// Author email id				:                           							  //
/// Creation Date				:                   									  //
///	Version	#					:                   									  //
///---------------------------------------------------------------------------------------//
/// Revison History				:   													  //
///	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using EPiServer.PlugIn;
using System.Web.UI.WebControls;

namespace Scandic.Scanweb.CMS.SpecializedProperties
{
    /// <summary>
    /// Custom PropertyData implementation
    /// </summary>
    [Serializable]
    [PageDefinitionTypePlugIn]
    public class FullWidthWithBox : EPiServer.Core.PropertyNumber
    {
        /// <summary>
        /// Width
        /// </summary>
        public enum Width
        {
            Full = 2,
            LeftCenterWithNoBox = 0
        }

        /// <summary>
        /// CreatePropertyControl
        /// </summary>
        /// <returns></returns>
        public override EPiServer.Core.IPropertyControl CreatePropertyControl()
        {
            return new FullWidthWithBoxControl();
        }
    }
}