﻿using Scandic.Scanweb.Core;
using Scandic.Scanweb.Core.Interface;

namespace Scandic.Scanweb.CMS.code.Common
{
    /// <summary>
    /// ScanwebAppManager
    /// </summary>
    public class ScanwebAppManager
    {
        /// <summary>
        /// InitlizeAllApps
        /// </summary>
        public static void InitlizeAllApps()
        {
            var applications = DependencyResolver.Instance.GetServices(typeof (IApplication));

            AppLogger.LogInfoMessage("Starting plugable application initialization.");
            if (applications != null)
            {
                foreach (var app in applications)
                {
                    ((IApplication) app).Initialize(null);
                }
            }
            else
            {
                AppLogger.LogInfoMessage("No plugable application is available.");
            }
        }
    }
}