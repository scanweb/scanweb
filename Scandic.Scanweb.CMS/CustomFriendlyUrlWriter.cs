﻿using System;
using System.Collections.Generic;
using System.Globalization;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using System.Text;
using EPiServer;
using Scandic.Scanweb.BookingEngine.Controller;
using Scandic.Scanweb.BookingEngine.Web;
using Scandic.Scanweb.BookingEngine.Web.Templates.Booking.Units;
using Scandic.Scanweb.CMS.DataAccessLayer;
using Scandic.Scanweb.Core;
using System.Text.RegularExpressions;
using System.Linq;
using EPiServer.Globalization;

namespace Scandic.Scanweb.CMS
{
    public class CustomFriendlyUrlWriter : EPiServer.Web.FriendlyUrlRewriteProvider
    {
        public CustomFriendlyUrlWriter()
            : base()
        {



        }

        public override bool ConvertToExternal(UrlBuilder url, object internalObject, Encoding toEncoding)
        {

            return base.ConvertToExternal(url, internalObject, toEncoding);
        }

        public override bool ConvertToInternal(UrlBuilder url, out object internalObject)
        {

            return base.ConvertToInternal(url, out internalObject);
        }


        /// <summary>
        /// Converts the internal URL(CMS Based) to External URL. 
        /// Converts internal Hotel, Country, City landing page url to friendly url.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="internalObject"></param>
        /// <param name="toEncoding"></param>
        /// <returns></returns>

        protected override bool ConvertToExternalInternal(UrlBuilder url, object internalObject, Encoding toEncoding)
        {
            int queryId = 0;
            string currentLanguage = string.Empty;
            int pageTypeId = default(int);
            string pageTypetoLog = string.Empty;
            try
            {
                string friendlyExternalURL;

                AppLogger.LogInfoMessage(string.Format("In function ConvertToExternalInternal: Building Friendly URL: Internal URL:{0}", url.Path));
                if (int.TryParse(url.QueryId, out queryId))
                {
                    PageData hotelOverviewPageData;
                    currentLanguage = url.Host != "" ? (url.QueryLanguage ?? LanguageSelection.GetLanguageFromHost())
                                                 : LanguageSelection.GetLanguageFromHost();

                    var pagedata = ContentDataAccess.GetPageData(queryId, currentLanguage);

                    if (pagedata != null)
                    {
                        pageTypeId = pagedata.PageTypeID;
                    }
                    switch (pageTypeId)
                    {
                        case AppConstants.HotelLandingPageTypeId:
                            pageTypetoLog = "HotelLandingPageType";
                            hotelOverviewPageData = ContentDataAccess.GetPageData(AppConstants.HotelOverviewPage, currentLanguage);

                            PageData cityPageData = ContentDataAccess.GetPageData(ContentDataAccess.GetPageData(pagedata.ParentLink.ID).ParentLink.ID, currentLanguage);
                            friendlyExternalURL = string.Format("/{0}/{1}/{2}/{3}/", hotelOverviewPageData.URLSegment,
                                                                ContentDataAccess.GetPageData(cityPageData.ParentLink.ID, currentLanguage).URLSegment,
                                                                cityPageData.URLSegment, pagedata.URLSegment.Trim());

                            break;

                        case AppConstants.CountryLandingPageTypeId:
                            pageTypetoLog = "CountryLandingPageType";
                            hotelOverviewPageData = ContentDataAccess.GetPageData(AppConstants.HotelOverviewPage, currentLanguage);
                            friendlyExternalURL = string.Format("/{0}/{1}/", hotelOverviewPageData.URLSegment, pagedata.URLSegment.Trim());
                            break;

                        case AppConstants.CityLandingPagetypeId:
                            pageTypetoLog = "CityLandingPagetype";
                            hotelOverviewPageData = ContentDataAccess.GetPageData(AppConstants.HotelOverviewPage, currentLanguage);
                            friendlyExternalURL = string.Format("/{0}/{1}/{2}/", hotelOverviewPageData.URLSegment,
                                                                  ContentDataAccess.GetPageData(pagedata.ParentLink.ID, currentLanguage).URLSegment.Trim(),
                                                                   pagedata.URLSegment.Trim());
                            break;
                        default:
                            pageTypetoLog = "Default";
                            return base.ConvertToExternalInternal(url, internalObject, toEncoding);

                    }
                    url.Path = friendlyExternalURL;
                    url.QueryCollection.Remove("id");
                    url.QueryCollection.Remove("epslanguage");
                    AppLogger.LogInfoMessage(string.Format("In function ExternalInternal: Building Friendly URL: Friendly URL:{0}", url.Path));
                    return true;
                }
                else
                    return base.ConvertToExternalInternal(url, internalObject, toEncoding);
            }

            catch (Exception ex)
            {
                string logMsg = string.Empty;
                if (url != null)
                    logMsg = string.Format("Exception occured in function ConvertToExternalInternal: URL Rewrite exception: {0}\n StackTrace: {1}\n URL Path: {2}\n URL QueryID: {3}\n URL Host: {4}\n URL QueryLanguage: {5}\n URI: {6}\n QueryID: {7}\n Current Language: {8}\n Page TypeId: {9}\n Page Type: {10}",
                    ex.Message, ex.StackTrace, url.Path, url.QueryId, url.Host, url.QueryLanguage, Convert.ToString(url.Uri), queryId, currentLanguage, pageTypeId, pageTypetoLog);
                else
                    logMsg = string.Format("Exception occured in function ConvertToExternalInternal: URL Rewrite exception: {0}\n StackTrace: {1}\n QueryID: {2}\n Current Language: {3}\n Page TypeId: {4}\n Page Type: {5}",
                        ex.Message, ex.StackTrace, queryId, currentLanguage, pageTypeId, pageTypetoLog);

                AppLogger.LogCustomException(ex, logMsg);
            }
            return base.ConvertToExternalInternal(url, internalObject, toEncoding);

        }



        /// <summary>
        /// Converts the external url to Internal URL. 
        /// Converts only Hotel URL to friendly url and bypasses all the other urls to default base coversion.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="internalObject"></param>
        /// <returns></returns>
        protected override bool ConvertToInternalInternal(UrlBuilder url, ref object internalObject)
        {
            try
            {
                DestinationSearch ds = new DestinationSearch();
                AppLogger.LogInfoMessage(
                    string.Format("In function ConverToInternalInternal:Before URL Internal Conversion: {0}",
                                  url.Path.ToString()));
                PageData hotelOverviewPage = ContentDataAccess.GetPageData(AppConstants.HotelOverviewPage, LanguageSelection.GetLanguageFromHost());
                PageData countryContainerPage = ContentDataAccess.GetPageData(AppConstants.CountryContainerPageID, LanguageSelection.GetLanguageFromHost());

                string hotelOverviewPageText = hotelOverviewPage["PageURLSegment"] != null ? hotelOverviewPage["PageURLSegment"].ToString() : hotelOverviewPage.PageLink.ID.ToString();
                string countryContainerPageText = countryContainerPage["PageURLSegment"] != null ? countryContainerPage["PageURLSegment"].ToString() : countryContainerPage.PageLink.ID.ToString();

                if (CheckForHotelPageRedirection(url, hotelOverviewPageText, countryContainerPageText))
                {
                    var path = new StringBuilder();
                    path.Append(url.Path);
                    path.Replace(path.ToString(), path.ToString().TrimEnd(new char[] { '/' }));
                    path.Replace(path.ToString(), path.ToString().TrimStart(new char[] { '/' }));
                    string[] urlFragments = path.ToString().ToLower().Split('/');
                    var internalurl = new StringBuilder();
                    List<CountryDestinatination> countryCollection;
                    switch (urlFragments.Length)
                    {
                        case ((int)AppConstants.UrlFragmentLength.HotelUrlFragmentLength):
                            if (HotelNameMatch(ds, urlFragments[2], urlFragments[3]))
                            {
                                for (int i = 0; i < urlFragments.Length; i++)
                                {
                                    internalurl.Append("/");
                                    if (i == (int) AppConstants.HotelCountrySubstringPos.HotelSubstring)
                                        internalurl.Append(getCountryString()).Append("/");
                                    if (i == (int) AppConstants.HotelCountrySubstringPos.Country)
                                        internalurl.Append(getHotelString(ds, urlFragments[2], urlFragments[3]))
                                                   .Append("/");

                                    internalurl.Append(urlFragments[i]);
                                }
                                url.Path = internalurl.Append("/").ToString();
                            }
                            break;
                        case ((int)AppConstants.UrlFragmentLength.CountryUrlFragmentLength):

                            countryCollection = ds.FetchAllCountryDestinations();

                            if (countryCollection.Count(d => d.CountryLinkUrl.ToLower().Contains(url.Path.ToLower())) > 0)
                            {
                                for (int i = 0; i < urlFragments.Length; i++)
                                {
                                    internalurl.Append("/");
                                    if (i == (int)AppConstants.HotelCountrySubstringPos.HotelSubstring)
                                        internalurl.Append(getCountryString()).Append("/");
                                    internalurl.Append(urlFragments[i]);
                                }
                                url.Path = internalurl.Append("/").ToString();
                            }
                            break;
                        case ((int)AppConstants.UrlFragmentLength.CityUrlFragmentLength):
                            countryCollection = ds.FetchAllCountryDestinations();
                            bool urlBuilt = false;
                            foreach (var ct in countryCollection.Select(cn => cn.SearchedCities))
                            {
                                foreach (var cityDestination in ct)
                                {
                                    if (url.Path.ToLower().Contains(cityDestination.CityLinkUrl.TrimEnd('/').ToLower()))
                                    {
                                        for (int i = 0; i < urlFragments.Length; i++)
                                        {
                                            internalurl.Append("/");
                                            if (i == (int)AppConstants.HotelCountrySubstringPos.HotelSubstring)
                                                internalurl.Append(getCountryString()).Append("/");
                                            internalurl.Append(urlFragments[i]);
                                        }
                                        url.Path = internalurl.Append("/").ToString();
                                        urlBuilt = true;
                                    }
                                }
                                if (urlBuilt)
                                    break;
                            }
                            break;
                        default:
                            return base.ConvertToInternalInternal(url, ref internalObject);
                    }

                    AppLogger.LogInfoMessage(
                        string.Format("In function ConverToInternalInternal:After URL Internal Conversion: {0}",
                                      url.Path.ToString()));

                    return base.ConvertToInternalInternal(url, ref internalObject);
                }
                else
                {
                    return base.ConvertToInternalInternal(url, ref internalObject);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogCustomException(ex, string.Format("Exception occured in function ConvertToInternalInternal: URL Rewrite exception: {0}\n{1}", ex.Message, ex.StackTrace));

            }
            return base.ConvertToInternalInternal(url, ref internalObject);
        }

        private bool HotelNameMatch(DestinationSearch ds, string cityname, string hotelname)
        {
            List<CountryDestinatination> countryCollection = ds.FetchAllCountryDestinations();
            if (countryCollection != null)
            {
                var selectedCity = (from countryDestinatination in countryCollection.Select(cn => cn.SearchedCities)
                                    from city in countryDestinatination.Select(ct => ct)
                                    where (string.Equals(city.PageNameInWebAddress, cityname, StringComparison.InvariantCultureIgnoreCase) || CompareCityName(city, cityname))
                                    select city).FirstOrDefault();
                if (selectedCity != null)
                {
                    var selectedHotel = (selectedCity.SearchedHotels.Where(hotel => hotel != null).
                                                      Where(
                                                          hotel =>
                                                          hotel.UrlSegment.Equals(hotelname,
                                                                            StringComparison.InvariantCultureIgnoreCase) ||
                                                          hotel.SearchableString.Equals(hotelname,
                                                                            StringComparison.InvariantCultureIgnoreCase)))
                        .FirstOrDefault();

                    if (selectedHotel != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool CheckForCountryType(string urlfrag)
        {

            throw new NotImplementedException();
        }

        private string getHotelString(DestinationSearch ds, string cityname, string hotelname)
        {
            string hotelContainerText = string.Empty;
            List<CountryDestinatination> countryCollection = ds.FetchAllCountryDestinations();
            if (countryCollection != null)
            {
                var selectedCity = (from countryDestinatination in countryCollection.Select(cn => cn.SearchedCities)
                                    from city in countryDestinatination.Select(ct => ct)
                                    where (string.Equals(city.PageNameInWebAddress, cityname, StringComparison.InvariantCultureIgnoreCase) || CompareCityName(city, cityname))
                                    select city).FirstOrDefault();
                if (selectedCity != null)
                {
                    var selectedHotel = (selectedCity.SearchedHotels.Where(hotel => hotel != null).
                                                      Where(
                                                          hotel =>
                                                          hotel.UrlSegment.Equals(hotelname,
                                                                            StringComparison.InvariantCultureIgnoreCase) ||
                                                          hotel.SearchableString.Equals(hotelname,
                                                                            StringComparison.InvariantCultureIgnoreCase)))
                        .FirstOrDefault();

                    if (selectedHotel != null)
                    {
                        PageData hotelPageData =
                            ContentDataAccess.GetPageData(Convert.ToInt32(selectedHotel.Id),
                                                          LanguageSelection.GetLanguageFromHost());
                        PageData hotelsContainerPageData =
                            ContentDataAccess.GetPageData(hotelPageData.ParentLink);
                        hotelContainerText = hotelsContainerPageData["PageURLSegment"] != null ? hotelsContainerPageData["PageURLSegment"].ToString() : hotelsContainerPageData.PageLink.ID.ToString();
                    }
                }
            }
            if (string.IsNullOrEmpty(hotelContainerText))
            {
                PageData pagedata;
                pagedata = ContentDataAccess.GetPageData(new PageReference(AppConstants.HotelContainerPageID));
                hotelContainerText = pagedata.URLSegment;
            }
            return hotelContainerText;
        }

        private bool CompareCityName(CityDestination city, string cityname)
        {
            bool isCityNameMacthing = false;
            if (!string.IsNullOrEmpty(city.CityLinkUrl))
            {
                string[] cityLinkUrlFragments = city.CityLinkUrl.TrimEnd('/').Split('/');
                isCityNameMacthing = String.Equals(cityLinkUrlFragments[cityLinkUrlFragments.Length - 1], cityname,
                                     StringComparison.InvariantCultureIgnoreCase);
            }
            return isCityNameMacthing;
        }

        private string getCountryString()
        {
            PageData pagedata;
            pagedata = ContentDataAccess.GetPageData(new PageReference(AppConstants.CountryContainerPageID)); ;
            return pagedata.URLSegment;
        }

        private bool CheckForHotelPageRedirection(UrlBuilder urlBrowser, string  hotelOverviewPageText, string countryContainerPageText)
        {
            return
                ((urlBrowser.Uri.AbsoluteUri).ToLower()
                                             .Contains(
                                                 string.Format("{0}/{1}", urlBrowser.Host, hotelOverviewPageText)
                                                       .ToLower()))
                &&
                !((urlBrowser.Uri.AbsoluteUri).ToLower()
                                             .Contains(
                                                 string.Format("{0}/{1}", hotelOverviewPageText,
                                                               countryContainerPageText).ToLower()));
        }
    }
}
