<%@ Page Language="C#" AutoEventWireup="False" Codebehind="Default.aspx.cs" MasterPageFile="~/Templates/Scanweb/MasterPages/MasterPageStart.master" Inherits="Scandic.Scanweb.CMS.Templates.Default"%>
<%--<%@ Register TagPrefix="Scanweb" TagName="Newslist" 	Src="~/Templates/Scanweb/Units/Static/StartpageNewslist.ascx" %>--%>
<%@ Register TagPrefix="Scanweb" TagName="StartPageInfoBox" 	Src="~/Templates/Scanweb/Units/Static/StartPageInfoBox.ascx" %>

<%@ Register TagPrefix="Scanweb" TagName="Promotion"  	Src="~/Templates/Scanweb/Units/Static/StartpagePromotion.ascx" %>
<%--<%@ Register TagPrefix="Scanweb" TagName="Puffs"  	    Src="~/Templates/Scanweb/Units/Static/StartpagePuffs.ascx" %>--%>

<%@ Register TagPrefix="Scanweb" TagName="PromoBoxStart" Src="~/Templates/Scanweb/Units/Placeable/PromoBoxStart.ascx" %>
<%@ Register TagPrefix="Scanweb" TagName="BookingModuleAlternativeBox" Src="~/Templates/Scanweb/Units/Placeable/BookingModuleAlternativeBox.ascx" %>
<%@ Register TagPrefix="Booking" TagName="Module" Src="~/Templates/Booking/Units/BookingModuleBig.ascx" %>
<%@ Register TagPrefix="Booking" TagName="FlyOut" Src="~/Templates/Booking/Units/BookingModuleFlyout.ascx" %>

<asp:Content ContentPlaceHolderID="MainContentRegion" runat="server">
    <div id="StartPage">
        <div id="LeftContentArea">
            <div id="BookingArea">                
               <Booking:Module ID="BE" runat="server" />
               <Booking:FlyOut ID="bookingFlyOut" runat="server"></Booking:FlyOut>
               <Scanweb:BookingModuleAlternativeBox CssClass="AlternativeBookingBoxLarge" ID="AlternativeBookingModule" runat="server" />
            </div>
            </div>
        
        <div id="RightContentArea">
            <div id="FlashArea">
                <Scanweb:Promotion ID="Promotion1" runat="server" />               
            </div>
        </div>     
            
        <div id="offersArea">
            <Scanweb:PromoBoxStart  ImagePropertyName ="PromoBoxImage1" 
                    PriceTextPropertyName ="PriceText1"  
                    FontColorProperty_Name ="PromoFontColor"
                    LinkTextPropertyName1 = "LinkText1Box1"
                    PageLinkPropertyName1 ="LinkToPage1Box1"
                    BackgroundColourBlackPropertyName = "BackgroundColour1" 
                    ImageMaxWidth="224" 
                    StoolImageMaxWidth ="204" 
                    StoolImageMaxHeight ="45" 
                    TrackPromoboxLink1Id ="TrackPromoboxLink1Box1"
                    ContentNameTrackLink1Id ="ContentNameLink1Box1"
                    BoxText ="Box1"
                    runat="server"/>
            <Scanweb:PromoBoxStart   ImagePropertyName ="PromoBoxImage2" 
                    OfferHeadingPropertyName ="Offer2HeadingText" 
                    PriceTextPropertyName ="PriceText2"  
                    FontColorProperty_Name ="PromoFontColor"
                    LinkTextPropertyName1 = "LinkText1Box2"
                    PageLinkPropertyName1 ="LinkToPage1Box2"
                    BackgroundColourBlackPropertyName = "BackgroundColour2"
                    ImageMaxWidth="224" 
                    StoolImageMaxWidth="204" 
                    StoolImageMaxHeight ="45" 
                    TrackPromoboxLink1Id ="TrackPromoboxLink1Box2"
                    ContentNameTrackLink1Id ="ContentNameLink1Box2"
                    BoxText ="Box2"
                    runat="server"/>
            <Scanweb:PromoBoxStart ImagePropertyName ="PromoBoxImage3" 
                    PriceTextPropertyName ="PriceText3"  
                    FontColorProperty_Name ="PromoFontColor"
                    LinkTextPropertyName1 = "LinkText1Box3"
                    PageLinkPropertyName1 ="LinkToPage1Box3"
                    BackgroundColourBlackPropertyName = "BackgroundColour3"
                    ImageMaxWidth="224"
                    StoolImageMaxWidth="204"
                    StoolImageMaxHeight ="45" 
                    TrackPromoboxLink1Id ="TrackPromoboxLink1Box3"
                    ContentNameTrackLink1Id ="ContentNameLink1Box3"
                    BoxText ="Box3"
                    runat="server"/>
            <Scanweb:PromoBoxStart ImagePropertyName ="PromoBoxImage4" 
                    PriceTextPropertyName ="PriceText4"  
                    FontColorProperty_Name ="PromoFontColor"
                    LinkTextPropertyName1 = "LinkText1Box4"
                    PageLinkPropertyName1 ="LinkToPage1Box4"
                    BackgroundColourBlackPropertyName = "BackgroundColour4"
                    ImageMaxWidth="224" 
                    StoolImageMaxWidth="204" 
                    StoolImageMaxHeight ="45" 
                    TrackPromoboxLink1Id ="TrackPromoboxLink1Box4"
                    ContentNameTrackLink1Id ="ContentNameLink1Box4"
                    BoxText ="Box4"
                    runat="server"/>
        </div>            
    </div>  
</asp:Content>
