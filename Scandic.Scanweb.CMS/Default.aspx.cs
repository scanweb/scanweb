//  Description					:   Default                                               //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI;
using Scandic.Scanweb.CMS.Util;
using System.Web;

namespace Scandic.Scanweb.CMS.Templates
{
    /// <summary>
    /// The default start page
    /// </summary>
    public partial class Default : ScandicTemplatePage
    {
        protected UserControl BE;

        private List<string> viewstateAllowedControls = new List<string>
                                                            {
                                                                "btnSearch",
                                                                "spnSearch",
                                                                "btnBonusChequeSearch",
                                                                "spnBonusChequeSearch",
                                                                "btnRewardNightSearch",
                                                                "spnRewardNightSearch",
                                                            };

        private bool DisableViewState(ControlCollection controls)
        {
            bool flag = true;
            foreach (Control control in controls)
            {
                if (control.HasControls())
                    if(DisableViewState(control.Controls))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                else
                {
                    if (!viewstateAllowedControls.Contains(control.ID))
                        control.EnableViewState = false;
                    else
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }


        /// <summary>
        /// OnInit event handler.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            
            base.OnInit(e);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Response.Cache.SetAllowResponseInBrowserHistory(true);
            var redirectionFlag = ConfigurationManager.AppSettings["EnableMobileDeviceRedirection"];
            if (!string.IsNullOrEmpty(redirectionFlag))
            {
                bool performRedirection = false;
                bool.TryParse(redirectionFlag, out performRedirection);
                if (performRedirection)
                {
                    if (string.IsNullOrEmpty(Request.QueryString["home"]) &&
                        string.IsNullOrEmpty(Request.QueryString["msl"]) &&
                        CMS.Util.MobileRedirect.Wurfl.Instance.CheckMobileRedirection(Request.UserAgent,
                                                                                      Request.Url.DnsSafeHost,
                                                                                      Request.UrlReferrer == null
                                                                                          ? ""
                                                                                          : Request.UrlReferrer.
                                                                                                DnsSafeHost))
                    {
                        string mobilePath = string.Empty;
                        var regexforIP = new Regex(ConfigurationManager.AppSettings["RegexForIP"]);
                        if (regexforIP.IsMatch(Request.Url.DnsSafeHost))
                        {
                            mobilePath = string.Format("{0}://{1}/mobile/", Request.Url.Scheme,
                                                           Request.Url.DnsSafeHost);
                        }
                        else
                        {
                            if (Request.Url.Port == 80)
                            {
                                mobilePath = string.Format("{0}://{1}/{2}/mobile/", Request.Url.Scheme,
                                                           Request.Url.DnsSafeHost, CurrentPage.LanguageBranch);
                            }
                            else
                            {
                                mobilePath = string.Format("{0}://{1}:{2}/{3}/mobile/", Request.Url.Scheme,
                                                           Request.Url.DnsSafeHost, Request.Url.Port,
                                                           CurrentPage.LanguageBranch);
                            }
                        }
                        var queryStringIndex = Request.RawUrl.IndexOf("?");
                        if (queryStringIndex >= 0)
                        {
                            var queryString = Request.RawUrl.Substring(queryStringIndex);
                            mobilePath = string.Format("{0}{1}", mobilePath, queryString);
                        }
                        Response.Redirect(mobilePath, false);
                        Response.StatusCode = (int) System.Net.HttpStatusCode.MovedPermanently;
                        Context.ApplicationInstance.CompleteRequest();
                    }
                }
            }
        }

        /// <summary>
        /// OnLoad event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            if(DisableViewState(Controls))
                Page.EnableViewState = false;

            base.OnLoad(e);

            

            BE.Visible = OWSVisibilityControl.BookingModuleShouldBeVisible;
            AlternativeBookingModule.Visible = !BE.Visible;
        }
    }
}