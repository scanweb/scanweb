//  Description					: SMSManager                                              //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
//	Version	#					:   													  //
//----------------------------------------------------------------------------------------//
//  Revison History				:                                                         //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.IO;
using System.Net;
using Scandic.Scanweb.ExceptionManager;

namespace Scandic.Scanweb.CMS.Util.SMSManager
{
    /// <summary>
    /// Class handles SMS Communication
    /// </summary>
    public class SMSManager
    {
        private string mobilePNumber = "";
        private string messageInternal = "";
        private static Random randomNumberGenerator = new Random();

        /// <summary>
        /// Contains errors that caused SendSMS() to return false
        /// </summary>
        public string errMessage = "";

        private int length;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="mobilePhoneNumber">Recipients mobile phone number</param>
        /// <param name="message">Message that is sent to the recipient</param>
        public SMSManager(string mobilePhoneNumber, string message)
        {
            length = Convert.ToInt32(ConfigurationManager.AppSettings["SMSLength"]);
            mobilePNumber = mobilePhoneNumber;
            messageInternal = message;
        }

        /// <summary>
        /// Sends the SMS
        /// </summary>
        /// <returns>True if the message was sent without problems.</returns>
        public bool SendSMS()
        {
            if (messageInternal.Length > length)
                throw new SMSManagerException("Message is to long:" + messageInternal.Length.ToString() +
                                              ". Max Length is " + length.ToString() + ".");

            HttpPost(ConfigurationManager.AppSettings["SMSTarget"].ToString(), BuildParameters());

            return true;
        }

        /// <summary>
        /// Builds the parameters that are sent to the SMS Supplier
        /// </summary>
        private string BuildParameters()
        {
            int randomNumber = randomNumberGenerator.Next();

            string parameters = "triggerid=" + ConfigurationManager.AppSettings["SMSTriggerid"].ToString();
            parameters = parameters + "&user=" + ConfigurationManager.AppSettings["SMSUser"].ToString();
            parameters = parameters + "&pass=" + ConfigurationManager.AppSettings["SMSPass"].ToString();
            parameters = parameters + "&customerid=" + ConfigurationManager.AppSettings["SMSCustomerid"].ToString();
            parameters = parameters + "&originalid=" + randomNumber.ToString();
            parameters = parameters + "&mobilephone=" + mobilePNumber;
            parameters = parameters + "&prop_10001=" + messageInternal.Replace("+", "%2B").Replace("&", "%26");
            return parameters;
        }

        /// <summary>
        /// Sends the message via HTTP Post and checks the response.
        /// </summary>
        private static void HttpPost(string url, string parameters)
        {
            string responseString = string.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                Stream stream = request.GetRequestStream();

                using (StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.Default))
                {
                    writer.Write(parameters);
                }
                stream.Close();

                WebResponse response = request.GetResponse();

                StreamReader responseStream = new StreamReader(response.GetResponseStream());
                responseString = responseStream.ReadToEnd();
                responseStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                throw new SMSManagerException("Failed to send request to service provicer. Reason: " + ex.ToString());
            }

            if (!responseString.Contains("OK"))
                throw new SMSManagerException("Request sent to service provider, but no OK response. Response: " +
                                              responseString);
        }
    }
}