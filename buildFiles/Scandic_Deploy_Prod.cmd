@ echo deploying Website in IIS... Please wait...


@echo off

set TimeStamp=%time%

if "%time:~0,1%"==" " set TimeStamp=0%time:~1%

set TimeStamp=%TimeStamp:~0,5%

set Create_dir_name=%date:/=-%-%TimeStamp::=-%


 
SET PathToExeToUnzip="D:\ftproot\DeploymentSettingDoNotDelete\7za920"

set myPath=%cd%
pushd ..
set parentPath=%cd%
popd

call set myDir=%%myPath:%parentPath%\=%%

echo %myDir%
set FolderLocationToUnzip=%myDir:~22%


@echo ________________________**********_______________________ 

set /p UserInputVirtualDir=Please enter path of website to backup before deployment:
set /p UserInputBackupPath=Please enter path where to backup website before deployment:


@ echo taking backup of previous deployment...


md %UserInputBackupPath%\%Create_dir_name%



xcopy /e %UserInputVirtualDir%\*.* %UserInputBackupPath%\%Create_dir_name%


@ echo unzipping the deployment packet...

echo %PathToExeToUnzip%\7za.exe x %FolderLocationToUnzip%.zip -o%FolderLocationToUnzip%

%PathToExeToUnzip%\7za.exe x %FolderLocationToUnzip%.zip -o%FolderLocationToUnzip%

@ echo Deploying the unzipped items to Virtual Directory

xcopy /e %FolderLocationToUnzip%\*.* %UserInputVirtualDir% /Y


@ echo Deployment finished...
IISRESET
pause