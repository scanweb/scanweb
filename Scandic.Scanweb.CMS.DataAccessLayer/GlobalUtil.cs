//  Description					: GlobalUtil                                	 		  //
//																						  //
//----------------------------------------------------------------------------------------//
// Author						:                                                         //
// Author email id				:                           							  //
// Creation Date				:                                                         //
//	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
// Revison History				: -NA-													  //
// Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using EPiServer;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.DataAccessLayer
{
    /// <summary>
    /// Contains some common data access methods
    /// </summary>
    public class GlobalUtil
    {
        /// <summary>
        /// Gets the URL to a specific page
        /// </summary>
        /// <param name="dynamicPropertyName">The name of the dynamic property on the root page that holds the page reference</param>
        /// <returns>The root-relative path and query of the URL for the page</returns>
        public static string GetUrlToPage(string dynamicPropertyName)
        {
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference pageLink = rootPage[dynamicPropertyName] as PageReference;

            if (PageReference.IsNullOrEmpty(pageLink))
                return string.Empty;
            else
            {
                PageData referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);

                UrlBuilder url = new UrlBuilder(referencedPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, referencedPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                return url.ToString();
            }
        }

        /// <summary>
        /// Gets PageID
        /// </summary>
        /// <param name="dynamicPropertyName"></param>
        /// <returns></returns>
        public static int GetPageID(string dynamicPropertyName)
        {
            int retval = 0;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference pageLink = rootPage[dynamicPropertyName] as PageReference;

            if (pageLink != null)
            {
                retval = pageLink.ID;
            }
            return retval;
        }


        /// <summary>
        /// Gets the URL to a specific page
        /// </summary>
        /// <param name="pageLink">The page reference for the page</param>
        /// <returns>The root-relative path and query of the URL for the page</returns>
        public static string GetUrlToPage(PageReference pageLink)
        {
            if (PageReference.IsNullOrEmpty(pageLink))
                return string.Empty;
            else
            {
                PageData referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);

                UrlBuilder url = new UrlBuilder(referencedPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, referencedPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                return url.ToString();
            }
        }

        /// <summary>
        /// Gets the URL to a specific page. If PageReference is null or empty
        /// it would return Default Help Page which would show the information 
        /// "No Help Available"
        /// </summary>
        /// <param name="pageLink">page reference</param>
        /// <returns>The root-relative path and query of the URL for the page</returns>
        public static string GetHelpPage(PageReference pageLink)
        {
            if (PageReference.IsNullOrEmpty(pageLink))
            {
                return GetUrlToPage("NoHelpPage");
            }
            else
            {
                PageData referencedPage = DataFactory.Instance.GetPage(pageLink, EPiServer.Security.AccessLevel.NoAccess);

                UrlBuilder url = new UrlBuilder(referencedPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, referencedPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                return url.ToString();
            }
        }

        /// <summary>
        /// Gets the property value of a specific EPiServer page
        /// </summary>
        /// <param name="page">Page to get the property from. This must be an EPiServer TemplatePage</param>
        /// <param name="PropertyName">Property Name</param>
        /// <returns>The string value of the property</returns>
        public static string GetPagePropertyValue(System.Web.UI.Page page, string propertyName)
        {
            TemplatePage templatePage = (TemplatePage)page;
            try
            {
                return templatePage.CurrentPage.Property[propertyName].Value.ToString();
            }
            catch (NullReferenceException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Takes the key and returns the text in current language
        /// </summary>
        /// <param name="key">A simplified XPath expression</param>
        /// <returns>The translated string</returns>
        public static string GetTranslateText(string key)
        {
            return LanguageManager.Instance.Translate(key);
        }


        public static bool ValidateCMSFieldLength(PageValidateEventArgs e, string cmsPropertyName, int length)
        {
            if (e.Page != null && e.Page[cmsPropertyName] != null)
            {
                if (Convert.ToString(e.Page[cmsPropertyName]).Length > length)
                {
                    e.IsValid = false;
                    if (string.IsNullOrEmpty(e.ErrorMessage))
                    {
                        e.ErrorMessage = string.Format("Page {0} Should be less than {1} Characters",cmsPropertyName,length.ToString());
                    }
                    else
                    {
                        e.ErrorMessage = e.ErrorMessage + "<li> " + string.Format("Page {0} Should be less than {1} Characters", cmsPropertyName, length.ToString());
                    }
                }
            }
            return e.IsValid;
        }

        public static bool ValidateCMSImageCarousal(PageValidateEventArgs e, string cmsPropertyName, int imageCount, int HotelOverViewPageId,
            bool CheckForImageCountOtherTemplates, int StandardPageId)
        {
            if (e.Page[cmsPropertyName] != null && bool.Parse(e.Page[cmsPropertyName].ToString()) && e.Page["Width"] == null && e.Page.PageTypeID == StandardPageId)
            {
                e.IsValid = false;
                e.ErrorMessage = "Select image width for carousel";
            }



            if (e.Page.PageTypeID == HotelOverViewPageId && e.Page[cmsPropertyName] != null && bool.Parse(e.Page[cmsPropertyName].ToString()))
            {
                if (imageCount == 0)
                {
                    e.IsValid = false;
                    e.ErrorMessage = "Please select an image for Image carousel, as Activate Image Carousel is checked.";
                }
                else if (imageCount > 10)
                {
                    e.IsValid = false;
                    e.ErrorMessage = "Please select only 10 images for image carousel.";

                }
            }
            else if (e.Page.PageTypeID != HotelOverViewPageId //handle all other templates.
                && e.Page[cmsPropertyName] != null && bool.Parse(e.Page[cmsPropertyName].ToString()))
            {
                if (!CheckForImageCountOtherTemplates)
                {
                    e.IsValid = false;
                    e.ErrorMessage = "Please select an image for Image carousel, as Activate Image Carousel is checked.";
                }

            }
            return e.IsValid;

        }

        public static bool ValidateCMSDuplicateOperaID(PageValidateEventArgs e, string cmsPropertyName, int duplicateOperaIDValue)
        {
            if (e.Page[cmsPropertyName] as string != null)
            {
                if (duplicateOperaIDValue != -1)
                {
                    e.IsValid = false;
                    if (string.IsNullOrEmpty(e.ErrorMessage))
                    {
                        e.ErrorMessage = string.Format
                            ("Page of the same page type and the same OperaID already exists ({0})",
                             duplicateOperaIDValue.ToString());
                    }
                    else
                    {
                        e.ErrorMessage = e.ErrorMessage + "<li> " +
                                         string.Format(
                                             " Page of the same page type and the same OperaID already exists ({0})",
                                             duplicateOperaIDValue.ToString()) + "</li>";
                    }
                }
            }
            return e.IsValid;
        }
    }
}