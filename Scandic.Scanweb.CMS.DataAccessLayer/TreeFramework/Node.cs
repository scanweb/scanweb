//  Description					: Node                                                    //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                                                         //
//	Version	#					: 1.0													  //
// ---------------------------------------------------------------------------------------//
//  Revison History				:   													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.DataAccessLayer
{
    /// <summary>
    /// This class represents Node entity.
    /// </summary>
    public class Node : INode
    {
        private PageData pageData;
        private List<Node> childPage = new List<Node>();

        /// <summary>
        /// Takes only the page data and list of child nodes.
        /// This constructor should be used as a composite node where the node have some leaf nodes.
        /// </summary>
        /// <param name="pd">PageData</param>
        /// <param name="childPage">list of child nodes</param>
        public Node(PageData pd)
        {
            this.pageData = pd;
        }

        public Node()
        {
        }

        /// <summary>
        /// Add this children.
        /// </summary>
        /// <param name="node">The children which need to be added.</param>
        public void AddChild(Node node)
        {
            childPage.Add(node);
        }


        /// <summary>
        /// Remove this children.
        /// </summary>
        /// <param name="node">The children which need to be removed.</param>
        public void RemoveChild(Node node)
        {
            childPage.Remove(node);
        }

        /// <summary>
        /// Get all the childrens.
        /// </summary>
        /// <returns>List of childrens</returns>
        public List<Node> GetChildren()
        {
            return childPage;
        }

        /// <summary>
        /// Get page data for this node.
        /// </summary>
        /// <returns>Page data of this node</returns>
        public PageData GetPageData()
        {
            return this.pageData;
        }
    }
}