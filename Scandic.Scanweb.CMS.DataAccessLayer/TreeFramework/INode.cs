using System.Collections.Generic;
using EPiServer.Core;

namespace Scandic.Scanweb.CMS.DataAccessLayer
{
    /// <summary>
    /// INode
    /// </summary>
    internal interface INode
    {
        void AddChild(Node node);
        void RemoveChild(Node node);
        List<Node> GetChildren();
        PageData GetPageData();
    }
}