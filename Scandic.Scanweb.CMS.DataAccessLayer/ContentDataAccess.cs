//  Description					:   HotelDestinationOpeningDateComparer                   //
//																						  //
//----------------------------------------------------------------------------------------//
//  Author						:                                                         //
//  Author email id				:                           							  //
//  Creation Date				:                   									  //
// 	Version	#					:                   									  //
//----------------------------------------------------------------------------------------//
//  Revison History				:   													  //
// 	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.ExceptionManager;
using log4net;
using System.Text.RegularExpressions;
using Scandic.Scanweb.Core.Core;

namespace Scandic.Scanweb.CMS.DataAccessLayer
{
    /// <summary>
    /// This will compare the opening date for the Hotels.
    /// </summary>
    public class HotelDestinationOpeningDateComparer : IComparer<HotelDestination>
    {
        #region IComparer<HotelDestination> Members

        /// <summary>
        /// Compare opening Date for each Hotels.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(HotelDestination x, HotelDestination y)
        {
            return x.CompareTo(y);
        }

        #endregion
    }

    /// <summary>
    /// Provides access to booking related content in CMS
    /// </summary>
    public class ContentDataAccess
    {
        private const string HotelDestinationsWithAllHotels = "HotelDestinationsWithAllHotels{0}";
        private const string CityDestinationsforTopCity = "CityDestinationsforTopCity{0}";
        private const string CityDestinationsforAllCity = "CityDestinationsforAllCity{0}";
        private const string AllCitiesAndHotelCacheKey = "CDALAllCitiesAndHotel{0}{1}";
        private const string DestinationsFromTheLastSixMonthscityListCacheKey = "DestinationsFromTheLastSixMonthscityListCacheKey{0}{1}";
        private const string AllCountryCitiesAndHotelCacheKey = "CDALAllCountryCitiesAndHotel{0}";
        private const string AllCountryCitiesAndBookableHotelCacheKey = "CDALAllCountryCitiesAndBookableHotel{0}";
        private const string AllNonBookableCitiesAndHotelCacheKey = "AllNonBookableCitiesAndHotelCacheKey{0}";
        private const string HotelDestinationsWithCityID = "HotelDestinationsWithCityID{0}{1}{2}";
        private const string AlternativeCityHotelsWithCityID = "AlternativeCityHotelsWithCityID{0}{1}";
        private const string PageListCacheKey = "CDALPageListWithPageTypeID{0}{1}";
        private const string RedemptionPageListCacheKey = "CDALPageListWithPageTypeID{0}{1}{2}";
        private const string HotelDestinationCacheKey = "CDALHotelDestinationWithOperaID{0}{1}";
        private const string HotelPageDataCacheKey = "CDALHotelPageDataWithOperaID{0}{1}";
        private const string RoomTypeCategoryMapCacheKey = "CDALRoomTypeCategoryMap{0}";
        private const string RateTypeCategoryMapCacheKey = "CDALRateTypeCategoryMap{0}";
        private const string AllTransactionsCacheKey = "CDALAllTransactions{0}";
        private const string ConfirmationTexts = "CDALConfirmationTexts{0}";
        private const string RedemptionTexts = "CDALRedemptionTexts{0}{1}";
        private const string MerchantProfileByHotelID = "MerchantProfileByHotelID{0}{1}";
        private const string CityPageDataCacheKey = "CDALCityPageDataWithOperaID{0}{1}";
        private const string AllBlockPages = "CDALBlockPages{0}";
        private const string HYPHEN = "-";
        private const string CountryDestinationCacheKey = "CDALAllCountries{0}{1}";
        private const string AllCountryCacheKey = "AllCountries{0}";
        private const string BookingTypeKey = "BookingTypeKey{0}{1}";
        public const string DEFAULT = "Default";
        public const string COMINGSOON_VALUE = "ComingSoon";
        public const string RECENTLYOPENED_VALUE = "RecentlyOpened";
        public const string ImagePart = "Image";
        public const string ImageTitlePart = "ImageTitle";
        public const string ImageDescriptionPart = "ImageDescription";
        private static string currentLanguageString
        {
            get { return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name; }
        }

        private const string UserInterestsCacheKey = "CDALUserInterestCacheKey{0}";
        private const string CountryCitiesAndHotelCacheKey = "CDALAllCountryCitiesAndHotel{0}{1}";
        private const string FileNotFoundCacheKey = "CDALFileNotFound";
        private const string ContainerPageTypeWithGUIDCacheKey = "ContainerPageTypeWithGUID-{0}";
        private const string PageDataCacheKey = "PageDataForPageLinkID-{0}";

        public delegate ReturnT Func<T1, T2, T3, T4, T5, ReturnT>(T1 arg1,
                                         T2 arg2, T3 arg3, T4 arg4, T5 arg5);
        #region Publilc methods

        #region Hotels and destinations

        /// <summary>
        /// Returns the valid OperaID if there exists a hotel or city with a specified OperaID (special chars replaced)
        /// </summary>
        /// <param name="destinationID">DestinationID</param>
        /// <returns>DestinationID</returns>
        public static string GetValidDestinationID(string destinationID)
        {
            List<CityDestination> destinations = GetCityAndHotel(true);

            foreach (CityDestination city in destinations)
            {
                if (ReplaceSpecialChars(city.OperaDestinationId.Trim()).ToUpper() ==
                    ReplaceSpecialChars(destinationID.Trim()).ToUpper())
                    return city.OperaDestinationId;
                foreach (HotelDestination hotel in city.SearchedHotels)
                {
                    if (ReplaceSpecialChars(hotel.OperaDestinationId.Trim().ToUpper()) ==
                        ReplaceSpecialChars(destinationID.Trim()).ToUpper())
                        return hotel.OperaDestinationId;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Returns the valid CountryCode if there exists a country with a specified CountryCode (special chars replaced)
        /// </summary>
        /// <param name="destinationID">DestinationID</param>
        /// <returns>countryCode</returns>
        public static string GetValidCountryCode(string destinationID)
        {
            string countryCode = string.Empty;
            PageData country = GetCountryByCountryCode(destinationID);
            if (country != null)
            {
                if (country["CountryCode"] != null)
                {
                    countryCode = country["CountryCode"].ToString();
                }
            }
            return countryCode;
        }

        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public static List<CityDestination> GetCityAndHotel(bool ignoreBookingVisibility)
        {
            return GetCityAndHotel(ignoreBookingVisibility, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static PageData GetPageData(int pageId, string language)
        {
            PageReference pageReference = new PageReference(pageId);

            PageData pageData = null;
            ILanguageSelector languageSelector = new LanguageSelector(language);
            pageData = DataFactory.Instance.GetPage(pageReference, languageSelector);
            return pageData;
        }

        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public static List<CityDestination> GetCityAndHotelForAutoSuggest(bool ignoreBookingVisibility)
        {
            return GetCityAndHotelForAutoSuggest(ignoreBookingVisibility, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <param name="removeChildrens"></param>
        /// <returns></returns>
        public static List<Scandic.Scanweb.Core.CountryDestinatination> GetCountryCityAndHotelForAutoSuggest(bool ignoreBookingVisibility, bool removeChildrens)
        {
            return GetCountryCityAndHotelForAutoSuggest(ignoreBookingVisibility, removeChildrens, false, string.Empty);
        }

        /// <summary>
        /// Fetches all destinations with non bookable status.
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        public static List<CityDestination> FetchAllDestinationsWithNonBookable(bool ignoreBookingVisibility)
        {
            return FetchAllDestinationsWithNonBookable(ignoreBookingVisibility, false);
        }

        #region R1.4 Marketting City CR

        /// <summary>
        /// Get all hotels configured under a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all hotels configured under a particular city from CMS
        /// </summary>
        /// <param name="cityCode">Unique code for city to be passed-OperaId</param>
        /// <param name="fetchAllHotels">Fetch All bookable and non bookable hotels-As per param passed</param>
        /// <returns>Return a list of hotels from a particular city</returns>
        private static List<HotelDestination> GetAllHotels(string cityCode, BookableNonBookableHotels bookableHotels)
        {
            return GetAllHotels(cityCode, bookableHotels, false);
        }

        /// <summary>
        /// Returs the List of all Bookable hotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public static List<HotelDestination> GetBookableHotels(string cityCode)
        {
            List<HotelDestination> bookableHotelsList = GetAllHotels(cityCode, BookableNonBookableHotels.BOOKABLE_HOTELS);
            return bookableHotelsList;
        }

        /// <summary>
        /// Returns the list of all Hotels (Bookable and non bookable)
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public static List<HotelDestination> GetAllTypesofHotels(string cityCode)
        {
            List<HotelDestination> bookableHotelsList = GetAllHotels(cityCode, BookableNonBookableHotels.BOOKABLE_HOTELS);
            List<HotelDestination> nonBookableHotelsList = GetAllHotels(cityCode,
                                                                        BookableNonBookableHotels.NONBOOKABLE_HOTELS);

            List<HotelDestination> allHotels = new List<HotelDestination>();
            allHotels.AddRange(bookableHotelsList);
            allHotels.AddRange(nonBookableHotelsList);

            return allHotels;
        }

        /// <summary>
        /// Get all hotels configured from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all hotels configured under a particular city from CMS
        /// </summary>
        /// <returns>Return a list of hotels from a particular city</returns>
        public static List<HotelDestination> GetAllHotels()
        {
            return GetAllHotels(false);
        }

        /// <summary>
        ///  Get all hotels configured from CMS
        ///  Hygiene | 1.8.2 | Also used for New Site Foot to get all new hotels configured for site Foot
        /// </summary>
        /// <returns>Return a list of hotels</returns>
        private static List<HotelDestination> CreateHotelDestinationList()
        {
            PageDataCollection cityResults = new PageDataCollection();
            if (ConfigurationManager.AppSettings["LimitCDALResults"] == null)
            {
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                cityResults = GetPagesWithPageTypeID(cityPageTypeID, countryContainer);
            }

            FilterSort sorter = new FilterSort(FilterSortOrder.Alphabetical);
            sorter.Sort(cityResults);

            RemoveNonPublishedPages(cityResults);

            List<HotelDestination> hotels = new List<HotelDestination>();

            foreach (PageData hotelPage in cityResults)
            {
                if (hotelPage["HotelCategory"] != null)
                {
                    if (hotelPage["HotelCategory"].ToString().Equals(COMINGSOON_VALUE.ToString()) ||
                        hotelPage["HotelCategory"].ToString().Equals(RECENTLYOPENED_VALUE.ToString()))
                    {
                        HotelDestination hotel =
                            GetHotelDestinationWithHotelName(hotelPage["Heading"] as string ?? string.Empty);
                        PageData hotelPageData = GetHotelPageData(hotelPage["Heading"] as string ?? string.Empty);
                        string hotelPageURL = string.Empty;
                        if (hotelPageData != null)
                        {
                            if (hotelPageData["PromotionalPageLink"] != null)
                            {
                                PageReference offerPageReference = hotelPageData["PromotionalPageLink"] as PageReference;
                                if (offerPageReference != null)
                                {
                                    PageData offerPageLink = DataFactory.Instance.GetPage(offerPageReference,
                                                                                          EPiServer.Security.AccessLevel
                                                                                              .NoAccess);
                                    if (offerPageLink != null)
                                    {
                                        UrlBuilder url = new UrlBuilder(offerPageLink.LinkURL);
                                        EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, offerPageLink.LinkURL,
                                                                                              System.Text.UTF8Encoding.
                                                                                                  UTF8);
                                        hotelPageURL = url.ToString();
                                        hotel.HotelPageURL = hotelPageURL;
                                    }
                                }
                            }
                            if (hotelPageData["OpeningDate"] != null)
                                hotel.OpeningDate = Convert.ToDateTime(hotelPageData["OpeningDate"]);
                            if (hotelPageData["HotelCategory"] != null)
                                hotel.HotelCategory = hotelPage["HotelCategory"].ToString();
                            if (hotelPageData["IgnoreOpeningDay"] != null)
                                hotel.IgnoreOpeningDate = (bool)hotelPage["IgnoreOpeningDay"];
                            if (hotelPageData["DisplayInSiteFooter"] != null)
                                hotel.IsDisplayedInFooter = Convert.ToBoolean(hotelPageData["DisplayInSiteFooter"]);
                            if (hotelPageData["FooterNewHotelsSortIndex"] != null)
                                hotel.FooterNewHotelsSortIndex =
                                    Convert.ToInt32(hotelPageData["FooterNewHotelsSortIndex"]);
                        }

                        PageData hotelContainer = DataFactory.Instance.GetPage(hotelPage.ParentLink,
                                                                               EPiServer.Security.AccessLevel.NoAccess);
                        PageData city = null;
                        if (hotelContainer != null)
                        {
                            city = DataFactory.Instance.GetPage(hotelContainer.ParentLink,
                                                                EPiServer.Security.AccessLevel.NoAccess);
                        }
                        PageData cityContainer = null;
                        if (city != null)
                        {
                            cityContainer = DataFactory.Instance.GetPage(city.ParentLink,
                                                                         EPiServer.Security.AccessLevel.NoAccess);
                        }

                        string countrycode = string.Empty;
                        if (cityContainer != null)
                        {
                            if (cityContainer["CountryCode"] != null)
                            {
                                countrycode = cityContainer["CountryCode"].ToString();
                            }
                        }
                        hotel.CountryCode = countrycode;
                        hotels.Add(hotel);
                    }
                }
            }
            if (hotels.Count > 0)
            {
                hotels.Sort(new HotelDestinationOpeningDateComparer());
            }
            return hotels;
        }

        /// <summary>
        /// Get all hotels configured from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all hotels configured under a particular city from CMS
        /// </summary>
        /// <returns>Return a list of hotels from a particular city</returns>
        public static List<DestinationInfo> GetAllDestinations(bool isAllCityList)
        {
            List<DestinationInfo> cityList = null;
            if (isAllCityList)
            {
                string cacheKey = string.Format(CityDestinationsforTopCity, currentLanguageString);
                cityList = GetAllDestinationCity(cacheKey, true);
            }
            else
            {
                string cacheKeyforAllCity = string.Format(CityDestinationsforAllCity, currentLanguageString);
                cityList = GetAllDestinationCity(cacheKeyforAllCity, false);
            }
            return cityList;
        }

        /// <summary>
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="isAllCityList"></param>
        /// <returns></returns>
        private static List<DestinationInfo> GetAllDestinationCity(string cacheKey, bool isAllCityList)
        {
            return GetAllDestinationCity(cacheKey, isAllCityList, false);
        }

        /// <summary>
        ///  Get all hotels configured from CMS
        ///  Reservation | 2.0 | Used in Booking module flyout page.
        /// </summary>
        /// <returns>Return a list of hotels</returns>
        private static PageDataCollection CreateAllDestinationsList(bool isAllCityList)
        {
            PageDataCollection cityResults = new PageDataCollection();
            if (ConfigurationManager.AppSettings["LimitCDALResults"] == null)
            {
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                cityResults = GetPagesWithPageTypeID(cityPageTypeID, countryContainer);
            }

            FilterSort sorter = new FilterSort(FilterSortOrder.Alphabetical);
            sorter.Sort(cityResults);

            RemoveNonPublishedPages(cityResults);


            return cityResults;
        }

        /// <summary>
        /// </summary>
        /// <param name="cityList"></param>
        /// <param name="isAllCityList"></param>
        /// <returns></returns>
        private static List<DestinationInfo> GetTopDestinationCity(PageDataCollection cityList, bool isAllCityList)
        {
            List<DestinationInfo> cityDestinations = new List<DestinationInfo>();
            if (cityList != null && isAllCityList)
            {
                foreach (PageData cityPage in cityList)
                {
                    if (cityPage["PageName"] != null)
                    {
                        if (cityPage["TopDestination"] != null)
                        {
                            DestinationInfo destInfo = new DestinationInfo();
                            destInfo.DestinationName = cityPage["PageName"] as string;
                            destInfo.OperaID = cityPage["OperaID"] as string;
                            cityDestinations.Add(destInfo);
                        }
                    }
                }
            }
            else
            {
                foreach (PageData cityPage in cityList)
                {
                    if (cityPage["PageName"] != null)
                    {
                        DestinationInfo destInfo = new DestinationInfo();
                        destInfo.DestinationName = cityPage["PageName"] as string;
                        destInfo.OperaID = cityPage["OperaID"] as string;
                        cityDestinations.Add(destInfo);
                    }
                }
            }
            return cityDestinations;
        }

        private static PageData GetHotelPageData(string hotelName)
        {
            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

            PageDataCollection hotelPages = GetPagesWithPageTypeID(hotelPageTypeID, countryContainer);

            FilterCompareTo nameFilter = new FilterCompareTo("Heading", hotelName);
            nameFilter.Condition = CompareCondition.Equal;
            nameFilter.Filter(hotelPages);

            RemoveNonPublishedPages(hotelPages);

            PageData hotelPageData = new PageData();


            if (hotelPages.Count > 0)
                hotelPageData = hotelPages[0];
            return hotelPageData;
        }

        /// <summary>
        ///  Get all hotels configured under a particular city from CMS
        ///  This will give All hotels 
        /// 1.(Only non bookable if BookableNonBookableHotels.BOOKABLE_AND_NONBOOKABLE_HOTELS is passed)/
        /// 2.(Only bookable hotels if BookableNonBookableHotels.BOOKABLE_HOTELS is passed)
        /// </summary>
        /// <param name="cityCode">Unique code for city to be passed-OperaId</param>
        /// <param name="bookableHotels">Fetch hotels as per param passed</param>
        /// <returns>Return a list of hotels from a particular city</returns>
        private static List<HotelDestination> CreateHotelDestinationList(string cityCode,
                                                                         BookableNonBookableHotels bookableHotels)
        {
            PageDataCollection cityResults = new PageDataCollection();
            if (ConfigurationManager.AppSettings["LimitCDALResults"] == null)
            {
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                cityResults = GetPagesWithPageTypeID(cityPageTypeID, countryContainer);
            }

            FilterSort sorter = new FilterSort(FilterSortOrder.Alphabetical);
            sorter.Sort(cityResults);

            RemoveNonPublishedPages(cityResults);

            List<HotelDestination> hotels = new List<HotelDestination>();

            foreach (PageData cityPage in cityResults)
            {
                string operaCityID = (cityPage["OperaID"] as string).Trim();

                if (operaCityID == cityCode)
                {
                    string countryCode = GetCountryCode(cityPage);
                    PageDataCollection containerPages = DataFactory.Instance.GetChildren(cityPage.PageLink);
                    int hotelContainerPageTypeID = PageType.Load(
                        new Guid(ConfigurationManager.AppSettings["HotelContainerPageTypeGUID"])).ID;

                    foreach (PageData containerPage in containerPages)
                    {
                        if (containerPage.PageTypeID == hotelContainerPageTypeID)
                        {
                            PageDataCollection hotelPages = DataFactory.Instance.GetChildren(containerPage.PageLink);
                            RemoveNonPublishedPages(hotelPages);
                            hotels = new List<HotelDestination>();

                            foreach (PageData hotelPage in hotelPages)
                            {
                                if ((bookableHotels.Equals(BookableNonBookableHotels.NONBOOKABLE_HOTELS)
                                     && (hotelPage["AvailableInBooking"] == null)) ||
                                    ((bookableHotels.Equals(BookableNonBookableHotels.BOOKABLE_HOTELS))
                                     && (hotelPage["AvailableInBooking"] != null)))
                                {
                                    string hotelSearchCode = hotelPage["Heading"] as string ?? string.Empty;

                                    if (!string.IsNullOrEmpty(hotelSearchCode))
                                    {
                                        try
                                        {
                                            HotelDestination hotel = GetHotelDestinationWithHotelName(hotelSearchCode);
                                            hotel.CountryCode = countryCode;
                                            hotels.Add(hotel);
                                        }
                                        catch (ContentDataAccessException cdaException)
                                        {
                                            AppLogger.LogFatalException(cdaException, string.Format("Exception occured while fetching the hotel- {0}", hotelSearchCode));
                                        }

                                    }
                                }
                            }
                            return hotels;
                        }
                    }
                    return hotels;
                }
            }
            return hotels;
        }

        /// <summary>
        /// This method returns 2 character country code of a particular city
        /// </summary>
        /// <param name="cityPage">cityPage</param>
        /// <returns>2 character country code</returns>
        private static string GetCountryCode(PageData cityPage)
        {
            PageData country = DataFactory.Instance.GetPage(cityPage.ParentLink, EPiServer.Security.AccessLevel.NoAccess);
            if (country != null)
            {
                if (country["CountryCode"] != null)
                {
                    return country["CountryCode"].ToString();
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// This will return city name of the hotel by passing parentlink ID.
        /// </summary>
        /// <param name="parentPage">PageData</param>
        /// <param name="operaId">operaId for city</param>
        /// <returns>City name of the hotel</returns>
        public static string GetCityName(PageData parentPage, out string operaId)
        {
            operaId = string.Empty;
            string cityName = string.Empty;
            PageData cityPage = DataFactory.Instance.GetPage(parentPage.ParentLink,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            if (cityPage != null)
            {
                PageData cityPageData = DataFactory.Instance.GetPage(cityPage.ParentLink,
                                                                     EPiServer.Security.AccessLevel.NoAccess);
                if (cityPageData != null)
                {
                    if (cityPageData["PageName"] != null)
                    {
                        operaId = cityPageData["OperaID"].ToString();
                        cityName = cityPageData["PageName"].ToString();
                    }
                }
            }
            return cityName;
        }

        /// <summary>
        /// Get all alternative city hotels for a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all alternative city hotels for a particular city from CMS
        /// </summary>
        /// <returns>Return a list of  alternative city hotels for a  particular city</returns>
        public static List<HotelDestination> GetAlternateCityHotels(string cityCode)
        {
            return GetAlternateCityHotels(cityCode, false);
        }

        /// <summary>
        /// Get all alternative city hotels for a particular city from CMS
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns>List of alternative city hotels for a particular city</returns>
        private static List<HotelDestination> CreateAlternateCityHotelList(string cityCode)
        {
            PageDataCollection cityResults = new PageDataCollection();

            if (ConfigurationManager.AppSettings["LimitCDALResults"] == null)
            {
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                cityResults = GetPagesWithPageTypeID(cityPageTypeID, countryContainer);
            }

            RemoveNonPublishedPages(cityResults);

            List<HotelDestination> hotels = new List<HotelDestination>();

            foreach (PageData cityPage in cityResults)
            {
                string operaCityID = (cityPage["OperaID"] as string).Trim();

                if (operaCityID == cityCode)
                {
                    int alternativeCityContainerPageTypeGUID = PageType.Load(
                        new Guid(ConfigurationManager.AppSettings["AlternativeCityContainerPageTypeGUID"])).ID;
                    PageDataCollection containerPages = DataFactory.Instance.GetChildren(cityPage.PageLink);

                    foreach (PageData containerPage in containerPages)
                    {
                        if (containerPage.PageTypeID == alternativeCityContainerPageTypeGUID)
                        {
                            PageDataCollection alternativeCityPages =
                                DataFactory.Instance.GetChildren(containerPage.PageLink);
                            RemoveNonPublishedPages(alternativeCityPages);
                            hotels = new List<HotelDestination>();
                            foreach (PageData alternativeCityPage in alternativeCityPages)
                            {
                                if (alternativeCityPage["AlternativeCityPage"] != null)
                                {
                                    PageData city = DataFactory.Instance.GetPage(
                                        (PageReference)alternativeCityPage["AlternativeCityPage"],
                                        EPiServer.Security.AccessLevel.NoAccess);
                                    string cityOperaID = city["OperaID"].ToString().Trim();

                                    List<HotelDestination> hotelDestinations = GetAllHotels(cityOperaID,
                                                                                            BookableNonBookableHotels.
                                                                                                BOOKABLE_HOTELS);
                                    if (hotelDestinations != null && hotelDestinations.Count > 0)
                                    {
                                        string distance = string.Empty;
                                        string direction = string.Empty;
                                        string drivingTime = string.Empty;
                                        if (alternativeCityPage["Distance"] != null)
                                        {
                                            distance = alternativeCityPage["Distance"].ToString().Trim();
                                        }
                                        if (alternativeCityPage["Direction"] != null)
                                        {
                                            direction = alternativeCityPage["Direction"].ToString().Trim();
                                        }
                                        if (alternativeCityPage["Driving Time"] != null)
                                        {
                                            drivingTime = alternativeCityPage["Driving Time"].ToString().Trim();
                                        }


                                        List<HotelDestination> hotelDests = new List<HotelDestination>();

                                        foreach (HotelDestination hotelDestination in hotelDestinations)
                                        {
                                            HotelDestination hotelDest = hotelDestination.Clone() as HotelDestination;

                                            hotelDest.AltCityDistance = distance;
                                            hotelDest.AltCityDirection = direction;
                                            hotelDest.AltCityDrivingTime = drivingTime;
                                            hotelDests.Add(hotelDest);
                                        }
                                        hotels.AddRange(hotelDests);
                                    }
                                }
                            }
                            return hotels;
                        }
                    }
                    return hotels;
                }
            }
            return hotels;
        }

        #endregion R1.4

        /// <summary>
        /// Get all Transactions
        /// </summary>
        /// <returns>Return a list of all Transactions</returns>
        public static List<Transaction> GetAllTransactions()
        {
            return GetAllTransactions(false);
        }

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID. 
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public static HotelDestination GetHotelByOperaID(string hotelOperaID, string language)
        {
            ContentDataAccess.LogMessage(string.Format("GetHotelByOperaID hotelOperaID:" + hotelOperaID));

            return GetHotelDestination(hotelOperaID, language);
        }

        /// <summary>
        /// Gets the Hotel based on the specified Opera ID. 
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public static HotelDestination GetHotelByOperaID(string hotelOperaID)
        {
            return GetHotelByOperaID(hotelOperaID, string.Empty);
        }


        /// <summary>
        /// Gets the Country based on the specified countryCode. 
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>Returns the Hotel object with the specified Opera ID. </returns>
        public static string GetCountryNamebyCountryID(string countryCode)
        {
            ContentDataAccess.LogMessage(string.Format("GetCountryNamebyCountryID countryCode:" + countryCode));

            string CountryName = string.Empty;
            PageData countryPageData = GetCountryByCountryCode(countryCode);
            if (null != countryPageData)
            {
                if (countryPageData.PageName != null)
                {
                    CountryName = countryPageData.PageName.ToString();
                }
            }

            return CountryName;
        }

        public static bool GetVATVisibilityForCountry(string countryCode)
        {
            bool hideVAT = false;
            PageData countryPageData = GetCountryByCountryCode(countryCode);
            if (countryPageData != null)
            {
                hideVAT = countryPageData["HideVAT"] != null ? Convert.ToBoolean(countryPageData["HideVAT"]) : false;
            }
            return hideVAT;
        }
        /// <summary>
        /// Gets the of Hotels that match the specified Opera IDs.
        /// </summary>
        /// <param name="hotelOperaIDs">An array of Opera IDs.</param>
        /// <returns>A list of Hotels that match the specified OperaIDs.</returns>
        public static List<HotelDestination> GetHotelsByOperaIDs(string[] hotelOperaIDs)
        {
            List<HotelDestination> hotels = new List<HotelDestination>();
            foreach (string s in hotelOperaIDs)
            {
                hotels.Add(GetHotelDestination(s));
            }
            return hotels;
        }

        /// <summary>
        /// Gets a list of alternative hotels to a specific hotel.
        /// </summary>
        /// <param name="hotelOperaID">The Opera ID of the Hotel</param>
        /// <returns>A list of references to the alternative hotels.</returns>
        public static List<AlternativeHotelReference> GetAlternativeHotelReferences(string hotelOperaID)
        {
            HotelDestination hotel = GetHotelByOperaID(hotelOperaID);
            return hotel.AlternativeHotels;
        }

        #endregion Hotels and destinations

        #region Rooms and Rates

        /// <summary>
        /// Gets a dictionary with RoomType keys and the corresponding RoomCategory
        /// </summary>
        /// <returns>A dictionary with all Room Type IDs and the each corresponding Room Category.</returns>
        public static Dictionary<string, RoomCategory> GetRoomTypeCategoryMap()
        {
            return GetRoomTypeCategoryMap(false);
        }

        /// <summary>
        /// Gets a dictionary with RateType keys and the corresponding RateCategories
        /// </summary>
        /// <returns>A dictionary with all Rate Type IDs and the each corresponding Rate Category.</returns>
        public static Dictionary<string, RateCategory> GetRateTypeCategoryMap(bool fetchContentInEnglish)
        {
            return GetRateTypeCategoryMap(fetchContentInEnglish, false);
        }

        /// <summary>
        /// Gets the bonus cheque rate for a specific country code and year
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="country">Country code</param>
        /// <param name="currencyCode">CurrencyCode</param>
        /// <returns>The bonus cheque rate</returns>
        /// <exception cref="ContentDataAccessException">Throws ContentDataAccessException if the rate is not found</exception>
        public static double GetBonusChequeRate(int year, string country, string currencyCode)
        {
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference bonusChequeRateContainer = (PageReference)rootPage["BonusChequeRateContainer"];
            PageDataCollection bonusChequeRates = DataFactory.Instance.GetChildren(bonusChequeRateContainer);

            FilterPublished publishedFilter = new FilterPublished(PagePublishedStatus.Published);
            FilterCompareTo yearFilter = new FilterCompareTo("ChequeYear", year.ToString());
            yearFilter.Condition = CompareCondition.Equal;
            FilterCompareTo countryFilter = new FilterCompareTo("ChequeCountry", country);
            countryFilter.Condition = CompareCondition.Equal;

            FilterCompareTo currencyFilter = new FilterCompareTo("Currency", currencyCode);
            currencyFilter.Condition = CompareCondition.Equal;

            publishedFilter.Filter(bonusChequeRates);
            yearFilter.Filter(bonusChequeRates);
            countryFilter.Filter(bonusChequeRates);
            currencyFilter.Filter(bonusChequeRates);

            if (bonusChequeRates.Count > 0)
                return (double)bonusChequeRates[0]["ChequeValue"];

            throw new ContentDataAccessException(
                "Bonus Cheque could not be found for the specified year, country and currency code.", AppConstants.BONUS_CHEQUE_NOT_FOUND);
        }

        #endregion Rooms and Rates

        /// <summary>
        /// This will give the editable text used in email template.
        /// This text are conofigured in CMS and editable by content editor.
        /// </summary>
        /// <param name="booking">Credit card booking type. e.g GTDCC or GTDEEP</param>
        /// <param name="bookingType">Either Booking/Modify/Cancel</param>
        /// <param name="creditcardStatus">
        /// Wheather this booking is done using creadit card or with out credit card
        /// Booking with Credit card, string should be - WITHCREDITCARD.
        /// Booking with out Credit card, string should be  - WITHOUTCREDITCARD.
        /// </param>
        /// <returns>Collection of dynamic placeholder and value fom CMS.</returns>
        /// <remarks>CR 4 | Text based email confirmation | Release 1.4</remarks>
        public static Dictionary<string, string> GetTextForEmailTemplate(string booking, string bookingType,
                                                                         string creditcardStatus, bool isPrepaidBooking)
        {
            return GetTextForEmailTemplate(booking, bookingType, creditcardStatus, isPrepaidBooking, false);
        }

        /// <summary>
        /// Overloaded method. This method is called during the Redemption booking
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns>
        /// Cancellation Policy Value.
        /// </returns>
        /// <remarks>
        /// </remarks>
        public static Dictionary<string, string> GetTextForEmailTemplate(string bookingType)
        {
            return GetTextForEmailTemplate(bookingType, false);
        }

        /// <summary>
        /// Gets the text for email template.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetConfirmationEmailTextFormCMS(string bookingType, string language)
        {
            return GetConfirmationEmailTextFormCMS(bookingType, language, false);
        }

        /// <summary>
        /// Gets the text for email template.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetConfirmationEmailTextFormCMS(string bookingType)
        {
            //This method will be called from Scanweb
            return GetConfirmationEmailTextFormCMS(bookingType, null);
        }

        /// <summary>
        /// This method will read the check box and decides wheather the third column will be displayed or not.
        /// </summary>
        /// <param name="offersPage"></param>
        /// <returns>
        /// true - if 'Show Special Rate' check box is checked in Epi server for offer page.
        /// false - if 'Show Special Rate' check box is not checked in Epi server for offer page.
        /// </returns>
        public static bool ShowSpecialRates(PageReference offersPage)
        {
            bool dispalyStatus = false;
            if (null != offersPage)
            {
                PageData pageData = DataFactory.Instance.GetPage(offersPage, EPiServer.Security.AccessLevel.NoAccess);
                if (null != pageData["Show Special Rate"])
                {
                    dispalyStatus = true;
                }
            }
            return dispalyStatus;
        }

        /// <summary>
        /// This method will return all Block information.
        /// </summary>
        /// <param name="blockCode"></param>
        /// <returns>Block information of corresponding block code captured from UE</returns>
        public static Block GetBlockCodePages(string blockCode)
        {
            return GetBlockCodePages(blockCode, false);
        }

        public static Dictionary<string, string> GetUserInterestList()
        {
            return GetUserInterestList(false);
        }

        /// <summary>
        /// This is short of a simillar method as "GetPagesWithPageTypeID",but this method does not cache the pages.
        /// So for each root page we have the correct child nodes.
        /// </summary>
        /// <param name="pageTypeId">Page type ID</param>
        /// <param name="rootPageLink">Page link for the root node.</param>
        /// <returns>Collection of child node based on the page type id and root page link.</returns>
        /// <remarks>Find a hotel release</remarks>
        public static PageDataCollection GetChildPagesWithPageTypeId(int pageTypeId, PageReference rootPageLink, string languageBranch)
        {
            PageDataCollection searchResults = null;
            PropertyCriteriaCollection searchCriterias = new PropertyCriteriaCollection();
            PropertyCriteria pageTypeCriteria = new PropertyCriteria();
            pageTypeCriteria.Condition = EPiServer.Filters.CompareCondition.Equal;
            pageTypeCriteria.Name = "PageTypeID";
            pageTypeCriteria.Required = true;
            pageTypeCriteria.Type = EPiServer.Core.PropertyDataType.PageType;
            pageTypeCriteria.Value = pageTypeId.ToString();
            searchCriterias.Add(pageTypeCriteria);
            if (!string.IsNullOrEmpty(languageBranch))
            {
                searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink, searchCriterias, languageBranch);
            }
            else
            {
                searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink, searchCriterias);
            }
            return searchResults;
        }

        /// <summary>
        /// This returns the merchant profile associated wiht the hotel
        /// </summary>
        /// <param name="hotelOperaID">hotelOperaID</param>
        /// <returns></returns>
        public static PageData GetMerchantByHotelOperaId(string hotelOperaID, string language)
        {
            return GetMerchantByHotelOperaId(hotelOperaID, language, false);
        }

        /// <summary>
        /// This returns the merchant profile associated with the hotel opera id
        /// </summary>
        /// <param name="hotelOperaID">hotelOperaID</param>
        /// <returns></returns>
        public static PageData GetMerchantByHotelOperaId(string hotelOperaID)
        {
            return GetMerchantByHotelOperaId(hotelOperaID, string.Empty);
        }

        public static bool GetInvertPaymentFallbackByHotelId(string hotelOperaID)
        {
            bool invertFallback = false;

            if (hotelOperaID != null)
            {
                PageData hotel = GetHotelPageDataByOperaID(hotelOperaID, "en");
                invertFallback = hotel["InvertPaymentFallback"] != null ? Convert.ToBoolean(hotel["InvertPaymentFallback"]) : false;
            }

            return invertFallback;
        }

        public static bool GetPaymentFallback(string hotelOperaID)
        {
            bool enableFallback = false;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                EPiServer.Security.AccessLevel.NoAccess);
            if (rootPage["PaymentFallback"] != null)
            {
                enableFallback = (bool)rootPage["PaymentFallback"];
            }

            if (!string.IsNullOrEmpty(hotelOperaID) && GetInvertPaymentFallbackByHotelId(hotelOperaID))
            {

                enableFallback = !enableFallback;
            }


            return enableFallback;

        }

        #endregion Publilc methods

        #region Private methods

        #region Internal methods

        #region Create Trasaction List

        /// <summary>
        /// Complete transactionlist will be created by fetching all 
        /// "transaction" page type under [Transactions] container.
        /// </summary>        
        /// <returns>A List of transactions of type "Transaction"</returns>
        private static List<Transaction> CreateTrasactionList()
        {
            int trasactionPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["TrasactionPageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference transactionContainer = (PageReference)rootPage["TransactionContainer"];

            PageDataCollection transactionResults = new PageDataCollection();

            transactionResults = GetPagesWithPageTypeID(trasactionPageTypeID, transactionContainer);

            List<Transaction> transactions = new List<Transaction>();

            foreach (PageData transactionPage in transactionResults)
            {
                string operaID = (transactionPage["OperaID"] as string).Trim();
                string transactionName = (transactionPage["TransactionName"] as string).Trim();

                Transaction transaction = new Transaction();
                transaction.OperaID = operaID;
                transaction.TransactionName = transactionName;

                transactions.Add(transaction);
            }

            return transactions;
        }

        #endregion Create Trasaction List

        /// <summary>
        /// <summary>
        /// Get a Copy of the Page Data Collection
        /// </summary>
        /// <param name="originalPageDataCollection">
        /// Page Data Collection
        /// </param>
        /// <returns>Copy of PageData Collection</returns>
        private static PageDataCollection GetCopyPageDataCollection(PageDataCollection originalPageDataCollection)
        {
            PageDataCollection copyPageDataCollection = new PageDataCollection();
            foreach (PageData pageData in originalPageDataCollection)
            {
                copyPageDataCollection.Add(pageData);
            }
            return copyPageDataCollection;
        }
        /// Gets the Rate with a specific rate ID
        /// </summary>
        /// <param name="rateID">Opera Rate ID</param>
        /// <returns>The rate that match the specified rate ID</returns>
        private static RateCategory GetRateCategoryByRateTypeID(string rateID, bool fetchContentInEnglish)
        {
            ContentDataAccess.LogMessage(string.Format("GetRateCategoryByRateTypeID rateID:" + rateID));

            int rateTypePageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["RateTypePageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference rateCategoryContainer = (PageReference)rootPage["RateCategoryContainer"];

            PageDataCollection rateTypePages = GetPagesWithPageTypeID(rateTypePageTypeID, rateCategoryContainer);

            FilterCompareTo rateTypeIDFilter = new FilterCompareTo("OperaID", rateID);
            rateTypeIDFilter.Condition = CompareCondition.Equal;
            rateTypeIDFilter.Filter(rateTypePages);

            RemoveNonPublishedPages(rateTypePages);

            PageData rateTypePage;

            if (rateTypePages.Count > 0)
                rateTypePage = rateTypePages[0];
            else
                throw new ContentDataAccessException("Could not find a RateCategory with the specified OperaID.");
            PageData rateCategoryPage = null;
            if (fetchContentInEnglish)
            {
                rateCategoryPage = DataFactory.Instance.GetPage(rateTypePage.ParentLink, new LanguageSelector("en"),
                                                                        EPiServer.Security.AccessLevel.NoAccess);
            }
            else
            {
                rateCategoryPage = DataFactory.Instance.GetPage(rateTypePage.ParentLink,
                                                                         EPiServer.Security.AccessLevel.NoAccess);
            }

            string rateCategoryName = rateCategoryPage.PageName;
            string rateCategoryID = rateCategoryPage["RateCategoryID"] as string ?? string.Empty;
            string rateCategoryDescription = rateCategoryPage["RateCategoryDescription"] as string ?? string.Empty;
            string rateCategoryCeditCardGuranteeType = rateCategoryPage["GuaranteeType"] as string ?? string.Empty;
            bool rateCategoryHoldGuranteeAvailable = rateCategoryPage["LateHoldAvailable"] != null;
            int coulmnNumber = rateCategoryPage["CoulmnNumber"] == null ? 0 : (int)rateCategoryPage["CoulmnNumber"];
            bool doShowDisplayMoreInfoLink = rateCategoryPage["DisplayLink"] == null ? false : true;
            string rateCategoryColor = rateCategoryPage["RateCategoryColor"] as string ?? string.Empty;
            string rateHighlightTextWeb = rateCategoryPage["RateHighlightTextWeb"] as string ?? string.Empty;
            string rateHighlightTextMobile = rateCategoryPage["RateHighlightTextMobile"] as string ?? string.Empty;
            string rateCategoryPageLanguage = rateCategoryPage.LanguageID as string ?? string.Empty;
            List<string> headers = new List<string>();
            headers.Add(rateCategoryPage["Heading1"] as string ?? string.Empty);
            headers.Add(rateCategoryPage["Heading2"] as string ?? string.Empty);
            headers.Add(rateCategoryPage["Heading3"] as string ?? string.Empty);

            string rateCategoryURL = string.Empty;
            PageReference rateCategoryDescriptionPageLink =
                rateCategoryPage["RateCategoryDescriptionPage"] as PageReference;
            if (rateCategoryDescriptionPageLink != null)
            {
                PageData rateCategoryDescriptionPage = DataFactory.Instance.GetPage(rateCategoryDescriptionPageLink,
                                                                                    EPiServer.Security.AccessLevel.
                                                                                        NoAccess);
                UrlBuilder url = new UrlBuilder(rateCategoryDescriptionPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, rateCategoryDescriptionPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                rateCategoryURL = url.ToString();
            }
            PageDataCollection childRateTypePages = DataFactory.Instance.GetChildren(rateTypePage.ParentLink);
            List<Rate> rateTypes = new List<Rate>();
            foreach (PageData r in childRateTypePages)
            {
                bool isCancellable = r["IsCancellable"] != null;
                bool isDisplayable = r["IsDisplayable"] != null;
                bool isModifiable = r["IsModifiable"] != null;
                string rateCodeDescription = r.PageName;
                string operaRateId = r["OperaID"] as string ?? string.Empty;
                Rate rate = new Rate(operaRateId, rateCodeDescription, rateCategoryName, isModifiable, isCancellable,
                                     isDisplayable);
                rateTypes.Add(rate);
            }
            RateCategory rateCategory = new RateCategory(rateCategoryID, rateCategoryName, rateCategoryDescription,
                                                         rateCategoryURL,
                                                         rateCategoryHoldGuranteeAvailable,
                                                         rateCategoryCeditCardGuranteeType, rateTypes,
                                                         coulmnNumber, doShowDisplayMoreInfoLink,
                                                         rateCategoryColor, rateHighlightTextWeb, rateHighlightTextMobile, rateCategoryPageLanguage, headers);
            return rateCategory;
        }

        /// Gets the Rate with a specific rate ID
        /// </summary>
        /// <param name="rateID">Opera Rate ID</param>
        /// <returns>The rate that match the specified rate ID</returns>
        private static RateCategory GetRateCategoryByRateTypeID(string rateID, string language)
        {
            ContentDataAccess.LogMessage(string.Format("GetRateCategoryByRateTypeID rateID:" + rateID));

            int rateTypePageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["RateTypePageTypeGUID"])).ID;
            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference rateCategoryContainer = (PageReference)rootPage["RateCategoryContainer"];

            PageDataCollection rateTypePages = GetPagesWithPageTypeID(rateTypePageTypeID, rateCategoryContainer);

            FilterCompareTo rateTypeIDFilter = new FilterCompareTo("OperaID", rateID);
            rateTypeIDFilter.Condition = CompareCondition.Equal;
            rateTypeIDFilter.Filter(rateTypePages);

            RemoveNonPublishedPages(rateTypePages);

            PageData rateTypePage;

            if (rateTypePages.Count > 0)
                rateTypePage = rateTypePages[0];
            else
                throw new ContentDataAccessException("Could not find a RateCategory with the specified OperaID.");

            ILanguageSelector languageSelector = new LanguageSelector(language);
            PageData rateCategoryPage = DataFactory.Instance.GetPage(rateTypePage.ParentLink, languageSelector,
                                                                     EPiServer.Security.AccessLevel.NoAccess);

            string rateCategoryName = rateCategoryPage.PageName;
            string rateCategoryID = rateCategoryPage["RateCategoryID"] as string ?? string.Empty;
            string rateCategoryDescription = rateCategoryPage["RateCategoryDescription"] as string ?? string.Empty;
            string rateCategoryCeditCardGuranteeType = rateCategoryPage["GuaranteeType"] as string ?? string.Empty;
            bool rateCategoryHoldGuranteeAvailable = rateCategoryPage["LateHoldAvailable"] != null;
            int coulmnNumber = rateCategoryPage["CoulmnNumber"] == null ? 0 : (int)rateCategoryPage["CoulmnNumber"];
            bool doShowDisplayMoreInfoLink = rateCategoryPage["DisplayLink"] == null ? false : true;
            string rateCategoryColor = rateCategoryPage["RateCategoryColor"] as string ?? string.Empty;
            string rateHighlightTextWeb = rateCategoryPage["RateHighlightTextWeb"] as string ?? string.Empty;
            string rateHighlightTextMobile = rateCategoryPage["RateHighlightTextMobile"] as string ?? string.Empty;
            string rateCategoryPageLanguage = rateCategoryPage.LanguageID as string ?? string.Empty;
            List<string> headers = new List<string>();
            headers.Add(rateCategoryPage["Heading1"] as string ?? string.Empty);
            headers.Add(rateCategoryPage["Heading2"] as string ?? string.Empty);
            headers.Add(rateCategoryPage["Heading3"] as string ?? string.Empty);

            string rateCategoryURL = string.Empty;
            PageReference rateCategoryDescriptionPageLink =
                rateCategoryPage["RateCategoryDescriptionPage"] as PageReference;
            if (rateCategoryDescriptionPageLink != null)
            {
                PageData rateCategoryDescriptionPage = DataFactory.Instance.GetPage(rateCategoryDescriptionPageLink,
                                                                                    EPiServer.Security.AccessLevel.
                                                                                        NoAccess);
                UrlBuilder url = new UrlBuilder(rateCategoryDescriptionPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, rateCategoryDescriptionPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                rateCategoryURL = url.ToString();
            }
            PageDataCollection childRateTypePages = DataFactory.Instance.GetChildren(rateTypePage.ParentLink);
            List<Rate> rateTypes = new List<Rate>();
            foreach (PageData r in childRateTypePages)
            {
                bool isCancellable = r["IsCancellable"] != null;
                bool isDisplayable = r["IsDisplayable"] != null;
                bool isModifiable = r["IsModifiable"] != null;
                string rateCodeDescription = r.PageName;
                string operaRateId = r["OperaID"] as string ?? string.Empty;
                Rate rate = new Rate(operaRateId, rateCodeDescription, rateCategoryName, isModifiable, isCancellable,
                                     isDisplayable);
                rateTypes.Add(rate);
            }
            RateCategory rateCategory = new RateCategory(rateCategoryID, rateCategoryName, rateCategoryDescription,
                                                         rateCategoryURL,
                                                         rateCategoryHoldGuranteeAvailable,
                                                         rateCategoryCeditCardGuranteeType, rateTypes,
                                                         coulmnNumber, doShowDisplayMoreInfoLink,
                                                         rateCategoryColor, rateHighlightTextWeb, rateHighlightTextMobile, rateCategoryPageLanguage, headers);
            return rateCategory;
        }

        private static RoomCategory GetRoomCategoryByPageReference(PageReference roomCategoryLink)
        {
            if (roomCategoryLink == null)
                return null;

            PageData roomCategoryPage = DataFactory.Instance.GetPage(roomCategoryLink,
                                                                     EPiServer.Security.AccessLevel.NoAccess);

            string roomCategoryDescription = roomCategoryPage["RoomCategoryDescription"] as string ?? string.Empty;
            string roomCategoryName = roomCategoryPage.PageName;
            string roomCategoryURL = string.Empty;

            PageReference roomCategoryDescriptionPageLink =
                roomCategoryPage["RoomCategoryDescriptionPage"] as PageReference;
            if (roomCategoryDescriptionPageLink != null)
            {
                PageData roomCategoryDescriptionPage = DataFactory.Instance.GetPage(roomCategoryDescriptionPageLink,
                                                                                    EPiServer.Security.AccessLevel.
                                                                                        NoAccess);
                UrlBuilder url = new UrlBuilder(roomCategoryDescriptionPage.LinkURL);
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, roomCategoryDescriptionPage.PageLink,
                                                                      System.Text.UTF8Encoding.UTF8);
                roomCategoryURL = url.ToString();
            }
            else
            {
                roomCategoryURL = string.Empty;
            }

            string roomCategoryID = string.Empty;

            if (!string.IsNullOrEmpty(roomCategoryPage["RoomCategoryID"] as string))
                roomCategoryID = (string)roomCategoryPage["RoomCategoryID"];
            else
                throw new ContentDataAccessException("RoomCategoryID property is missing on the RoomCategory.");

            PageDataCollection childRoomTypePages = DataFactory.Instance.GetChildren(roomCategoryLink);

            List<RoomType> roomTypes = new List<RoomType>();

            foreach (PageData r in childRoomTypePages)
            {
                if (r.CheckPublishedStatus(PagePublishedStatus.Published))
                {
                    RoomType t = new RoomType((string)r["OperaID"],
                                              r.PageName,
                                              roomCategoryName,
                                              (int)r["Priority"],
                                              (r["BedTypeCode"] as string ?? string.Empty),
                                              (r["BedTypeDescription"] as string ?? string.Empty));

                    roomTypes.Add(t);
                }
            }

            RoomCategory roomCategory = new RoomCategory(roomCategoryID, roomCategoryName, roomCategoryDescription,
                                                         roomCategoryURL, roomTypes);
            return roomCategory;
        }

        /// <summary>
        /// This class is made public to get the complete collection under any page type.
        /// This is made public in Hygiene Release 1.8.2 for site footer changes
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <returns></returns>
        public static PageDataCollection GetPagesWithPageTypeID(int pageTypeID, PageReference rootPageLink)
        {
            return GetPagesWithPageTypeID(pageTypeID, rootPageLink, false);
        }

        /// <summary>
        /// Returns hotels based on the language
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static PageDataCollection GetPagesWithPageTypeIDAndLanguage(int pageTypeID, PageReference rootPageLink, string language)
        {
            return GetPagesWithPageTypeIDAndLanguage(pageTypeID, rootPageLink, language, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operaID"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        private static HotelDestination GetHotelDestination(string operaID, string language)
        {
            return GetHotelDestination(operaID, language, false);
        }

        private static HotelDestination GetHotelDestination(string operaID)
        {
            return GetHotelDestination(operaID, string.Empty);
        }

        /// <summary>
        /// Gets the hotel destination with the Hotel Name 
        /// </summary>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        public static HotelDestination GetHotelDestinationWithHotelName(string hotelName)
        {
            return GetHotelDestinationWithHotelName(hotelName, false);
        }

        /// <summary>
        /// This function returns the country PageData using countryCode
        /// </summary>
        /// <param name="countryCode">CountryCode</param>
        /// <returns></returns>
        public static PageDataCollection GetCountryList()
        {
            return GetCountryList(false);
        }

        /// <summary>
        /// This function returns the country PageData using countryCode
        /// </summary>
        /// <param name="countryCode">CountryCode</param>
        /// <returns></returns>
        private static PageData GetCountryByCountryCode(string countryCode)
        {
            return GetCountryByCountryCode(countryCode, false);
        }

        /// <summary>
        /// This wil create the hotel destination out of the Hotel Page data
        /// </summary>
        /// <param name="hotelPageData">Hotel page data</param>
        /// <returns>Hotel destination</returns>
        /// <remarks>Refactored during Find a hotel release</remarks>
        private static HotelDestination CreateHotelDestination(PageData hotelPageData)
        {
            #region Set Hotel Properties
            int alternativeHotelContainerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["AlternativeHotelContainerPageTypeGUID"])).ID;
            int hotelRoomDescriptionContainerPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelRoomDescriptionContainerPageTypeGUID"])).ID;
            string hotelName = hotelPageData["Heading"] as string ?? string.Empty;
            //AMS Fix: artf1050325 : Scandic Bromma not bookable from properties page 
            //Remove all the spaces from the Heading text
            if (!string.IsNullOrEmpty(hotelName))
            {
                hotelName = hotelName.Trim();
            }
            string hotelID = hotelPageData.PageLink.ID.ToString();
            string hotelSearchableString = hotelPageData.PageName;
            string hotelOperaDestinationId = hotelPageData["OperaID"] as string ?? string.Empty;
            hotelOperaDestinationId = hotelOperaDestinationId.Trim();
            string hotelDescription = hotelPageData["HotelBookingDescription"] as string ?? string.Empty;
            // Archana | PostalCity Changes for XMLAPI     

            Address hotelAddress = new Address(hotelPageData["StreetAddress"] as string ?? string.Empty,
                                             hotelPageData["PostCode"] as string ?? string.Empty,
                                            hotelPageData["PostalCity"] as string ?? string.Empty,
                                             hotelPageData["City"] as string ?? string.Empty,
                                             hotelPageData["Country"] as string ?? string.Empty);
            //artf1045081 : Hotel landing page | User is able to book a non bookable hotel. |Release DP
            //If available in booking then make 'IsAvailableInBooking' property of 'HotelDestination' to true.
            bool availableInBooking = false;
            if (null != hotelPageData["AvailableInBooking"])
            {
                availableInBooking = true;
            }

            //Hygiene 1.8.2|New Site Footer |Changes
            bool displayedInSiteFooter = false;
            if (null != hotelPageData["DisplayInSiteFooter"])
            {
                displayedInSiteFooter = true;
            }

            //Hygiene 1.8.2|New Site Footer |Changes
            int footerNewHotelsSortIndex = 0;
            if (null != hotelPageData["FooterNewHotelsSortIndex"])
            {
                footerNewHotelsSortIndex = Convert.ToInt32(hotelPageData["FooterNewHotelsSortIndex"]);
            }
            //Hygiene 1.8.2|New Site Footer |Changes
            string promotionalPageLinkText = string.Empty;
            if (null != hotelPageData["PromotionalPageLinkText"])
            {
                promotionalPageLinkText = hotelPageData["PromotionalPageLinkText"] as string;
            }
            //Hygiene 1.8.2|New Site Footer |Changes
            string promotionalPageLink = string.Empty;
            if (null != hotelPageData["PromotionalPageLink"])
            {
                promotionalPageLink = GlobalUtil.GetUrlToPage(hotelPageData["PromotionalPageLink"] as PageReference);
            }

            // Set image URL
            string hotelImageURL = string.Empty;
            string imageString = hotelPageData["HotelBookingImage"] as string;
            if (!String.IsNullOrEmpty(imageString))
            {
                ImageStoreNET.Classes.Util.UrlBuilder ub = ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);

                if (ub != null)
                {
                    ub.Width = Int32.Parse(ConfigurationManager.AppSettings["HotelThumbnailImageWidth"]);
                    ub.PreserveAspectRatio = true;
                    ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                    hotelImageURL = ub.ToString();
                }
            }

            string hotelAirport1Name = hotelPageData["Airport1Name"] as string ?? string.Empty;

            double hotelAirport1Distance = 0;
            if (hotelPageData["Airport1Distance"] != null)
                hotelAirport1Distance = (double)hotelPageData["Airport1Distance"];

            string hotelAirport2Name = hotelPageData["Airport2Name"] as string ?? string.Empty;

            double hotelAirport2Distance = 0;
            if (hotelPageData["Airport2Distance"] != null)
                hotelAirport2Distance = (double)hotelPageData["Airport2Distance"];

            string hotelCityCenterName = hotelPageData["CityCenterName"] as string ?? string.Empty;

            double hotelCityCenterDistance = 0;
            if (hotelPageData["CityCenterDistance"] != null)
                hotelCityCenterDistance = (double)hotelPageData["CityCenterDistance"];
            //Merchandising - Enhancement of Ajax search
            string postalCity = hotelPageData["PostalCity"] as string ?? string.Empty;
            string city = hotelPageData["City"] as string ?? string.Empty;

            Point hotelCoordinates = new Point(0, 0);
            if (hotelPageData["GeoX"] != null && hotelPageData["GeoY"] != null)
                hotelCoordinates = new Point((double)hotelPageData["GeoX"], (double)hotelPageData["GeoY"]);

            string hotelTelephone = hotelPageData["Phone"] as string ?? string.Empty;
            string hotelEmail = hotelPageData["Email"] as string ?? string.Empty;

            // Release R2.0 | Confirmation Page map | Reads relevant data - START
            string customerCarePhone = hotelPageData["CentralReservationNumber"] as string ?? string.Empty;
            string fax = hotelPageData["Fax"] as string ?? string.Empty;
            string ecoLabeled = hotelPageData["EcoLabeled"] as string ?? string.Empty;

            int? noOfRooms = null;
            if (hotelPageData["NoOfRooms"] != null)
            {
                int tempVal;
                noOfRooms = int.TryParse(hotelPageData["NoOfRooms"].ToString(), out tempVal) ? tempVal : (int?)null;
            }

            int? noOfNonSmokingRooms = 0;
            if (hotelPageData["NonSmokingRooms"] != null)
            {
                int tempVal;
                noOfNonSmokingRooms = int.TryParse(hotelPageData["NonSmokingRooms"].ToString(), out tempVal) ? tempVal : (int?)null;
            }

            bool isRoomsForDisabledAvailable = hotelPageData["RoomsForDisabled"] != null ? true : false;
            bool isRelaxCenterAvailable = hotelPageData["RelaxCenter"] != null ? true : false;
            bool isRestaurantAndBarAvailable = hotelPageData["RestaurantBar"] != null ? true : false;
            bool isGarageAvailable = hotelPageData["Garage"] != null ? true : false;
            bool isOutDoorParkingAvailable = hotelPageData["OutdoorParking"] != null ? true : false;
            bool isShopAvailable = hotelPageData["Shop"] != null ? true : false;
            bool isMeetingRoomAvailable = hotelPageData["MeetingFacilities"] != null ? true : false;
            double? trainStationDistance = null;
            if (hotelPageData["DistanceTrain"] != null)
            {
                double tempVal;
                trainStationDistance = double.TryParse(hotelPageData["DistanceTrain"].ToString(), out tempVal) ? tempVal : (double?)null;
            }
            bool isWiFiAvailable = hotelPageData["WirelessInternet"] != null ? true : false;
            string specialAlert = string.Empty;
            if (hotelPageData["OperaID"] != null)
            {
                specialAlert = GetSpecialAlert(hotelPageData["OperaID"].ToString(), false);
            }
            // Release R2.0 | Confirmation Page map | Reads relevant data - END

            UrlBuilder url = new UrlBuilder(hotelPageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, hotelPageData.LinkURL, System.Text.UTF8Encoding.UTF8);
            string hotelPageURL = url.ToString();

            string shortURL = hotelPageData["PageExternalURL"] as string ?? string.Empty;

            int taLocationID = 0;
            if (hotelPageData["TALocationID"] != null)
                taLocationID = Convert.ToInt32(hotelPageData["TALocationID"]);

            bool hideHotelTARating = false;
            if (hotelPageData["HideHotelTARating"] != null)
                hideHotelTARating = Convert.ToBoolean(hotelPageData["HideHotelTARating"]);

            #endregion

            #region Get Alternative Hotels
            List<AlternativeHotelReference> alternativeHotelReferences = new List<AlternativeHotelReference>();

            PageDataCollection hotelChildPages = DataFactory.Instance.GetChildren(hotelPageData.PageLink);
            foreach (PageData container in hotelChildPages)
            {
                if (container.PageTypeID == alternativeHotelContainerPageTypeID)
                {
                    PageDataCollection alternativeHotels = DataFactory.Instance.GetChildren(container.PageLink);
                    foreach (PageData alternativeHotelPage in alternativeHotels)
                    {
                        if (alternativeHotelPage.CheckPublishedStatus(PagePublishedStatus.Published) && (alternativeHotelPage["AlternativeHotel"] as PageReference != null))
                        {
                            PageData hotelPage = DataFactory.Instance.GetPage((PageReference)alternativeHotelPage["AlternativeHotel"], EPiServer.Security.AccessLevel.NoAccess);
                            //CR 14 |Release 1.3(May release) |Distance by city center.
                            //alternativeHotelPage["DistanceToCityCentreOfSearchedHotel"] property is added to access the added property.
                            if (null != hotelPage["AvailableInBooking"] && hotelPage.CheckPublishedStatus(PagePublishedStatus.Published))
                            {
                                AlternativeHotelReference a = new AlternativeHotelReference(hotelPage["OperaID"] as string ?? string.Empty,
                                    (double)alternativeHotelPage["Distance"],
                                    alternativeHotelPage["DistanceToCityCentreOfSearchedHotel"] == null ? HYPHEN : alternativeHotelPage["DistanceToCityCentreOfSearchedHotel"].ToString());
                                alternativeHotelReferences.Add(a);
                            }
                        }
                    }
                }
            }

            #endregion

            #region Get hotel room descriptions
            List<HotelRoomDescription> hotelRoomDescriptions = new List<HotelRoomDescription>();

            hotelChildPages = DataFactory.Instance.GetChildren(hotelPageData.PageLink);
            foreach (PageData container in hotelChildPages)
            {
                if (container.PageTypeID == hotelRoomDescriptionContainerPageTypeID)
                {

                    PageDataCollection hotelRoomDescriptionPages = DataFactory.Instance.GetChildren(container.PageLink);
                    foreach (PageData hotelRoomDescriptionPage in hotelRoomDescriptionPages)
                    {
                        if (hotelRoomDescriptionPage.CheckPublishedStatus(PagePublishedStatus.Published))
                        {
                            string roomDescription = hotelRoomDescriptionPage["RoomDescription"] as string ?? string.Empty;

                            //R1.2| CR5| 501392 | Display default room categories per hotel as per CMS configuration
                            //Get the value of the property "ShowAsDefault" from hotel description from EpiServer
                            bool isDefault = hotelRoomDescriptionPage["ShowAsDefault"] != null;

                            string roomDescriptionUrl = UriSupport.AddQueryString(hotelPageURL, "hotelpage", "rooms");
                            roomDescriptionUrl = UriSupport.AddQueryString(roomDescriptionUrl, "roomid", hotelRoomDescriptionPage.PageLink.ID.ToString());

                            RoomCategory hotelRoomCategory = GetRoomCategoryByPageReference((PageReference)hotelRoomDescriptionPage["RoomCategory"]);

                            //Shameem -21st june 2010:Reservation 2.0 Implemention:Added unique room description for each room
                            hotelRoomCategory.RoomCategoryteserText = hotelRoomDescriptionPage["RoomTeaserText"] as string ?? string.Empty;

                            //Ranajit : 22 June:Reservation 2.0 Implemention: Set the Image Url of Hotel room categories as its requeir
                            //to show on rate description page
                            hotelRoomCategory.ImageURL = GetMainImageURL(hotelRoomDescriptionPage);
                            //Ranajit : 22 June:Reservation 2.0 Implemention: Set the RoomCategoryToolTipText of Hotel room categories as its requeir
                            //to show on rate description page
                            hotelRoomCategory.RoomCategoryTooltipText = hotelRoomDescriptionPage["RoomCategoryToolTipText"] as string ?? string.Empty;
                            if (!string.IsNullOrEmpty(roomDescription))
                                hotelRoomCategory.RoomCategoryDescription = roomDescription;

                            //R1.2| CR5| 501392 | Display default room categories per hotel as per CMS configuration
                            //Pass the bool value to know whether this room type is set as default or not.
                            HotelRoomDescription hotelRoomDescription = new HotelRoomDescription(roomDescription, hotelRoomCategory, roomDescriptionUrl, isDefault);
                            hotelRoomDescriptions.Add(hotelRoomDescription);
                        }
                    }
                }
            }
            #endregion Get hotel room descriptions

            //artf1045081 : Hotel landing page | User is able to book a non bookable hotel. |Release DP
            //Added the last parameter in the method to populate 'IsAvailableInBooking' property of 'HotelDestination'.
            // Release R2.0 | Confirmation Page map | Creating CMS object with reads info
            HotelDestination hotel = new HotelDestination(
                                                        hotelOperaDestinationId, hotelName, hotelID, hotelSearchableString, hotelDescription,
                                                        hotelAddress, hotelImageURL, hotelPageURL, hotelCoordinates, hotelAirport1Name,
                                                        hotelAirport1Distance, hotelAirport2Name, hotelAirport2Distance, hotelCityCenterName,
                                                        hotelCityCenterDistance, city, postalCity, hotelTelephone, hotelEmail, hotelRoomDescriptions, alternativeHotelReferences,
                                                        availableInBooking, displayedInSiteFooter, footerNewHotelsSortIndex, promotionalPageLink, promotionalPageLinkText,
                                                        customerCarePhone, fax, ecoLabeled,
                                                        noOfRooms,
                                                        noOfNonSmokingRooms,
                                                        isRoomsForDisabledAvailable,
                                                        isRelaxCenterAvailable,
                                                        isRestaurantAndBarAvailable,
                                                        isGarageAvailable,
                                                        isOutDoorParkingAvailable,
                                                        isShopAvailable,
                                                        isMeetingRoomAvailable,
                                                        trainStationDistance,
                                                        isWiFiAvailable,
                                                        specialAlert,
                                                        false,
                                                        shortURL,
                                                        taLocationID,
                                                        hideHotelTARating, hotelPageData.URLSegment, hotelPageData.PageTypeName);

            return hotel;
        }

        /// <summary>
        /// This wil create the hotel destination out of the Hotel Page data
        /// </summary>
        /// <param name="hotelPageData">Hotel page data</param>
        /// <returns>Hotel destination</returns>
        /// <remarks>Refactored during Find a hotel release</remarks>
        private static HotelDestination CreateHotelDestinationForAutoSuggest(PageData hotelPageData)
        {
            #region Set Hotel Properties

            int alternativeHotelContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["AlternativeHotelContainerPageTypeGUID"])).ID;
            int hotelRoomDescriptionContainerPageTypeID =
                PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelRoomDescriptionContainerPageTypeGUID"])).
                    ID;
            string hotelName = hotelPageData["Heading"] as string ?? string.Empty;
            if (!string.IsNullOrEmpty(hotelName))
            {
                hotelName = hotelName.Trim();
            }
            string hotelID = hotelPageData.PageLink.ID.ToString();
            Point hotelCoordinate = new Point(Convert.ToDouble(hotelPageData["GeoY"], CultureInfo.InvariantCulture),
                Convert.ToDouble(hotelPageData["GeoX"], CultureInfo.InvariantCulture));

            string hotelSearchableString = hotelPageData.PageName;
            string hotelOperaDestinationId = hotelPageData["OperaID"] as string ?? string.Empty;
            hotelOperaDestinationId = hotelOperaDestinationId.Trim();
            string postalCity = hotelPageData["PostalCity"] as string ?? string.Empty;
            string city = hotelPageData["City"] as string ?? string.Empty;
            string hotelPageURL = GetPageData(hotelPageData.PageLink).LinkURL;
            UrlBuilder url = new UrlBuilder(hotelPageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, hotelPageData.LinkURL, System.Text.UTF8Encoding.UTF8);
            hotelPageURL = url.ToString();
            #endregion
            HotelDestination hotel = new HotelDestination(
                                                        hotelOperaDestinationId, hotelName, hotelID, hotelSearchableString, string.Empty,
                                                        null, string.Empty, hotelPageURL, hotelCoordinate, string.Empty,
                                                        0, string.Empty, 0, string.Empty,
                                                        0, city, postalCity, string.Empty, string.Empty, null, null,
                                                        false, false, 0, string.Empty, string.Empty,
                                                        string.Empty, string.Empty, string.Empty,
                                                        0,
                                                        0,
                                                        false,
                                                        false,
                                                        false,
                                                        false,
                                                        false,
                                                        false,
                                                        false,
                                                        0,
                                                        false,
                                                        string.Empty,
                                                        false,
                                                        string.Empty,
                                                        0,
                                                        false, hotelPageData.URLSegment, hotelPageData.PageTypeName);

            return hotel;
        }

        /// <summary>
        /// Get the Image Url of the Room to show the Image on Model popup on select rate discription
        /// </summary>
        /// <param>PageData type parameter is used as input to get the image Url</param> 
        /// <returns>
        /// Url of the Image to be displayed
        /// </returns>
        private static string GetMainImageURL(PageData roomPage)
        {
            string imageString = string.Empty;
            if (roomPage["RoomImage"] != null)
            {
                imageString = roomPage["RoomImage"] as string;
                if (!string.IsNullOrEmpty(imageString))
                {
                    ImageStoreNET.Classes.Util.UrlBuilder ub =
                        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                    if (ub != null)
                    {
                        var imageWidth = Convert.ToInt32(AppConstants.GenricImageWidth);
                        ub.PreserveAspectRatio = true;
                        ub.Width = imageWidth;
                        ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                        imageString = ub.ToString();
                    }
                }
            }
            return imageString;
        }

        /// <summary>
        /// Removes all non-published pages from the collection
        /// </summary>
        /// <param name="pages">PageDataCollection</param>
        private static void RemoveNonPublishedPages(PageDataCollection pages)
        {
            if (!IsEditOrPreviewMode)
            {
                for (int i = pages.Count - 1; i >= 0; i--)
                {
                    if (!pages[i].CheckPublishedStatus(PagePublishedStatus.Published))
                        pages.Remove(pages[i]);
                }
            }
        }
        public static bool IsEditOrPreviewMode
        {
            get
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Request.QueryString["id"] != null))
                {
                    PageReference pageVersionReference = PageReference.Parse(HttpContext.Current.Request.QueryString["id"]);

                    if ((pageVersionReference != null && pageVersionReference.WorkID > 0) || (HttpContext.Current.Request.UrlReferrer != null &&
                        Regex.IsMatch(HttpContext.Current.Request.UrlReferrer.LocalPath, "/admin/|/edit/", RegexOptions.IgnoreCase)))
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Removes all non-published node from the collection
        /// </summary>
        /// <param name="pages">List of nodes</param>
        /// <remarks>Find a Hotel page</remarks>
        private static void RemoveNonPublishedPages(List<Node> pages)
        {
            pages.RemoveAll(RemoveUnPublishedPage);
        }

        private static void RemoveUnpublisedBeyondSixMonths(List<Node> pages)
        {
            pages.RemoveAll(RemoveUnpublisedBeyondSixMonths);
        }
        /// <summary>
        /// This is predicate for removing unpublished beyond 6 months pages.
        /// </summary>
        /// <param name="pages">Page</param>
        /// <returns>true if page is not published beyond 6 months else false</returns>
        /// <remarks>Find a Hotel page</remarks>
        public static bool RemoveUnpublisedBeyondSixMonths(Node pages)
        {
            int difference = ((DateTime.Now.Year - pages.GetPageData().StopPublish.Year) * 12) + DateTime.Now.Month -
            pages.GetPageData().StopPublish.Month;
            return pages.GetPageData().PendingPublish || (difference > 6);
        }

        /// <summary>
        /// This is predicate for removing non published pages.
        /// </summary>
        /// <param name="pages">Page</param>
        /// <returns>true if page is not published else false</returns>
        /// <remarks>Find a Hotel page</remarks>
        public static bool RemoveUnPublishedPage(Node pages)
        {
            if (!pages.GetPageData().CheckPublishedStatus(PagePublishedStatus.Published))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Helps to remove all hotels which is not available for booking.
        /// </summary>
        /// <param name="node">Node</param>
        /// <returns>True/False</returns>
        /// <remarks>Find a hotel Release | artf1014833 and artf1014958</remarks>
        private static bool RemoveAllUnBookableHotels(Node node)
        {
            if (node.GetPageData()["AvailableInBooking"] == null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Helps to remove all city or countries which is dont have any childs:artf1016983
        /// </summary>
        /// <param name="node">Node</param>
        /// <returns>True/False</returns>
        /// <remarks>Find a hotel Release </remarks>
        private static bool RemoveWithNochildrens(Node node)
        {
            int countryPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
            int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;

            if (((node.GetPageData().PageTypeID == cityPageTypeID) ||
                 (node.GetPageData().PageTypeID == countryPageTypeID)) && (node.GetChildren().Count < 1))
                return true;
            else
                return false;
        }

        #endregion

        public static void LogMessage(string message)
        {
            ILog customLog = LogManager.GetLogger("sogeti");
            if (customLog.IsInfoEnabled)
                customLog.Info(message);
        }

        private static string ReplaceSpecialChars(string input)
        {
            input = input.Replace('�', 'O');
            input = input.Replace('�', 'o');
            input = input.Replace('�', 'O');
            input = input.Replace('�', 'o');
            input = input.Replace('�', 'A');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'A');
            input = input.Replace('�', 'a');
            input = input.Replace('�', 'U');
            input = input.Replace('�', 'u');
            return input;
        }

        /// <summary>
        /// This will generating a subtree from the CMS tree with the requested page types.
        /// Please use cache who so ever is interested on this method as it will alawys fetch data from the Database.
        /// </summary>
        /// <param name="rootPageID">The root page id from where the tree should be fetched</param>
        /// <param name="pageTypeId">Page type ids which need to be fetched starting from the root node</param>
        /// <remarks>Find a hotel release</remarks>
        private static Node GetSubTree(int rootPageID, params int[] pageTypeId)
        {
            return GetSubTree(rootPageID, string.Empty, pageTypeId);
        }

        private static Node GetSubTree(int rootPageID, string language, params int[] pageTypeId)
        {
            int depthCounter = pageTypeId.Length - 1;
            int sequenceCounter = -1;
            PageData rootPage = null;
            if (string.IsNullOrEmpty(language))
            {
                rootPage = DataFactory.Instance.GetPage(new PageReference(rootPageID),
                                                             EPiServer.Security.AccessLevel.NoAccess);
            }
            else
            {
                ILanguageSelector languageSelector = new LanguageSelector(language);
                rootPage = DataFactory.Instance.GetPage(new PageReference(rootPageID), languageSelector,
                                                        EPiServer.Security.AccessLevel.NoAccess);
            }
            Node mainNode = new Node(rootPage);
            InitiateRecursion(rootPage.PageLink, sequenceCounter, depthCounter, mainNode, language, pageTypeId);
            return mainNode;
        }

        private static Node GetSubTreeRegardlessUnpublished(int rootPageID, params int[] pageTypeId)
        {
            int depthCounter = pageTypeId.Length - 1;
            int sequenceCounter = -1;
            PageData rootPage = DataFactory.Instance.GetPage(new PageReference(rootPageID),
                                                             EPiServer.Security.AccessLevel.NoAccess);
            Node mainNode = new Node(rootPage);
            InitiateRecursionRegardlessUnpublished(rootPage.PageLink, sequenceCounter, depthCounter, mainNode, pageTypeId);
            return mainNode;
        }
        private static void InitiateRecursionRegardlessUnpublished(PageReference reference, int sequenceCount, int depthCount,
                                      Node masterNode, params int[] pageTypeId)
        {
            sequenceCount++;
            PageDataCollection descendedNodes = GetChildPagesWithPageTypeIdRegardlessUnpublished(pageTypeId[sequenceCount], reference, currentLanguageString);
            foreach (PageData eachDescendedNode in descendedNodes)
            {
                Node mediateNode = new Node(eachDescendedNode);
                if (sequenceCount < depthCount)
                {
                    InitiateRecursionRegardlessUnpublished(eachDescendedNode.PageLink, sequenceCount, depthCount, mediateNode, pageTypeId);
                }
                masterNode.AddChild(mediateNode);
            }
        }
        public static PageDataCollection GetChildPagesWithPageTypeIdRegardlessUnpublished(int pageTypeId, PageReference rootPageLink, string languageBranch)
        {
            PageDataCollection searchResults = null;
            PropertyCriteriaCollection searchCriterias = new PropertyCriteriaCollection();
            PropertyCriteria pageTypeCriteria = new PropertyCriteria();
            pageTypeCriteria.Condition = EPiServer.Filters.CompareCondition.Equal;
            pageTypeCriteria.Name = "PageTypeID";
            pageTypeCriteria.Required = true;
            pageTypeCriteria.Type = EPiServer.Core.PropertyDataType.PageType;
            pageTypeCriteria.Value = pageTypeId.ToString();
            searchCriterias.Add(pageTypeCriteria);
            ILanguageSelector languageSelector = new LanguageSelector(languageBranch);
            if (!string.IsNullOrEmpty(languageBranch))
            {
                searchResults = DataFactory.Instance.FindAllPagesWithCriteria(rootPageLink, searchCriterias, languageBranch, languageSelector);
            }
            else
            {
                searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink, searchCriterias);
            }
            return searchResults;
        }

        /// <summary>
        /// This method will do a recursive search of nodes until the required tree is formed.        
        /// </summary>
        /// <param name="reference">Reference of the node from where the search will begin.</param>
        /// <param name="sequenceCount">Counter for the node level</param>
        /// <param name="depthCount">The depth leve of the tree requested.</param>
        /// <param name="masterNode">Parent node from where the search begin.</param>
        /// <param name="pageTypeId">Collection of the page type Id which need to be listed</param>
        /// <remarks>Find a hotel release</remarks>
        private static void InitiateRecursion(PageReference reference, int sequenceCount, int depthCount,
                                              Node masterNode, string language, params int[] pageTypeId)
        {
            sequenceCount++;
            PageDataCollection descendedNodes = GetChildPagesWithPageTypeId(pageTypeId[sequenceCount], reference, language);
            foreach (PageData eachDescendedNode in descendedNodes)
            {
                Node mediateNode = new Node(eachDescendedNode);
                if (sequenceCount < depthCount)
                {
                    InitiateRecursion(eachDescendedNode.PageLink, sequenceCount, depthCount, mediateNode, language, pageTypeId);
                }
                masterNode.AddChild(mediateNode);
            }
        }

        #region ImageGallery Region

        /// <summary>
        /// This method gets the pagedata for the specified operaID.
        /// </summary>
        /// <param name="operaID"></param>
        /// <returns></returns>
        public static PageData GetPageDataByOperaID(string operaID, string hotelName)
        {
            string cacheKey = string.Format(HotelDestinationCacheKey, operaID, currentLanguageString);
            PageData hotelPageData = new PageData();

            ContentDataAccess.LogMessage(string.Format("CDAL - Reloading Hotel With OperaID={0}", cacheKey));

            int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;

            PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                             EPiServer.Security.AccessLevel.NoAccess);
            PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

            PageDataCollection hotelPages = GetPagesWithPageTypeID(hotelPageTypeID, countryContainer);

            if (operaID.Equals("0"))
            {
                FilterCompareTo nameFilter = new FilterCompareTo("PageName", hotelName);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(hotelPages);
            }
            else
            {
                FilterCompareTo nameFilter = new FilterCompareTo("OperaID", operaID);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(hotelPages);
            }

            RemoveNonPublishedPages(hotelPages);

            if (hotelPages.Count > 0)
                hotelPageData = hotelPages[0];
            else
                throw new ContentDataAccessException(string.Format(TrackerConstants.MISSING_OPERAID, operaID));

            return hotelPageData;
        }

        #endregion

        /// <summary>
        /// while showing in the image gallery
        /// </summary>
        /// <returns></returns>
        private static string GetImageURL(object url)
        {
            string returnString = string.Empty;
            try
            {
                string imageString = url as string;
                int imageWidth = 472;
                if (imageString != null)
                {
                    ImageStoreNET.Classes.Util.UrlBuilder ub =
                        ImageStoreNET.Classes.Util.UrlBuilder.ParseUrl(imageString);
                    {
                        ub.Width = imageWidth;
                        ub.PreserveAspectRatio = true;
                        var lowercaseImageString = imageString.ToLower();
                        if (lowercaseImageString.Contains(".tif") || lowercaseImageString.Contains(".tiff"))
                        {
                            ub.Filename = null;
                            ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.Jpeg;
                        }
                        else
                        {
                            ub.ConversionFormatType = ImageStoreNET.Classes.Data.ConversionFormatTypes.WebSafe;
                        }
                        returnString = ub.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
            }
            return returnString;
        }

        /// <summary>
        /// Creates alternate hotel references 
        /// </summary>
        /// <param name="hotelChildPages"></param>
        /// <param name="alternativeHotelContainerPageTypeID"></param>
        /// <returns></returns>
        private static List<AlternativeHotelReference> CreateAlternateHotelReferences(PageDataCollection hotelChildPages, int alternativeHotelContainerPageTypeID)
        {
            List<AlternativeHotelReference> alternativeHotelReferences = new List<AlternativeHotelReference>();
            foreach (PageData container in hotelChildPages)
            {
                if (container.PageTypeID == alternativeHotelContainerPageTypeID)
                {
                    PageDataCollection alternativeHotels = DataFactory.Instance.GetChildren(container.PageLink);
                    foreach (PageData alternativeHotelPage in alternativeHotels)
                    {
                        if (alternativeHotelPage.CheckPublishedStatus(PagePublishedStatus.Published) &&
                            (alternativeHotelPage["AlternativeHotel"] as PageReference != null))
                        {
                            PageData hotelPage =
                                DataFactory.Instance.GetPage((PageReference)alternativeHotelPage["AlternativeHotel"],
                                                             EPiServer.Security.AccessLevel.NoAccess);
                            AlternativeHotelReference a =
                                new AlternativeHotelReference(hotelPage["OperaID"] as string ?? string.Empty,
                                                              (double)alternativeHotelPage["Distance"],
                                                              alternativeHotelPage["DistanceToCityCentreOfSearchedHotel"
                                                                  ] == null
                                                                  ? HYPHEN
                                                                  : alternativeHotelPage[
                                                                      "DistanceToCityCentreOfSearchedHotel"].ToString());
                            alternativeHotelReferences.Add(a);
                        }
                    }
                }
            }
            return alternativeHotelReferences;
        }

        /// <summary>
        /// Creates hotel room descriptions.
        /// </summary>
        /// <param name="hotelChildPages"></param>
        /// <param name="hotelRoomDescriptionContainerPageTypeID"></param>
        /// <param name="hotelPageURL"></param>
        private static List<HotelRoomDescription> CreateHotelRoomDescriptions(PageDataCollection hotelChildPages,
                                                  int hotelRoomDescriptionContainerPageTypeID, string hotelPageURL)
        {
            List<HotelRoomDescription> hotelRoomDescriptions = new List<HotelRoomDescription>();

            foreach (PageData container in hotelChildPages)
            {
                if (container.PageTypeID == hotelRoomDescriptionContainerPageTypeID)
                {
                    PageDataCollection hotelRoomDescriptionPages = DataFactory.Instance.GetChildren(container.PageLink);
                    foreach (PageData hotelRoomDescriptionPage in hotelRoomDescriptionPages)
                    {
                        if (hotelRoomDescriptionPage.CheckPublishedStatus(PagePublishedStatus.Published))
                        {
                            string roomDescription = hotelRoomDescriptionPage["RoomDescription"] as string ??
                                                     string.Empty;

                            bool isDefault = hotelRoomDescriptionPage["ShowAsDefault"] != null;

                            string roomDescriptionUrl = UriSupport.AddQueryString(hotelPageURL, "hotelpage", "rooms");
                            roomDescriptionUrl = UriSupport.AddQueryString(roomDescriptionUrl, "roomid",
                                                                           hotelRoomDescriptionPage.PageLink.ID.ToString
                                                                               ());

                            RoomCategory hotelRoomCategory =
                                GetRoomCategoryByPageReference((PageReference)hotelRoomDescriptionPage["RoomCategory"]);

                            hotelRoomCategory.RoomCategoryteserText =
                                hotelRoomDescriptionPage["RoomTeaserText"] as string ?? string.Empty;

                            hotelRoomCategory.ImageURL = GetMainImageURL(hotelRoomDescriptionPage);
                            hotelRoomCategory.RoomCategoryTooltipText =
                                hotelRoomDescriptionPage["RoomCategoryToolTipText"] as string ?? string.Empty;
                            if (!string.IsNullOrEmpty(roomDescription))
                                hotelRoomCategory.RoomCategoryDescription = roomDescription;

                            HotelRoomDescription hotelRoomDescription = new HotelRoomDescription(roomDescription,
                                                                                                 hotelRoomCategory,
                                                                                                 roomDescriptionUrl,
                                                                                                 isDefault);
                            hotelRoomDescriptions.Add(hotelRoomDescription);
                        }
                    }
                }
            }
            return hotelRoomDescriptions;
        }

        #endregion Private methods

        #region Image Gallery Region

        private static void CreateImageDetailList(PageData hotelPageData, Dictionary<string, HotelImageDetailsItemEntity> imageDetailsList, string imageType)
        {
            const int maxImages = 10;

            for (var index = 1; index <= maxImages; index++)
            {
                var imageIndex = string.Concat(imageType, ImagePart, index);
                var imageTitle = string.Concat(imageType, ImageTitlePart, index);
                var imageDescription = string.Concat(imageType, ImageDescriptionPart, index);

                var hotelGeneralImage1Details = new HotelImageDetailsItemEntity
                                                     {
                                                         ImageUrl = GetImageURL(hotelPageData[imageIndex]),
                                                         ImageTitle = hotelPageData[imageTitle] as string ?? string.Empty,
                                                         ImageDescription = hotelPageData[imageDescription] as string ?? string.Empty,
                                                         CMSImagePath = hotelPageData[imageIndex] != null ? hotelPageData[imageIndex].ToString() : string.Empty,
                                                         CMSLanguageId = hotelPageData.LanguageID
                                                     };
                imageDetailsList.Add(imageIndex, hotelGeneralImage1Details);
            }
        }

        private static void Create360ImageDetailList(PageData hotelPageData, Dictionary<string, HotelImageDetailsItemEntity> imageDetailsList)
        {
            const int maxImages = 4;

            for (var index = 1; index <= maxImages; index++)
            {
                var imageIndex = index != 1 ? string.Concat("360Image", index) : "360Image";
                var mediaTile = index != 1 ? string.Concat("ComplexMediaTitle", index) : "ComplexMediaTitle";
                var lineText = index != 1 ? string.Concat("ByLineText360Image", index) : "ByLineText360Image";
                var mediaDescription = index != 1
                                           ? string.Concat("ComplexMediaDescription", index)
                                           : "ComplexMediaDescription";

                var hotel360ImageDetails = new HotelImageDetailsItemEntity
                                               {
                                                   ImageUrl = hotelPageData[imageIndex] as string ?? string.Empty,
                                                   ImageTitle = hotelPageData[mediaTile] as string ?? string.Empty,
                                                   ByLineText = hotelPageData[lineText] as string ?? string.Empty,
                                                   ImageDescription = hotelPageData[mediaDescription] as string ?? string.Empty
                                               };
                imageDetailsList.Add(imageIndex, hotel360ImageDetails);
            }
        }

        /// <summary>
        /// Gets hotel destination image details.
        /// </summary>
        /// <param name="operaID"></param>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        public static HotelDestination GetHotelDestinationImageDetails(string operaID, string hotelName)
        {
            PageData hotelPageData = GetPageDataByOperaID(operaID, hotelName);
            var imageDetailsList = new Dictionary<string, HotelImageDetailsItemEntity>();

            CreateImageDetailList(hotelPageData, imageDetailsList, "General");
            CreateImageDetailList(hotelPageData, imageDetailsList, "Room");
            CreateImageDetailList(hotelPageData, imageDetailsList, "RestBar");
            CreateImageDetailList(hotelPageData, imageDetailsList, "Leisure");
            Create360ImageDetailList(hotelPageData, imageDetailsList);

            var hotelDestination = new HotelDestination { ImageDetailsList = imageDetailsList };
            return hotelDestination;
        }

        #endregion

        /// <summary>
        /// This method will be called by Tree View Control to get the PageData of all the relevant pages as node structure based 
        /// on the root page and page type IDs being passed.
        /// This will use the country container as root node and populate the node structure for page types country/city/hotel by calling GetSubTree
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        public static Node GetPageDataNodeTree()
        {
            return GetPageDataNodeTreeAll(false);
        }

        /// <summary>
        /// Added for DP- Release
        /// This is an overloaded and refined method to GetPageDataNodeTree()
        /// </summary>
        /// <param name="isBookable">Whether to include non-bookable hotels in the tree or not</param>
        /// <returns>country container node</returns>
        public static Node GetPageDataNodeTree(bool isBookable)
        {
            return GetPageDataNodeTreeByBookableStatus(isBookable, false);
        }

        #region Special Alerts

        public static string GetSpecialAlert(string hotelOperaID, bool isHoteLandingPage)
        {
            string spAlert = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(hotelOperaID))
                {
                    PageData hotel = ContentDataAccess.GetHotelPageDataByOperaID(hotelOperaID, string.Empty);
                    Boolean specialAlerts = hotel["ShowInBookingFlow"] != null ? true : false;
                    PageReference spAlertsPageReference = hotel["SpecialAlert"] != null
                                                              ? hotel["SpecialAlert"] as PageReference
                                                              : null;

                    if (specialAlerts || isHoteLandingPage)
                    {
                        PageData spAlertPageData = GetSpAlertPageData(spAlertsPageReference);

                        if (spAlertPageData != null &&
                            spAlertPageData.CheckPublishedStatus(PagePublishedStatus.Published))
                        {
                            spAlert = spAlertPageData["BoxContent"] as string;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogInfoMessage(ex.Message);
            }
            return spAlert;
        }

        private static PageData GetSpAlertPageData(PageReference spAlertsPageReference)
        {
            PageData spAlertPageLink = null;

            if (spAlertsPageReference != null)
            {
                spAlertPageLink = DataFactory.Instance.GetPage(spAlertsPageReference);
                if (spAlertPageLink["PageShortcutLink"] as PageReference != null)
                {
                    PageReference shortCutReference = spAlertPageLink["PageShortcutLink"] as PageReference;
                    spAlertPageLink = DataFactory.Instance.GetPage(shortCutReference);
                }
            }

            return spAlertPageLink;
        }

        public static PageData GetHotelPageDataByOperaID(string OperaID, string language)
        {
            return GetHotelPageDataByOperaID(OperaID, language, false);
        }

        public static PageData GetCityPageDataByOperaID(string OperaID)
        {
            return GetCityPageDataByOperaID(OperaID, false);
        }

        public static PageDataCollection GetChildren(PageReference parentLink)
        {
            PageDataCollection childPages = null;

            if (parentLink != null)
            {
                childPages = DataFactory.Instance.GetChildren(parentLink);
            }

            return childPages;
        }

        /// <summary>
        /// This class is made public to get the complete collection under any page type.
        /// This is made public in Hygiene Release 1.8.2 for site footer changes
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <returns></returns>
        public static PageDataCollection GetChildrenWithPageTypeID(int pageTypeID, PageReference rootPageLink)
        {
            return GetChildrenWithPageTypeID(pageTypeID, rootPageLink, false);
        }

        public static PageData GetPageData(PageReference pageLink)
        {
            if (pageLink != null)
            {
                return DataFactory.Instance.GetPage(pageLink);

            }

            return null;
        }

        public static PageData GetPageData(int pageId)
        {
            PageReference pageLink = new PageReference(pageId);

            if (pageLink != null)
            {
                return DataFactory.Instance.GetPage(pageLink);

            }

            return null;
        }
        #endregion

        /// <summary>
        /// Gets guarantee text from CMS based on language value
        /// </summary>
        /// <param name="bookingType"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string GetGuaranteeTextForReceipt(string bookingType, string language)
        {
            return GetGuaranteeTextForReceipt(bookingType, language, false);
        }

        public static bool GetCityTripAdvisorFlag(string cityOperaId)
        {
            bool hideCityTARatings = false;
            try
            {
                PageData pdCity = GetCityPageDataByOperaID(cityOperaId);
                hideCityTARatings = (pdCity !=null && pdCity["HideCityTARatings"] != null) ? Convert.ToBoolean(pdCity["HideCityTARatings"]) : false;
            }
            catch (ContentDataAccessException cex)
            {
                AppLogger.LogInfoMessage(cex.Message);
            }            
            return hideCityTARatings;
        }

        #region Cache Filler Methods
        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <param name="isCacheFillerRequest"> </param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public static List<CityDestination> GetCityAndHotel(bool ignoreBookingVisibility, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(AllCitiesAndHotelCacheKey, currentLanguageString, ignoreBookingVisibility);
            if ((ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - All Destinations - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                int[] pageTypeId = new int[] { cityPageTypeID, hotelPageTypeID };

                Node CityAndHotel = GetSubTree(countryContainer.ID, pageTypeId);

                List<Node> cityPages = CityAndHotel.GetChildren();

                cityPages.RemoveAll(RemoveWithNochildrens);

                cityPages.Sort(new NameComparer());

                RemoveNonPublishedPages(cityPages);
                List<CityDestination> cityDestinations = new List<CityDestination>();
                foreach (Node eachCity in cityPages)
                {
                    if (eachCity.GetPageData().StopPublish > DateTime.Now)
                    {
                        List<Node> hotelPages = eachCity.GetChildren();
                        List<HotelDestination> hotels = new List<HotelDestination>();
                        foreach (Node eachHotel in hotelPages)
                        {
                            if (ignoreBookingVisibility || eachHotel.GetPageData()["AvailableInBooking"] != null)
                            {
                                if (eachHotel.GetPageData().StopPublish > DateTime.Now)
                                {

                                    HotelDestination hotel = CreateHotelDestination(eachHotel.GetPageData());
                                    hotels.Add(hotel);
                                }
                            }
                        }
                        hotels.Sort(new HotelDestinationComparer());
                        string operaCityID = (eachCity.GetPageData()["OperaID"] as string).Trim();
                        string cityName = eachCity.GetPageData().PageName;
                        CityDestination city = new CityDestination(operaCityID, cityName, hotels);
                        string cityMapId = eachCity.GetPageData()["MapID"] as string;
                        if (cityMapId != null)
                        {
                            city.MapID = (cityMapId).Trim();
                        }

                        cityDestinations.Add(city);
                    }
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, cityDestinations, new Collection<object> { ignoreBookingVisibility, true },
                    (Func<bool, bool, List<CityDestination>>)GetCityAndHotel);
                return cityDestinations;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<CityDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey);
                return cachedDestinations;
            }
        }
        public static List<CityDestination> GetDestinationsFromTheLastSixMonthscityList(bool ignoreBookingVisibility)
        {
            return GetDestinationsFromTheLastSixMonthscityList(ignoreBookingVisibility, false);
        }
        /// <summary>
        /// List of Hotels other than which were unpublished beyond six months
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static List<CityDestination> GetDestinationsFromTheLastSixMonthscityList(bool ignoreBookingVisibility, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(DestinationsFromTheLastSixMonthscityListCacheKey, currentLanguageString, ignoreBookingVisibility);
            if ((ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - All Destinations - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                int[] pageTypeId = new int[] { cityPageTypeID, hotelPageTypeID };

                Node CityAndHotel = GetSubTreeRegardlessUnpublished(countryContainer.ID, pageTypeId);
                List<Node> cityPages = CityAndHotel.GetChildren();

                cityPages.Sort(new NameComparer());

                List<CityDestination> cityDestinations = new List<CityDestination>();
                foreach (Node eachCity in cityPages)
                {
                    List<Node> hotelPages = eachCity.GetChildren();
                    RemoveUnpublisedBeyondSixMonths(hotelPages);
                    List<HotelDestination> hotels = new List<HotelDestination>();
                    foreach (Node eachHotel in hotelPages)
                    {
                        HotelDestination hotel = CreateHotelDestination(eachHotel.GetPageData());
                        hotels.Add(hotel);
                    }
                    hotels.Sort(new HotelDestinationComparer());
                    string operaCityID = (eachCity.GetPageData()["OperaID"] as string).Trim();
                    string cityName = eachCity.GetPageData().PageName;
                    CityDestination city = new CityDestination(operaCityID, cityName, hotels);
                    string cityMapId = eachCity.GetPageData()["MapID"] as string;
                    if (cityMapId != null)
                    {
                        city.MapID = (cityMapId).Trim();
                    }

                    cityDestinations.Add(city);
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, cityDestinations, new Collection<object> { ignoreBookingVisibility, true },
                    (Func<bool, bool, List<CityDestination>>)GetDestinationsFromTheLastSixMonthscityList);
                return cityDestinations;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<CityDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey);
                return cachedDestinations;
            }
        }
        /// <summary>
        /// This mehthod is consolida the GetAllDestinations() and GetAllDestinationsIgnoreVisibility().
        /// This is implementating the new tree framework.
        /// </summary>
        /// <param name="ignoreBookingVisibility">If True then Ignores the hotel i.e hotel is not available for booking</param>
        /// <param name="isCacheFillerRequest"> </param>
        /// <returns>List of city</returns>
        /// <remarks>Find a hotel release.</remarks>
        public static List<CityDestination> GetCityAndHotelForAutoSuggest(bool ignoreBookingVisibility, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(AllCitiesAndHotelCacheKey, currentLanguageString, ignoreBookingVisibility);
            if ((ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - All Destinations - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                int[] pageTypeId = new int[] { cityPageTypeID, hotelPageTypeID };

                Node CityAndHotel = GetSubTree(countryContainer.ID, pageTypeId);

                List<Node> cityPages = CityAndHotel.GetChildren();

                cityPages.RemoveAll(RemoveWithNochildrens);

                cityPages.Sort(new NameComparer());

                RemoveNonPublishedPages(cityPages);
                List<CityDestination> cityDestinations = new List<CityDestination>();
                foreach (Node eachCity in cityPages)
                {
                    List<Node> hotelPages = eachCity.GetChildren();
                    List<HotelDestination> hotels = new List<HotelDestination>();
                    foreach (Node eachHotel in hotelPages)
                    {
                        if (ignoreBookingVisibility || eachHotel.GetPageData()["AvailableInBooking"] != null)
                        {
                            HotelDestination hotel = CreateHotelDestinationForAutoSuggest(eachHotel.GetPageData());
                            hotels.Add(hotel);
                        }
                    }
                    hotels.Sort(new HotelDestinationComparer());
                    string operaCityID = (eachCity.GetPageData()["OperaID"] as string).Trim();
                    string cityName = eachCity.GetPageData().PageName;
                    Point cityCoordinate = new Point(Convert.ToDouble(eachCity.GetPageData()["CenterGeoY"], CultureInfo.InvariantCulture),
                        Convert.ToDouble(eachCity.GetPageData()["CenterGeoX"], CultureInfo.InvariantCulture));
                    CityDestination city = new CityDestination(operaCityID, cityName, hotels);
                    city.Coordinate = cityCoordinate;
                    string cityMapId = eachCity.GetPageData()["MapID"] as string;
                    if (cityMapId != null)
                    {
                        city.MapID = (cityMapId).Trim();
                    }
                    string altCityNames = eachCity.GetPageData()["AlternateCityNames"] != null ? eachCity.GetPageData()["AlternateCityNames"] as string : string.Empty;
                    if (!string.IsNullOrEmpty(altCityNames))
                    {
                        city.AltCityNames = altCityNames.Trim();
                    }

                    cityDestinations.Add(city);
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, cityDestinations, new Collection<object> { ignoreBookingVisibility, true },
                    (Func<bool, bool, List<CityDestination>>)GetCityAndHotelForAutoSuggest);
                return cityDestinations;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<CityDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey);
                return cachedDestinations;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <param name="removeChildrens"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static List<CountryDestinatination>
                      GetCountryCityAndHotelForAutoSuggest(bool ignoreBookingVisibility, bool removeChildrens, bool isCacheFillerRequest, string language)
        {
            var currLanguage = string.IsNullOrEmpty(language) ? currentLanguageString : language;
            string cacheKey = string.Format(CountryCitiesAndHotelCacheKey, currLanguage, ignoreBookingVisibility, removeChildrens);
            if ((ScanwebCacheManager.Instance.LookInCache<List<CountryDestinatination>>(cacheKey)) == null || isCacheFillerRequest)
            {
                string message = "CDAL - All Country Destinations - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                int countryPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                int[] pageTypeId = new int[] { countryPageTypeID, cityPageTypeID, hotelPageTypeID };

                Node CountryCityAndHotel = GetSubTree(countryContainer.ID, currLanguage, pageTypeId);

                List<Node> countryPages = CountryCityAndHotel.GetChildren();

                if (removeChildrens)
                    countryPages.RemoveAll(RemoveWithNochildrens);

                countryPages.Sort(new NameComparer());

                RemoveNonPublishedPages(countryPages);
                for (int i = 0; i < countryPages.Count; i++)
                {
                    RemoveUnPublishedPage(countryPages[i]);
                    for (int j = 0; j < countryPages[i].GetChildren().Count; j++)
                    {
                        RemoveNonPublishedPages(countryPages[i].GetChildren());
                    }
                }
                List<Scandic.Scanweb.Core.CountryDestinatination> countryDestinations = new List<Scandic.Scanweb.Core.CountryDestinatination>();

                foreach (Node eachCountry in countryPages)
                {
                    List<CityDestination> cityDestinations = new List<CityDestination>();
                    List<Node> cityPages = eachCountry.GetChildren();
                    cityPages.Sort(new NameComparer());

                    foreach (Node eachCity in cityPages)
                    {
                        List<Node> hotelPages = eachCity.GetChildren();
                        List<HotelDestination> hotels = new List<HotelDestination>();
                        foreach (Node eachHotel in hotelPages)
                        {
                            if (ignoreBookingVisibility || eachHotel.GetPageData()["AvailableInBooking"] != null)
                            {
                                HotelDestination hotel = CreateHotelDestinationForAutoSuggest(eachHotel.GetPageData());
                                hotels.Add(hotel);
                            }
                        }
                        hotels.Sort(new HotelDestinationComparer());
                        string operaCityID = (eachCity.GetPageData()["OperaID"] as string).Trim();
                        string cityName = eachCity.GetPageData().PageName;

                        UrlBuilder url1 = new UrlBuilder(eachCity.GetPageData().LinkURL);
                        url1.QueryLanguage = currentLanguageString;
                        EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url1, eachCity.GetPageData().LinkURL, System.Text.UTF8Encoding.UTF8);
                        string cityLinkUrl = url1.ToString();

                        bool topDestination = false;
                        topDestination = Convert.ToBoolean(eachCity.GetPageData()["TopDestination"]);
                        CityDestination city = new CityDestination(operaCityID, cityName, hotels, topDestination, cityLinkUrl, eachCity.GetPageData().PageLink.ID);
                        string cityMapId = eachCity.GetPageData()["MapID"] as string;
                        if (cityMapId != null)
                        {
                            city.MapID = (cityMapId).Trim();
                        }
                        string altCityNames = eachCity.GetPageData()["AlternateCityNames"] != null ? eachCity.GetPageData()["AlternateCityNames"] as string : string.Empty;
                        if (!string.IsNullOrEmpty(altCityNames))
                        {
                            city.AltCityNames = altCityNames.Trim();
                        }

                        cityDestinations.Add(city);
                    }
                    string countryCode = (eachCountry.GetPageData()["CountryCode"] as string).Trim();
                    string countryName = eachCountry.GetPageData().PageName;

                    UrlBuilder url2 = new UrlBuilder(eachCountry.GetPageData().LinkURL);
                    url2.QueryLanguage = currentLanguageString;
                    EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url2, eachCountry.GetPageData().LinkURL, System.Text.UTF8Encoding.UTF8);
                    string countryLinkUrl = url2.ToString();

                    Scandic.Scanweb.Core.CountryDestinatination countryDestination = new Scandic.Scanweb.Core.CountryDestinatination(countryCode, countryName, cityDestinations, countryLinkUrl, Convert.ToString(eachCountry.GetPageData().PageLink.ID));
                    countryDestinations.Add(countryDestination);
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, countryDestinations, new Collection<object> { ignoreBookingVisibility, removeChildrens, true, currLanguage },
                    (Func<bool, bool, bool, string, List<CountryDestinatination>>)GetCountryCityAndHotelForAutoSuggest);
                return countryDestinations;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<Scandic.Scanweb.Core.CountryDestinatination> cachedDestinations =
                    ScanwebCacheManager.Instance.LookInCache<List<CountryDestinatination>>(cacheKey);
                return cachedDestinations;
            }
        }


        /// <summary>
        /// Fetches all destinations with non bookable status.
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <param name="isCacheFillerRequest"> </param>
        /// <returns></returns>
        public static List<CityDestination> FetchAllDestinationsWithNonBookable(bool ignoreBookingVisibility, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(AllNonBookableCitiesAndHotelCacheKey, currentLanguageString,
                                            ignoreBookingVisibility);
            if ((ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - All Destinations - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                int[] pageTypeId = new int[] { cityPageTypeID, hotelPageTypeID };

                Node CityAndHotel = GetSubTree(countryContainer.ID, pageTypeId);
                List<Node> cityPages = CityAndHotel.GetChildren();
                cityPages.RemoveAll(RemoveWithNochildrens);
                RemoveNonPublishedPages(cityPages);
                cityPages.Sort(new NameComparer());
                List<CityDestination> cityDestinations = new List<CityDestination>();
                foreach (Node eachCity in cityPages)
                {
                    List<Node> hotelPages = eachCity.GetChildren();
                    List<HotelDestination> hotels = new List<HotelDestination>();
                    foreach (Node eachHotel in hotelPages)
                    {
                        HotelDestination hotel = CreateHotelDestinationForAutoSuggest(eachHotel.GetPageData());
                        hotels.Add(hotel);
                    }
                    hotels.Sort(new HotelDestinationComparer());
                    string operaCityID = (eachCity.GetPageData()["OperaID"] as string).Trim();
                    string cityName = eachCity.GetPageData().PageName;
                    UrlBuilder url = new UrlBuilder(eachCity.GetPageData().LinkURL);
                    EPiServer.Global.UrlRewriteProvider.ConvertToExternal(url, eachCity.GetPageData().LinkURL, System.Text.UTF8Encoding.UTF8);
                    string cityLinkUrl = url.ToString();
                    bool topDestination = false;
                    topDestination = Convert.ToBoolean(eachCity.GetPageData()["TopDestination"]);
                    CityDestination city = new CityDestination(operaCityID, cityName, hotels, topDestination, cityLinkUrl, eachCity.GetPageData().PageLink.ID);
                    string cityMapId = eachCity.GetPageData()["MapID"] as string;
                    if (cityMapId != null)
                    {
                        city.MapID = (cityMapId).Trim();
                    }
                    string altCityNames = eachCity.GetPageData()["AlternateCityNames"] != null ? eachCity.GetPageData()["AlternateCityNames"] as string : string.Empty;
                    if (!string.IsNullOrEmpty(altCityNames))
                    {
                        city.AltCityNames = altCityNames.Trim();
                    }
                    // Add city to search results
                    cityDestinations.Add(city);
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, cityDestinations, new Collection<object> { ignoreBookingVisibility, true },
                                     (Func<bool, bool, List<CityDestination>>)FetchAllDestinationsWithNonBookable);
                return cityDestinations;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<CityDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<CityDestination>>(cacheKey);
                return cachedDestinations;
            }
        }

        /// <summary>
        /// Get all hotels configured under a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all hotels configured under a particular city from CMS
        /// </summary>
        /// <param name="cityCode">Unique code for city to be passed-OperaId</param>
        /// <param name="fetchAllHotels">Fetch All bookable and non bookable hotels-As per param passed</param>
        /// <returns>Return a list of hotels from a particular city</returns>
        private static List<HotelDestination> GetAllHotels(string cityCode, BookableNonBookableHotels bookableHotels, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(HotelDestinationsWithCityID, currentLanguageString, cityCode,
                                            bookableHotels.ToString());
            if ((ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - Hotel Destinations With City ID - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<HotelDestination> hotelDestinations = CreateHotelDestinationList(cityCode, bookableHotels);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelDestinations, new Collection<object> { cityCode, bookableHotels, true },
                    (Func<string, BookableNonBookableHotels, bool, List<HotelDestination>>)GetAllHotels);
                return hotelDestinations;
            }
            else
            {
                string message = "CDAL - Hotel Destinations With City ID - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                List<HotelDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey);
                return cachedDestinations;
            }
        }

        /// <summary>
        /// Get all hotels configured from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all hotels configured under a particular city from CMS
        /// </summary>
        /// <returns>Return a list of hotels from a particular city</returns>
        public static List<HotelDestination> GetAllHotels(bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(HotelDestinationsWithAllHotels, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - Hotel Destinations With City ID - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<HotelDestination> hotelDestinations = CreateHotelDestinationList();
                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelDestinations, new Collection<object> { true },
                    (Func<bool, List<HotelDestination>>)GetAllHotels);

                return hotelDestinations;
            }
            else
            {
                string message = "CDAL - Hotel Destinations With City ID - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<HotelDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey);

                return cachedDestinations;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="pgReference"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static PageData GetPageData(PageReference pgReference, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(PageDataCacheKey, pgReference);
            if (ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey) == null || isCacheFillerRequest)
            {

                PageData rootPage = DataFactory.Instance.GetPage(pgReference);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, rootPage, new Collection<object> { pgReference, true },
                (Func<PageReference, bool, PageData>)GetPageData);
                return rootPage;
            }
            else
            {
                PageData rootPage = ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey);
                return rootPage;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="pageTypeGUID"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static PageType GetContainerPageType(string pageTypeGUID, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(ContainerPageTypeWithGUIDCacheKey, pageTypeGUID);
            if (ScanwebCacheManager.Instance.LookInCache<PageType>(cacheKey) == null || isCacheFillerRequest)
            {
                PageType ContainerPageType = PageType.Load(new Guid(pageTypeGUID));
                ScanwebCacheManager.Instance.AddToCache(cacheKey, ContainerPageType, new Collection<object> { pageTypeGUID, true },
                    (Func<string, bool, PageType>)GetContainerPageType);
                return ContainerPageType;
            }
            else
            {
                PageType ContainerPageType = ScanwebCacheManager.Instance.LookInCache<PageType>(cacheKey);
                return ContainerPageType;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="isAllCityList"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        private static List<DestinationInfo> GetAllDestinationCity(string cacheKey, bool isAllCityList, bool isCacheFillerRequest)
        {
            List<DestinationInfo> cityList = null;
            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - Hotel Destinations With City ID - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                PageDataCollection hotelDestinations = CreateAllDestinationsList(isAllCityList);
                if (hotelDestinations != null)
                    ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelDestinations, new Collection<object> { cacheKey, isAllCityList, true },
                        (Func<string, bool, bool, List<DestinationInfo>>)GetAllDestinationCity);
                cityList = GetTopDestinationCity(hotelDestinations, isAllCityList);
            }
            else
            {
                string message = "CDAL - Hotel Destinations With City ID - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                PageDataCollection cachedDestinations = ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey);
                cityList = GetTopDestinationCity(cachedDestinations, isAllCityList);
            }
            return cityList;
        }

        /// <summary>
        /// Get all alternative city hotels for a particular city from Cache.If it is not available 
        /// in Cache , call CreateHotelDestinationList() method which will get 
        /// all alternative city hotels for a particular city from CMS
        /// </summary>
        /// <returns>Return a list of  alternative city hotels for a  particular city</returns>
        public static List<HotelDestination> GetAlternateCityHotels(string cityCode, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(AlternativeCityHotelsWithCityID, currentLanguageString, cityCode);

            if ((ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - Hotel Destinations With City ID - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<HotelDestination> hotelDestinations = CreateAlternateCityHotelList(cityCode);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelDestinations, new Collection<object> { cityCode, true },
                    (Func<string, bool, List<HotelDestination>>)GetAlternateCityHotels);
                return hotelDestinations;
            }
            else
            {
                string message = "CDAL - Hotel Destinations With City ID - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<HotelDestination> cachedDestinations = ScanwebCacheManager.Instance.LookInCache<List<HotelDestination>>(cacheKey);

                return cachedDestinations;
            }
        }

        /// <summary>
        /// Get all Transactions
        /// </summary>
        /// <returns>Return a list of all Transactions</returns>
        public static List<Transaction> GetAllTransactions(bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(AllTransactionsCacheKey, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<List<Transaction>>(cacheKey) == null) || isCacheFillerRequest)
            {
                string message = "CDAL - All Transactions - Repopulating Cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<Transaction> transactions = CreateTrasactionList();

                ScanwebCacheManager.Instance.AddToCache(cacheKey, transactions, new Collection<object> { true },
                    (Func<bool, List<Transaction>>)GetAllTransactions);

                return transactions;
            }
            else
            {
                string message = "CDAL - All Transactions - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);

                List<Transaction> cachedTransactions = ScanwebCacheManager.Instance.LookInCache<List<Transaction>>(cacheKey);

                return cachedTransactions;
            }
        }

        /// <summary>
        /// Gets a dictionary with RoomType keys and the corresponding RoomCategory
        /// </summary>
        /// <returns>A dictionary with all Room Type IDs and the each corresponding Room Category.</returns>
        public static Dictionary<string, RoomCategory> GetRoomTypeCategoryMap(bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(RoomTypeCategoryMapCacheKey, currentLanguageString);

            if (ScanwebCacheManager.Instance.LookInCache<Dictionary<string, RoomCategory>>(cacheKey) == null || isCacheFillerRequest)
            {
                int roomTypePageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["RoomTypePageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference roomCategoryContainer = (PageReference)rootPage["RoomCategoryContainer"];

                PageDataCollection roomTypePages = GetPagesWithPageTypeID(roomTypePageTypeID, roomCategoryContainer);

                Dictionary<string, RoomCategory> roomTypeCategoryMap = new Dictionary<string, RoomCategory>();

                foreach (PageData roomTypePage in roomTypePages)
                {
                    string roomTypeID = (string)roomTypePage["OperaID"];
                    RoomCategory roomCategory = GetRoomCategoryByPageReference(roomTypePage.ParentLink);

                    if (roomTypeCategoryMap.ContainsKey(roomTypeID))
                        throw new ContentDataAccessException(
                            "Duplicate entry in RoomTypeCategoryMap. There must be only one RoomType with a specific OperaID.");
                    else
                        roomTypeCategoryMap.Add(roomTypeID, roomCategory);
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, roomTypeCategoryMap, new Collection<object> { true },
                    (Func<bool, Dictionary<string, RoomCategory>>)GetRoomTypeCategoryMap);
                return roomTypeCategoryMap;
            }
            else
            {
                Dictionary<string, RoomCategory> cachedRoomTypeCategoryMap =
                    ScanwebCacheManager.Instance.LookInCache<Dictionary<string, RoomCategory>>(cacheKey);
                return cachedRoomTypeCategoryMap;
            }
        }

        /// <summary>
        /// Gets a dictionary with RateType keys and the corresponding RateCategories
        /// </summary>
        /// <returns>A dictionary with all Rate Type IDs and the each corresponding Rate Category.</returns>
        public static Dictionary<string, RateCategory> GetRateTypeCategoryMap(bool fetchContentInEnglish, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(RateTypeCategoryMapCacheKey, fetchContentInEnglish ? "en" : currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, RateCategory>>(cacheKey) == null) || isCacheFillerRequest)
            {
                int rateTypePageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["RateTypePageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference rateCategoryContainer = (PageReference)rootPage["RateCategoryContainer"];

                PageDataCollection rateTypePages = GetPagesWithPageTypeID(rateTypePageTypeID, rateCategoryContainer);

                Dictionary<string, RateCategory> rateTypeCategoryMap = new Dictionary<string, RateCategory>();

                RemoveNonPublishedPages(rateTypePages);

                foreach (PageData rateTypePage in rateTypePages)
                {
                    string rateTypeID = (string)rateTypePage["OperaID"];
                    RateCategory rateCategory = GetRateCategoryByRateTypeID(rateTypeID, fetchContentInEnglish);
                    if (rateTypeID != null && !rateTypeCategoryMap.ContainsKey(rateTypeID))
                        rateTypeCategoryMap.Add(rateTypeID, rateCategory);
                }

                ScanwebCacheManager.Instance.AddToCache(cacheKey, rateTypeCategoryMap, new Collection<object> { fetchContentInEnglish, true },
                    (Func<bool, bool, Dictionary<string, RateCategory>>)GetRateTypeCategoryMap);
                Dictionary<string, RateCategory> newRateTypeCategoryMap = new Dictionary<string, RateCategory>();

                foreach (KeyValuePair<string, RateCategory> k in rateTypeCategoryMap)
                {
                    newRateTypeCategoryMap.Add(k.Key, (RateCategory)k.Value.Clone());
                }
                return newRateTypeCategoryMap;
            }
            else
            {
                Dictionary<string, RateCategory> cachedRateTypeCategoryMap =
                    ScanwebCacheManager.Instance.LookInCache<Dictionary<string, RateCategory>>(cacheKey);

                return cachedRateTypeCategoryMap;
            }
        }



        /// <summary>
        /// This will give the editable text used in email template.
        /// This text are conofigured in CMS and editable by content editor.
        /// </summary>
        /// <param name="booking">Credit card booking type. e.g GTDCC or GTDEEP</param>
        /// <param name="bookingType">Either Booking/Modify/Cancel</param>
        /// <param name="creditcardStatus">
        /// Wheather this booking is done using creadit card or with out credit card
        /// Booking with Credit card, string should be - WITHCREDITCARD.
        /// Booking with out Credit card, string should be  - WITHOUTCREDITCARD.
        /// </param>
        /// <returns>Collection of dynamic placeholder and value fom CMS.</returns>
        /// <remarks>CR 4 | Text based email confirmation | Release 1.4</remarks>
        public static Dictionary<string, string> GetTextForEmailTemplate(string booking, string bookingType,
                                                                         string creditcardStatus, bool isPrepaidBooking, bool isCacheFillerRequest)
        {
            Dictionary<string, string> returnValue = null;

            string bookingTypeKey = ConfirmationTexts + booking + bookingType + creditcardStatus;
            string cacheKey = string.Format(bookingTypeKey, currentLanguageString);
            if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey) == null) || isCacheFillerRequest)
            {
                Dictionary<string, string> cmsMap = new Dictionary<string, string>();

                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                if ((null != rootPage) && (null != rootPage["EmailConfirmationContainer"]))
                {
                    PageReference emailConfirmationContainer = (PageReference)rootPage["EmailConfirmationContainer"];

                    PageDataCollection emailConfirmationResults = null;
                    emailConfirmationResults = DataFactory.Instance.GetChildren(emailConfirmationContainer);

                    if (emailConfirmationResults != null && emailConfirmationResults.Count > 0)
                    {
                        for (int confirmationPageCounter = 0;
                             confirmationPageCounter < emailConfirmationResults.Count;
                             confirmationPageCounter++)
                        {
                            if ((null != emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"]) &&
                                (bookingType.ToLower().Trim() ==
                                 emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"].ToString()
                                     .ToLower().Trim()))
                            {
                                PageData individualConfirmationPage = emailConfirmationResults[confirmationPageCounter];

                                cmsMap["CONFIRMATIONTEXT"] =
                                    (null != individualConfirmationPage["ConfirmationText"]
                                         ? individualConfirmationPage["ConfirmationText"].ToString()
                                         : string.Empty);
                                cmsMap["CONTACTUSTEXT"] =
                                    (null != individualConfirmationPage["ContactUsText"]
                                         ? individualConfirmationPage["ContactUsText"].ToString()
                                         : string.Empty);

                                cmsMap["STORYBOXTEXT"] =
                                    (null != individualConfirmationPage["StoryBoxText"]
                                         ? individualConfirmationPage["StoryBoxText"].ToString()
                                         : string.Empty);

                                if (string.Equals(booking.ToLower(), ConfigurationManager.AppSettings["Save.Gurantee"].ToLower()) &&
                                    isPrepaidBooking)
                                {
                                    cmsMap["POLICYTEXT"] =
                                    (null != individualConfirmationPage["CancellationPolicyTextPrepaid"]
                                         ? individualConfirmationPage["CancellationPolicyTextPrepaid"].ToString()
                                         : string.Empty);
                                }
                                else
                                {
                                    string cancelKey = "CancellationPolicyText" + creditcardStatus + booking;
                                    cmsMap["POLICYTEXT"] =
                                        (null != individualConfirmationPage[cancelKey]
                                             ? individualConfirmationPage[cancelKey].ToString()
                                             : string.Empty);
                                }

                                cmsMap["HTMLCONFIRMATIONHEADER"] =
                                    (null != individualConfirmationPage["GuaranteeAndCancellationHeaderHTML"])
                                        ? individualConfirmationPage["GuaranteeAndCancellationHeaderHTML"].ToString()
                                        : string.Empty;

                                ScanwebCacheManager.Instance.AddToCache(cacheKey, cmsMap, new Collection<object> { booking, bookingType, creditcardStatus, isPrepaidBooking, true },
                                                                        (Func<string, string, string, bool, bool, Dictionary<string, string>>)GetTextForEmailTemplate);
                                break;
                            }
                        }
                    }
                }
                returnValue = cmsMap;
            }
            else
            {
                Dictionary<string, string> cmsMap = ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey);
                returnValue = cmsMap;
            }
            return returnValue;
        }

        /// <summary>
        /// Overloaded method. This method is called during the Redemption booking
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns>
        /// Cancellation Policy Value.
        /// </returns>
        /// <remarks>
        /// </remarks>
        public static Dictionary<string, string> GetTextForEmailTemplate(string bookingType, bool isCacheFillerRequest)
        {
            string redemptionKey = RedemptionTexts;
            string cacheKey = string.Format(redemptionKey, bookingType, currentLanguageString);
            Dictionary<string, string> cmsMap = new Dictionary<string, string>();
            if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey) == null) || isCacheFillerRequest)
            {
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                if ((null != rootPage) && (null != rootPage["EmailConfirmationContainer"]))
                {
                    PageReference emailConfirmationContainer = (PageReference)rootPage["EmailConfirmationContainer"];
                    PageDataCollection emailConfirmationResults = null;
                    emailConfirmationResults = DataFactory.Instance.GetChildren(emailConfirmationContainer);

                    if (emailConfirmationResults != null && emailConfirmationResults.Count > 0)
                    {
                        for (int confirmationPageCounter = 0;
                             confirmationPageCounter < emailConfirmationResults.Count;
                             confirmationPageCounter++)
                        {
                            if ((null != emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"]) &&
                                (bookingType.ToLower().Trim() ==
                                 emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"].ToString()
                                     .ToLower().Trim()))
                            {
                                cmsMap["POLICYTEXT"] = (null !=
                                                        emailConfirmationResults[confirmationPageCounter].Property[
                                                            "RewardNightCancelPolicyHTML"])
                                                           ? emailConfirmationResults[confirmationPageCounter].Property[
                                                               "RewardNightCancelPolicyHTML"].ToString()
                                                           : string.Empty;
                                ScanwebCacheManager.Instance.AddToCache(cacheKey, cmsMap, new Collection<object> { bookingType, true },
                                    (Func<string, bool, Dictionary<string, string>>)GetTextForEmailTemplate);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                cmsMap = ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey);
            }
            return cmsMap;
        }


        /// <summary>
        /// Gets the text for email template.
        /// </summary>
        /// <param name="bookingType">Type of the booking.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetConfirmationEmailTextFormCMS(string bookingType, string language, bool isCacheFillerRequest)
        {
            Dictionary<string, string> returnValue = null;
            //Language parameter will be set in the case this method is called for mobile application
            var lang = string.IsNullOrEmpty(language) ? currentLanguageString : language;
            //For different types of booking,booking types and credit card status the key are different.
            string cacheKey = string.Format(BookingTypeKey, bookingType, currentLanguageString);
            //If the cache key is not present then access from CDAL 
            if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey) == null) || isCacheFillerRequest)
            {
                Dictionary<string, string> cmsMap = new Dictionary<string, string>();

                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
                //Check if the root page is not null and the 'EmailConfirmationContainer' property is not null.
                if ((null != rootPage) && (null != rootPage["EmailConfirmationContainer"]))
                {
                    PageReference emailConfirmationContainer = (PageReference)rootPage["EmailConfirmationContainer"];

                    PageDataCollection emailConfirmationResults = null;
                    ILanguageSelector languageSelector = new LanguageSelector(lang);
                    emailConfirmationResults = DataFactory.Instance.GetChildren(emailConfirmationContainer, languageSelector);

                    if (emailConfirmationResults != null && emailConfirmationResults.Count > 0)
                    {
                        for (int confirmationPageCounter = 0;
                             confirmationPageCounter < emailConfirmationResults.Count;
                             confirmationPageCounter++)
                        {
                            if ((null != emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"]) &&
                                (bookingType.ToLower().Trim() ==
                                 emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"].ToString()
                                     .ToLower().Trim()))
                            {
                                PageData individualConfirmationPage = emailConfirmationResults[confirmationPageCounter];

                                cmsMap["CONFIRMATIONTEXT"] =
                                    (null != individualConfirmationPage["ConfirmationText"]
                                         ? individualConfirmationPage["ConfirmationText"].ToString()
                                         : string.Empty);

                                cmsMap["CONTACTUSTEXT"] =
                                    (null != individualConfirmationPage["ContactUsText"]
                                         ? individualConfirmationPage["ContactUsText"].ToString()
                                         : string.Empty);

                                cmsMap["DIDYOUKNOWTEXTHEADER"] =
                                    (null != individualConfirmationPage["StoryBoxTextHeader"]
                                         ? individualConfirmationPage["StoryBoxTextHeader"].ToString()
                                         : string.Empty);
                                cmsMap["DIDYOUKNOWTEXT"] =
                                    (null != individualConfirmationPage["StoryBoxText"]
                                         ? individualConfirmationPage["StoryBoxText"].ToString()
                                         : string.Empty);
                                cmsMap["OTHERIMPORTANTINFORMATIONTEXTHEADER"] =
                                    (null != individualConfirmationPage["OtherImportantInformationTextHeader"]
                                         ? individualConfirmationPage["OtherImportantInformationTextHeader"].ToString()
                                         : string.Empty);
                                cmsMap["OTHERIMPORTANTINFORMATIONTEXT"] =
                                    (null != individualConfirmationPage["OtherImportantInformationText"]
                                         ? individualConfirmationPage["OtherImportantInformationText"].ToString()
                                         : string.Empty);
                                cmsMap["GUARANTEEANDCANCELLATIONHEADER"] =
                                    (null != individualConfirmationPage["GuaranteeAndCancellationHeaderHTML"]
                                         ? individualConfirmationPage["GuaranteeAndCancellationHeaderHTML"].ToString()
                                         : string.Empty);

                                ScanwebCacheManager.Instance.AddToCache(cacheKey, cmsMap, new Collection<object> { bookingType, language, true },
                                    (Func<string, string, bool, Dictionary<string, string>>)GetConfirmationEmailTextFormCMS);
                                break;
                            }
                        }
                    }
                }
                returnValue = cmsMap;
            }
            else
            {
                Dictionary<string, string> cmsMap =
                    ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey);
                returnValue = cmsMap;
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetUserInterestList(bool isCacheFillerRequest)
        {
            var cacheKey = string.Format(UserInterestsCacheKey, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey) == null) || isCacheFillerRequest)
            {
                var rootPage = DataFactory.Instance.GetPage(PageReference.RootPage, EPiServer.Security.AccessLevel.NoAccess);
                var userInterestContainer = (PageReference)rootPage["UserInterestContainer"];
                var userInterests = DataFactory.Instance.GetChildren(userInterestContainer);
                var userInterestList = new Dictionary<string, string>();

                RemoveNonPublishedPages(userInterests);

                foreach (var userInterestPage in userInterests)
                {
                    var interestId = (string)userInterestPage["OperaID"];
                    var interestDescription = (string)userInterestPage["InterestDescription"];
                    userInterestList.Add(interestId, interestDescription);
                }

                ScanwebCacheManager.Instance.AddToCache(cacheKey, userInterestList, new BindingList<object> { true },
                    (Func<bool, Dictionary<string, string>>)GetUserInterestList);

                return userInterestList;
            }
            else
            {
                var cachedUserInterestList =
                    ScanwebCacheManager.Instance.LookInCache<Dictionary<string, string>>(cacheKey);
                return cachedUserInterestList;
            }
        }

        /// <summary>
        /// This returns the merchant profile associated wiht the hotel
        /// </summary>
        /// <param name="hotelOperaID">hotelOperaID</param>
        /// <returns></returns>
        public static PageData GetMerchantByHotelOperaId(string hotelOperaID, string language, bool isCacheFillerRequest)
        {
            string cacheKey = string.Empty;

            if (!string.IsNullOrEmpty(language))
                cacheKey = string.Format(MerchantProfileByHotelID, "HotelCode", hotelOperaID + "_" + language);
            else
                cacheKey = string.Format(MerchantProfileByHotelID, "HotelCode", hotelOperaID);

            if ((ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey) == null) || isCacheFillerRequest)
            {
                PageData merchantProfile = null;

                if (hotelOperaID != null)
                {
                    PageData hotel = ContentDataAccess.GetHotelPageDataByOperaID(hotelOperaID, language);
                    PageReference profile = null;
                    if (AppConstants.UseNetsTestMerchant)
                    {
                        profile = hotel["MerchantProfileTest"] != null ? hotel["MerchantProfileTest"] as PageReference : null;
                    }
                    else
                    {
                        profile = hotel["MerchantProfile"] != null ? hotel["MerchantProfile"] as PageReference : null;
                    }
                    if (profile != null)
                    {
                        merchantProfile = GetPageData(profile);
                        ScanwebCacheManager.Instance.AddToCache(cacheKey, merchantProfile, new Collection<object> { hotelOperaID, language, true },
                            (Func<string, string, bool, PageData>)GetMerchantByHotelOperaId);
                    }
                }
                return merchantProfile;
            }
            else
            {
                string message = "CDAL - Merchant Profile With Hotel ID - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                return ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey);
            }
        }

        /// <summary>
        /// This class is made public to get the complete collection under any page type.
        /// This is made public in Hygiene Release 1.8.2 for site footer changes
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <returns></returns>
        public static PageDataCollection GetPagesWithPageTypeID(int pageTypeID, PageReference rootPageLink, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(PageListCacheKey, pageTypeID.ToString(), currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeID - Refreshing List - Key={0}", cacheKey));

                PropertyCriteriaCollection searchCriterias = new PropertyCriteriaCollection();

                PropertyCriteria pageTypeCriteria = new PropertyCriteria();
                pageTypeCriteria.Condition = EPiServer.Filters.CompareCondition.Equal;
                pageTypeCriteria.Name = "PageTypeID";
                pageTypeCriteria.Required = true;
                pageTypeCriteria.Type = EPiServer.Core.PropertyDataType.PageType;
                pageTypeCriteria.Value = pageTypeID.ToString();
                searchCriterias.Add(pageTypeCriteria);

                LogMessage(string.Format("CDAL - Calling findPagesWithCriteria PageTypeID={0} RootPageLinkID={1}",
                                         pageTypeID.ToString(), rootPageLink.ID.ToString()));

                //Removed the language parameter i.e currentLanguageString, since fallback functionality is not working if we pass language parameter.
                PageDataCollection searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink,
                    searchCriterias);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, searchResults, new Collection<object> { pageTypeID, rootPageLink, true },
                    (Func<int, PageReference, bool, PageDataCollection>)GetPagesWithPageTypeID);
                return GetCopyPageDataCollection(searchResults);
            }
            else
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeID  - Returned list from cache - Key={0}", cacheKey));
                return GetCopyPageDataCollection(ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey));
            }
        }

        /// <summary>
        /// Returns hotels based on the language
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static PageDataCollection GetPagesWithPageTypeIDAndLanguage(int pageTypeID, PageReference rootPageLink, string language, bool isCacheFillerRequest)
        {
            string cacheKey = string.Empty;

            //cache key by language
            if (!string.IsNullOrEmpty(language))
                cacheKey = string.Format(PageListCacheKey, pageTypeID.ToString(), language);
            else
                cacheKey = string.Format(PageListCacheKey, pageTypeID.ToString(), currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeIDAndLanguage - Refreshing List - Key={0}", cacheKey));

                PropertyCriteriaCollection searchCriterias = new PropertyCriteriaCollection();

                PropertyCriteria pageTypeCriteria = new PropertyCriteria();
                pageTypeCriteria.Condition = EPiServer.Filters.CompareCondition.Equal;
                pageTypeCriteria.Name = "PageTypeID";
                pageTypeCriteria.Required = true;
                pageTypeCriteria.Type = EPiServer.Core.PropertyDataType.PageType;
                pageTypeCriteria.Value = pageTypeID.ToString();
                searchCriterias.Add(pageTypeCriteria);

                LogMessage(string.Format("CDAL - Calling findPagesWithCriteria PageTypeID={0} RootPageLinkID={1}",
                                         pageTypeID.ToString(), rootPageLink.ID.ToString()));

                PageDataCollection searchResults = null;

                if (!string.IsNullOrEmpty(language))
                {
                    ILanguageSelector langSelector = new LanguageSelector(language);
                    searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink, searchCriterias, language, langSelector);
                }
                else
                {
                    searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink, searchCriterias);
                }

                ScanwebCacheManager.Instance.AddToCache(cacheKey, searchResults, new Collection<object> { pageTypeID, rootPageLink, language, true },
                    (Func<int, PageReference, string, bool, PageDataCollection>)GetPagesWithPageTypeIDAndLanguage);
                return GetCopyPageDataCollection(searchResults);
            }
            else
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeID  - Returned list from cache - Key={0}", cacheKey));
                // There are pages in the cache                
                return GetCopyPageDataCollection(ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey));
            }
        }

        private static HotelDestination GetHotelDestination(string operaID, string language, bool isCacheFillerRequest)
        {
            string cacheKey = string.Empty;

            //cache key by language
            if (!string.IsNullOrEmpty(language))
                cacheKey = string.Format(HotelDestinationCacheKey, operaID, language);
            else
                cacheKey = string.Format(HotelDestinationCacheKey, operaID, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<HotelDestination>(cacheKey) == null) || isCacheFillerRequest)
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Reloading Hotel With OperaID={0}", cacheKey));

                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                PageDataCollection hotelPages = GetPagesWithPageTypeIDAndLanguage(hotelPageTypeID, countryContainer, language);

                FilterCompareTo nameFilter = new FilterCompareTo("OperaID", operaID);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(hotelPages);

                RemoveNonPublishedPages(hotelPages);

                PageData hotelPageData = new PageData();


                if (hotelPages.Count > 0)
                    hotelPageData = hotelPages[0];
                else
                    throw new ContentDataAccessException(string.Format(TrackerConstants.MISSING_OPERAID, operaID));

                HotelDestination hotel = CreateHotelDestination(hotelPageData);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotel, new Collection<object> { operaID, language, true },
                    (Func<string, string, bool, HotelDestination>)GetHotelDestination);
                //return (HotelDestination)hotel.Clone();
                return hotel;
            }
            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Hotel With OperaID={0}", cacheKey));
                HotelDestination cachedHotel = ScanwebCacheManager.Instance.LookInCache<HotelDestination>(cacheKey);
                return cachedHotel;
            }
        }

        /// <summary>
        /// Gets the hotel destination with the Hotel Name 
        /// </summary>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        public static HotelDestination GetHotelDestinationWithHotelName(string hotelName, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(HotelDestinationCacheKey, hotelName, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<HotelDestination>(cacheKey) == null) || isCacheFillerRequest)
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Reloading Hotel With Name={0}", cacheKey));

                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                PageDataCollection hotelPages = GetPagesWithPageTypeID(hotelPageTypeID, countryContainer);

                FilterCompareTo nameFilter = new FilterCompareTo("Heading", hotelName);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(hotelPages);

                RemoveNonPublishedPages(hotelPages);

                PageData hotelPageData = new PageData();

                if (hotelPages.Count > 0)
                    hotelPageData = hotelPages[0];
                else
                    throw new ContentDataAccessException(
                        "Could not find a HotelDestination with the specified hotelName.");

                HotelDestination hotel = CreateHotelDestination(hotelPageData);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotel, new Collection<object> { hotelName, true },
                    (Func<string, bool, HotelDestination>)GetHotelDestinationWithHotelName);
                return (HotelDestination)hotel.Clone();
            }
            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Hotel With hotelName={0}", cacheKey));

                HotelDestination cachedHotel = ScanwebCacheManager.Instance.LookInCache<HotelDestination>(cacheKey);
                return cachedHotel;
            }
        }

        /// <summary>
        /// This function returns the country PageData using countryCode
        /// </summary>
        /// <param name="countryCode">CountryCode</param>
        /// <returns></returns>
        public static PageDataCollection GetCountryList(bool isCacheFillerRequest)
        {
            PageData countryPage = null;
            PageDataCollection countryPages = null;
            string cacheKey = string.Format(AllCountryCacheKey, currentLanguageString);
            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                int countryPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;

                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                countryPages = GetPagesWithPageTypeID(countryPageTypeID, countryContainer);

                RemoveNonPublishedPages(countryPages);

                if (countryPages.Count > 0)
                {
                }
                else
                    throw new ContentDataAccessException("Could not find a Country with the specified CountryCode.");

                ScanwebCacheManager.Instance.AddToCache(cacheKey, countryPages, new Collection<object> { true },
                    (Func<bool, PageDataCollection>)GetCountryList);
            }

            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Country With CountryCode={0}",
                                                           cacheKey));

                PageDataCollection cachedCountryPageData = ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey);
                countryPages = cachedCountryPageData;
            }

            return countryPages;
        }

        /// <summary>
        /// This function returns the country PageData using countryCode
        /// </summary>
        /// <param name="countryCode">CountryCode</param>
        /// <returns></returns>
        private static PageData GetCountryByCountryCode(string countryCode, bool isCacheFillerRequest)
        {
            PageData countryPage = null;
            string cacheKey = string.Format(CountryDestinationCacheKey, countryCode, currentLanguageString);
            if ((ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey) == null) || isCacheFillerRequest)
            {
                int countryPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;

                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                PageDataCollection countryPages = GetPagesWithPageTypeID(countryPageTypeID, countryContainer);

                FilterCompareTo nameFilter = new FilterCompareTo("CountryCode", countryCode);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(countryPages);

                RemoveNonPublishedPages(countryPages);

                if (countryPages.Count > 0)
                {
                    countryPage = countryPages[0];
                }
                else
                    throw new ContentDataAccessException("Could not find a Country with the specified CountryCode.");

                ScanwebCacheManager.Instance.AddToCache(cacheKey, countryPage, new Collection<object> { countryCode, true },
                    (Func<string, bool, PageData>)GetCountryByCountryCode);
            }

            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Country With CountryCode={0}",
                                                           cacheKey));

                PageData cachedCountryPageData = ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey);
                countryPage = cachedCountryPageData;
            }

            return countryPage;
        }

        /// <summary>
        /// This method will be called by Tree View Control to get the PageData of all the relevant pages as node structure based 
        /// on the root page and page type IDs being passed.
        /// This will use the country container as root node and populate the node structure for page types country/city/hotel by calling GetSubTree
        /// </summary>
        /// <remarks>Find a hotel release</remarks>
        public static Node GetPageDataNodeTreeAll(bool isCacheFillerRequest)
        {

            string cacheKey = string.Format(AllCountryCitiesAndHotelCacheKey, currentLanguageString);
            if ((ScanwebCacheManager.Instance.LookInCache<Node>(cacheKey) == null) || isCacheFillerRequest)
            {
                int countryPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                int[] pageTypeId = { countryPageTypeID, cityPageTypeID, hotelPageTypeID };
                Node tree = GetSubTree(countryContainer.ID, pageTypeId);


                List<Node> countries = tree.GetChildren();

                countries.RemoveAll(RemoveWithNochildrens);

                foreach (Node country in countries)
                {
                    List<Node> cities = country.GetChildren();
                    cities.RemoveAll(RemoveWithNochildrens);
                }

                countries.Sort(new NameComparer());
                foreach (Node eachCountryNode in countries)
                {
                    List<Node> cities = eachCountryNode.GetChildren();

                    cities.Sort(new NameComparer());

                    foreach (Node city in cities)
                    {
                        List<Node> hotels = city.GetChildren();

                        hotels.Sort(new HotelNameComparer());
                        hotels.RemoveAll(RemoveAllUnBookableHotels);
                        hotels.RemoveAll(RemoveUnPublishedPage);
                    }
                }
                ScanwebCacheManager.Instance.AddToCache(cacheKey, tree, new Collection<object> { true },
                    (Func<bool, Node>)GetPageDataNodeTreeAll);
                return tree;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                return ScanwebCacheManager.Instance.LookInCache<Node>(cacheKey);
            }
        }

        /// <summary>
        /// Added for DP- Release
        /// This is an overloaded and refined method to GetPageDataNodeTree()
        /// </summary>
        /// <param name="isBookable">Whether to include non-bookable hotels in the tree or not</param>
        /// <returns>country container node</returns>
        public static Node GetPageDataNodeTreeByBookableStatus(bool isBookable, bool isCacheFillerRequest)
        {
            Node rootNode = null;
            string cacheKey = string.Format(AllCountryCitiesAndBookableHotelCacheKey, currentLanguageString);
            if ((ScanwebCacheManager.Instance.LookInCache<Node>(cacheKey) == null) || isCacheFillerRequest)
            {
                int countryPageTypeID =
                    PageType.Load(new Guid(ConfigurationManager.AppSettings["CountryPageTypeGUID"])).ID;
                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];
                int[] pageTypeId = { countryPageTypeID, cityPageTypeID, hotelPageTypeID };
                Node tree = GetSubTree(countryContainer.ID, pageTypeId);

                List<Node> countries = tree.GetChildren();
                foreach (Node country in countries)
                {
                    List<Node> cities = country.GetChildren();
                    foreach (Node city in cities)
                    {
                        List<Node> hotels = city.GetChildren();

                        hotels.RemoveAll(RemoveUnPublishedPage);

                        if (isBookable == true)
                        {
                            hotels.RemoveAll(RemoveAllUnBookableHotels);
                        }

                        hotels.Sort(new HotelNameComparer());
                    }

                    cities.RemoveAll(RemoveUnPublishedPage);

                    cities.RemoveAll(RemoveWithNochildrens);

                    cities.Sort(new NameComparer());
                }

                countries.RemoveAll(RemoveUnPublishedPage);

                countries.RemoveAll(RemoveWithNochildrens);

                countries.Sort(new NameComparer());

                ScanwebCacheManager.Instance.AddToCache(cacheKey, tree, new Collection<object> { isBookable, true },
                    (Func<bool, bool, Node>)GetPageDataNodeTreeByBookableStatus);

                rootNode = tree;
            }
            else
            {
                string message = "CDAL - All Destinations - Return value from cache - Key=" + cacheKey;
                ContentDataAccess.LogMessage(message);
                rootNode = ScanwebCacheManager.Instance.LookInCache<Node>(cacheKey);
            }

            return rootNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OperaID"></param>
        /// <param name="language"></param>
        /// <param name="isCacheFillerRequest"></param>
        /// <returns></returns>
        public static PageData GetHotelPageDataByOperaID(string OperaID, string language, bool isCacheFillerRequest)
        {
            string cacheKey = string.Empty;

            if (!string.IsNullOrEmpty(language))
                cacheKey = string.Format(HotelPageDataCacheKey, OperaID, language);
            else
                cacheKey = string.Format(HotelPageDataCacheKey, OperaID, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey) == null) || isCacheFillerRequest)
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Reloading Hotel With Name={0}", cacheKey));

                int hotelPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["HotelPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                PageDataCollection hotelPages = GetPagesWithPageTypeIDAndLanguage(hotelPageTypeID, countryContainer, language);

                FilterCompareTo nameFilter = new FilterCompareTo("OperaID", OperaID);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(hotelPages);

                RemoveNonPublishedPages(hotelPages);

                PageData hotelPageData = new PageData();

                if (hotelPages.Count > 0)
                    hotelPageData = hotelPages[0];
                else
                    throw new ContentDataAccessException("Could not find a Hotel pageData with the specified OperaID.");


                ScanwebCacheManager.Instance.AddToCache(cacheKey, hotelPageData, new Collection<object> { OperaID, language, true },
                    (Func<string, string, bool, PageData>)GetHotelPageDataByOperaID);
                return hotelPageData;
            }
            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Hotel With OperaID={0}", cacheKey));

                PageData cachedHotel = ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey);
                return cachedHotel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OperaID"></param>
        /// <param name="isCallerFillerRequest"></param>
        /// <returns></returns>
        public static PageData GetCityPageDataByOperaID(string OperaID, bool isCallerFillerRequest)
        {
            string cacheKey = string.Format(CityPageDataCacheKey, OperaID, currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey) == null) || isCallerFillerRequest)
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Reloading City With Name={0}", cacheKey));

                int cityPageTypeID = PageType.Load(new Guid(ConfigurationManager.AppSettings["CityPageTypeGUID"])).ID;
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                PageReference countryContainer = (PageReference)rootPage["CountryContainer"];

                PageDataCollection cityPages = GetPagesWithPageTypeID(cityPageTypeID, countryContainer);

                FilterCompareTo nameFilter = new FilterCompareTo("OperaID", OperaID);
                nameFilter.Condition = CompareCondition.Equal;
                nameFilter.Filter(cityPages);

                RemoveNonPublishedPages(cityPages);

                PageData cityPageData = new PageData();

                if (cityPages.Count > 0)
                    cityPageData = cityPages[0];
                else
                    throw new ContentDataAccessException(string.Format("Could not find a City pageData with the specified OperaID: {0}", OperaID));


                ScanwebCacheManager.Instance.AddToCache(cacheKey, cityPageData, new Collection<object> { OperaID, true },
                    (Func<string, bool, PageData>)GetCityPageDataByOperaID);
                return cityPageData;
            }
            else
            {
                ContentDataAccess.LogMessage(string.Format("CDAL - Returning Cached Hotel With OperaID={0}", cacheKey));

                PageData cachedHotel = ScanwebCacheManager.Instance.LookInCache<PageData>(cacheKey);
                return cachedHotel;
            }
        }

        /// <summary>
        /// This class is made public to get the complete collection under any page type.
        /// This is made public in Hygiene Release 1.8.2 for site footer changes
        /// </summary>
        /// <param name="pageTypeID"></param>
        /// <param name="rootPageLink"></param>
        /// <returns></returns>
        public static PageDataCollection GetChildrenWithPageTypeID(int pageTypeID, PageReference rootPageLink, bool isCacheFillerRequest)
        {
            string cacheKey = string.Format(RedemptionPageListCacheKey, rootPageLink, pageTypeID.ToString(), currentLanguageString);

            if ((ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey) == null) || isCacheFillerRequest)
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeID - Refreshing List - Key={0}", cacheKey));

                PropertyCriteriaCollection searchCriterias = new PropertyCriteriaCollection();

                PropertyCriteria pageTypeCriteria = new PropertyCriteria();
                pageTypeCriteria.Condition = EPiServer.Filters.CompareCondition.Equal;
                pageTypeCriteria.Name = "PageTypeID";
                pageTypeCriteria.Required = true;
                pageTypeCriteria.Type = EPiServer.Core.PropertyDataType.PageType;
                pageTypeCriteria.Value = pageTypeID.ToString();
                searchCriterias.Add(pageTypeCriteria);

                LogMessage(string.Format("CDAL - Calling findPagesWithCriteria PageTypeID={0} RootPageLinkID={1}",
                                         pageTypeID.ToString(), rootPageLink.ID.ToString()));

                PageDataCollection searchResults = DataFactory.Instance.FindPagesWithCriteria(rootPageLink,
                                                                                              searchCriterias);
                ScanwebCacheManager.Instance.AddToCache(cacheKey, searchResults, new Collection<object> { pageTypeID, rootPageLink, true },
                    (Func<int, PageReference, bool, PageDataCollection>)GetChildrenWithPageTypeID);
                return GetCopyPageDataCollection(searchResults);
            }
            else
            {
                LogMessage(string.Format("CDAL - GetPagesWithPageTypeID  - Returned list from cache - Key={0}", cacheKey));
                // There are pages in the cache                
                return ScanwebCacheManager.Instance.LookInCache<PageDataCollection>(cacheKey);
            }
        }

        /// <summary>
        /// Gets guarantee text from CMS based on language value
        /// </summary>
        /// <param name="bookingType"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string GetGuaranteeTextForReceipt(string bookingType, string language, bool isCacheFillerRequest)
        {
            string guaranteeText = string.Empty;
            string cacheKey = string.Format(bookingType, language);

            if ((ScanwebCacheManager.Instance.LookInCache<string>(cacheKey) == null) || isCacheFillerRequest)
            {
                PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                 EPiServer.Security.AccessLevel.NoAccess);
                if ((null != rootPage) && (null != rootPage["EmailConfirmationContainer"]))
                {
                    PageReference emailConfirmationContainer = (PageReference)rootPage["EmailConfirmationContainer"];

                    PageDataCollection emailConfirmationResults = null;

                    if (!string.IsNullOrEmpty(language))
                    {
                        ILanguageSelector languageSelector = new LanguageSelector(language);
                        emailConfirmationResults = DataFactory.Instance.GetChildren(emailConfirmationContainer, languageSelector);
                    }
                    else
                    {
                        emailConfirmationResults = DataFactory.Instance.GetChildren(emailConfirmationContainer);
                    }

                    if (emailConfirmationResults != null && emailConfirmationResults.Count > 0)
                    {
                        for (int confirmationPageCounter = 0;
                             confirmationPageCounter < emailConfirmationResults.Count;
                             confirmationPageCounter++)
                        {
                            if ((null != emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"]) &&
                                (bookingType.ToLower().Trim() ==
                                 emailConfirmationResults[confirmationPageCounter].Property["PageIdentifier"].ToString()
                                     .ToLower().Trim()))
                            {
                                PageData individualConfirmationPage = emailConfirmationResults[confirmationPageCounter];

                                guaranteeText = (null != individualConfirmationPage["CancellationPolicyTextPrepaid"]
                                      ? individualConfirmationPage["CancellationPolicyTextPrepaid"].ToString()
                                      : string.Empty);

                                ScanwebCacheManager.Instance.AddToCache(cacheKey, guaranteeText, new Collection<object> { bookingType, language, true },
                                    (Func<string, string, bool, string>)GetGuaranteeTextForReceipt);
                            }
                        }
                    }

                }
            }
            else
            {
                guaranteeText = ScanwebCacheManager.Instance.LookInCache<string>(cacheKey);
            }

            return guaranteeText;

        }

        /// <summary>
        /// This method will return all Block information.
        /// </summary>
        /// <param name="blockCode"></param>
        /// <returns>Block information of corresponding block code captured from UE</returns>
        public static Block GetBlockCodePages(string blockCode, bool isCacheFillerRequest)
        {
            Block returnValue = null;

            if (!string.IsNullOrEmpty(blockCode))
            {
                string AllBlockPageKey = AllBlockPages;
                string cacheKey = string.Format(AllBlockPageKey, currentLanguageString);
                Dictionary<string, Block> blockPagesCollection = new Dictionary<string, Block>();

                if ((ScanwebCacheManager.Instance.LookInCache<Dictionary<string, Block>>(cacheKey) == null) || isCacheFillerRequest)
                {
                    PageData rootPage = DataFactory.Instance.GetPage(PageReference.RootPage,
                                                                     EPiServer.Security.AccessLevel.NoAccess);

                    if ((null != rootPage) && (null != rootPage["BlockCategoryContainer"]))
                    {
                        PageReference blockCategoryContainer = (PageReference)rootPage["BlockCategoryContainer"];
                        PageDataCollection blockCategories = DataFactory.Instance.GetChildren(blockCategoryContainer);

                        foreach (PageData blockCategoryPage in blockCategories)
                        {
                            PageDataCollection blockCodePages =
                                DataFactory.Instance.GetChildren(blockCategoryPage.PageLink);
                            RemoveNonPublishedPages(blockCodePages);

                            int blockCodePageCount = blockCodePages.Count;
                            for (int count = 0; count < blockCodePageCount; count++)
                            {
                                string blockID = (null != blockCodePages[count]["OperaID"])
                                                     ? blockCodePages[count]["OperaID"].ToString().Trim()
                                                     : string.Empty;

                                string blockName = (null != blockCodePages[count]["PageName"].ToString().Trim())
                                                       ? blockCodePages[count]["PageName"].ToString().Trim()
                                                       : string.Empty;

                                string guranteeType = (null != blockCategoryPage["GuranteeTypeCreditCard"])
                                                          ? blockCategoryPage["GuranteeTypeCreditCard"].ToString().
                                                                Trim()
                                                          : string.Empty;

                                string blockCategory = (null != blockCategoryPage["PageName"])
                                                           ? blockCategoryPage["PageName"].ToString().Trim()
                                                           : string.Empty;

                                string blockDescription = (null != blockCodePages[count]["BlockDescription"])
                                                              ? blockCodePages[count]["BlockDescription"].ToString().
                                                                    Trim()
                                                              : string.Empty;

                                DateTime date = (null != blockCodePages[count]["CancelModifyDate"])
                                                    ? (DateTime)blockCodePages[count]["CancelModifyDate"]
                                                    : DateTime.Today;

                                bool captureGuarantee = (null != blockCategoryPage["DisplayGuarantee"]) ? true : false;

                                bool sixPMHoldAvailable = (null != blockCategoryPage["6PMHold"]) ? true : false;

                                List<string> headers = new List<string>();
                                headers.Add(blockCategoryPage["Heading1"] as string ?? string.Empty);
                                headers.Add(blockCategoryPage["Heading2"] as string ?? string.Empty);
                                headers.Add(blockCategoryPage["Heading3"] as string ?? string.Empty);
                                string blockCategoryColor = blockCategoryPage["RateCategoryColor"] as string ??
                                                            string.Empty;

                                bool hideARBPrice = (null != blockCodePages[count]["HideARBPrice"]) ?
                                 Convert.ToBoolean(blockCodePages[count]["HideARBPrice"].ToString().Trim()) : false;

                                Block blockPages = new Block(blockID, blockName, guranteeType, blockCategory, blockDescription, date, captureGuarantee, sixPMHoldAvailable,
                                    blockCodePages[count].PageLink.ID, headers, blockCategoryColor, hideARBPrice);
                                //Add the data block pages to the collection.
                                blockPagesCollection[blockID] = blockPages;
                            }
                        }
                    }
                }
                else
                {
                    blockPagesCollection = ScanwebCacheManager.Instance.LookInCache<Dictionary<string, Block>>(cacheKey);
                }

                if (blockPagesCollection.ContainsKey(blockCode))
                {
                    returnValue = blockPagesCollection[blockCode];
                }
            }
            return returnValue;
        }
        #endregion
    }

    /// <summary>
    /// This will help to sort the pages based on "Name" property.
    /// </summary>
    internal class NameComparer : IComparer<Node>
    {
        public int Compare(Node first, Node second)
        {
            return first.GetPageData().PageName.CompareTo(second.GetPageData().PageName);
        }
    }

    /// <summary>
    /// This will help to sort the pages based on "Heading" property.
    /// </summary>
    internal class HotelNameComparer : IComparer<Node>
    {
        public int Compare(Node first, Node second)
        {
            return
                first.GetPageData().Property["Heading"].ToString().CompareTo(
                    second.GetPageData().Property["Heading"].ToString());
        }
    }

    /// <summary>
    /// This will help to sort the pages based on "Heading" property.
    /// </summary>
    internal class HotelDestinationComparer : IComparer<HotelDestination>
    {
        public int Compare(HotelDestination first, HotelDestination second)
        {
            return first.Name.CompareTo(second.Name);
        }
    }

    /// <summary>
    /// FAH-Bookable and Non bookable hotels
    /// </summary>
    public enum BookableNonBookableHotels
    {
        BOOKABLE_HOTELS,
        NONBOOKABLE_HOTELS
    }

}