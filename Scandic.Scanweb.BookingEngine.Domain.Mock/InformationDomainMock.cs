﻿//  Description					: Mock information domain for unit testing controller 	  //
//								  project.												  //
//----------------------------------------------------------------------------------------//
//  Author						: Srinivas                                                //
//  Author email id				:                           							  //
//  Creation Date				: 22nd August 2012                                        //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Domain.Mock
{
    /// <summary>
    /// Mock information domain for unit testing controller project.
    /// </summary>
    public class InformationDomainMock : IInformationDomain
    {
        #region IInformationDomain Members
        /// <summary>
        /// GetHotelInformationResponse
        /// </summary>
        /// <param name="HotelCode"></param>
        /// <returns></returns>
        HotelInformationResponse IInformationDomain.GetHotelInformationResponse(string HotelCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetHotelCountryCode
        /// </summary>
        /// <param name="HotelCode"></param>
        /// <returns></returns>
        string IInformationDomain.GetHotelCountryCode(string HotelCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCurrency
        /// </summary>
        /// <param name="sourceCurrencyCode"></param>
        /// <param name="destinationCurrencyCode"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        double IInformationDomain.GetCurrency(string sourceCurrencyCode, string destinationCurrencyCode, double value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetMembershipTypes
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> IInformationDomain.GetMembershipTypes()
        {
            return new Dictionary<string, string>();
        }

        /// <summary>
        /// GetConfiguredTitles
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        Dictionary<string, string> IInformationDomain.GetConfiguredTitles(string languageCode)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get UserInterest Types
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> IInformationDomain.GetUserInterestTypes()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
