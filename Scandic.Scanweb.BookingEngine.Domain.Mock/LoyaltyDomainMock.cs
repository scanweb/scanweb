﻿//  Description					: Mock loyalty domain for unit testing controller project.//
//								                                                          //
//----------------------------------------------------------------------------------------//
//  Author						: Srinivas                                                //
//  Author email id				:                           							  //
//  Creation Date				: 22nd August 2012                                        //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Information;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Membership;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Name;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Domain.Mock
{
    /// <summary>
    /// Mock loyalty domain for unit testing controller project.
    /// </summary>
    public class LoyaltyDomainMock : ILoyaltyDomain
    {
        #region ILoyaltyDomain Members
        /// <summary>
        /// PrimaryLanguageID
        /// </summary>
        string ILoyaltyDomain.PrimaryLanguageID
        {
            get { return "E"; }
            set
            {
            }
        }

        /// <summary>
        /// UpdateMembershipWithPromoCode
        /// </summary>
        /// <param name="membershipOperaId"></param>
        /// <param name="promoCode"></param>
        /// <returns></returns>
        AddPromoSubscriptionResponse ILoyaltyDomain.UpdateMembershipWithPromoCode(string membershipOperaId, string promoCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchMembershipTransactions
        /// </summary>
        /// <param name="membershipOperaId"></param>
        /// <returns></returns>
        FetchMembershipTransactionsResponse ILoyaltyDomain.FetchMembershipTransactions(string membershipOperaId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPrivacyOptionUserProfile
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="informationEmailType"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.FetchPrivacyOptionUserProfile(string nameId, string informationEmailType, FetchProfileResponse response)
        {
            if (!string.IsNullOrEmpty(nameId))
            {
                return true;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchNameUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        UserProfileEntity ILoyaltyDomain.FetchNameUserProfile(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return GetTestUserProfileEntity(@"..\TestData\UserProfileValidTestData.xml");
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// GetAddressForLanguage
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        UserProfileEntity ILoyaltyDomain.GetAddressForLanguage(string nameID, string primaryLangID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchAddressUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        UserProfileEntity ILoyaltyDomain.FetchAddressUserProfile(string nameID)
        {
           if(!string.IsNullOrEmpty(nameID))
           {
               return null;
           }
           throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchPhoneUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        List<PhoneDetailsEntity> ILoyaltyDomain.FetchPhoneUserProfile(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }



        /// <summary>
        /// FetchInterestsListUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        UserInterestsEntity[] ILoyaltyDomain.FetchUserInterestsListUserProfile(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }


        /// <summary>
        /// FetchPreferencesListUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        UserPreferenceEntity[] ILoyaltyDomain.FetchPreferencesListUserProfile(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchGuestCardList
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="membershipType"></param>
        /// <returns></returns>
        ArrayList ILoyaltyDomain.FetchGuestCardList(string nameId, string membershipType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchEmail
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        EmailDetailsEntity ILoyaltyDomain.FetchEmail(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchNonBusinessEmail
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        EmailDetailsEntity ILoyaltyDomain.FetchNonBusinessEmail(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchComment
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        CommentsEntity ILoyaltyDomain.FetchComment(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// UpdateName
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateName(string nameID, UserProfileEntity profile)
        {
            if(!string.IsNullOrEmpty(nameID))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// UpdatePreferredLanguage
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="profile"></param>
		/// <param name="response"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdatePreferredLanguage(string nameID, UserProfileEntity profile, FetchNameResponse response)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// InsertComment
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertComment(string nameId, CommentsEntity commentsEntity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// NotImplementedException
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="commentsEntity"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateComment(string nameId, CommentsEntity commentsEntity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// UpdateAddress
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateAddress(UserProfileEntity userProfile)
        {
            if (string.IsNullOrEmpty(userProfile.AddressLine1))
            {
                throw new Exception("Invalid Address");
            }
            if(!string.IsNullOrEmpty(userProfile.AddressLine1) && !string.IsNullOrEmpty(userProfile.City)
                && !string.IsNullOrEmpty(userProfile.Country))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// InsertAddress
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertAddress(string nameID, UserProfileEntity userProfile)
        {
            return true;
        }

        /// <summary>
        /// UpdatePhone
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="phoneOperaID"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="phoneType"></param>
        /// <param name="phoneRole"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdatePhone(string nameId, long phoneOperaID, string phoneNumber, string phoneType, string phoneRole)
        {
           if(!string.IsNullOrEmpty(nameId) && (phoneOperaID!= 0) && !string.IsNullOrEmpty(phoneNumber))
           {
               return true;
           }
            return false;
        }

        /// <summary>
        /// UpdateEmail
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="emailOperaID"></param>
        /// <param name="email"></param>
        /// <param name="isPrimary"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateEmail(string nameId, long emailOperaID, string email, bool isPrimary)
        {
            if(!string.IsNullOrEmpty(nameId) && (emailOperaID !=0) && !string.IsNullOrEmpty(email))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// UpdateGuestCardDetails
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="cardOperaID"></param>
        /// <param name="membershipType"></param>
        /// <param name="membershipNumber"></param>
        /// <param name="memberName"></param>
        /// <param name="displaySequence"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateGuestCardDetails(string nameId, long cardOperaID, string membershipType, string membershipNumber, string memberName, int displaySequence)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteGuestCard
        /// </summary>
        /// <param name="guestCardOperaId"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.DeleteGuestCard(string guestCardOperaId)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// UpdateInterestsListUserProfile
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userInterests"></param>
		/// <param name="userInterestsOld"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateInterestsListUserProfile(string nameId, UserInterestsEntity[] userInterests, UserInterestsEntity[] userInterestsOld)
        {
            return true;
        }

        /// <summary>
        /// UpdatePreferencesListUserProfile
        /// </summary>
        /// <param name="nameId"></param>
        /// <param name="userPreferences"></param>
		/// <param name="userPreferencesOld"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdatePreferencesListUserProfile(string nameId, UserPreferenceEntity[] userPreferences, UserPreferenceEntity[] userPreferencesOld)
        {
            return true;
        }

        /// <summary>
        /// UpdatePassword
        /// </summary>
        /// <param name="MembershipId"></param>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdatePassword(string MembershipId, string OldPassword, string NewPassword)
        {
            return true;
        }

        /// <summary>
        /// DeletePreferences
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="preferenceType"></param>
        /// <param name="preferenceValue"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.DeletePreferences(string nameID, string preferenceType, string preferenceValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeletePhone
        /// </summary>
        /// <param name="phoneOperaID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.DeletePhone(long phoneOperaID)
        {
            if(phoneOperaID != 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// DeleteGuestCardDetails
        /// </summary>
        /// <param name="partnerProgramOperaID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.DeleteGuestCardDetails(long partnerProgramOperaID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// EnrollUser
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        string ILoyaltyDomain.EnrollUser(UserProfileEntity profile)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RegisterUser
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        string ILoyaltyDomain.RegisterUser(UserProfileEntity profile)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// InsertUpdatePrivacyUserDetail
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="scandicEmail"></param>
        /// <param name="thirdPartyEmail"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertUpdatePrivacyUserDetail(string nameID, bool scandicEmail, bool thirdPartyEmail, bool scandicEMailPrev)
        {
            return true;
        }

        /// <summary>
        /// FetchNextCardNumber
        /// </summary>
        /// <param name="membershipType"></param>
        /// <returns></returns>
        string ILoyaltyDomain.FetchNextCardNumber(string membershipType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// CreateUser
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password"></param>
        /// <param name="nameID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.CreateUser(string loginName, string password, string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// InsertUserGuestCard
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="membershipType"></param>
        /// <param name="membershipNumber"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="memberName"></param>
        /// <param name="displaySequence"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertUserGuestCard(string nameID, string membershipType, string membershipNumber, string membershipLevel, string memberName, int displaySequence)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// InsertEmail
        /// </summary>
        /// <param name="email"></param>
        /// <param name="nameID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertEmail(string email, string nameID)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(nameID))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// InsertPhone
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="phoneType"></param>
        /// <param name="phoneRole"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertPhone(string nameID, string phoneNumber, string phoneType, string phoneRole)
        {
            if(!string.IsNullOrEmpty(nameID) && !string.IsNullOrEmpty(phoneNumber))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// InsertPreference
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="preferenceType"></param>
        /// <param name="preferenceValue"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertPreference(string nameID, string preferenceType, string preferenceValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// UpdateQuestionNAnswer
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="question"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateQuestionNAnswer(string nameID, string question, string answer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPassword
        /// </summary>
        /// <param name="membershipId"></param>
        /// <param name="questionId"></param>
        /// <param name="questionString"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        string ILoyaltyDomain.FetchPassword(string membershipId, string questionId, string questionString, string answer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchNameResponse ILoyaltyDomain.FetchNameService(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchAddressNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="primaryLangID"></param>
        /// <returns></returns>
        FetchAddressListResponse ILoyaltyDomain.FetchAddressNameService(string nameID, string primaryLangID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPhoneNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchPhoneListResponse ILoyaltyDomain.FetchPhoneNameService(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPrivacyOption
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchPrivacyOptionResponse ILoyaltyDomain.FetchPrivacyOption(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPreferencesListNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchPreferenceListResponse ILoyaltyDomain.FetchPreferencesListNameService(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPartnerProgramNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchGuestCardListResponse ILoyaltyDomain.FetchPartnerProgramNameService(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                FetchGuestCardListResponse fetchGuestCardListResponse = new FetchGuestCardListResponse();
                fetchGuestCardListResponse.GuestCardList = null;
                return fetchGuestCardListResponse;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// FetchCreditCardDetailsNameService
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        FetchCreditCardListResponse ILoyaltyDomain.FetchCreditCardDetailsNameService(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchCreditCardDetailsUserProfile
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        Dictionary<string, CreditCardEntity> ILoyaltyDomain.FetchCreditCardDetailsUserProfile(string nameID)
        {
            if (!string.IsNullOrEmpty(nameID))
            {
                return null;
            }
            throw new Exception("NameId empty");
        }

        /// <summary>
        /// UpdateCreditCardDetails
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="creditCardOperaID"></param>
        /// <param name="isPrimary"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.UpdateCreditCardDetails(CreditCardEntity creditCard, long creditCardOperaID, bool isPrimary)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// InsertCreditCard
        /// </summary>
        /// <param name="nameID"></param>
        /// <param name="userCreditCard"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.InsertCreditCard(string nameID, CreditCardEntity userCreditCard)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteCreditCard
        /// </summary>
        /// <param name="creditCardOperaID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.DeleteCreditCard(long creditCardOperaID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetNewPassword
        /// </summary>
        /// <param name="membershipNumber"></param>
        /// <returns></returns>
        string ILoyaltyDomain.GetNewPassword(string membershipNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchNameForOWSMonitor
        /// </summary>
        /// <param name="nameID"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.FetchNameForOWSMonitor(string nameID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchPromoSubscriptionForOWSMonitor
        /// </summary>
        /// <returns></returns>
        bool ILoyaltyDomain.FetchPromoSubscriptionForOWSMonitor()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AuthenticateUserForOWSMonitor
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool ILoyaltyDomain.AuthenticateUserForOWSMonitor(string userName, string password)
        {
            throw new NotImplementedException();
        }

        /// Fetches only the Prepaid card types if the fetchOnlyPrepaidCCTypes is set to true. otherwise returns all the credit cards.
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="fetchOnlyPrepaidCCTypes">sets only prepaid card types only are required or not</param>
        /// <returns>Array of CreditCardEntity</returns>
        public Dictionary<string, CreditCardEntity> FetchCreditCardDetailsUserProfile(string nameID,
                                                                               bool fetchOnlyPrepaidCCTypes)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Fetches the credit card details which matches with the pan hash
        /// </summary>
        /// <param name="nameID">Name ID</param>
        /// <param name="panHash">the pan hash value of the credit card (NameonCard)</param>
        /// <returns>Array of CreditCardEntity</returns>
        public CreditCardEntity FetchCreditCardDetailsByPanHash(string nameID, string panHash)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Gets test user profile entity from the xml.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        private UserProfileEntity GetTestUserProfileEntity(string testDataXMLPath)
        {
            UserProfileEntity testUserProfileEntity = new UserProfileEntity();
            int userNameId = 0;
            using (XmlReader reader = XmlReader.Create(testDataXMLPath))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name.Equals("PreferredLanguage", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.PreferredLanguage = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressLine1", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressLine1 = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressLine2", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressLine2 = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressType", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressType = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("City", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.City = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("Country", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.Country = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("DateOfBirth", StringComparison.InvariantCulture))
                        {
                            DateOfBirthEntity dateOfBirthEntity = new DateOfBirthEntity(Convert.ToDateTime(reader.ReadInnerXml().Replace("\n", string.Empty)));
                            testUserProfileEntity.DateOfBirth = dateOfBirthEntity;
                        }
                        if (reader.Name.Equals("EmailID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.EmailID = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("EmailOperaId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.EmailOperaId = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("MembershipId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.MembershipId = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("MembershipId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.MembershipId = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                    }
                }
            }
            testUserProfileEntity.ProfileType = new UserProfileType();
            return testUserProfileEntity;
        }
        #endregion 
    
        public bool FetchPrivacyOptionUserProfile(string nameId, string informationEmailType, FetchProfileResponse response)
        {
            throw new NotImplementedException();
        }

        public UserProfileEntity FetchUserProfile(string nameID)
        {
            throw new NotImplementedException();
        }
    }
}
