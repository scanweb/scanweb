﻿//  Description					: Mock availability domain for unit testing controller 	  //
//								  project.												  //
//----------------------------------------------------------------------------------------//
//  Author						: Srinivas                                                //
//  Author email id				:                           							  //
//  Creation Date				: 22nd August 2012                                        //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using Scandic.Scanweb.BookingEngine.DomainContracts;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Domain.Mock
{
    /// <summary>
    /// Mock availability domain for unit testing controller project.
    /// </summary>
    public class AvailabilityDomainMock : IAvailabilityDomain
    {
        #region IAvailabilityDomain Members

        /// <summary>
        /// GeneralAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <returns></returns>
        AvailabilityResponse IAvailabilityDomain.GeneralAvailability(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RedemptionAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="membershipLevel"></param>
        /// <returns></returns>
        AvailabilityResponse IAvailabilityDomain.RedemptionAvailability(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode, string membershipLevel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GeneralAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        void IAvailabilityDomain.GeneralAvailability(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode, System.Collections.Hashtable availabilityServiceRequestState, bool isMultiDateSearch)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RedemptionAvailability
        /// </summary>
        /// <param name="hotelSearchEntity"></param>
        /// <param name="roomSearch"></param>
        /// <param name="hotelCode"></param>
        /// <param name="chainCode"></param>
        /// <param name="membershipLevel"></param>
        /// <param name="availabilityServiceRequestState"></param>
        /// <param name="isMultiDateSearch"></param>
        void IAvailabilityDomain.RedemptionAvailability(HotelSearchEntity hotelSearchEntity, HotelSearchRoomEntity roomSearch, string hotelCode, string chainCode, string membershipLevel, System.Collections.Hashtable availabilityServiceRequestState, bool isMultiDateSearch)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AuthenticateUser
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        string IAvailabilityDomain.AuthenticateUser(string userName, string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AvailabilityForOWSMonitor
        /// </summary>
        /// <returns></returns>
        bool IAvailabilityDomain.AvailabilityForOWSMonitor()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
