﻿//  Description					: This class contains unit test methods to test          //
//								  controller methods.                                    //
//                                                                                       //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using NUnit.Framework;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.BookingEngine.Domain.Mock;
using Scandic.Scanweb.BookingEngine.DomainContracts;

namespace Scandic.Scanweb.BookingEngine.Controller.Test
{
    /// <summary>
    /// This class contains unit test methods to test controller methods. 
    /// </summary>
    [TestFixture]
    public class ControllerUnitTests
    {
        #region Test Methods
        /// <summary>
        /// This tests the execution of name controller's UpdateUserDetails method with valid user data. 
        /// </summary>
        [Test]
        public void UpdateUserDetailsWithValidUserData()
        {
            try
            {
                ILoyaltyDomain loyaltyDomain = new LoyaltyDomainMock();
                ISessionManager sessionManager = new SessionManagerMock();
                NameController nameController = new NameController(loyaltyDomain, sessionManager);
                List<PartnershipProgram> newPartnershipProgramList = new List<PartnershipProgram>();
                int nameId = 0;
                UserProfileEntity userProfileEntity = GetTestUserProfileEntity(out nameId, @"..\TestData\UserProfileValidTestData.xml");
                UserProfileEntity userProfileEntityformUI = GetTestUserProfileEntity(out nameId, @"..\TestData\UserProfileValidTestData.xml");
                nameController.PrimaryLangaueID = userProfileEntity.PreferredLanguage;
                nameController.UpdateUserDetails(nameId.ToString(), userProfileEntity, userProfileEntityformUI, newPartnershipProgramList);
                Assert.Pass();
            }
            catch(SuccessException ex)
            {
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// This tests the execution of name controller's UpdateUserDetails method with invalid user data. 
        /// </summary>
        [Test]
        public void UpdateUserDetailsWithInValidUserData()
        {
            try
            {
                ILoyaltyDomain loyaltyDomain = new LoyaltyDomainMock();
                ISessionManager sessionManager = new SessionManagerMock();
                NameController nameController = new NameController(loyaltyDomain, sessionManager);
                List<PartnershipProgram> newPartnershipProgramList = new List<PartnershipProgram>();
                int nameId = 0;
                UserProfileEntity userProfileEntity = GetTestUserProfileEntity(out nameId, @"..\TestData\UserProfileInvalidTestData.xml");
                nameController.PrimaryLangaueID = userProfileEntity.PreferredLanguage;
                nameController.UpdateUserDetails(nameId.ToString(), userProfileEntity,userProfileEntity, newPartnershipProgramList);
                Assert.Pass();
            }
            catch (Exception ex)
            {
                if (!ex.Message.Equals("Invalid Address", StringComparison.InvariantCultureIgnoreCase))
                {
                    Assert.Fail();
                }
            }
        }

        /// <summary>
        /// This tests the execution of name controller's FetchUserDetails method with valid nameId.
        /// </summary>
        [Test]
        public void FetchNameUserProfileWithValidNameId()
        {
            try
            {
                ILoyaltyDomain loyaltyDomain = new LoyaltyDomainMock();
                ISessionManager sessionManager = new SessionManagerMock();
                NameController nameController = new NameController(loyaltyDomain, sessionManager);
                UserProfileEntity testUserProfileEntity = nameController.FetchUserDetails("77332011");
                Assert.NotNull(testUserProfileEntity);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }


        /// <summary>
        /// This tests the execution of name controller's FetchUserDetails method with valid nameId.
        /// </summary>
        [Test]
        public void FetchNameUserProfileWithInValidNameId()
        {
            try
            {
                ILoyaltyDomain loyaltyDomain = new LoyaltyDomainMock();
                ISessionManager sessionManager = new SessionManagerMock();
                NameController nameController = new NameController(loyaltyDomain, sessionManager);
                UserProfileEntity testUserProfileEntity = nameController.FetchUserDetails(string.Empty);
                Assert.Equals(testUserProfileEntity.MembershipId, "30812381722661");
            }
            catch (Exception ex)
            {
                if (!ex.Message.Equals("NameId empty", StringComparison.InvariantCultureIgnoreCase))
                {
                    Assert.Fail();
                }
            }
        }

        /// <summary>
        /// This tests the execution of information controller's GetMembershipTypes method.
        /// </summary>
        /// <returns></returns>
        [Test]
        public void GetMembershipTypes()
        {
            try
            {
                IInformationDomain informationDomain = new InformationDomainMock();
                InformationController informationController = new InformationController(informationDomain);
                Dictionary<string, string> membershipTypes = informationController.GetMembershipTypes();
                Assert.NotNull(membershipTypes);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Gets test user profile entity from the xml.
        /// </summary>
        /// <param name="nameId"></param>
        /// <returns></returns>
        private UserProfileEntity GetTestUserProfileEntity(out int nameId, string testDataXMLPath)
        {
            UserProfileEntity testUserProfileEntity = new UserProfileEntity();
            int userNameId = 0;
            using (XmlReader reader = XmlReader.Create(testDataXMLPath))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        if(reader.Name.Equals("PreferredLanguage", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.PreferredLanguage = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressLine1", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressLine1 = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressLine2", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressLine2 = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressOperaID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressOperaID = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("AddressType", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.AddressType = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("City", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.City = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("Country", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.Country = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("DateOfBirth", StringComparison.InvariantCulture))
                        {
                            DateOfBirthEntity dateOfBirthEntity = new DateOfBirthEntity(Convert.ToDateTime(reader.ReadInnerXml().Replace("\n", string.Empty)));
                            testUserProfileEntity.DateOfBirth = dateOfBirthEntity;
                        }
                        if (reader.Name.Equals("EmailID", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.EmailID = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("EmailOperaId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.EmailOperaId = Convert.ToInt64(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("NameID", StringComparison.InvariantCulture))
                        {
                            userNameId = Convert.ToInt32(reader.ReadInnerXml().Replace("\n", string.Empty));
                        }
                        if (reader.Name.Equals("MembershipId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.MembershipId = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                        if (reader.Name.Equals("MembershipId", StringComparison.InvariantCulture))
                        {
                            testUserProfileEntity.MembershipId = reader.ReadInnerXml().Replace("\n", string.Empty);
                        }
                    }
                }
            }
            testUserProfileEntity.ProfileType = new UserProfileType();
            nameId = userNameId;
            return testUserProfileEntity;
        }
        #endregion 
    }
}
