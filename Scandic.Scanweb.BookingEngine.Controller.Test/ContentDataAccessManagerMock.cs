﻿//  Description					: This class represents mock content data access manager  //
//								  , which will be used to test controller methods using   //
//                                nunints.                                                //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Text;
using Scandic.Scanweb.Core;
using Scandic.Scanweb.BookingEngine.Controller.Entity;
using Scandic.Scanweb.Entity;

namespace Scandic.Scanweb.BookingEngine.Controller.Test
{
    /// <summary>
    /// This class represents mock content data access manager, which will be used to test controller methods using nunints. 
    /// </summary>
    class ContentDataAccessManagerMock : IContentDataAccessManager
    {
        #region IContentDataAccessManager Members
        /// <summary>
        /// GetRateTypeCategoryMap
        /// </summary>
        /// <returns></returns>
        Dictionary<string, RateCategory> IContentDataAccessManager.GetRateTypeCategoryMap()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetRoomTypeCategoryMap
        /// </summary>
        /// <returns></returns>
        Dictionary<string, RoomCategory> IContentDataAccessManager.GetRoomTypeCategoryMap()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetBookableHotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        List<HotelDestination> IContentDataAccessManager.GetBookableHotels(string cityCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetAlternateCityHotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        List<HotelDestination> IContentDataAccessManager.GetAlternateCityHotels(string cityCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetHotelByOperaID
        /// </summary>
        /// <param name="hotelOperaID"></param>
        /// <returns></returns>
        HotelDestination IContentDataAccessManager.GetHotelByOperaID(string hotelOperaID)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetHotelByOperaID
        /// </summary>
        /// <param name="hotelOperaID"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        HotelDestination IContentDataAccessManager.GetHotelByOperaID(string hotelOperaID, string language)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCityAndHotel
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        List<CityDestination> IContentDataAccessManager.GetCityAndHotel(bool ignoreBookingVisibility)
        {
            throw new NotImplementedException();
        }

        public List<CityDestination> GetDestinationsFromTheLastSixMonthscityList(bool ignoreBookingVisibility)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCityAndHotelForAutoSuggest
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        List<CityDestination> IContentDataAccessManager.GetCityAndHotelForAutoSuggest(bool ignoreBookingVisibility)
        {
            throw new NotImplementedException();
        }

        List<Scandic.Scanweb.Core.CountryDestinatination> IContentDataAccessManager.GetCountryCityAndHotelForAutoSuggest(bool ignoreBookingVisibility,bool removeChildrens)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// FetchAllDestinationsWithNonBookable
        /// </summary>
        /// <param name="ignoreBookingVisibility"></param>
        /// <returns></returns>
        List<CityDestination> IContentDataAccessManager.FetchAllDestinationsWithNonBookable(bool ignoreBookingVisibility)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetAllTransactions
        /// </summary>
        /// <returns></returns>
        List<Transaction> IContentDataAccessManager.GetAllTransactions()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetBonusChequeRate
        /// </summary>
        /// <param name="year"></param>
        /// <param name="country"></param>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        double IContentDataAccessManager.GetBonusChequeRate(int year, string country, string currencyCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCountryNamebyCountryID
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        string IContentDataAccessManager.GetCountryNamebyCountryID(string countryCode)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetHotelDestinationWithHotelName
        /// </summary>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        HotelDestination IContentDataAccessManager.GetHotelDestinationWithHotelName(string hotelName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetAllTypesofHotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        List<HotelDestination> IContentDataAccessManager.GetAllTypesofHotels(string cityCode)
        {
            throw new NotImplementedException();
        }

        public IList<HotelRedemptionPoints> GetAllHotelRedemptionPoints()
        {
            throw new NotImplementedException();
        }

        public MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID)
        {
            throw new NotImplementedException();
        }

        public MerchantProfileEntity GetMerchantByHotelOperaId(string hotelOperaID, string language)
        {
            throw new NotImplementedException();
        }

        public bool GetPaymentFallback(string hotelOperaID)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
