﻿//  Description					: This class represents mock session manager, which will  //
//								  be used to test controller methods using nunints.       //
//                                                                                        //			            
//----------------------------------------------------------------------------------------//
//  Author						: Sapient                                                 //
//  Author email id				:                           							  //
//  Creation Date				: 20th August 2007									      //
// 	Version	#					: 1.0													  //
//----------------------------------------------------------------------------------------//
//  Revison History				: -NA-													  //
//	Last Modified Date			:														  //
////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.SessionState;
using Scandic.Scanweb.BookingEngine.ServiceProxies.Availability;
using Scandic.Scanweb.Entity;
using Scandic.Scanweb.Core;

namespace Scandic.Scanweb.BookingEngine.Controller.Test
{
    /// <summary>
    /// This class represents mock session manager, which will be used to test controller methods using nunints.
    /// </summary>
    class SessionManagerMock : ISessionManager
    {
        #region ISessionManager Members
        /// <summary>
        /// AltCityHotelsSearchDone
        /// </summary>
        bool ISessionManager.AltCityHotelsSearchDone
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// TotalRegionalAvailableHotels
        /// </summary>
        int ISessionManager.TotalRegionalAvailableHotels
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// SetAvailabilityTable
        /// </summary>
        /// <param name="session"></param>
        /// <param name="availabilityTable"></param>
        void ISessionManager.SetAvailabilityTable(HttpSessionState session, Hashtable availabilityTable)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// TotalGeneralAvailableHotels
        /// </summary>
        int ISessionManager.TotalGeneralAvailableHotels
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// NoRedemptionHotelsAvailable
        /// </summary>
        bool ISessionManager.NoRedemptionHotelsAvailable
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// GetAvailabilityTable
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        Hashtable ISessionManager.GetAvailabilityTable(HttpSessionState session)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// HotelResults
        /// </summary>
        List<IHotelDetails> ISessionManager.HotelResults
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// GoogleMapResults
        /// </summary>
        List<Dictionary<string, object>> ISessionManager.GoogleMapResults
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// GetHotelResults
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        List<IHotelDetails> ISessionManager.GetHotelResults(HttpSessionState session)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SetHotelResults
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        void ISessionManager.SetHotelResults(HttpSessionState session, List<IHotelDetails> hotels)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetGoogleMapResults
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        List<Dictionary<string, object>> ISessionManager.GetGoogleMapResults(HttpSessionState session)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SetGoogleMapResults
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        void ISessionManager.SetGoogleMapResults(HttpSessionState session, List<Dictionary<string, object>> value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        int ISessionManager.GetTotalGeneralAvailableHotels(HttpSessionState session)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SetTotalGeneralAvailableHotels
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hotels"></param>
        void ISessionManager.SetTotalGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ListRoomStay
        /// </summary>
        IList<RoomStay> ISessionManager.ListRoomStay
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// LoyaltyDetails
        /// </summary>
        LoyaltyDetailsEntity ISessionManager.LoyaltyDetails
        {
            get { return null; }
            set
            {
            }
        }

        /// <summary>
        /// IsComboReservation
        /// </summary>
        bool ISessionManager.IsComboReservation
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// GetLoyaltyDetails
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        LoyaltyDetailsEntity ISessionManager.GetLoyaltyDetails(HttpSessionState session)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SearchCriteria
        /// </summary>
        HotelSearchEntity ISessionManager.SearchCriteria
        {
            get { return new HotelSearchEntity(); }
            set
            {
            }
        }

        /// <summary>
        /// GetHotelDestinationResults
        /// </summary>
        /// <returns></returns>
        List<HotelDestination> ISessionManager.GetHotelDestinationResults()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SetHotelDestinationResults
        /// </summary>
        /// <param name="hotels"></param>
        void ISessionManager.SetHotelDestinationResults(List<HotelDestination> hotels)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// UpdateProfileBookingInfo
        /// </summary>
        bool ISessionManager.UpdateProfileBookingInfo
        {
            get { return true; }
            set
            {
            }
        }

        /// <summary>
        /// GuestBookingInformation
        /// </summary>
        GuestInformationEntity ISessionManager.GuestBookingInformation
        {
            get
            {
               GuestInformationEntity guestInformationEntity = new GuestInformationEntity();
               guestInformationEntity.GuranteeInformation = new GuranteeInformationEntity();
               return guestInformationEntity;
            }
            set
            {
            }
        }

        /// <summary>
        /// UserLoggedIn
        /// </summary>
        bool ISessionManager.UserLoggedIn
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// UserProfileInformation
        /// </summary>
        UserProfileEntity ISessionManager.UserProfileInformation
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// DirectlyModifyContactDetails
        /// </summary>
        bool ISessionManager.DirectlyModifyContactDetails
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// BookingDetails
        /// </summary>
        BookingDetailsEntity ISessionManager.BookingDetails
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// GetLegBookingDetails
        /// </summary>
        /// <param name="legNumber"></param>
        /// <param name="reservationNumber"></param>
        /// <returns></returns>
        BookingDetailsEntity ISessionManager.GetLegBookingDetails(string legNumber, string reservationNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ActiveBookingDetails
        /// </summary>
        List<BookingDetailsEntity> ISessionManager.ActiveBookingDetails
        {
            get { throw new NotImplementedException(); }
        }

        //Merchandising:R3:Display ordinary rates for unavailable promo rates 
        IHotelDetails ISessionManager.HotelDetails
        {
            get {throw new NotImplementedException();}
            set {throw new NotImplementedException();}
        }
        bool ISessionManager.IsDestinationAlternateFlow
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        int ISessionManager.TotalRegionalAltCityHotelCount
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        List<HotelDestination> ISessionManager.CityAltHotelDetails
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        int ISessionManager.PromoGeneralAvailableHotels
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        int ISessionManager.GetPromoGeneralAvailableHotels(HttpSessionState session)
        {
            throw new NotImplementedException();
        }
        void ISessionManager.SetPromoGeneralAvailableHotels(HttpSessionState session, int hotels)
        {
            throw new NotImplementedException();
        }
        int ISessionManager.GetTotalRegionalAvailableHotels(HttpSessionState session)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
